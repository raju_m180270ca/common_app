/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, {useState, useEffect} from 'react';
import type {Node} from 'react';
import {SafeAreaView, StatusBar, Platform, Linking, View} from 'react-native';
import {Colors} from 'react-native/Libraries/NewAppScreen';
import RootNavigator from '@navigation/RootNavigator';

import {
  NetworkError,
  Loader,
  AppUpdateDialog,
  ApiError,
  RefreshComponent,
  DoubleLogin,
  CustomToast,
} from '@components';

import {getVersion} from 'react-native-device-info';
import SplashScreen from 'react-native-splash-screen';
import NetInfo from '@react-native-community/netinfo';

import Orientation from 'react-native-orientation';
import {AuthContext} from '@contexts/auth-context';
import {ApiEndPoint} from '@constants';
import {API} from '@api';
import {Mixpanel} from 'mixpanel-react-native';
import {useStores} from '@mobx/hooks';
import {SessionTimeOutDialog} from '@hoc';
console.disableYellowBox = true;

const App: () => Node = () => {
  const store = useStores();

  const [isAuth, setIsAuth] = useState(false);
  const [didTryAutoLogin, setDidTryAutoLogin] = useState(false);
  const [versionUpdateEnabled, setVersionUpdateEnabled] = useState(false);
  const [versionInfo, setVersionInfo] = useState({});
  const [enableRouteView, setRouteViewEnabled] = useState(false);
  const [mixPanelObj, setMixPanelObj] = useState(null);
  const [ToastData, setToastData] = useState(null);

  const showToast = ({title, description, bgcolor, timeout = 5000}) => {
    try {
      setToastData({
        Visibility: true,
        Toast_title: title,
        Toast_desc: description,
        Toast_bgcolor: bgcolor,
      });
      setTimeout(() => {
        hideToast();
      }, timeout);
    } catch (err) {
      console.log(err);
    }
  };

  const hideToast = () => {
    setToastData(null);
  };

  const login = () => {
    setIsAuth(true);
    setDidTryAutoLogin(true);
  };

  const logout = () => {
    setIsAuth(false);
    setDidTryAutoLogin(false);
  };

  const setAutoLogin = () => {
    setDidTryAutoLogin(true);
  };

  const appVersionCompare = (v1, v2) => {
    // vnum stores each numeric
    // part of version
    var vnum1 = 0,
      vnum2 = 0;

    // loop until both string are
    // processed
    for (var i = 0, j = 0; i < v1.length || j < v2.length; ) {
      // storing numeric part of
      // version 1 in vnum1
      while (i < v1.length && v1[i] != '.') {
        vnum1 = vnum1 * 10 + (v1[i] - '0');
        i++;
      }

      // storing numeric part of
      // version 2 in vnum2
      while (j < v2.length && v2[j] != '.') {
        vnum2 = vnum2 * 10 + (v2[j] - '0');
        j++;
      }

      if (vnum1 > vnum2) return 1;
      if (vnum2 > vnum1) return -1;

      // if equal, reset variables and
      // go for next numeric part
      vnum1 = vnum2 = 0;
      i++;
      j++;
    }
    return 0;
  };

  useEffect(() => {
    async function forceUpdateCall() {
      const response = await API(ApiEndPoint.FORCE_UPDATE, {store});
      setVersionInfo(response?.data?.responseData);
      let serverVersion =
        response?.data?.responseData?.student[Platform.OS]?.version;
      let appVersion = getVersion();

      if (appVersionCompare(appVersion, serverVersion) < 0) {
        setVersionUpdateEnabled(true);
        SplashScreen.hide();
      } else {
        setRouteViewEnabled(true);
        initialSetup();
      }
    }
    configMixpanel();
    forceUpdateCall();
  }, []);

  const configMixpanel = async () => {
    let mixpanel = await Mixpanel.init('352f0ee62ccef8e73a5b773acbe314af');
    setMixPanelObj(mixpanel);
  };

  const trackIdentity = async username => {
    mixPanelObj.identify(username);
  };

  const trackEvent = async (type, eventName, eventInfo = null) => {
    if (type == 'mixpanel') {
      mixPanelObj.track(eventName, eventInfo);
    } else {
      // CleverTap.recordEvent(eventName, eventInfo);
    }
  };

  const setUserProfile = async (userInfo, type) => {
    if (userInfo) {
      if (type == 'mixpanel') {
        const {
          isFreeTrail,
          name,
          isB2CUser,
          grade,
          section,
          schoolName,
          language,
          parentDetails,
          schoolCode,
          username,
        } = userInfo;
        mixPanelObj.getPeople().set('isB2CUser', isB2CUser);
        mixPanelObj.getPeople().set('isFreeTrial', isFreeTrail);
        mixPanelObj.getPeople().set('class', grade || '');
        mixPanelObj.getPeople().set('section', section || '');
        mixPanelObj.getPeople().set('schoolName', schoolName || '');
        mixPanelObj.getPeople().set('schoolCode', schoolCode || '');
        mixPanelObj.getPeople().set('context', language || '');
        mixPanelObj.getPeople().set('User Category', 'Student');
        mixPanelObj.getPeople().set('Username', username || '');
        mixPanelObj.getPeople().set('Product', 'Mindspark');

        if (parentDetails) {
          const {parent1, parent2} = parentDetails;
          if (parent1) {
            const {name, email, mobile} = parent1;
            mixPanelObj.getPeople().set('parent1Name', name || '');
            mixPanelObj.getPeople().set('parent1Email', email.email || '');
            mixPanelObj.getPeople().set('parent1Mobile', mobile.number || '');
          }

          if (parent2) {
            const {name, email, mobile} = parent2;
            mixPanelObj.getPeople().set('parent2Name', name || '');
            mixPanelObj.getPeople().set('parent2Email', email.email || '');
            mixPanelObj.getPeople().set('parent2Mobile', mobile.number || '');
          }
        }

        if (name) {
          mixPanelObj.getPeople().set('Name', name);
        }
      } else {
        const {isFreeTrail, name, grade, username} = userInfo;
        let userLoginInfo = {};
        if (name) {
          userLoginInfo['Student Name'] = name;
        }

        if (username) {
          userLoginInfo['Username'] = username;
          userLoginInfo['Identity'] = username;
        }
        userLoginInfo['Lead type'] = 'B2C';
        userLoginInfo['Class'] = grade || '';
        userLoginInfo['Lead_source'] = 'app';
        userLoginInfo['isFreeTrial'] = isFreeTrail;

        // CleverTap.onUserLogin(userLoginInfo);
        // CleverTap.createNotificationChannel(
        //   'Mindspark Student',
        //   'Mindspark Student',
        //   'Mindspark Student Description',
        //   3,
        //   true,
        // );
      }
    }
  };

  const initialSetup = () => {
    Orientation.lockToPortrait();
    NetInfo.addEventListener(state => {
      // store.uiStore.setIsNetConnected(state.isConnected);
    });
  };

  return (
    <SafeAreaView style={{backgroundColor: Colors.lighter, flex: 1}}>
      <StatusBar barStyle={'light-content'} />
      {enableRouteView ? (
        <View style={{flex: 1}}>
          <AuthContext.Provider
            value={{
              isAuth,
              didTryAutoLogin,
              login,
              logout,
              setDidTryAutoLogin: setAutoLogin,
              trackIdentity,
              setUserProfile,
              trackEvent,
              showToast,
              hideToast,
            }}>
            <RootNavigator />
          </AuthContext.Provider>
          <ApiError />
          <NetworkError />
          <Loader />
          <AppUpdateDialog />
          <DoubleLogin />
          <SessionTimeOutDialog />
          {ToastData && (
            <CustomToast
              title_text={ToastData.Toast_title}
              textdesc={ToastData.Toast_desc}
              background_color={ToastData.Toast_bgcolor}
              hideToast={hideToast}
            />
          )}
        </View>
      ) : (
        <RefreshComponent
          forceUpdate={
            Object.keys(versionInfo).length > 0
              ? versionInfo?.student[Platform.OS]?.forceUpdate
              : false
          }
          visible={versionUpdateEnabled}
          description={
            Object.keys(versionInfo).length > 0
              ? versionInfo?.student[Platform.OS]?.message
              : ''
          }
          heading={
            Object.keys(versionInfo).length > 0
              ? versionInfo?.student[Platform.OS]?.headerText
              : ''
          }
          onPress={() => {
            Linking.openURL(versionInfo?.student[Platform.OS]?.url);
            setVersionUpdateEnabled(false);
          }}
          onCancel={() => {
            initialSetup();
            setVersionUpdateEnabled(false);
          }}
          onHide={() => setRouteViewEnabled(true)}
        />
      )}
    </SafeAreaView>
  );
};

export default App;
