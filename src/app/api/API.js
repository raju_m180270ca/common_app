import {getAsValue} from '@utils';
import axiosAuth from '../axios/axiosAuth';

const API = async (endPoint, req, isMindsparkApi) => {
  const jwt = await getAsValue('jwt');
  const request = {
    url: endPoint,
    body: req.body,
  };
  let signalObj = null;
  if (req.signal) {
    signalObj = {
      cancelToken: req.signal.token,
    };
  }
  const axios = axiosAuth(req.store, isMindsparkApi);
  axios.defaults.headers.common['jwt'] = jwt;
  axios.defaults.headers.common['Auth'] = 'EISecret';
  console.log('JWT::\n', jwt);
  console.log(`\n\n${endPoint} API Request: ` + JSON.stringify(req.body));
  console.log('api2', axios.defaults.headers.common['Content-Type']);
  // return axios.post(request.url, req.body, signalObj);
  const respo = axios.post(request.url, req.body, signalObj);
  console.log('APP respp', endPoint, respo);
  return respo;
};

export default API;
