import {getAsValue} from '@utils';
import axiosAuth from '../axios/axiosAuth';

const API = async (endPoint, req) => {
  const jwt = await getAsValue('jwt');
  const request = {
    url: endPoint,
    body: req.body,
  };
  const axios = axiosAuth(req.store);
  axios.defaults.headers.common.jwt = jwt;
  axios.defaults.headers.common['Content-Type'] = 'multipart/form-data';
  console.log('api2', axios.defaults.headers.common['Content-Type']);
  console.log(`\n\n${endPoint} API Request: ` + JSON.stringify(req.body));
  return axios.post(request.url, req.body);
};

export default API;
