/* eslint-disable dot-notation */
import axios from 'axios';
import {MINDSPARK_BASE_URL, BASE_URL, CONFIG_KEY} from '@env';
import {setAsValue, getProductName} from '@utils';
import {Toast} from 'native-base';

const axiosAuth = (store, isMindsparkApi) => {
  const instance = axios.create({
    baseURL: isMindsparkApi ? `${MINDSPARK_BASE_URL}` : `${BASE_URL}`,
  });

  instance.defaults.headers.common['config_key'] = CONFIG_KEY;
  instance.defaults.headers.common['Content-Type'] = 'application/json';

  let timerID = null;
  instance.interceptors.request.use(
    request => {
      //Set loader to true
      store.uiStore.setLoader(true);
      store.qnaStore.setDisableBTn(true);
      store.uiStore.setShowInactivePopUp(false);

      if (store.uiStore.lastUpdatedTime) {
        var diff = Math.abs(store.uiStore.lastUpdatedTime - new Date());
        if (diff > 0) {
          var minutes = Math.floor(diff / 1000 / 60);
          if (minutes >= 10) {
            // Show in active popup
            store.uiStore.setShowInactivePopUp(true);
          }
        }
      }
      store.uiStore.setLastUpdatedTime(new Date());

      console.log(
        `API Base URL>>>>>>${
          isMindsparkApi ? `${MINDSPARK_BASE_URL}` : `${BASE_URL}`
        }`,
      );
      console.log(`\n\nRequest Log Data>>>>${JSON.stringify(request.data)}`);
      return request;
    },
    error => {
      store.uiStore.setLoader(false);
      store.qnaStore.setDisableBTn(false);
      store.qnaStore.setLoader(false);
      clearTimeout(timerID);
      return Promise.reject(error);
    },
  );

  instance.interceptors.response.use(
    async response => {
      //Set loader to false
      console.log(`Axios Auth Response>>>>${JSON.stringify(response.data)}`);
      store.uiStore.setLoader(false);
      store.qnaStore.setDisableBTn(false);
      clearTimeout(timerID);
      if (response.data.resultMessage === 'redirect') {
        if (response.data.redirectionData.sessionTimeExceededFlag === true) {
          store.uiStore.setSessionExceeded(true);
          // store.loginStore.setIsAuth(false);
          await setAsValue('jwt', '');
          store.appStore.setJwt(null);
        }
      }

      console.log(
        `\nResponse Log Header>>>>${JSON.stringify(response.headers.common)}`,
      );
      return response;
    },
    async error => {
      clearTimeout(timerID);
      store.uiStore.setLoader(false);
      store.qnaStore.setLoader(false);
      store.qnaStore.setDisableBTn(false);
      try {
        console.log(
          `Axios Response Error0091>>>${JSON.stringify(error.response.data)}`,
        );
        console.log('API REPOSE STATUS:', error.response.status);
        if (error.response.status === 401) {
          if (store.appStore.isTrusted || store.appStore.trustedDeviceId) {
            const req = {
              username: store.appStore.username,
              trustedDeviceId: store.appStore.trustedDeviceId,
              productName: getProductName(store.appStore.selectedSubject),
            };

            let intialReq = error.config;
            const response = await axios.post(
              `${BASE_URL}/CommonLogin/StartNewSession`,
              req,
            );
            if (response && response.data.resultCode === 'C001') {
              let jwt = response.headers.jwt;
              await setAsValue('jwt', jwt);
              store.appStore.setJwt(jwt);

              intialReq.headers['jwt'] = jwt;
              return instance.request(intialReq);
            } else {
              store.loginStore.setIsAuth(false);
              await setAsValue('jwt', '');
              store.appStore.setJwt(null);
              error.response.data.resultMessage =
                'Session Expired, Login Again';
              store.uiStore.setSessionExceeded(true);
            }
          } else {
            store.loginStore.setIsAuth(false);
            await setAsValue('jwt', '');
            store.appStore.setJwt(null);
            error.response.data.resultMessage = 'Session Expired, Login Again';
            store.uiStore.setSessionExceeded(true);
          }
        }
        if (error.response.status === 400) {
          if (
            error.response.data?.redirectionData?.sessionTimeExceededFlag ==
            true
          ) {
            store.uiStore.setSessionExceeded(true);
            // store.loginStore.setIsAuth(false);
            await setAsValue('jwt', '');
            store.appStore.setJwt(null);
          } else {
            store.uiStore.apiErrorInit({
              code: '400',
              message: 'Bad Request',
            });
          }
        }
        if (error.response.status === 500) {
          Toast.show({
            text: 'Sorry, our servers are not available at the moment. Please try later.',
            buttonText: 'OK',
            duration: 5000,
          });
        } else if (error.response.status === 402) {
          store.uiStore.setDoubleLogin(true);
        } else {
          if (
            error.resposne.data &&
            error.resposne.data?.resultMessage &&
            error.resposne.data?.resultMessage != ''
          ) {
            store.uiStore.apiErrorInit({
              code: error.response.status,
              message: error.response.data?.resultMessage,
            });
          }
        }
      } catch (err) {
        // return Promise.reject(error);
        console.log('API ERROR', JSON.stringify(err));
      }
      console.log('API ERROR', JSON.stringify(error));
      return error;
    },
  );

  return instance;
};

export default axiosAuth;
