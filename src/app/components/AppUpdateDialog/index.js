/* eslint-disable prettier/prettier */
import React from 'react';
import Modal from 'react-native-modal';
import {View, TouchableOpacity, Linking} from 'react-native';
import styles from './style';
import {RobotoMediumTextView, RobotoRegTextView} from '@components';
import {AppUpdateIcon} from '@images';
import {useStores} from '@mobx/hooks';
import {observer} from 'mobx-react';
import {APK_URL} from '@env';
import {Toast} from 'native-base';
import {useLanguage} from '@hooks';

const AppUpdateDialog = props => {
  const {uiStore, loginStore} = useStores();
  const {
    appUpdateAlertMsg,
    appInstallInstructionMsg,
    appInstallMSG,
    updateLabel,
    laterLabel,
    unableToOpenUrlText,
  } = useLanguage();

  const languageData = uiStore.languageData;
  const titleText = appUpdateAlertMsg;
  const instalInstructionText = appInstallInstructionMsg;
  const descriptionText = appInstallMSG;

  const updateText = updateLabel;
  const laterText = laterLabel;

  const APKDetails = loginStore?.apkDetails;
  console.log(`Offline APK details>>>>>${APKDetails?.localPath}`);
  const APKURL = APK_URL + '/' + APKDetails?.localPath;
  // const disableLaterButton =
  //   APKDetails?.isForceUpdate || APKDetails?.isForceUpdate === 'true';
  const disableLaterButton =
    APKDetails?.isForceUpdate === 'true' ? true : false;

  const updateBtnPressed = async () => {
    const supported = await Linking.canOpenURL(APKURL);
    if (supported) {
      await Linking.openURL(APKURL);
    } else {
      Toast.show({text: unableToOpenUrlText});
    }

    //loginStore?.setAppUpdateDialog(false);
  };

  return (
    <Modal isVisible={loginStore?.appUpdateDialog}>
      <View style={styles.container}>
        <View style={styles.titleContainer}>
          <RobotoMediumTextView style={styles.titleText}>
            {titleText}
          </RobotoMediumTextView>
        </View>
        <View style={styles.childContainer}>
          <AppUpdateIcon />
          <RobotoRegTextView style={styles.descriptionText}>
            {instalInstructionText}
          </RobotoRegTextView>
          <RobotoRegTextView style={styles.descriptionText}>
            {descriptionText}
          </RobotoRegTextView>
          <View style={styles.buttonMainContainer}>
            {disableLaterButton === false && (
              <TouchableOpacity
                onPress={() => loginStore?.setAppUpdateDialog(false)}
                style={styles.secondaryButtonContainer}>
                <RobotoMediumTextView style={styles.secondaryButtonText}>
                  {laterText}
                </RobotoMediumTextView>
              </TouchableOpacity>
            )}
            <TouchableOpacity
              onPress={() => updateBtnPressed()}
              style={styles.buttonContainer}>
              <RobotoMediumTextView style={styles.buttonText}>
                {updateText}
              </RobotoMediumTextView>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    </Modal>
  );
};

export default observer(AppUpdateDialog);
