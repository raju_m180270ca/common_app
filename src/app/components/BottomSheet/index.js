//
import React, {useRef, useState, Fragment, useEffect, useContext} from 'react';
import {StyleSheet, View, FlatList, Keyboard} from 'react-native';
import BottomSheet from 'reanimated-bottom-sheet';
import Animated from 'react-native-reanimated';
import {getHp, getWp} from '@utils/ViewUtils';
import {BottomSheetHeader, SparkyCard} from '@components';
import {ArrowUpMaroon, ArrowDownMaroon} from '@images';
import {ScrollView} from 'react-native-gesture-handler';
import {useStores} from '@mobx/hooks';
import {observer} from 'mobx-react';
import {widthPercentageToDP} from 'react-native-responsive-screen';
import Orientation from 'react-native-orientation';
import moment from 'moment';
import {AuthContext} from '@contexts/auth-context';
import {MixpanelCategories, MixpanelEvents, MixpanelActions} from '@constants';

const {block, set, greaterThan, lessThan, Value, cond, sub} = Animated;

const BottomSheetComp = props => {
  const {loginStore} = useStores();
  const bottomSheetData = loginStore.sparkiesChamp;
  const {} = props;
  const [sheetOpen, setSheetOpen] = useState(false);
  const [keyboardOpen, setKeyboardOpen] = useState(false);
  const trans = new Value(0);
  const untraversedPos = new Value(0);
  const prevTrans = new Value(0);
  const [snapPoints, setSnapPoints] = useState([getHp(100), getHp(500)]);
  const auth = useContext(AuthContext);

  let desc = null;

  if (loginStore.sparkyFromDate && loginStore.sparkyToDate) {
    console.log(
      `date ==========> ${loginStore.sparkyFromDate} =>>>>>>>>> and time ${loginStore.sparkyToDate}`,
    );
    desc = `${moment(loginStore.sparkyFromDate, 'MM-DD-YYYY').format(
      'DD MMM',
    )} to ${moment(loginStore.sparkyToDate, 'MM-DD-YYYY').format(
      'DD MMM YYYY',
    )}`;
  }

  const headerPos = block([
    cond(
      lessThan(untraversedPos, sub(trans, 200)),
      set(untraversedPos, sub(trans, 200)),
    ),
    cond(greaterThan(untraversedPos, trans), set(untraversedPos, trans)),
    set(prevTrans, trans),
    untraversedPos,
  ]);

  const keyboardWillShow = () => {
    setKeyboardOpen(true);
  };

  const keyboardWillHide = () => {
    setKeyboardOpen(false);
  };

  const orientationChange = orientation => {
    if (orientation === 'LANDSCAPE') {
      // do something with landscape layout
      setSnapPoints(() => [getHp(20), 0]);
    } else {
      // do something with portrait layout

      setSnapPoints(() => [getHp(100), getHp(500)]);
      setTimeout(() => {
        btmSheet.current.snapTo(0);
      }, 200);
    }
  };

  useEffect(() => {
    setSnapPoints([getHp(100), getHp(500)]);
    Orientation.addOrientationListener(orientationChange);

    const keyboardWillShowListener = Keyboard.addListener(
      'keyboardDidShow',
      keyboardWillShow,
    );
    const keyboardWillHideListener = Keyboard.addListener(
      'keyboardDidHide',
      keyboardWillHide,
    );
    return () => {
      keyboardWillShowListener.remove();
      keyboardWillHideListener.remove();
      Orientation.removeOrientationListener(orientationChange);
    };
  }, []);

  const bottomSheetRender = itemData => {
    const {studentName, orgName, sparkieCount, grade} = itemData.item;
    return (
      <SparkyCard
        title={`${studentName} - Class ${grade}`}
        subtitle={orgName}
        pts={sparkieCount}
        key={itemData.index}
      />
    );
  };

  const SheetContent = () => (
    <ScrollView
      style={{
        backgroundColor: '#F6F6F6',
        // height: getHp(449),
        paddingHorizontal: getWp(32),
        // paddingVertical: getHp(32),
        borderWidth: 3,
        borderRadius: 5,
        borderColor: '#dddd',
        borderBottomWidth: 0,
        borderLeftWidth: 0,
        borderTopWidth: 0,
        shadowColor: '#000000',
        shadowOffset: {width: 0, height: 2},
        shadowOpacity: 0.9,
        shadowRadius: 5,
        elevation: 4,
      }}>
      <Fragment>
        <FlatList
          data={bottomSheetData}
          renderItem={bottomSheetRender}
          keyExtractor={(item, index) => `${item.studentId}_${index}`}
          snapToEnd={true}
          scrollEnabled={false}
        />
      </Fragment>
    </ScrollView>
  );

  const onOpen = () => {
    auth.trackEvent('mixpanel', MixpanelEvents.EP_COL_SPARKY, {
      Category: MixpanelCategories.LOGIN,
      Action: MixpanelActions.OPEN,
      Label: '',
    });
    setSheetOpen(true);
  };
  const onClose = () => {
    auth.trackEvent('mixpanel', MixpanelEvents.EP_COL_SPARKY, {
      Category: MixpanelCategories.LOGIN,
      Action: MixpanelActions.CLOSED,
      Label: '',
    });
    setSheetOpen(false);
  };

  const btmSheet = useRef(null);
  let SvgRight = sheetOpen ? ArrowDownMaroon : ArrowUpMaroon;
  let snapToPoint = 0;
  const onClickArrow = () => {
    snapToPoint = sheetOpen ? snapToPoint : 1;
    btmSheet.current.snapTo(snapToPoint);
  };

  const header = () => (
    <Animated.View
      style={{
        transform: [
          {
            translateY: headerPos,
          },
        ],
        borderWidth: 3,
        borderRadius: 5,
        borderColor: '#dddd',
        borderBottomWidth: 0,
        borderLeftWidth: 0,
        shadowColor: '#0000000',
        shadowOffset: {width: 0, height: 2},
        shadowOpacity: 0.9,
        shadowRadius: 5,
        elevation: 4,
      }}>
      <BottomSheetHeader
        onPress={onClickArrow}
        SvgRight={SvgRight}
        desc={desc}
      />
    </Animated.View>
  );

  const renderInner = () => <SheetContent />;

  let bottomSheetContent = (
    <BottomSheet
      ref={btmSheet}
      contentPosition={trans}
      snapPoints={snapPoints}
      renderContent={renderInner}
      renderHeader={bottomSheetData.length > 0 ? header : null}
      //renderHeader={header}
      onOpenStart={onOpen}
      onCloseStart={onClose}
      enabledContentGestureInteraction={false}
    />
  );

  if (keyboardOpen) {
    bottomSheetContent = null;
  }

  return (
    <View style={styles.container}>
      {bottomSheetContent}
      {props.children}
    </View>
  );
};

const IMAGE_SIZE = 200;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    width: widthPercentageToDP('100'),
    alignSelf: 'center',
    // backgroundColor: 'red',
    borderRadius: 10,
    elevation: 2,
  },
  box: {
    width: IMAGE_SIZE,
    height: IMAGE_SIZE,
  },
});

BottomSheetComp.propTypes = {};

BottomSheetComp.defaultProps = {};

export default observer(BottomSheetComp);
