import React from 'react';
import {View, Animated, Easing, Image} from 'react-native';
import {useStores} from '@mobx/hooks';
import styles from './indexCss';
import {useSound} from '@hooks';
import {HappyBuddy, SadBuddy} from '@images';

const Buddy = props => {
  const {playSound} = useSound();
  const {qnaStore} = useStores();
  const {style, isAnimated} = props;
  let rotateValueHolder = new Animated.Value(0);

  const rotateImage = () => {
    rotateValueHolder.setValue(0);
    Animated.timing(rotateValueHolder, {
      toValue: 1,
      duration: 400,
      easing: Easing.linear,
      useNativeDriver: true,
    }).start();
  };
 
  const RotateBuddy = rotateValueHolder.interpolate({
    inputRange: [0, 1],
    outputRange: ['0deg', '360deg'],
  });

  return (
    <View style={style ? style : styles.container}>
      {qnaStore.showDefaultBuddy === false &&
      qnaStore.isAnswerCorrect === true &&
      qnaStore.isTimeTest === false &&
      isAnimated === true ? (
        <Animated.Image
          style={{
            height: 80,
            width: 52,
            transform: [{rotate: RotateBuddy}],
          }}
          source={HappyBuddy}
          onLoad={() => {
            rotateImage();
            playSound('', '', 'correct.mp3');
          }}
        />
      ) : qnaStore.showDefaultBuddy === false &&
        qnaStore.isAnswerCorrect === false &&
        qnaStore.isTimeTest === false &&
        isAnimated === true ? (
        <Image
          style={styles.buddyImage}
          source={SadBuddy}
          onLoad={() => playSound('', '', 'incorrect.mp3')}
        />
      ) : (
        <Image style={styles.buddyImage} source={HappyBuddy} />
      )}
    </View>
  );
};
export default Buddy;
