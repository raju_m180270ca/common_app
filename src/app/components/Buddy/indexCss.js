import { StyleSheet } from 'react-native';
import { getHp, getWp } from '@utils/ViewUtils';

export default StyleSheet.create({
  container: {
    position: 'absolute',
    top: 80,
    right: 5,
    zIndex: 999,
  },
  buddyImage: {
    height: 80,
    width: 52,
  }
});
