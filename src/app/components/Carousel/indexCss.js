import {StyleSheet, Dimensions} from 'react-native';
import {COLORS} from '@constants/COLORS';
import DIMEN from '@constants/DIMEN';
const {width: viewportWidth, height: viewportHeight} = Dimensions.get('window');
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';

export default StyleSheet.create({
  carousel: {
    width: wp(100),
    height: hp(10),
  },
  slider: {
    // marginBottom: -47,
    paddingBottom: 0,
    // backgroundColor: 'red',
  },
  sliderContentContainer: {
    // paddingVertical: 10, // for custom animation
  },
  paginationContainer: {
    // paddingVertical: 8,
    alignItems: 'flex-start',
    justifyContent: 'center',
    margin: 0,
    padding: 0,
  },
  paginationDot: {
    width: 14,
    height: 14,
    borderRadius: 7,
    marginHorizontal: 1,
  },
  //   activeDotColor: COLORS.carouselDotStyle,
  //   inativeDotColor: COLORS.inactiveDotStyle,
  sliderHeight: hp(50),
});
