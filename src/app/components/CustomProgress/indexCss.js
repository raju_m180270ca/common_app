import {StyleSheet} from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';

export default StyleSheet.create({
  container: {
    // flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    paddingTop: 0,
    backgroundColor: 'transparent',
    padding: 0,
  },
  progressBar: {
    flexDirection: 'row',
    height: 20,
    width: wp('50'),
    backgroundColor: 'green',
    borderColor: 'transparent',
    borderWidth: 0,
    borderRadius: 10,
  },
});
