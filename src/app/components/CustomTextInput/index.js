import React from 'react';
import {TextInput, View} from 'react-native';
import styles from './indexCss';
import {COLORS} from '../../constants/COLORS';
import PropTypes from 'prop-types';

const CustomTextInput = props => {
  const {testID, inputStyle, input} = props;
  let bdrColor = null;
  const {isError} = props;
  if (isError) {
    bdrColor = {borderColor: COLORS.red};
  }
  return (
    <TextInput
      accessible={true}
      testID={testID}
      accessibilityLabel={testID}
      {...props}
      style={{...styles.input, ...props.style, ...bdrColor}}
      autoCapitalize="none"
      autoCompleteType="off"
      autoCorrect={false}
    />
  );
};

CustomTextInput.propTypes = {
  testID: PropTypes.testID,
  styles: PropTypes.object,
  onTextChange: PropTypes.func,
  placeholder: PropTypes.string,
  value: PropTypes.string,
};

export default CustomTextInput;
