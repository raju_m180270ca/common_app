import React, {useState} from 'react';
import {Text, View} from 'react-native';
import {Home, Hamburger, Back, Logo} from '@images';
import styles from './indexCss';
import PropTypes from 'prop-types';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';

const DashboardHeader = props => {
  const {
    svgHamburger,
    svgLogo,
    svgHamburgerStyle,
    svgLogoStyle,
    clicked,
    showHome,
    showBackArrow,
    onHamburgerClick,
    onBackButtonPressed,
  } = props;
  const [showMenu, setShowMenu] = useState(false);
  return (
    <View key="container" style={styles.container}>
      <View style={styles.hamBurger}>
        {showHome === true ? (
          <Home
            width={svgHamburgerStyle.width}
            height={svgHamburgerStyle.height}
            onPress={clicked}
          />
        ) : {showBackArrow} ? (
          <Back
            width={wp('13.52')}
            height={wp('12.52')}
            onPress={onBackButtonPressed}
          />
        ) : (
          <Hamburger
            onPress={onHamburgerClick}
            width={svgHamburgerStyle.width}
            height={svgHamburgerStyle.height}
          />
        )}
      </View>
      <View style={styles.logo}>
        <Logo width={svgLogoStyle.width} height={svgLogoStyle.height} />
      </View>
    </View>
  );
};

DashboardHeader.propTypes = {
  svgHamburger: PropTypes.func,
  svgLogo: PropTypes.func,
  clicked: PropTypes.func,
  svgLogoStyle: PropTypes.object,
  svgHamburgerStyle: PropTypes.object,
  showBackArrow: PropTypes.bool,
  onBackButtonPressed: PropTypes.func,
};

DashboardHeader.defaultProps = {
  svgHamburger: Hamburger,
  svgLogo: Logo,
  svgLogoStyle: {width: wp('32'), height: hp('9')},
  svgHamburgerStyle: {width: wp('13.5'), height: hp('6')},
  showHome: false,
  showBackArrow: false,
  onBackButtonPressed: () => {},
};

export default DashboardHeader;
