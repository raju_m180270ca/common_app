import {StyleSheet} from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';

export default StyleSheet.create({
  container: {
    width: '100%',
    height: hp('11.1'),
    alignItems: 'flex-end',
    justifyContent: 'center',
    flexDirection: 'row',
  },
  hamBurger: {
    flex: 1,
    justifyContent: 'flex-end',
    alignItems: 'flex-start',
    marginBottom: hp('3'),
    marginLeft: wp('4.5'),
  },
  logo: {
    flex: 4,
    justifyContent: 'flex-end',
    alignItems: 'flex-start',
    marginBottom: hp('2'),
    marginLeft: wp('10'),
  },
});
