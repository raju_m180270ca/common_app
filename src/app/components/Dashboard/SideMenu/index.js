import React from 'react';
import {View} from 'react-native';
import SideMenuItem from './SideMenuItem';
import {
  Save,
  Message,
  Leaderboard,
  Reward,
  Store,
  StarredQuestion,
  Setting,
  Preview,
  Notification,
} from '@images';
import styles from './indexCss';
import {useNavigation} from '@react-navigation/native';
import {useStores} from '@mobx/hooks';
import PropTypes from 'prop-types';
import {observer} from 'mobx-react';
import {useBackHandler} from '@react-native-community/hooks';

const SideMenu = props => {
  const {
    testID,
    containerStyle,
    SvgMessage,
    SvgLeaderboard,
    SvgReward,
    SvgStarredQuestions,
    SvgNotification,
    SvgSetting,
    clickedMenuItem,
    permissions,
  } = props;

  const navigation = useNavigation();
  const store = useStores();

  useBackHandler(() => {
    return props.navigation.navigate('DashboardScreen');
  });

  return (
    <View
      accessible={true}
      testID={testID}
      accessibilityLabel={testID}
      style={{...styles.container, ...containerStyle}}>
      <View style={styles.iconContainer}>
        {/* <SideMenuItem SvgItem={SvgSave} clicked={() => alert('hi')} /> */}
        {permissions.starredQuestions && (
          <SideMenuItem
            testID="SideMenuItemSideMenuStarredQuestion"
            SvgItem={SvgStarredQuestions}
            clicked={() => navigation.navigate('StarredQuestionsScreen')}
          />
        )}
        {permissions.mailbox && (
          <SideMenuItem
            testID="SideMenuItemSideMenuMessage"
            SvgItem={SvgMessage}
            clicked={() => clickedMenuItem('message')}
          />
        )}
        {permissions.leaderboard && (
          <SideMenuItem
            testID="SideMenuItemSideMenuLeaderboard"
            SvgItem={SvgLeaderboard}
            clicked={() => navigation.navigate('Leaderboard')}
          />
        )}
        {permissions.reward && (
          <SideMenuItem
            testID="SideMenuItemSideMenuReward"
            SvgItem={SvgReward}
            clicked={() => navigation.navigate('RewardsScreen')}
          />
        )}
        {store?.uiStore?.menuDataPermissions?.home?.notification && (
          <SideMenuItem
            testID="SideMenuItemSideMenuNotification"
            SvgItem={SvgNotification}
            notificationCount={store?.appStore?.userData?.notificationCount}
            clicked={() => clickedMenuItem('notification')}
          />
        )}
        {permissions.profile && (
          <SideMenuItem
            testID="SideMenuItemSideMenuSetting"
            SvgItem={SvgSetting}
            clicked={() => navigation.navigate('ProfileScreen')}
          />
        )}
        {store.uiStore.menuDataPermissions?.home?.contentPreview && (
          <SideMenuItem
            testID="SideMenuItemSideMenuPreview"
            SvgItem={Preview}
            clicked={() => navigation.navigate('PreviewQnASearchScreen')}
          />
        )}
        {/* {Config.IS_SEARCH_SHOWN === 'true' && (
          <SideMenuItem
            SvgItem={SearchYellow}
            clicked={() => navigation.navigate('SearchQuestionScreen')}
          />
        )} */}
        {/* {permissions.store && (
          <SideMenuItem
            SvgItem={SvgStore}
            clicked={() => clickedMenuItem('save')}
          />
        )}
        {permissions.notification && (
          <SideMenuItem
            SvgItem={SvgNotification}
            clicked={() => clickedMenuItem('save')}
          />
        )}
        {false && (
          <SideMenuItem
            SvgItem={SvgSetting}
            clicked={() => clickedMenuItem('save')}
          />
        )} */}
        <View
          style={{
            alignItems: 'center',
            justifyContent: 'center',
          }}>
          {/* <Save width={30} height={30} style={{marginBottom: 30}} /> */}
          {/* <CustomProgress
              currentProgress={54}
              progressColor={COLORS.red}
              borderRadius={30}
              containerStyle={{height: 7}}
              progressBarStyle={{backgroundColor: COLORS.disabledGray}}
              commonStyle={{
                borderTopLeftRadius: 10,
                borderBottomLeftRadius: 10,
                borderRadius: 30,
                height: 7,
              }}
            /> */}
        </View>
      </View>
    </View>
  );
};

SideMenu.propTypes = {
  testID: PropTypes.string,
  containerStyle: PropTypes.func,
  SvgSave: PropTypes.func,
  SvgMessage: PropTypes.func,
  SvgLeaderboard: PropTypes.func,
  SvgReward: PropTypes.func,
  SvgStore: PropTypes.func,
  SvgStarredQuestions: PropTypes.func,
  SvgNotification: PropTypes.func,
  SvgSetting: PropTypes.func,
  clickedMenuItem: PropTypes.func,
};

SideMenu.defaultProps = {
  testID: 'SideMenu',
  SvgSave: Save,
  SvgMessage: Message,
  SvgLeaderboard: Leaderboard,
  SvgReward: Reward,
  SvgStore: Store,
  SvgStarredQuestions: StarredQuestion,
  SvgSetting: Setting,
  SvgNotification: Notification,
  clickedMenuItem: () => {},
};

export default observer(SideMenu);
