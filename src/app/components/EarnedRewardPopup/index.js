/* eslint-disable react-native/no-inline-styles */
import React, {Fragment} from 'react';
import {Text, View, TouchableOpacity} from 'react-native';
import PropTypes from 'prop-types';
import Modal from 'react-native-modal';
import {BalooThambiRegTextView, RoundedButton} from '@components';
import {SimpleLottie} from '@components';
import {YouRecievedSVG} from '@images';
import {useStores} from '@mobx/hooks';
import {getWp, getHp} from '@utils';
import styles from './indexCss';
import {SvgUri} from 'react-native-svg';
import {observer} from 'mobx-react';
import {useLanguage} from '@hooks';

const EarnedRewardPopup = params => {
  const {loginStore, appStore} = useStores();
  const {show = true, renderOkay = false} = params;
  const {tapAnyWhereToClose, okayBtnText} = useLanguage();

  const handlePress = () => {
    console.log('BUTTON PRESSED ');
    if (appStore.earnedRewardData.onPress) {
      appStore.earnedRewardData.onPress();
    }
    appStore.setEarnedRewardData(appStore.earnedRewardDataSchema);
  };
  const EarnedRewardContent = () => {
    const {title = undefined, badge = undefined} =
      appStore.earnedRewardData?.earnedContent;
    if (title) {
      return (
        <View style={styles.earndRewardContentContainer}>
          <SvgUri
            uri={title.titleImg}
            height={getWp(40)}
            width={getWp(240)}
            preserveAspectRatio={'none'}
          />
          <BalooThambiRegTextView style={styles.earnedBadgeTextStyle}>
            {title.titleName}
          </BalooThambiRegTextView>
        </View>
      );
    } else if (badge && !title) {
      return (
        <View
          style={{...styles.earndRewardContentContainer, marginTop: getHp(35)}}>
          <SvgUri
            uri={badge.badgeImg}
            height={getWp(130)}
            width={getWp(130)}
            preserveAspectRatio={'none'}
          />
          <BalooThambiRegTextView
            style={{
              ...styles.earnedBadgeTextStyle,
              position: 'relative',
              marginTop: getHp(15),
              color: '#000',
            }}>
            {badge.badgeName}
          </BalooThambiRegTextView>
        </View>
      );
    } else {
      return null;
    }
  };
  return (
    <Modal
      isVisible={appStore.earnedRewardData?.isVisible || false}
      onBackdropPress={handlePress}>
      <TouchableOpacity onPress={handlePress}>
        <View style={styles.modalOuter}>
          <View style={styles.modalContainer}>
            <View style={styles.youRecivedConatiner}>
              <YouRecievedSVG />
            </View>
            <View style={styles.bgRaysContainer}>
              <SimpleLottie
                theme={`rewardLotties`}
                jsonFileName="rewardBGRays"
                speed={0.3}
              />
            </View>
            {/* <CapSVG height={getWp(150)} width={getWp(150)} style={styles.capSvgStyle} /> */}
            <EarnedRewardContent />
            <BalooThambiRegTextView style={styles.bottomTextStyle}>
              {tapAnyWhereToClose}
            </BalooThambiRegTextView>
            {renderOkay && (
              <RoundedButton
                text={okayBtnText}
                type={`elevatedOrange`}
                width={styles.okayBtnContainerStyle.width}
                height={styles.okayBtnContainerStyle.height}
                containerStyle={styles.okayBtnContainerStyle}
              />
            )}
          </View>
        </View>
      </TouchableOpacity>
    </Modal>
  );
};

EarnedRewardPopup.propTypes = {
  isModalVisible: PropTypes.bool.isRequired,
};

EarnedRewardPopup.defaultProps = {
  isModalVisible: false,
};

export default observer(EarnedRewardPopup);
