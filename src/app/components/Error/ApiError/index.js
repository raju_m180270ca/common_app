import React from 'react';
import {View} from 'react-native';
import {CustomModal} from '@components';
import {BalooThambiBoldTextView, BalooThambiRegTextView} from '@components';
import {Internet} from '@images';
import {useStores} from '@mobx/hooks';
import {observer} from 'mobx-react';
import styles from './indexCss';
import {useLanguage} from '@hooks';

const ApiError = props => {
  const store = useStores();
  let title = null;
  let message = null;
  const {loggedOutText, sessionLoggedOutMsg, loginAgainBtnText, okayBtnText} =
    useLanguage();
  const apiErrorHandler = () => {
    if (store?.uiStore?.apiStatusCode === 401) {
      store.loginStore.setIsAuth(false);
      store.appStore.setJwt(null);
      store.loginStore.setSkipOnBoardingScreen(true);
    }
    store.uiStore.apiErrorReset();
  };

  if (store.uiStore.apiStatusCode) {
    if (store.uiStore.apiStatusCode === 'Oops!') {
      title = store.uiStore.apiStatusCode;
    } else {
      title =
        store?.uiStore?.apiStatusCode === 401
          ? loggedOutText
          : `Status ${store.uiStore.apiStatusCode}`;
    }
  }
  if (store.uiStore.apiErrorMessage) {
    message = (
      <BalooThambiRegTextView style={styles.secondaryTextStyle}>
        {store?.uiStore?.apiStatusCode === 401
          ? sessionLoggedOutMsg
          : store.uiStore.apiErrorMessage}
      </BalooThambiRegTextView>
    );
  }

  return (
    <CustomModal
      show={store.uiStore.apiError}
      onPress={apiErrorHandler}
      btnText={
        store?.uiStore?.apiStatusCode === 401 ? loginAgainBtnText : okayBtnText
      }
      {...props}
      containerStyle={styles.modalContainerStyle}>
      <View style={styles.errorView}>
        <View style={styles.svgContainer}>
          <Internet />
        </View>
        <BalooThambiBoldTextView style={styles.textStyle}>
          {title}
        </BalooThambiBoldTextView>
        {message}
      </View>
    </CustomModal>
  );
};

ApiError.propTypes = {};

ApiError.defaultProps = {};

export default observer(ApiError);
