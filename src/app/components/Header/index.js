import React, {useContext} from 'react';
import {View, Platform, StatusBar, Text, TouchableOpacity} from 'react-native';
import {
  Home,
  Logo,
  Hamburger,
  Back,
  NaandiLogout,
  HomeMenuIcon,
  LeaderboardIcon,
} from '@images';
import styles from './style';
import PropTypes from 'prop-types';
import {getWp, getHp, getStatusBarHeight} from '@utils';
import {COLORS} from '@constants';
import {
  BalooThambiRegTextView,
  SVGImageBackground,
  RoundedVerticalImageButton,
} from '@components';
import {useStores} from '@mobx/hooks';
import {AuthContext} from '@contexts/auth-context';
import {SvgUri} from 'react-native-svg';
import {MixpanelCategories, MixpanelEvents, MixpanelActions} from '@constants';
import {useNavigation} from '@react-navigation/native';
import {walkthroughable, CopilotStep} from 'react-native-copilot';

const WalkthroughableView = walkthroughable(View);

const DashboardHeader = props => {
  const {
    testID,
    type,
    onClick,
    title,
    desc,
    svgUrl,
    qnaScreen,
    secondaryBtnPressed,
    enableSecondaryBtn,
    containerStyle,
    customTitleStyle,
    fromHome,
  } = props;
  const navigation = useNavigation();
  const store = useStores();
  const auth = useContext(AuthContext);
  const languageData = store.uiStore.languageData;
  const homeText =
    languageData &&
    languageData?.hasOwnProperty('btn') &&
    languageData?.btn?.hasOwnProperty('home')
      ? languageData?.btn?.home
      : 'Home';

  const renderLogo = () => {
    switch (type) {
      case 'home':
        return <Home width={getWp(56)} height={getHp(52)} onPress={onClick} />;
      case 'menu':
        return (
          <Hamburger onPress={onClick} width={getWp(56)} height={getHp(52)} />
        );
      case 'back':
        return <Back onPress={onClick} width={getWp(56)} height={getHp(52)} />;
      case 'logout':
        return (
          <TouchableOpacity style={styles.buttonContainer} onPress={onClick}>
            <NaandiLogout width={getWp(56)} height={getHp(52)} />
          </TouchableOpacity>
        );
      case 'naandi_home':
        return (
          <RoundedVerticalImageButton
            text={homeText}
            SvgImage={HomeMenuIcon}
            onPress={onClick}
          />
        );
    }
  };

  let descText = null;

  if (desc) {
    descText = (
      <BalooThambiRegTextView style={styles.description}>
        {desc}
      </BalooThambiRegTextView>
    );
  }

  let svgImage = null;
  if (svgUrl) {
    {
      svgImage = svgUrl.includes('.svg') && (
        <View style={styles.svgContainer}>
          <SvgUri
            accessible={true}
            testID="HeaderSvgUri"
            accessibilityLabel="HeaderSvgUri"
            width={styles.topicIcon.width}
            height={styles.topicIcon.height}
            uri={svgUrl}
          />
        </View>
      );
    }
  }

  let feedbackButton = null;
  if (
    fromHome &&
    store.uiStore.menuDataPermissions &&
    store.uiStore.menuDataPermissions.home &&
    store.uiStore.menuDataPermissions.home.leaderboard
  ) {
    feedbackButton = (
      <View style={styles.feedbackContainer}>
        <LeaderboardIcon
          onPress={() => {
            auth.trackEvent('mixpanel', MixpanelEvents.GO_TO_LEADERBOARD, {
              Category: MixpanelCategories.LEADERBOARD,
              Action: MixpanelActions.CLICKED,
              Label: '',
            });
            navigation.navigate('Leaderboard');
          }}
          width={getWp(56)}
          height={getHp(52)}
        />
      </View>
    );
  }

  /**
   * This is to use this header in Search screen
   */

  if (qnaScreen) {
    styles.mainContainer = {
      ...styles.mainContainer,
      position: 'absolute',
    };
  } else {
    styles.mainContainer = {
      ...styles.mainContainer,
      position: 'relative',
    };
  }

  return (
    <View
      accessible={true}
      testID={testID}
      accessibilityLabel={testID}
      style={[styles.mainContainer, containerStyle]}>
      <SVGImageBackground
        testID="SVGImageBackgroundHeaderSVGImage"
        SvgImage="bgHeader"
        themeBased
        screenBg>
        {Platform.OS === 'android' ? (
          <StatusBar
            accessible={true}
            testID="HeaderStatusBarAndroid"
            accessibilityLabel="HeaderStatusBarAndroid"
            backgroundColor={COLORS.white}
            barStyle="dark-content"
          />
        ) : (
          <View>
            <StatusBar
              accessible={true}
              testID="HeaderStatusBar"
              accessibilityLabel="HeaderStatusBar"
              barStyle="dark-content"
            />
            <View style={{height: getStatusBarHeight()}} />
          </View>
        )}
        <View key="container" style={styles.container}>
          {title != null && title.length > 0 ? (
            <View style={styles.titleContainer}>
              {svgImage}
              <BalooThambiRegTextView
                testID="HeaderTitleText"
                style={{...styles.titleText, ...customTitleStyle}}
                numberOfLines={1}>
                {title}
              </BalooThambiRegTextView>
              <Text
                style={{
                  flex: 1,
                  textAlign: 'center',
                  fontSize: 14,
                }}>
                {descText}
              </Text>
            </View>
          ) : (
            <View style={styles.logo}>
              <Logo width={getWp(152)} height={getHp(74)} />
            </View>
          )}

          {store.uiStore.showHomepageOverlay ? (
            <CopilotStep
              text="For more exciting features, click here."
              order={1}
              name="Menu">
              <WalkthroughableView style={styles.hamBurger}>
                {renderLogo()}
              </WalkthroughableView>
            </CopilotStep>
          ) : (
            <View style={styles.hamBurger}>{renderLogo()}</View>
          )}
          {store.uiStore.showHomepageOverlay ? (
            <CopilotStep
              text="To benchmark your learning with your peers at an international and national level, click here."
              order={2}
              name="LeaderBoard">
              <WalkthroughableView style={styles.rightContainer}>
                {enableSecondaryBtn == true ? (
                  <TouchableOpacity
                    style={styles.searchQuestionContainer}
                    onPress={secondaryBtnPressed}>
                    <BalooThambiRegTextView
                      testID="HeaderQuestionText"
                      style={styles.searchQuestionText}>
                      Search Questions
                    </BalooThambiRegTextView>
                  </TouchableOpacity>
                ) : (
                  feedbackButton
                )}
              </WalkthroughableView>
            </CopilotStep>
          ) : (
            <View style={styles.rightContainer}>
              {enableSecondaryBtn == true ? (
                <TouchableOpacity
                  style={styles.searchQuestionContainer}
                  onPress={secondaryBtnPressed}>
                  <BalooThambiRegTextView
                    testID="HeaderQuestionText"
                    style={styles.searchQuestionText}>
                    Search Questions
                  </BalooThambiRegTextView>
                </TouchableOpacity>
              ) : (
                feedbackButton
              )}
            </View>
          )}
        </View>
      </SVGImageBackground>
    </View>
  );
};

DashboardHeader.propTypes = {
  testID: PropTypes.string,
  onClick: PropTypes.func,
  type: PropTypes.string,
  title: PropTypes.string,
  containerStyle: PropTypes.object,
};

DashboardHeader.defaultProps = {
  testID: 'DashboardHeader',
  onClick: () => {},
  title: null,
  hideEILogo: false,
  containerStyle: {flex: 0.15},
};

export default DashboardHeader;
