import {StyleSheet, Platform} from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {COLORS, TEXTFONTSIZE} from '@constants';
import {getWp, getHp} from '@utils';
import DeviceInfo from 'react-native-device-info';

export default StyleSheet.create({
  mainContainer: {
    backgroundColor: 'transparent',
    height:Platform.OS === 'ios' ? getHp(90) : getHp(104),
    width: '100%',
    flex:0.15
  },

  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'flex-end',
    flexDirection: 'column',
    // backgroundColor: 'red',
    position: 'relative',
    paddingBottom: getHp(18),
  },

  hamBurger: {
    position: 'absolute',
    justifyContent: 'flex-end',
    alignItems: 'flex-start',
    bottom: getHp(20),
    //left: getWp(10),
    left: getWp(5),
  },

  logo: {
    position: 'absolute',
    justifyContent: 'flex-end',
    alignItems: 'center',
    bottom: getHp(20),
  },

  logoutButtonContainer: {
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
    width: getWp(67),
    height: getWp(59),
    backgroundColor: COLORS.soundButtonBackgroundColor,
    borderRadius: getWp(9),
  },

  titleContainer: {
    alignItems: 'center',
    justifyContent: 'flex-end',
    marginLeft: '10%',//getWp(40),
    marginRight: '5%',//getWp(10),
    marginTop:'2%',
    textAlign: 'center',
    // backgroundColor:'red',
    paddingHorizontal: getWp(15),
  },

  logoutText: {
    fontSize: TEXTFONTSIZE.Text16,
    color: COLORS.white,
  },

  titleText: {
    fontSize: TEXTFONTSIZE.Text15,
    color: COLORS.screenTestDescriptionTextColor,
    lineHeight: DeviceInfo.isTablet() ? getHp(8) :  getHp(18),
    textAlign: 'center',
    width: getWp(318)
  },
  description: {
    color: COLORS.infoMessageGray,
    fontSize: TEXTFONTSIZE.Text12,
    alignSelf: 'center',
  },
  topicIcon: {
    width: getWp(40),
    height: getHp(40),
  },
  svgContainer: {
    marginBottom: getHp(3),
    // marginTop: getWp(20)
  },

  buttonContainer: {
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
    width: getWp(67),
    height: getWp(59),
    backgroundColor: COLORS.soundButtonBackgroundColor,
    borderRadius: getWp(9),
  },
  rightContainer:{
    alignSelf: 'flex-end',
  },
  searchQuestionContainer: {
    width: getWp(70),
    height: getWp(60),
    borderRadius: getWp(10),
    backgroundColor: COLORS.soundButtonBackgroundColor,
    alignItems: 'center',
    justifyContent: 'center',
    overflow: 'hidden',
  },
  feedbackContainer: {
    overflow: 'hidden',
    alignSelf: 'flex-end',
    marginRight: getWp(18),
    alignItems: 'center',
    justifyContent: 'center',
  },
  searchQuestionText: {
    fontSize: TEXTFONTSIZE.Text13,
    color: COLORS.white,
    textAlign: 'center',
  },
});
