import {StyleSheet} from 'react-native';
import {COLORS} from '@constants/COLORS';
import DIMEN from '@constants/DIMEN';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {getHp, getWp} from '@utils/ViewUtils';

export default StyleSheet.create({
  container: {
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row',
    // backgroundColor: '#38A8DF',
    marginTop: getHp(10),
    marginBottom: getHp(50),
  },
  itemContainer: {flex: 1, alignItems: 'center'},
  svgTextStyle: {color: COLORS.white},
});
