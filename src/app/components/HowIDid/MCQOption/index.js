import React from 'react';
import {View} from 'react-native';
import styles from './style';
import {RobotoRegTextView, MyAutoHeightWebView} from '@components';
import PropTypes from 'prop-types';
import {COLORS} from '@constants';
import getHtmlTemplate from '@utils/getHtmlTemplate';

import {useStores} from '@mobx/hooks';
const MCQOption = props => {
  const {
    testID,
    index,
    option,
    showAns,
    answer,
    userAnswer,
    containerStyle,
    optionContainerStyle,
    optionTextStyle,
    webContentStyle,
    isRTL,
  } = props;
  const char = 'A';
  var optionHtml = getHtmlTemplate(option, false, isRTL);

  return (
    <View
      accessible={true}
      testID={testID}
      accessibilityLabel={testID}
      style={[isRTL ? styles.RTLContainer : styles.container, containerStyle]}>
      <View
        style={[
          styles.optionContainer,
          optionContainerStyle,
          showAns &&
            index === userAnswer &&
            userAnswer === answer && {
              backgroundColor: COLORS.worksheetCorrectCountBackgroundColor,
            },
          userAnswer !== answer &&
            index === userAnswer && {
              backgroundColor: COLORS.worksheetWrongCountBackgroundColor,
            },
        ]}>
        <RobotoRegTextView
          testID="MCQOptionTextView"
          style={[
            styles.option,
            optionTextStyle,
            showAns &&
              index === userAnswer &&
              userAnswer === answer && {color: COLORS.white},
            userAnswer !== answer &&
              index === userAnswer && {color: COLORS.white},
          ]}>
          {String.fromCharCode(char.charCodeAt(0) + index)}
        </RobotoRegTextView>
      </View>

      <MyAutoHeightWebView
        testID="MyAutoHeightWebViewMCQOption"
        style={[styles.webViewStyle, webContentStyle]}
        files={[]}
        customScript={''}
        customStyle={`
          `}
        onSizeUpdated={() => {}}
        source={{
          html: optionHtml,
        }}
        zoomable={true}
      />
    </View>
  );
};

MCQOption.propTypes = {
  testID: PropTypes.string,
  index: PropTypes.number,
  showAns: PropTypes.bool,
  containerStyle: PropTypes.any,
  optionContainerStyle: PropTypes.any,
  optionTextStyle: PropTypes.any,
  webContentStyle: PropTypes.any,
};

MCQOption.defaultProps = {
  testID: 'MCQOption',
  type: 'neutral',
};
export default MCQOption;
