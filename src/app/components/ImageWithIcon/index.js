import React from 'react';
import {View, Image} from 'react-native';
import styles from './indexCss';
import PropTypes from 'prop-types';
import {SvgUri} from 'react-native-svg';
import {getHp} from '@utils/ViewUtils';
import {BoyPng, GirlPng, NeutralPng} from '@images';

const ImageWithIcon = props => {
  const {
    testID,
    imageUrl,
    containerStyle,
    imageStyle,
    iconContainerStyle,
    gender,
  } = props;
  var imageExt = imageUrl.split('.').pop();

  return (
    <View
      accessible={true}
      testID={testID}
      accessibilityLabel={testID}
      key="container"
      style={{...styles.container, ...containerStyle}}>
      {imageExt == 'svg' ? (
        <SvgUri
          accessible={true}
          testID="ImageWithIconImage"
          accessibilityLabel="ImageWithIconImage"
          width={getHp(70)}
          height={getHp(70)}
          uri={imageUrl}
        />
      ) : (
        <Image
          accessible={true}
          testID="ImageWithIconImage"
          accessibilityLabel="ImageWithIconImage"
          style={{...styles.image, ...imageStyle}}
          source={
            imageUrl !== ''
              ? {uri: imageUrl}
              : gender === 'F'
              ? GirlPng
              : gender === 'M'
              ? BoyPng
              : NeutralPng
          }
        />
      )}
    </View>
  );
};

ImageWithIcon.propTypes = {
  testID: PropTypes.string,
  imageUrl: PropTypes.string.isRequired,
  iconUrl: PropTypes.string.isRequired,
  containerStyle: PropTypes.object,
  imageStyle: PropTypes.object,
  iconContainerStyle: PropTypes.object,
};

ImageWithIcon.defaultProps = {
  testID: 'ImageWithIcon',
  imageUrl: '',
  iconUrl: '',
  imageStyle: styles.image,
};

export default ImageWithIcon;
