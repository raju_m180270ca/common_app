import React, {useContext} from 'react';
import {View, TouchableWithoutFeedback} from 'react-native';
import {CustomButton,RoundedButton} from '@components';
import {SourceSansProRegTextView} from '@components';
import {getHp} from '@utils/ViewUtils';
import {useNavigation} from '@react-navigation/native';
import {useStores} from '@mobx/hooks';
import {AuthContext} from '@contexts/auth-context';
import {MixpanelCategories, MixpanelEvents, MixpanelActions} from '@constants';
import styles from './indexCss';
import PropTypes from 'prop-types';
import {useLanguage} from '@hooks';

const LoginFooterBtn = props => {
  const {
    testID,
    type,
    text,
    onPress,
    containerStyle,
    textStyle,
    btnStyle,
    isForgotPasswordShown,
    ppContainerStyle,
    forgotpassword,
  } = props;
  const navigation = useNavigation();
  const {forgotPasswordText} = useLanguage();
  const auth = useContext(AuthContext);
  const {loginStore} = useStores();

  let btnContainerStyle = {
    ...styles.container,
    ...containerStyle,
  };

  if (ppContainerStyle) {
    btnContainerStyle = {
      ...ppContainerStyle,
    };
  }

  const onForgotPassWordClick = () => {
    loginStore.setIsOtpLogin(false);
    auth.trackEvent('mixpanel', MixpanelEvents.FORGOT_PASSWORD_OPEN, {
      Category: MixpanelCategories.LOGIN,
      Action: MixpanelActions.CLICKED,
      Label: '',
    });
    navigation.navigate('ForgotPasswordScreen');
  };

  console.log('CStyle>>>', JSON.stringify(btnStyle.width));

  let forgotPassword = (
    <TouchableWithoutFeedback onPress={onForgotPassWordClick}>
      <View>
        <SourceSansProRegTextView
          testID="LoginFooterBtnforgotPasswordText"
          style={{...styles.text, ...styles.subTitle}}>
          {forgotPasswordText}
        </SourceSansProRegTextView>
      </View>
    </TouchableWithoutFeedback>
  );

  if (!forgotpassword) {
    forgotPassword = null;
  }
  return (
    <View style={btnContainerStyle}>
      {/* <CustomButton
        disabled={props.disabled}
        testId={testID}
        onSubmit={onPress}
        btnText={text}
      /> */}

      <RoundedButton
        testID="RoundedButtonLoginFooterBtn"
        type={type}
        text={text}
        width={btnStyle.width ? btnStyle.width : styles.submitBtn.width}
        height={btnStyle.height ? btnStyle.height : styles.submitBtn.height}
        borderRadius={50}
        containerStyle={{ ...styles.submitBtn, ...btnStyle }}
        onPress={onPress}
        textStyle={{ ...styles.btnText, ...textStyle }}
        {...props}
      />
    </View>
  );
};

LoginFooterBtn.propTypes = {
  testID: PropTypes.string,
  text: PropTypes.string.isRequired,
  onPress: PropTypes.func.isRequired,
  containerStyle: PropTypes.object,
  textStyle: PropTypes.object,
  btnStyle: PropTypes.object,
  isForgotPasswordShown: PropTypes.bool,
  type: 'primaryOrange',
  forgotpassword: PropTypes.bool,
};

LoginFooterBtn.defaultProps = {
  testID: 'LoginFooterBtn',
  text: 'LOGIN',
  isForgotPasswordShown: true,
  onPress: () => alert('clicked!'),
  btnStyle: {
    width: '100%',
    height: getHp(60),
  },
  forgotpassword: true,
};

export default LoginFooterBtn;
