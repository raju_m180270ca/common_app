import {StyleSheet} from 'react-native';
import {COLORS, DIMEN, TEXTFONTSIZE} from '@constants';
import {getHp, getWp} from '@utils/ViewUtils';

export default StyleSheet.create({
  container: {
    position: 'absolute',
    bottom: getHp(20),
    left: 0,
    right: 0,
    paddingHorizontal: getWp(33),
    alignItems: 'center',
  },
  submitBtn: {
    width: '100%',
    height: getHp(60),
  },
  btnText: {
    fontSize: TEXTFONTSIZE.Text16,
  },
  text: {
    marginBottom: getHp(24),
    fontSize: TEXTFONTSIZE.Text20,
  },
  subTitle: {fontSize: TEXTFONTSIZE.Text20, color: COLORS.orange},
});
