import React from 'react';
import {View} from 'react-native';
import {useNavigation} from '@react-navigation/native';
import {COLORS} from '@constants';
import {SmallRoundButton} from '@components';
import styles from './indexCss';
import PropTypes from 'prop-types';
import {Logo, GuestAccount, Leaves, BackgroundFigure} from '@images';

const Header = props => {
  const {testID, containerStyle, iconStyle, hideBackButton, onBack, isNewFlow} =
    props;
  var navigation = useNavigation();

  const onBackPress = () => {
    if (onBack) {
      onBack();
    } else {
      navigation.goBack();
    }
  };

  return (
    <View
      accessible={true}
      testID={testID}
      accessibilityLabel={testID}
      style={{...styles.container, ...containerStyle}}>
      {/* <SimpleLottie
        jsonFileName={lottieFileName}
        theme={theme}
        style={styles.lottie}
      /> */}
      <View style={styles.lottieContainer}>
        {/* <SimpleLottie
          testID={"SimpleLottieLoginHeader"}
          jsonFileName={lottieFileName}
          theme={theme}
          style={styles.lottie}
          autoResize={true}
          resizeMode="cover"
        /> */}
        <BackgroundFigure
          accessible={true}
          testID="LoginHeaderBackgroundImg"
          accessibilityLabel="LoginHeaderBackgroundImg"
          style={styles.backgroundFigure}
        />
        <Leaves
          accessible={true}
          testID="LoginHeaderLeavesImg"
          accessibilityLabel="LoginHeaderLeavesImg"
          style={styles.leaves}
        />
        <Logo
          accessible={true}
          testID="LoginHeaderLogoImg"
          accessibilityLabel="LoginHeaderLogoImg"
          style={styles.logo}
          height={styles.logo.height}
          width={styles.logo.width}
        />
        <GuestAccount
          accessible={true}
          testID="LoginGuestAccountImg"
          accessibilityLabel="LoginGuestAccountImg"
          style={styles.students}
        />
      </View>
      {!hideBackButton && (
        <View
          style={{
            ...(isNewFlow ? styles.newRoundBtn : styles.roundBtn),
            ...iconStyle,
          }}>
          <SmallRoundButton
            testID={'SmallRoundButtonLoginHeader'}
            onPress={() => onBackPress()}
            iconName={iconStyle.iconName}
            iconColor={iconStyle.iconColor}
            iconTheme={iconStyle.iconTheme}
            type={iconStyle.type}
            width={iconStyle.width}
            height={iconStyle.height}
            borderRadius={20}
          />
        </View>
      )}
    </View>
  );
};

Header.propTypes = {
  testID: PropTypes.string,
  theme: PropTypes.string,
  lottieFileName: PropTypes.string,
  onPress: PropTypes.func.isRequired,
  containerStyle: PropTypes.object,
  iconStyle: PropTypes.object,
  hideBackButton: PropTypes.bool,
};

Header.defaultProps = {
  testID: 'LoginHeader',
  theme: 'ocean',
  lottieFileName: 'naandi_header',
  hideBackButton: false,
  iconStyle: {
    iconName: 'left',
    iconColor: COLORS.white,
    iconTheme: 'AntDesign',
    type: 'lightPink',
    width: 40,
    height: 40,
  },
};

export default Header;
