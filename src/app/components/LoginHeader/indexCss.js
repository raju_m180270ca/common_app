import {StyleSheet} from 'react-native';
import {COLORS, DIMEN} from '@constants';
import {getHp, getWp} from '@utils/ViewUtils';

export default StyleSheet.create({
  container: {
    width: '100%',
    height: getHp(200),
    backgroundColor: COLORS.white,
  },

  lottieContainer: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    width: getWp(414),
  },

  lottie: {
    width: '100%',
    height: '100%',
    padding: 0,
    marginStart: 0,
    marginEnd: 0,
    // backgroundColor: 'red',
  },
  roundBtn: {position: 'absolute', top: getHp(30), left: getWp(32)},
  newRoundBtn: {
    position: 'absolute',
    top: getHp(99),
    left: getWp(62),
  },
  backgroundFigure: {
    marginTop: getHp(35),
  },

  leaves: {
    position: 'absolute',
    top: getHp(82),
    left: getWp(-25),
  },
  logo: {
    position: 'absolute',
    top: getHp(75),
    left: getWp(113),
    height: '45%',
    width: '47%',
  },
  students: {
    position: 'absolute',
    top: getHp(50),
    right: getWp(12),
  },
});
