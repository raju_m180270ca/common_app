'use strict';
import React, {
  useState,
  useEffect,
  useRef,
  useImperativeHandle,
  forwardRef,
} from 'react';

import {StyleSheet, Platform, ViewPropTypes} from 'react-native';

import PropTypes from 'prop-types';

import {WebView} from 'react-native-webview';
import UserAgent from 'react-native-user-agent';

import {
  reduceData,
  getWidth,
  isSizeChanged,
  shouldUpdate,
} from '@utils/AutoHeightWebViewUtils';
import {Toast} from 'native-base';
import {CONFIG_KEY} from '@env';

const MyAutoHeightWebView = forwardRef((props, ref) => {
  const {testID, style, onMessage, onSizeUpdated, source, androidAcceleration} =
    props;

  if (!source) {
    return null;
  }

  let webView = useRef();
  let userAgent = UserAgent.getUserAgent();
  useImperativeHandle(ref, () => ({
    stopLoading: () => webView.current.stopLoading(),
    goForward: () => webView.current.goForward(),
    goBack: () => webView.current.goBack(),
    reload: () => webView.current.reload(),
    postMessage: e => webView.current.postMessage(e),
    injectJavaScript: script => webView.current.injectJavaScript(script),
  }));

  const [size, setSize] = useState({
    height: style && style.height ? style.height : 0,
    width: getWidth(style),
  });
  const handleMessage = event => {
    try {
      // console.log('HANDLEMESSAGE>>', event);
      //Toast.show({text: 'HANDLEMESSAGE:::' + event.nativeEvent.data});
      onMessage && onMessage(event);

      if (!event.nativeEvent) {
        return;
      }
      let data = {};
      // Sometimes the message is invalid JSON, so we ignore that case
      try {
        data = JSON.parse(event.nativeEvent.data);
      } catch (error) {
        //console.error(error);
        return;
      }
      const {height, width} = data;
      const {height: previousHeight, width: previousWidth} = size;
      isSizeChanged({height, previousHeight, width, previousWidth}) &&
        setSize({
          height,
          width,
        });
    } catch (err) {
      Toast.show({text: 'HANDLEMESSAGE error:::' + err});
    }
  };

  const {currentSource, script} = reduceData(props);

  const {width, height} = size;
  useEffect(
    () =>
      onSizeUpdated &&
      onSizeUpdated({
        height,
        width,
      }),
    [width, height, onSizeUpdated],
  );

  return (
    <WebView
      accessible={true}
      testID={testID}
      accessibilityLabel={testID}
      {...props}
      ref={webView}
      onMessage={handleMessage}
      userAgent={
        Platform.OS === 'android'
          ? `${CONFIG_KEY + '||' + userAgent}`
          : 'EISecret_ConfigKey||edicine/4.3.11 Safari'
      }
      style={[
        styles.webView,
        {
          width,
          height,
        },
        style,
      ]}
      injectedJavaScript={props.script}
      source={currentSource}
      textZoom={100}
      androidHardwareAccelerationDisabled={
        androidAcceleration === 'false' ? false : true
      }
    />
  );
});
(prevProps, nextProps) => !shouldUpdate({prevProps, nextProps});

MyAutoHeightWebView.propTypes = {
  testID: PropTypes.string,
  onSizeUpdated: PropTypes.func,
  // add baseUrl/files... to android/app/src/assets/ on android
  // add baseUrl/files... to project root on iOS
  files: PropTypes.arrayOf(
    PropTypes.shape({
      href: PropTypes.string,
      type: PropTypes.string,
      rel: PropTypes.string,
    }),
  ),
  style: ViewPropTypes.style,
  customScript: PropTypes.string,
  customStyle: PropTypes.string,
  zoomable: PropTypes.bool,
  // webview props
  originWhitelist: PropTypes.arrayOf(PropTypes.string),
  onMessage: PropTypes.func,
  // baseUrl now contained by source
  // 'web/' by default on iOS
  // 'file:///android_asset/web/' by default on Android
  source: PropTypes.object,
};

let defaultProps = {
  testID: 'MyAutoHeightWebView',
  showsVerticalScrollIndicator: false,
  showsHorizontalScrollIndicator: false,
  originWhitelist: ['*'],
  zoomable: true,
};

Platform.OS === 'android' &&
  Object.assign(defaultProps, {
    // if set to true may cause some layout issues (width of container will be than width of screen) on android
    scalesPageToFit: false,
  });

MyAutoHeightWebView.defaultProps = defaultProps;

const styles = StyleSheet.create({
  webView: {
    backgroundColor: 'transparent',
    flex: 1,
  },
});

export default React.memo(MyAutoHeightWebView);
