/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {View, Image, TouchableOpacity} from 'react-native';
import {
  CustomProgress,
  ProfileTitle,
  BalooThambiRegTextView,
} from '@components';
import {Coin, BoyPng, GirlPng, NeutralPng} from '@images';
import {useStores} from '@mobx/hooks';
import {COLORS} from '@constants';
import styles from './indexCss.js';
import {SvgUri} from 'react-native-svg';
import {getWp, getHp} from '@utils';
import {TitleBg} from '@images';
import {useLanguage} from '@hooks';
import PropTypes from 'prop-types';
import {CopilotStep, walkthroughable} from 'react-native-copilot';

const WalkthroughableView = walkthroughable(View);
const WalkthroughableTouchableOpacity = walkthroughable(TouchableOpacity);

const NavHeader = props => {
  const {testID, permissions, onPress, onSubjectSelect} = props;
  const {appStore, uiStore} = useStores();
  const {profileCompleteText, changeSubject} = useLanguage();

  const {name, avatar, sparkies, profileProgress, profileDetails, gender} =
    appStore.userData;
  console.log('PROFILE_RESPONSE - ', profileDetails);
  console.log('PERMISSION - ', JSON.stringify(permissions));

  let showChangeSubject = false;
  if (appStore.isTrusted && appStore.subjects && appStore.subjects.length > 0) {
    showChangeSubject = true;
  }
  var profileImageExt = avatar?.split('.').pop();

  return (
    <View
      accessible={true}
      testID={testID}
      accessibilityLabel={testID}
      key="container"
      style={styles.container}>
      {uiStore.showNavbarOverlay ? (
        <CopilotStep
          text="Personalise your account by adding your picture here."
          order={1}
          name="ProfilePicture">
          <WalkthroughableTouchableOpacity onPress={onPress}>
            <View key="profileDescription" style={styles.profileDescription}>
              <View key="bgPurpleRec" style={{...styles.bgPurpleRec}}>
                {/* <TouchableOpacity onPress={onPress}> */}
                <View style={styles.profileNameTitleContainer}>
                  {permissions?.profileName && (
                    <BalooThambiRegTextView
                      testID="NavHeaderName"
                      style={styles.name}>
                      {name}
                    </BalooThambiRegTextView>
                  )}
                  {permissions?.profileTitle &&
                    profileDetails?.title?.titleName && (
                      <ProfileTitle
                        testID="ProfileTitleNavHeader"
                        key="profileTitle"
                        SvgImage={TitleBg}
                        text={profileDetails?.title?.titleName || 'NA'}
                        containerStyle={styles.profileTitle}
                        textStyle={styles.profileTitleText}
                      />
                    )}
                </View>
                {/* </TouchableOpacity> */}
              </View>

              {permissions?.profilePicture && (
                <View key="profileBg" style={styles.profilePicture}>
                  <TouchableOpacity onPress={onPress}>
                    {profileImageExt == 'svg' ? (
                      <SvgUri
                        accessible={true}
                        testID="ProfileCardImage"
                        accessibilityLabel="ProfileCardImage"
                        width={getHp(90)}
                        height={getHp(90)}
                        uri={avatar}
                      />
                    ) : (
                      <Image
                        accessible={true}
                        testID="NavHeaderImageAvatar"
                        accessibilityLabel="NavHeaderImageAvatar"
                        style={styles.profileImg}
                        source={
                          avatar !== ''
                            ? {uri: avatar}
                            : gender === 'F'
                            ? GirlPng
                            : gender === 'M'
                            ? BoyPng
                            : NeutralPng
                        }
                      />
                    )}
                  </TouchableOpacity>
                </View>
              )}

              {permissions?.myBadge &&
                profileDetails?.badge?.badgeImg?.length > 0 && (
                  <View
                    key="profileImgContainer"
                    style={styles.profileImgContainer}>
                    {/* <Thumbnail
              style={styles.thumbIcon}
              small
              source={{
                uri:
                  'https://image.shutterstock.com/image-vector/star-vector-icon-600w-594937739.jpg',
              }}
            /> */}
                    <SvgUri
                      accessible={true}
                      testID="NavHeaderSvgUri"
                      accessibilityLabel="NavHeaderSvgUri"
                      uri={profileDetails.badge.badgeImg}
                      width={getWp(32)}
                      height={getWp(32)}
                    />
                  </View>
                )}
            </View>
          </WalkthroughableTouchableOpacity>
        </CopilotStep>
      ) : (
        <TouchableOpacity onPress={onPress}>
          <View key="profileDescription" style={styles.profileDescription}>
            <View key="bgPurpleRec" style={{...styles.bgPurpleRec}}>
              {/* <TouchableOpacity onPress={onPress}> */}
              <View style={styles.profileNameTitleContainer}>
                {permissions?.profileName && (
                  <BalooThambiRegTextView
                    testID="NavHeaderName"
                    style={styles.name}>
                    {name}
                  </BalooThambiRegTextView>
                )}
                {permissions?.profileTitle &&
                  profileDetails?.title?.titleName && (
                    <ProfileTitle
                      testID="ProfileTitleNavHeader"
                      key="profileTitle"
                      SvgImage={TitleBg}
                      text={profileDetails?.title?.titleName || 'NA'}
                      containerStyle={styles.profileTitle}
                      textStyle={styles.profileTitleText}
                    />
                  )}
              </View>
              {/* </TouchableOpacity> */}
            </View>

            {permissions?.profilePicture && (
              <View key="profileBg" style={styles.profilePicture}>
                <TouchableOpacity onPress={onPress}>
                  {profileImageExt == 'svg' ? (
                    <SvgUri
                      accessible={true}
                      testID="ProfileCardImage"
                      accessibilityLabel="ProfileCardImage"
                      width={getHp(90)}
                      height={getHp(90)}
                      uri={avatar}
                    />
                  ) : (
                    <Image
                      accessible={true}
                      testID="NavHeaderImageAvatar"
                      accessibilityLabel="NavHeaderImageAvatar"
                      style={styles.profileImg}
                      source={
                        avatar !== ''
                          ? {uri: avatar}
                          : gender === 'F'
                          ? GirlPng
                          : gender === 'M'
                          ? BoyPng
                          : NeutralPng
                      }
                    />
                  )}
                </TouchableOpacity>
              </View>
            )}

            {permissions?.myBadge &&
              profileDetails?.badge?.badgeImg?.length > 0 && (
                <View
                  key="profileImgContainer"
                  style={styles.profileImgContainer}>
                  {/* <Thumbnail
            style={styles.thumbIcon}
            small
            source={{
              uri:
                'https://image.shutterstock.com/image-vector/star-vector-icon-600w-594937739.jpg',
            }}
          /> */}
                  <SvgUri
                    accessible={true}
                    testID="NavHeaderSvgUri"
                    accessibilityLabel="NavHeaderSvgUri"
                    uri={profileDetails.badge.badgeImg}
                    width={getWp(32)}
                    height={getWp(32)}
                  />
                </View>
              )}
          </View>
        </TouchableOpacity>
      )}
      <TouchableOpacity
        accessible={true}
        testID="NavHeaderProgressAndSparky"
        accessibilityLabel="NavHeaderProgressAndSparky"
        key="progressAndSparky"
        style={styles.progressAndSparky}
        onPress={onPress}>
        {permissions.myProgress &&
          (uiStore.showNavbarOverlay ? (
            <CopilotStep
              text="Check your profile completion status here."
              order={2}
              name="Progress">
              <WalkthroughableView
                key="progressContainer"
                style={styles.progressContainer}>
                <BalooThambiRegTextView
                  testID="NavHeaderProfileProgressText"
                  style={styles.progressLabel}>
                  {profileCompleteText}
                </BalooThambiRegTextView>
                <View key="progressView" style={styles.progressView}>
                  <CustomProgress
                    testID="CustomProgressNavHeader"
                    key="customProgress"
                    currentProgress={profileProgress}
                    progressColor={COLORS.parrotGreen}
                    borderRadius={30}
                    containerStyle={styles.customProgressContainerStyle}
                    progressBarStyle={styles.customProgressBarStyle}
                    commonStyle={styles.customProgressCommonStyle}
                  />

                  <BalooThambiRegTextView
                    testID="NavHeaderProfileProgress"
                    style={styles.percentageLabel}>
                    {`${profileProgress}%`}
                  </BalooThambiRegTextView>
                </View>
              </WalkthroughableView>
            </CopilotStep>
          ) : (
            <View key="progressContainer" style={styles.progressContainer}>
              <BalooThambiRegTextView
                testID="NavHeaderProfileProgressText"
                style={styles.progressLabel}>
                {profileCompleteText}
              </BalooThambiRegTextView>
              <View key="progressView" style={styles.progressView}>
                <CustomProgress
                  testID="CustomProgressNavHeader"
                  key="customProgress"
                  currentProgress={profileProgress}
                  progressColor={COLORS.parrotGreen}
                  borderRadius={30}
                  containerStyle={styles.customProgressContainerStyle}
                  progressBarStyle={styles.customProgressBarStyle}
                  commonStyle={styles.customProgressCommonStyle}
                />

                <BalooThambiRegTextView
                  testID="NavHeaderProfileProgress"
                  style={styles.percentageLabel}>
                  {`${profileProgress}%`}
                </BalooThambiRegTextView>
              </View>
            </View>
          ))}
        {permissions.mySparkies && (
          <View key="sparkyContainer" style={styles.sparkyContainer}>
            <View style={styles.sparkieImgContainer}>
              <Coin
                accessible={true}
                testID="NavHeaderCoinSvg"
                accessibilityLabel="NavHeaderCoinSvg"
                width={styles.sparkieImg.width}
                height={styles.sparkieImg.height}
              />
            </View>
            <BalooThambiRegTextView
              testID="NavHeaderSparkies"
              style={styles.sparkyPts}>
              {sparkies}
            </BalooThambiRegTextView>
          </View>
        )}
      </TouchableOpacity>
      {showChangeSubject && (
        <View style={styles.changeSubjectContainer}>
          <TouchableOpacity onPress={onSubjectSelect}>
            <View style={styles.changeSubjectBtn}>
              <BalooThambiRegTextView style={styles.changeSubjectTxt}>
                {changeSubject}
              </BalooThambiRegTextView>
            </View>
          </TouchableOpacity>
        </View>
      )}
    </View>
  );
};

NavHeader.propTypes = {
  testID: PropTypes.string,
};

NavHeader.defaultProps = {
  testID: 'NavHeader',
};
export default NavHeader;
