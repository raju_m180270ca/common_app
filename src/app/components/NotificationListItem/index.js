// External Imports
import React from 'react';
import {View, TouchableOpacity} from 'react-native';
import PropTypes from 'prop-types';
import {SvgUri} from 'react-native-svg';

// Internal Imports
import styles from './style';
import {BalooThambiRegTextView, RobotoRegTextView} from '@components';
import {getWp} from '@utils';

const NotificationListItem = props => {
  const {
    testID,
    imageURL,
    title,
    message,
    dateTime,
    isActive,
    hasNotificationScreen,
    onPress,
  } = props;

  return (
    <TouchableOpacity
      accessible={true}
      testID={`NotificationListItem${testID}`}
      accessibilityLabel={`NotificationListItem${testID}`}
      style={hasNotificationScreen ? styles.container : styles.popupContainer}
      onPress={onPress}>
      {imageURL !== null && imageURL !== '' && (
        <View style={styles.imageContainer}>
          <SvgUri
            accessible={true}
            testID="NotificationListItemSvgUri"
            accessibilityLabel="NotificationListItemSvgUri"
            width={getWp(65)}
            height={getWp(65)}
            uri={imageURL}
          />
          {/* {isActive ? <View style={styles.activeContainer}/> : <View />} */}
        </View>
      )}
      <View
        style={
          hasNotificationScreen
            ? styles.messageContainer
            : styles.popupMessageContainer
        }>
        {title !== null && title !== '' && isActive && (
          <BalooThambiRegTextView
            testID="NotificationListItemTitleTextUnread"
            style={
              hasNotificationScreen
                ? styles.titleTextUnRead
                : styles.popupTitleTextUnRead
            }>
            {title.replace('_', ' ')}
          </BalooThambiRegTextView>
        )}
        {title !== null && title !== '' && !isActive && (
          <BalooThambiRegTextView
            testID="NotificationListItemTitleTextRead"
            style={
              hasNotificationScreen
                ? styles.titleTextRead
                : styles.popupTitleTextRead
            }>
            {title.replace('_', ' ')}
          </BalooThambiRegTextView>
        )}
        {message !== null && message !== '' && isActive && (
          <RobotoRegTextView
            testID="NotificationListItemPopupMessage"
            style={
              hasNotificationScreen
                ? styles.messageTextUnread
                : styles.popupMessageText
            }>
            {message}
          </RobotoRegTextView>
        )}
        {message !== null && message !== '' && !isActive && (
          <RobotoRegTextView
            testID="NotificationListItemReadMessage"
            style={
              hasNotificationScreen
                ? styles.messageTextRead
                : styles.popupMessageText
            }>
            {message}
          </RobotoRegTextView>
        )}
      </View>
      <>
        {dateTime !== null && dateTime !== '' && isActive && (
          <BalooThambiRegTextView
            testID="NotificationListItemDateTimeUnread"
            style={
              hasNotificationScreen
                ? styles.timeTextUnRead
                : styles.popupMessageTimeTextUnRead
            }>
            {dateTime}
          </BalooThambiRegTextView>
        )}
        {dateTime !== null && dateTime !== '' && !isActive && (
          <BalooThambiRegTextView
            testID="NotificationListItemdateTimeRead"
            style={
              hasNotificationScreen
                ? styles.timeTextRead
                : styles.popupMessageTimeText
            }>
            {dateTime}
          </BalooThambiRegTextView>
        )}
      </>
    </TouchableOpacity>
  );
};

NotificationListItem.propTypes = {
  testID: PropTypes.string,
  imageURL: PropTypes.string,
  title: PropTypes.string,
  message: PropTypes.string,
  dateTime: PropTypes.string,
  isActive: PropTypes.bool,
  hasNotificationScreen: PropTypes.bool,
  onPress: PropTypes.func,
};

NotificationListItem.defaultProps = {
  testID: 'NotificationListItem',
  isActive: false,
  hasNotificationScreen: true,
  onPress: () => {},
};

export default NotificationListItem;
