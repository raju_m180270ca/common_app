import React, {
  Fragment,
  useEffect,
  useState,
  useRef,
  useImperativeHandle,
  forwardRef,
} from 'react';
import { Text, View, TextInput, TouchableOpacity } from 'react-native';
import PropTypes from 'prop-types';
import DeviceInfo from 'react-native-device-info';

import styles from './indexCss';

const OTPInput = forwardRef((props, refs) => {
  const { testID, otpLength, otp, isNewFlow } = props;
  const [otpFields, setOtpFields] = useState([]);
  let assignOtpVal = new Array(otpLength).fill(0);
  let assignRefs = new Array(otpLength).fill(1);
  const [otpValue, setOtpValue] = useState([...assignOtpVal.map(itr => '')]);
  const [inputRefs, setInputRefs] = useState([
    ...assignRefs.map(itr => useRef()),
  ]);
  useImperativeHandle(refs, () => ({
    getInputOtp: () => {
      return otp.length === 0 ? otpValue.join('') : otp.join('');
    },
    resetOtp: () => {
      for (let index = otpFields.length - 1; index >= 0; index--) {
        inputRefs[index].current.clear();
        if (index !== 0) {
          if (inputRefs[index]) {
            const otp = otpValue;
            otp[index] = '';
            setOtpValue(otp);
          }
        }
      }
    },
  }));


  const initOtpFields = () => {
    let otpInputField = [];
    for (let i = 0; i < otpLength; i++) {
      otpInputField.push({
        i,
        component: (
          <TouchableOpacity style={isNewFlow ? styles.newOtpInputContainer : styles.otpInputContainer}>
            { otp.length === 0 ? <TextInput
              style={{ width: '100%', height: 45, fontSize: DeviceInfo.isTablet() ? 40 : 20 }}
              accessible={true}
              testID={`OTPInput${i}`}
              accessibilityLabel={`OTPInput${i}`}
              maxLength={1}
              keyboardType='numeric'
              ref={inputRefs[i]}
              //keyboardType={'numeric'}
              onChangeText={text => {
                otpInputHandler(i, text);
              }}
              textAlign={'center'}
              secureTextEntry={true}
              onKeyPress={e => focusPrevious(e.nativeEvent.key, i)}
            /> : 
            <TextInput
              style={{ width: '100%', height: 45, fontSize: DeviceInfo.isTablet() ? 40 : 20 }}
              accessible={true}
              testID={`OTPInput${i}`}
              accessibilityLabel={`OTPInput${i}`}
              maxLength={1}
              keyboardType='numeric'
              ref={inputRefs[i]}
              textAlign={'center'}
              secureTextEntry={true}
              value={otp[i]}
              editable
            />}
          </TouchableOpacity>
        ),
      });
    }
    setOtpFields(otpInputField);
  };
  const otpInputHandler = (index, value) => {
    if (index < otpValue.length - 1 && value) {
      inputRefs[index + 1].current.focus();
    }
    if (index === otpValue.length - 1) {
      inputRefs[index].current.blur();
    }
    const otp = otpValue;
    otp[index] = value;
    setOtpValue(otp);
  };
  const focusPrevious = (key, index) => {
    if (key === 'Backspace' && index !== 0) {
      if (inputRefs[index]) {
        const otp = otpValue;
        otp[index] = '';
        setOtpValue(otp);
        inputRefs[index - 1].current.focus();
      }
    }
  };
  useEffect(() => {
    setTimeout(() => {
      initOtpFields();
    }, 1000);
  }, [otp]);



  return (
    <View accessible={true} testID={testID} accessibilityLabel={testID} style={styles.container}>
      {otpFields.map((singleField, index) => {
        return singleField.component;
      })}
    </View>
  );
});

OTPInput.propTypes = {
  testID: PropTypes.string,
  otpLength: PropTypes.number
};

OTPInput.defaultProps = {
  testID: 'OTPInput',
  otpLength: 4,
};

export default OTPInput;
