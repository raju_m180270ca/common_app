import {StyleSheet} from 'react-native';
import {COLORS, TEXTFONTSIZE} from '@constants';
import {getHp, getWp} from '@utils/ViewUtils';

export default StyleSheet.create({
  container: {
    width: getWp(80),
    position: 'relative',
  },

  text: {
    color: COLORS.white,
    fontSize: TEXTFONTSIZE.Text20,
    textAlign: 'center',
  },

  selected: {
    position: 'absolute',
    top: 0,
    right: 4,
  },
});
