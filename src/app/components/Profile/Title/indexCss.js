import {StyleSheet} from 'react-native';
import {COLORS} from '../../../constants/COLORS';
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen';
import {getWp, getHp} from '@utils';
import {TEXTFONTSIZE} from '../../../constants';
export default StyleSheet.create({
  container: {
    // justifyContent: 'center',
    // alignItems: 'flex-start',
    // paddingHorizontal: getWp(4),
    // paddingVertical: getHp(0),
    // borderTopStartRadius: 50,
    // width: wp('37.4'),
    // height: hp('3'),
  },
  svgContainer: {
    position: 'absolute',
    top: 0,
    right: 0,
    bottom: 0,
    left: 0,
    borderBottomLeftRadius: getWp(50),
  },
  textContainer: {
     
  },
  text: {
    fontSize: TEXTFONTSIZE.Text10,
    color: COLORS.white,
    fontFamily: 'BalooThambi-Regular',
    textAlign: "center"
  },
});
