import {CDN_LINKS} from '../components/htmlComponents/CDN';
import {
  STYLES,
  qBodyImageStyle,
  qBodyFont,
} from '../components/htmlComponents/STYLES';
import {REACT_NATIVE_BRIDGE} from '../components/htmlComponents/REACT_NATIVE_BRIDGE';
import {
  MATHJS_CONFIG,
  HandleTheMathJax,
  HandleTheMathJaxEdicine,
} from '../components/htmlComponents/MATHJS_CONFIG';
// import {HandleTheMathJax} from '@utils';

import {
  IFRAME_BRIDGE_INIT,
  IFRAME_BRIDGE_LISTENER,
  IFRAME_DYNAMIC_HEIGHT_SETTER,
} from '../components/htmlComponents/IFRAME_BRIDGE';
import {CONTENT_SERVICE_BRIDGE} from '../components/htmlComponents/CONTENT_SERVICE_BRIDGE';
//  

export const QHtmlTemplateForIframe = (
  htmlContent,
  width,
  alignContentToCenter,
  store,
  qStatement,
  qBody,
) => {
  // if (Config.ENV === 'naandi') {
  //   var content = HandleTheMathJax(htmlContent);
  // } else {
  //   var content = HandleTheMathJaxEdicine(htmlContent);
  //   content = HandleTheMathJax(content);
  // }

  var content = HandleTheMathJaxEdicine(htmlContent);
  content = HandleTheMathJax(content);

  // console.log(`MathJax Content >>>>${content}`);
  var style = alignContentToCenter
    ? 'display: flex; justify-content: center; align-items: center;'
    : 'text-align: left;padding-right:5px';
  const direction =
    store &&
    store?.appStore &&
    store?.appStore?.userLanguage &&
    store?.appStore?.userLanguage === 'ur'
      ? 'rtl'
      : 'ltr';

  return `
  <!DOCTYPE html>
    <html>
    <head>
        <meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
        ${CDN_LINKS}
        ${MATHJS_CONFIG}
        <script>
          ${REACT_NATIVE_BRIDGE}
          ${CONTENT_SERVICE_BRIDGE}
        </script>
        <style>
           ${STYLES}
           ${qStatement ? qBodyImageStyle : ''}
           ${qBody ? qBodyFont : ''}
        </style>
    </head>
    <body style="width:99%">
        <div dir="${direction}" id="questionnaireContainer" class="${
    qStatement ? 'questionnaireContainer' : ''
  }" style="${style}" >
          <fieldset id="contentField" style="border:none;padding:5px;">
          ${content}
         </fieldset>
        </div>
        <script type="text/javascript">
          ${IFRAME_BRIDGE_INIT}
          ${IFRAME_BRIDGE_LISTENER}
          ${IFRAME_DYNAMIC_HEIGHT_SETTER(width)}
        </script>
    </body>
    </html>
      `;
};

const isAlphaNumeric = data => {
  var regex = /^[A-Za-z0-9 !@#$%^*(),./?><;":']+$/;
  var isValid = regex.test(data);
  return isValid;
};
