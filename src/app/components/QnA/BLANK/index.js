/* eslint-disable react-native/no-inline-styles */
import React, {useState, useEffect, useRef} from 'react';
import {
  View,
  ScrollView,
  KeyboardAvoidingView,
} from 'react-native';
import {HintBox, MyAutoHeightWebView} from '@components';
import {heightPercentageToDP as hp} from 'react-native-responsive-screen';
import {useStores} from '@mobx/hooks';
import {checkForAudio, getWp} from '@utils';
import styles from './indexCss';
import getHtmlTemplate from '@utils/getHtmlTemplate';
import {useLanguage} from '@hooks';

const BLANK = props => {
  const [showHintBox, setShowHintBox] = useState(false);
  const [newHeight, setnewHeight] = useState(hp('1'));
  const {showHint, disableWebView, webref} = props;

  const [trials, setTrials] = useState(-1);

  const {qnaStore, uiStore} = useStores();
  const isRTL = uiStore.isRTL;

  console.log('hint', showHint);
  useEffect(() => {
    setTrials(qnaStore.trialCount);
  }, [qnaStore.trialCount]);

  var question = props.questionRes;

  let questionBody = '';
  try {
    questionBody = decodeURI(question.data[0].questionBody);
  } catch (err) {
    questionBody = question.data[0].questionBody;
  }
  let choices = question.data[0].response;
  for (var choice in choices) {
    if (choices.hasOwnProperty(choice)) {
      let size = 5;
      let type = 'email';
      let pattern = '[a-zA-Z]*';
      let isNumeric = false;
      let inputMode = 'text';
      // console.log('CHOICE:', choices[choice]);
      try {
        if (
          choices[choice].hasOwnProperty('attributes') &&
          choices[choice].attributes.hasOwnProperty('size')
        ) {
          size = choices[choice].attributes.size;
          size = size > 20 ? 20 : size; //max size
          size = size < 5 ? 5 : size; //min size
        }
        if (
          choices[choice].hasOwnProperty('attributes') &&
          choices[choice].attributes.hasOwnProperty('numeric')
        ) {
          isNumeric = choices[choice].attributes.numeric;
        }
      } catch (err) {}
      if (isNumeric) {
        type = 'number';
        pattern = '[0-9]+';
        inputMode = 'numeric';
      }
      let userResponse = '';
      let userResponses = props.userResponse;
      if (userResponses?.hasOwnProperty(choice)) {
        userResponse = userResponses[choice].hasOwnProperty('userAnswer')
          ? userResponses[choice].userAnswer
          : '';
      }
      // console.log('blank q ==>>:', questionBody);
      questionBody = questionBody + `<br/>`;
      questionBody = questionBody.replace(
        '[' + choice + ']',
        `<input 
              type="${type}" 
              id="${choice}" 
              pattern="${pattern}"
              value="${userResponse}"
              autocomplete="nope"
              spellcheck="false"  
              autocorrect="off"
              autocapitalize="none" 
              size="${size}" 
              oninput="inputChangeFunction(this.id)" 
              style='width:${size}em;height:2em;border-radius:25px;border:1px solid #969696; padding:2px 8px;text-align:center;font-size:16px;'
            />`,
      );
    }
  }
  questionBody = checkForAudio(questionBody);
  // questionBody += '<br/>';

  // console.log('QBODY:::', questionBody);
  questionBody = getHtmlTemplate(questionBody, false, isRTL);

  const hintToggle = showBox => {
    setShowHintBox(showBox);
  };

  if (disableWebView) {
    const run = `
    disableWebView();
      `;
    webref && webref.current.injectJavaScript(run);
  }
  // const generateAssetsFontCss = (fontFamily, fileFormat = 'ttf') => {
  //   const fileName = Platform.select({
  //     ios: `file:///assets/fonts/${fontFamily}.${fileFormat}`,
  //     android: `file:///android_asset/fonts/${fontFamily}.${fileFormat}`,
  //   });

  //   return `@font-face {
  //     font-family: '${fontFamily}';
  //     src: url("${fileName}") format('truetype');
  // }`;
  // };
  return (
    <View style={styles.container}>
      <ScrollView style={{}}>
        <View style={styles.innerContainer}>
          {/* <KeyboardAvoidingView behavior="padding" enabled={true}> */}
          <KeyboardAvoidingView
            enabled={true}
            style={styles.keyboard}
            behavior={'height'}>
            <MyAutoHeightWebView
              ref={webref}
              showsHorizontalScrollIndicator
              onMessage={props.onWebViewCallback}
              style={[styles.webViewContainer, {height: newHeight}]}
              onSizeUpdated={size => {
                console.log(size);
                //console.log('NBLANK:' + size + ':' + hp('1'));
                setnewHeight(size.height + hp('2'));
              }}
              source={{html: questionBody, baseUrl: ''}}
              startInLoadingState={true}
              zoomable={false}
              bounces={false}
            />
            <HintBox
              hintList={question.data[0].hints}
              toggleCallback={hintToggle}
              showBtn={showHint}
              showHints={showHintBox}
              showHide={!showHintBox}
              trials={trials}
            />
          </KeyboardAvoidingView>

          {/* {this.renderHintButton()}
        {this.renderHint()} */}
        </View>
      </ScrollView>
    </View>
  );
};

BLANK.propTypes = {};

BLANK.defaultProps = {};

export default BLANK;
