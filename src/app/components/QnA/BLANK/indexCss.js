import { StyleSheet, Dimensions } from 'react-native';

import { getWp, getHp } from '@utils';
// const { height } = Dimensions.get('window');

export default StyleSheet.create({
  container: {
    margin: 0,
    paddingBottom: 0,
    justifyContent: 'center',
    flex: 1,
    marginTop: getHp(27),
  },

  innserContainer: {
    margin: 0,
    paddingBottom: 0,
  },
  webViewContainer: {
    width: getWp(393),
    // minHeight: height-185
    // marginStart: isRTL ? wp('2') : wp('4'),
    // paddingEnd: isRTL ? wp('2') : wp('4'),
    // height: newHeight,
  },
  keyboard: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
 },
});
