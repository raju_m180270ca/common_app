/* eslint-disable react-native/no-inline-styles */
import React, {useState, useRef, useEffect} from 'react';
import {View, KeyboardAvoidingView} from 'react-native';
import {HintBox, MyAutoHeightWebView, ChoiceList} from '@components';

import {useStores} from '@mobx/hooks';
import {checkForAudio} from '@utils';
import {useLanguage} from '@hooks';
import getHtmlTemplate from '@utils/getHtmlTemplate';
import styles from './indexCss';

const BLANK_DROPDOWN = props => {
  const [showHintBox, setShowHintBox] = useState(false);
  const [listChoices, setListChoices] = useState(null);
  const [trials, setTrials] = useState(-1);
  const [showChoiceModal, setShowChoiceModal] = useState(false);
  const [btnId, setBtnId] = useState(null);
  const {showHint, onWebViewCallback, webref} = props;
  const {qnaStore, uiStore} = useStores();
  const {selectText} = useLanguage();
  const isRTL = uiStore.isRTL;

  console.log('hint', showHint);
  useEffect(() => {
    console.log('Inside Blank dropdown ');
    setTrials(qnaStore.trialCount);
  }, [qnaStore.trialCount]);
  const initContentService = () => {
    const run = ` 
        initContentService(${JSON.stringify(props.questionRes.data)}); 
    `;
    webref.current.injectJavaScript(run);
  };

  const csGetTemplate = () => {
    initContentService();
    const run = ` 
      getTemplate(); 
    `;
    webref.current.injectJavaScript(run);
  };

  const csEvaluateAnswer = () => {
    //this.initContentService();
    const run = ` 
     evaluateAnswer(${JSON.stringify(props.questionRes.data)},${JSON.stringify(
      props.inputData,
    )}); 
    `;
    console.log('cs Evaluate Answer');
    webref.current.injectJavaScript(run);
  };

  const onMessage = m => {
    console.log('event', m.nativeEvent.data);
  };

  var question = props.questionRes;

  let questionBody = '';
  try {
    questionBody = decodeURI(question.data[0].questionBody);
  } catch (err) {
    questionBody = question.data[0].questionBody;
  }

  let choices = question.data[0].response;
  let fields = choices;
  let userAnswer = selectText;
  let userResponses =
    props.questionRes?.contentParams?.userAttemptData?.userResponse;
  for (var field in fields) {
    if (fields.hasOwnProperty(field)) {
      let fieldArr = field.split('_');

      switch (fieldArr[0]) {
        case 'blank':
          let size = 10;
          let type = 'email';
          let pattern = '[*]*';
          let isNumeric = false;
          try {
            console.log('CHOICE:', choices[field]);
            if (
              choices[field].hasOwnProperty('attributes') &&
              choices[field].attributes.hasOwnProperty('size')
            ) {
              size = choices[field].attributes.size;
              size = size > 20 ? 20 : size; //max size
              size = size < 5 ? 5 : size; //min size
            }
            if (
              choices[field].hasOwnProperty('attributes') &&
              choices[field].attributes.hasOwnProperty('numeric')
            ) {
              isNumeric = choices[field].attributes.numeric;
            }
            if (isNumeric) {
              type = 'number';
              pattern = '[0-9]*';
            }
          } catch (err) {
            console.log('ERROR IN BLANK_DROPDOWN:', err);
          }
          let userResponse = '';
          let userResponses = props.userResponse;
          if (userResponses?.hasOwnProperty(field)) {
            userResponse = userResponses[field].hasOwnProperty('userAnswer')
              ? userResponses[field].userAnswer
              : '';
          }
          console.log('CHOICE Size:', size);
          questionBody = questionBody.replace(
            '[' + field + ']',
            `<input 
                type="${type}" 
                pattern="${pattern}"
                value="${userResponse}"
                autocomplete="off"
                spellcheck="false" 
                size="${size}"
                autocorrect="off"
                id="${field}"
                style='width:${size}em;height:2em;border-radius:25px;border:1px solid #969696; padding:2px 8px;text-align:center;font-size:16px'
                oninput="inputChangeFunction(this.id)" 
             />`,
          );
          break;
        case 'dropdown':
          if (userResponses?.hasOwnProperty(field)) {
            userResponse = parseInt(
              userResponses[field].hasOwnProperty('userAnswer')
                ? userResponses[field].userAnswer
                : -1,
            );
          }

          if (
            userResponse !== null &&
            userResponse !== undefined &&
            userResponse !== -1 &&
            choices[field] !== null &&
            choices[field] !== undefined &&
            choices[field].choices[userResponse] !== null &&
            choices[field].choices[userResponse] !== undefined
          ) {
            userAnswer = choices[field].choices[userResponse].value;
          }
          //let ddButton = `<button type="button" id="${field}" class="dropdown_button" onclick="ddButtonClickHandler(this)">${selectText}<i class="arrow down"></i></button>`;
          let ddButton = `<button type="button" id="${field}" class="dropdown_button" onclick="ddButtonClickHandler(this)">${userAnswer}<i class="arrow down"></i></button>`;
          questionBody = questionBody.replace('[' + field + ']', ddButton);
          break;
      }
    }
  }

  const ddBtnWebViewMsgHandler = event => {
    console.log(event.nativeEvent.data);

    let msgData = JSON.parse(event.nativeEvent.data);
    console.log(msgData.type);
    if (msgData && msgData.type == 'select') {
      console.log(`dropdown response>>>>choices`);
      console.log(fields[msgData.id].choices);
      setBtnId(msgData.id);
      setListChoices(() => {
        let ddChoices = question.data[0].response[msgData.id];
        console.log('MAPPER:::::::::::::::' + ddChoices['mapper']);
        var tmpChoices = [];
        var mapper1 = ddChoices?.mapper;
        var localChoices = ddChoices?.choices;
        if (mapper1) {
          mapper1.forEach(function(val, index) {
            tmpChoices.push({
              id: val,
              value: localChoices[val].value,
            });
          });
        }
        return tmpChoices;
      });
      setShowChoiceModal(true);
    } else if (msgData && msgData.type == 'ContentService') {
      onWebViewCallback(event);
    }
  };

  const selectChoiceHandler = choiceItem => {
    setShowChoiceModal(false);
    console.log(`Selected choice item`);
    console.log(choiceItem);
    const run = `
ddChoiceSelectHandler("${btnId}","${choiceItem.value}");
  `;
    webref.current.injectJavaScript(run);

    props.setInputResponse(btnId, choiceItem.id);

    const updateHeightScript = `
    updateWebviewHeight();
      `;

    webref.current.injectJavaScript(updateHeightScript);
  };

  if (props.disableWebView) {
    const run = `
disableWebView();
  `;
    webref && webref.current.injectJavaScript(run);
  }

  const onBackdropHandler = () => {
    setShowChoiceModal(false);
  };

  const hintToggle = showBox => {
    setShowHintBox(showBox);
  };
  questionBody = checkForAudio(questionBody);
  // questionBody += '<br/><br/><br/><br/><br/><br/><br/><br/>';
  questionBody = getHtmlTemplate(questionBody, true, isRTL);
  // console.log(`Q body >>>>>>>>>>>${questionBody}`);
  return (
    <View style={styles.container}>
      <KeyboardAvoidingView enabled={true}>
        <MyAutoHeightWebView
          ref={webref}
          onMessage={event => {
            let webViewData = JSON.parse(event.nativeEvent.data);
            if (
              webViewData.hasOwnProperty('type') &&
              webViewData.type === 'select'
            ) {
              ddBtnWebViewMsgHandler(event);
            } else {
              props.onWebViewCallback(event);
            }
          }}
          style={styles.webViewContainer}
          onSizeUpdated={size => {
            console.log(size.height);
          }}
          source={{html: questionBody}}
          zoomable={false}
          bounces={false}
        />
        <HintBox
          hintList={question.data[0].hints}
          toggleCallback={hintToggle}
          showBtn={showHint}
          showHints={showHintBox}
          showHide={!showHintBox}
          trials={trials}
        />
        {listChoices && (
          <ChoiceList
            choices={listChoices}
            show={showChoiceModal}
            selectChoiceHandler={selectChoiceHandler}
            onBackdropHandler={onBackdropHandler}
          />
        )}
      </KeyboardAvoidingView>

      {/* {this.renderHintButton()}
        {this.renderHint()} */}
      {/* </ScrollView> */}
    </View>
  );
};

BLANK_DROPDOWN.propTypes = {};

BLANK_DROPDOWN.defaultProps = {};

export default BLANK_DROPDOWN;
