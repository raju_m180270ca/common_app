/* eslint-disable prettier/prettier */
import React from 'react';
import {View, TouchableOpacity} from 'react-native';
import styles from './style';
import {MyAutoHeightWebView} from '@components';
import {useStores} from '@mobx/hooks';
import {WhiteCloseIcon} from '@images';
import getHtmlTemplate from '@utils/getHtmlTemplate';

const ClassificationButton = ({
  item,
  callBack,
  containerType,
  disableClick,
}) => {
  const store = useStores();
  const isRTL = store?.uiStore?.isRTL;

  // let questionTree = QHtmlTemplateForIframe(item.value, wp(20), store);
  let questionTree = getHtmlTemplate(item.value, false, isRTL);
  return (
    <View style={isRTL ? styles.RTLContainer : styles.container}>
      <TouchableOpacity
        disabled={disableClick}
        onPress={() => {
          callBack(item, containerType);
        }}>
        <WhiteCloseIcon />
      </TouchableOpacity>
      <View style={styles.webviewContainer}>
        <MyAutoHeightWebView
          key="mcqItemWebView"
          style={styles.webviewContentContainer}
          customScript={''}
          onSizeUpdated={() => {}}
          source={{html: questionTree}}
          zoomable={false}
        />
      </View>
    </View>
  );
};

export default React.memo(ClassificationButton);
