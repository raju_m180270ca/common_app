import {StyleSheet} from 'react-native';
import {getWp} from '@utils';

export default StyleSheet.create({
  container: {
    flexDirection: 'row',
    marginLeft: getWp(10),
  },

  RTLContainer: {
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row-reverse',
    marginHorizontal: getWp(16),
  },

  webviewContainer: {
    alignItems: 'center',
    justifyContent: 'center',
    width: getWp(100),
  },

  webviewContentContainer: {
    width: getWp(100),
  },
});
