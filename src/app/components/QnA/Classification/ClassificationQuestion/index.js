import React from 'react';
import {View, Animated, TouchableOpacity} from 'react-native';
import ClassificationPan from '@hooks/ClassificationPan';
import styles from './indexCss';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';
import {QHtmlTemplateForIframe, MyAutoHeightWebView} from '@components';
import {useStores} from '@mobx/hooks';
import PropTypes from 'prop-types';
import getHtmlTemplate from '@utils/getHtmlTemplate';

const ClassificationQuestion = props => {
  const {
    testID,
    item,
    dragToContainer1,
    dragToContainer2,
    dragAndDropCallback,
    disableClick,
    updatedZIndex,
    onDragEnd,
  } = props;

  const store = useStores();
  const isRTL = store?.uiStore?.isRTL;
  
  const [animateValue, panResponder] = ClassificationPan(
    item,
    dragToContainer1,
    dragToContainer2,
    isRTL,
    updatedZIndex,
  );
  const animatedStyle = {
    transform: animateValue.getTranslateTransform(),
  };

  // let questionTree = QHtmlTemplateForIframe(item?.value, wp(20), store, true);
  let questionTree = getHtmlTemplate(item?.value, false, isRTL);
  return (
    <View
      accessible={true}
      testID={testID}
      accessibilityLabel={testID}
      style={{...styles.arrangeLetterContainerParentView, zIndex: item.zIndex}}>
      {item.isSelected === false ? (
        <Animated.View style={{...animatedStyle}} {...panResponder.panHandlers}>
          <TouchableOpacity
            accessible={true}
            testID="ClarificationQuestionTouchableComp"
            accessibilityLabel="ClarificationQuestionTouchableComp"
            style={styles.elevatedContainer}
            disabled={disableClick}
            onPressIn={() => dragAndDropCallback(true)}
            onPressOut={() => dragAndDropCallback(false)}>
            <View
              pointerEvents={'none'}
              style={styles.arrangeLetterContainerChildView}>
              <MyAutoHeightWebView
                testID="MyAutoHeightWebViewClarificationQuestion"
                key="mcqItemWebView"
                style={styles.webviewContainer}
                customScript={''}
                onSizeUpdated={() => {}}
                scrollEnabled={false}
                source={{html: questionTree}}
                zoomable={false}
              />
            </View>
          </TouchableOpacity>
        </Animated.View>
      ) : (
        onDragEnd(item.index)
      )}
    </View>
  );
};

ClassificationQuestion.propTypes = {
  testID: PropTypes.string,
};
ClassificationQuestion.defaultProps = {
  testID: 'ClassificationQuestion',
  onDragEnd: () => {},
};

export default ClassificationQuestion;
