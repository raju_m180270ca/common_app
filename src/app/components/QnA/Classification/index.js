import React, {
  useState,
  Fragment,
  useCallback,
  useRef,
  useImperativeHandle,
  useEffect,
} from 'react';
import {View} from 'react-native';
import style from './indexCss';
import {
  QuestionContainer,
  ClassificationQuestion,
  ClassificationBucket,
} from '@components';
import {Base64} from 'js-base64';
import {useStores} from '@mobx/hooks';

const Classification = React.forwardRef(
  ({questionTree, dragAndDropCallback}, ref) => {
    let bucketRef1 = useRef();
    let bucketRef2 = useRef();
    const {uiStore} = useStores();

    const [container1, setContainer1] = useState([]);
    const [container2, setContainer2] = useState([]);
    const [disableClick, setDisableClick] = useState(false);
    const [displayIndex, setDisplayIndex] = useState(0);
    const [dragDropEnd, setDragDropEnd] = useState(false);
    const [showChoicesOneByOne, setShowChoicesOneByOne] = useState(false);
    const [dragContainer, setDragContainer] = useState([]);

    useEffect(() => {
      setDragContainer(
        questionTree?.response?.choices.map((item, index) => ({
          ...item,
          index,
          isSelected: false,
          zIndex: 0,
        })),
      );

      setShowChoicesOneByOne(questionTree?.display?.showChoicesOneByOne);

      return () => {};
    }, [questionTree]);

    const isRTL = uiStore.isRTL;

    // const showChoicesOneByOne = questionTree?.display?.showChoicesOneByOne;

    const updatedZIndex = useCallback(
      item => {
        let updatedDragContainerForzIndex = dragContainer.map(
          singleContainer => {
            if (singleContainer.identifier === item.identifier) {
              return {...singleContainer, zIndex: 10};
            } else {
              return {...singleContainer, zIndex: 0};
            }
          },
        );
        setDragContainer(updatedDragContainerForzIndex);
      },
      [dragContainer],
    );
    const callbackForResetComponent = useCallback(
      (item, containerType) => {
        let newDragContainerData = [];

        newDragContainerData = dragContainer.map(singleDrag => {
          return singleDrag.identifier === item.identifier
            ? {...singleDrag, isSelected: false}
            : singleDrag;
        });
        if (showChoicesOneByOne) {
          //Filter the dragContainer
          // newDragContainerData = dragContainer.filter(
          //   containerItem => containerItem.identifier === item.identifier,
          // );
          newDragContainerData.push({
            ...item,
            isSelected: false,
          });
        }
        if (containerType === '1') {
          let newContainer1 = container1.filter(
            entry => entry.identifier !== item.identifier,
          );
          setContainer1(newContainer1);
        } else {
          let newContainer2 = container2.filter(
            entry => entry.identifier !== item.identifier,
          );
          setContainer2(newContainer2);
        }

        setDragContainer(newDragContainerData);
      },
      [dragContainer, container1, container2],
    );

    const dragToContainer1 = useCallback(
      item => {
        let updatedDragComponent = dragContainer.map(singleDrag => {
          return singleDrag.identifier === item.identifier
            ? {...singleDrag, isSelected: true}
            : singleDrag;
        });
        setDragContainer(updatedDragComponent);
        setContainer1(prevState => [...prevState, item]);
        bucketRef1.current.scrollToFlatList();
      },
      [dragContainer],
    );

    const dragToContainer2 = useCallback(
      item => {
        let updatedDragComponent = dragContainer.map(singleDrag => {
          return singleDrag.identifier === item.identifier
            ? {...singleDrag, isSelected: true}
            : singleDrag;
        });
        setDragContainer(updatedDragComponent);
        setContainer2(prevState => [...prevState, item]);
        bucketRef2.current.scrollToFlatList();
      },
      [dragContainer],
    );

    useImperativeHandle(ref, () => ({
      evaluteAnswer() {
        if (
          (container1 && container1.length > 0) ||
          (container2 && container2.length > 0)
        ) {
          let userInputData = {};
          let container1InputData = container1.map(item => {
            return item?.index;
          });
          let container2InputData = container2.map(item => {
            return item?.index;
          });

          let keyList = questionTree.stems.map((matchStems, index) => {
            return matchStems.identifier;
          });
          userInputData[keyList[0]] = container1InputData;
          userInputData[keyList[1]] = container2InputData;

          let payload = {};
          payload.isDynamic = questionTree?.isDynamic;
          payload.contentID = questionTree?.contentID;
          payload.score = isValidUserResponse(userInputData)
            ? questionTree?.responseValidation?.validResponse?.score
            : 0;
          payload.result = isValidUserResponse(userInputData)
            ? Base64.encode('pass')
            : Base64.encode('fail');
          payload.userResponse = {};
          payload.userResponse.type = questionTree?.template;
          payload.userResponse.Classification = userInputData;
          payload.userAttemptData = {
            trials: [
              {
                userResponse: {
                  type: questionTree?.template,
                  Classification: userInputData,
                },
                result: isValidUserResponse(userInputData)
                  ? Base64.encode('true')
                  : Base64.encode('false'),
                score: isValidUserResponse(userInputData)
                  ? questionTree?.responseValidation?.validResponse?.score
                  : 0,
              },
            ],
          };
          let contentInfo = {};
          contentInfo.contentID = questionTree?.contentID;
          contentInfo.contentVersionID = questionTree?._id;
          contentInfo.contentType = questionTree?.contentType;
          contentInfo.questionType = questionTree?.template;
          contentInfo.revisionNum = questionTree?.revisionNo;
          contentInfo.langCode = questionTree?.langCode;
          payload.contentInfo = contentInfo;
          payload.remainingTime = 0;
          payload.nextContentSeqNum = null;
          setDisableClick(true);

          return payload;
        }

        return null;
      },

      reset() {
        setDisableClick(false);
      },
    }));

    const isValidUserResponse = userInputData => {
      const {scoringType, validResponse} = questionTree.responseValidation;
      const {identifier} = validResponse;
      let isValidResponse = false;

      if (scoringType === 'exact' && userInputData) {
        for (let i = 0; i < questionTree?.stems?.length; i++) {
          let matchStems = questionTree?.stems[i];
          let key = matchStems.identifier;
          let decodeValidResponse = Base64.decode(identifier[key]);
          let userResponseIndex = userInputData[key][0];
          let decodeUserIdentifier = Base64.decode(
            dragContainer[userResponseIndex]?.identifier,
          );
          isValidResponse =
            decodeValidResponse.indexOf(decodeUserIdentifier) > 0;

          if (isValidResponse === false) {
            return false;
          }
        }
      }

      return isValidResponse;
    };

    let container1Label = '';
    let container2Label = '';
    if (questionTree?.stems && questionTree?.stems.length > 0) {
      container1Label = questionTree?.stems[0]
        ? questionTree?.stems[0]?.value
        : '';
      container2Label = questionTree?.stems[1]
        ? questionTree?.stems[1]?.value
        : '';
    }

    const dragHandler = selectedIndex => {
      setDisplayIndex(prevState => prevState + 1);
    };
    console.log(`Drag Container Data >>>>`);
    console.log(dragContainer);
    return (
      <Fragment>
        <QuestionContainer questionTree={questionTree} />
        <View
          style={
            isRTL
              ? style.RTLDragContainQuestionParentView
              : style.dragContainQuestionParentView
          }>
          {dragContainer.map((singleMap, i) => {
            if (showChoicesOneByOne) {
              if (displayIndex === i) {
                return (
                  <ClassificationQuestion
                    dragToContainer1={dragToContainer1}
                    dragToContainer2={dragToContainer2}
                    item={singleMap}
                    disableClick={disableClick}
                    dragAndDropCallback={dragAndDropCallback}
                    updatedZIndex={updatedZIndex}
                    onDragEnd={dragHandler}
                  />
                );
              }
            } else {
              console.log(`Cmg here not one by one >>>>>>`);
              return (
                <ClassificationQuestion
                  dragToContainer1={dragToContainer1}
                  dragToContainer2={dragToContainer2}
                  item={singleMap}
                  disableClick={disableClick}
                  dragAndDropCallback={dragAndDropCallback}
                  updatedZIndex={updatedZIndex}
                />
              );
            }
          })}
        </View>

        <View style={style.separateView} />

        <View
          style={
            isRTL
              ? style.RTLDragContainQuestionBucketView
              : style.dragContainQuestionBucketView
          }>
          {container1Label && container1Label !== '' && (
            <ClassificationBucket
              ref={bucketRef1}
              containerType={'1'}
              data={container1}
              disableClick={disableClick}
              callbackForResetComponent={callbackForResetComponent}
              containerLabel={container1Label}
            />
          )}
          {container2Label && container2Label !== '' && (
            <ClassificationBucket
              ref={bucketRef2}
              containerType={'2'}
              data={container2}
              disableClick={disableClick}
              callbackForResetComponent={callbackForResetComponent}
              containerLabel={container2Label}
            />
          )}
        </View>
      </Fragment>
    );
  },
);

export default Classification;
