import {StyleSheet} from 'react-native';
import {heightPercentageToDP as hp} from 'react-native-responsive-screen';
import {getWp} from '@utils';
import {COLORS} from '@constants';

export default StyleSheet.create({
  dragContainQuestionBucketView: {
    zIndex: -10,
    marginTop: hp(2),
    flexDirection: 'row',
    alignSelf: 'center',
    marginBottom: hp(3),
  },

  RTLDragContainQuestionBucketView: {
    zIndex: -10,
    marginTop: hp(2),
    flexDirection: 'row-reverse',
    alignSelf: 'center',
    marginBottom: hp(3),
  },

  dragContainQuestionParentView: {
    zIndex: 10,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    flexWrap: 'wrap',
    alignContent: 'center',
    display: 'flex',
  },

  RTLDragContainQuestionParentView: {
    zIndex: 10,
    flexDirection: 'row-reverse',
    alignItems: 'center',
    justifyContent: 'center',
    flexWrap: 'wrap',
    alignContent: 'center',
    display: 'flex',
  },

  questionAnswerSeprator: {
    marginVertical: 100,
    borderBottomColor: '#707070',
    borderBottomWidth: 1,
  },

  separateView: {
    height: getWp(0.5),
    width: getWp(390),
    marginTop: getWp(20),
    marginBottom: getWp(20),
    backgroundColor: COLORS.sortListSeparateColor,
  },
});
