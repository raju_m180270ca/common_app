import React, {Fragment, useRef} from 'react';
import {View} from 'react-native';
import {Base64} from 'js-base64';
import styles from './style';
import {useStores} from '@mobx/hooks';
import {ClassificationBucket} from '@components';

const ClassificationCorrectAnswer = props => {
  const store = useStores();
  const {response: responseobj} = props;
  let {responseValidation, response, stems} = responseobj;
  let identifier = responseValidation?.validResponse?.identifier;

  let bucketRef1 = useRef();
  let bucketRef2 = useRef();
  const isRTL = store?.uiStore?.isRTL;

  let stemsArray = [];
  let answerArray = [];
  for (var key in identifier) {
    let stemIdentifier = stems.find(stemItr => stemItr?.identifier === key);
    let stemKeysArr = Base64.decode(identifier[key])
      .replace('[', '')
      .replace(']', '')
      .split(',')
      .map(itr => parseInt(itr, 10))
      .map(i => Base64.encode(i))
      .map(itr => response?.choices.find(item => item?.identifier === itr));

    stemsArray.push(stemIdentifier);
    answerArray.push(stemKeysArr);
  }

  let container1 = [];
  let container2 = [];
  if (answerArray && answerArray.length > 0) {
    container1 =
      answerArray[0] && answerArray[0].length > 0 ? answerArray[0] : [];
    container2 =
      answerArray[1] && answerArray[1].length > 0 ? answerArray[1] : [];
  }

  return (
    <Fragment>
      <View style={isRTL ? styles.RTLContainer : styles.container}>
        {container1 && container1.length > 0 ? (
          <ClassificationBucket
            ref={bucketRef1}
            containerType={'1'}
            data={container1}
            disableClick={true}
            callbackForResetComponent={() => {}}
            containerLabel={
              stemsArray && stemsArray.length > 0 && stemsArray[0]
                ? stemsArray[0].value
                : ''
            }
          />
        ) : null}

        {container2 && container2.length > 0 ? (
          <ClassificationBucket
            ref={bucketRef2}
            containerType={'2'}
            data={container2}
            disableClick={true}
            callbackForResetComponent={() => {}}
            containerLabel={
              stemsArray && stemsArray.length > 0 && stemsArray[1]
                ? stemsArray[1].value
                : ''
            }
          />
        ) : null}
      </View>
    </Fragment>
  );
};

export default ClassificationCorrectAnswer;
