import React from 'react';
import {View} from 'react-native';
import PropTypes from 'prop-types';
import {
  RobotoRegTextView,
  BalooThambiRegTextView,
  MyAutoHeightWebView,
  SourceSansProBoldTextView,
} from '@components';
import {useStores} from '@mobx/hooks';
import styles from './indexCss';
import {Base64} from 'js-base64';

const CorrectMCQ = props => {
  const {response: responseobj, mcqData, isCorrect} = props;

  let {responseValidation, response, encrypted} = responseobj;
  let identifier = responseValidation?.validResponse?.identifier;
  let options = response?.mcqPattern?.choices.map((item, index) => {
    item.index = index;
    return item;
  });

  let correctAnswer = [];
  if (identifier && identifier.length > 0) {
    correctAnswer = identifier.map(item => {
      const answer = options.find(option => option.identifier === item);
      return answer;
    });
  } else if (
    response.mcqPattern.correctAnswer &&
    response.mcqPattern.correctAnswer !== ''
  ) {
    let index = [Number(Base64.decode(response.mcqPattern.correctAnswer))];
    correctAnswer = [options[index]];
  }

  const displayAnswer =
    mcqData && mcqData?.item?.displayAnswer
      ? encrypted
        ? Base64.decode(mcqData?.item?.displayAnswer)
        : mcqData?.item?.displayAnswer
      : null;

  const {uiStore} = useStores();
  const languageData = uiStore?.languageData;
  const correctAnswerText =
    languageData && languageData?.hasOwnProperty('correct_answer')
      ? languageData?.correct_answer
      : 'Correct Answer';
  const isRTL = uiStore.isRTL;

  return (
    <View>
      <View
        key="container"
        style={isRTL ? styles.RTLContainer : styles.container}>
        <BalooThambiRegTextView style={styles.text}>
          {correctAnswerText}
        </BalooThambiRegTextView>
        <View key="optionContainer" style={styles.optionContainer}>
          <RobotoRegTextView style={styles.optionText}>
            {String.fromCharCode(correctAnswer[0]?.index + 65)}
          </RobotoRegTextView>
        </View>
      </View>
      {displayAnswer && displayAnswer !== '' && (
        <View style={{margin: 16}}>
          <SourceSansProBoldTextView
            style={
              isRTL ? styles.RTLDisplayAnswerText : styles.displayAnswerText
            }>
            {isCorrect
              ? 'Why you are correct'
              : 'Why you might have gone wrong'}
          </SourceSansProBoldTextView>
          <MyAutoHeightWebView
            style={styles.webViewContainer}
            onMessage={() => {}}
            customScript={''}
            onSizeUpdated={size => {
              console.log(size.height);
            }}
            source={{html: displayAnswer}}
            zoomable={false}
          />
        </View>
      )}
    </View>
  );
};

CorrectMCQ.propTypes = {
  response: PropTypes.object,
};

CorrectMCQ.defaultProps = {};
export default CorrectMCQ;
