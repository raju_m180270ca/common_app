import React from 'react';
import {View} from 'react-native';
import styles from './style';
import {BalooThambiRegTextView, RobotoRegTextView} from '@components';
import PropTypes from 'prop-types';
import {useStores} from '@mobx/hooks';
import {useLanguage} from '@hooks';

const CorrectMultiSelectMCQ = props => {
  const {response: responseobj} = props;
  let {responseValidation, response} = responseobj;
  const {uiStore} = useStores();
  const isRTL = uiStore.isRTL;

  const {correctAnswerText} = useLanguage();

  let identifier = responseValidation?.validResponse?.identifier;
  let options = response?.mcqPattern?.choices.map((item, index) => {
    item.index = index;
    return item;
  });

  const correctAnswer = identifier?.map(item => {
    const answer = options.find(option => option.identifier === item);
    return answer;
  });

  return correctAnswer && correctAnswer.length > 0 ? (
    <View style={[isRTL ? styles.RTLAnswerContainer : styles.answerContainer]}>
      <BalooThambiRegTextView style={styles.text}>
        {correctAnswerText}
      </BalooThambiRegTextView>

      <View style={styles.optionContainer}>
        {correctAnswer.map((item, index) => {
          let option = String.fromCharCode(item.index + 65);
          option = index !== correctAnswer.length - 1 ? option + ', ' : option;
          return (
            <RobotoRegTextView style={styles.optionText}>
              {option}
            </RobotoRegTextView>
          );
        })}
      </View>
    </View>
  ) : null;
};

CorrectMultiSelectMCQ.propTypes = {
  response: PropTypes.object,
  data: PropTypes.array,
};

CorrectMultiSelectMCQ.defaultProps = {
  data: [],
};

export default CorrectMultiSelectMCQ;
