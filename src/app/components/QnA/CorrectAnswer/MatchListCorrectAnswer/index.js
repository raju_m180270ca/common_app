import React, {Fragment, useState, useEffect} from 'react';
import {View} from 'react-native';
import {MatchAnswer, MatchQuestion} from '@components';
import styles from './style';
import {Base64} from 'js-base64';
import {useStores} from '@mobx/hooks';

const MatchListCorrectAnswer = props => {
  const store = useStores();
  const {response: responseobj} = props;
  let {responseValidation, response, stems} = responseobj;
  let identifier = responseValidation?.validResponse?.identifier;

  const [options, setOptions] = useState([]);
  const [stemsList, setStemsList] = useState([]);
  const [Answer, setAnswer] = useState([]);
  const [encrypted, setEncrypted] = useState(false);

  console.log('mathclist stems list set :>> ', stemsList);

  useEffect(() => {
    setOptions(
      response?.choices &&
        response?.choices.map((item, index) => {
          item.index = index;
          item.store = store;
          item.isImage = item.value.indexOf('img') > 0;
          return item;
        }),
    );
    setEncrypted(responseobj.encrypted);
    return () => {};
  }, [responseobj]);
  useEffect(() => {
    setStemsList(
      stems &&
        options &&
        options.length > 0 &&
        stems.map((item, index) => {
          item.isImage = options[index]?.isImage;
          item.store = store;
          return item;
        }),
    );
    return () => {};
  }, [options]);

  useEffect(() => {
    let ansArr = [];
    for (var key in identifier) {
      let stemFind =
        stemsList && stemsList.find(stemItr => stemItr?.identifier === key);
      let choiceFind = options.find(choiceItr => {
        let decodeValidResponse = encrypted
          ? Base64.decode(identifier[key])
          : identifier[key];
        let decodeOptionIdentifier = encrypted
          ? Base64.decode(choiceItr?.identifier)
          : choiceItr?.identifier;
        console.log(
          `Comparing ${decodeValidResponse} with ${decodeOptionIdentifier}`,
        );
        console.log(
          `compare result>>>>${decodeValidResponse.indexOf(
            decodeOptionIdentifier,
          )}`,
        );
        if (decodeValidResponse.indexOf(decodeOptionIdentifier) != -1) {
          return choiceItr;
        }
      });
      console.log(`object`);
      ansArr.push(choiceFind);
    }
    setAnswer(ansArr);
    return () => {};
  }, [stemsList]);

  // let Question = [];
  // let Answer = [];
  // const options =
  //   response?.choices &&
  //   response?.choices.map((item, index) => {
  //     item.index = index;
  //     item.store = store;
  //     item.isImage = item.value.indexOf('img') > 0;
  //     return item;
  //   });

  // const stemsList =
  //   stems &&
  //   options &&
  //   options.length > 0 &&
  //   stems.map((item, index) => {
  //     item.isImage = options[index]?.isImage;
  //     item.store = store;
  //     return item;
  //   });

  // for (var key in identifier) {
  //   let stemFind =
  //     stemsList && stemsList.find(stemItr => stemItr?.identifier === key);
  //   let choiceFind = options.find(choiceItr => {
  //     let decodeValidResponse = Base64.decode(identifier[key]);
  //     let decodeOptionIdentifier = Base64.decode(choiceItr?.identifier);
  //     if (decodeValidResponse.indexOf(decodeOptionIdentifier) > 0) {
  //       return choiceItr;
  //     }
  //   });
  //   Answer.push(choiceFind);
  // }

  const isRTL = store?.uiStore?.isRTL;

  return (
    <Fragment>
      <View style={isRTL ? styles.RTLContainer : styles.container}>
        <View>
          {stemsList &&
            stemsList.map((item, index) => {
              return (
                <MatchQuestion
                  item={item}
                  index={index}
                  isExplaination={true}
                />
              );
            })}
        </View>
        <View style={styles.dragableListContainer}>
          {Answer &&
            Answer.map((item, index) => {
              return (
                <MatchAnswer
                  item={item}
                  index={index}
                  drag={() => {}}
                  isExplaination={true}
                />
              );
            })}
        </View>
      </View>
    </Fragment>
  );
};

export default MatchListCorrectAnswer;
