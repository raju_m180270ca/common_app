import React, {Fragment} from 'react';
import {View} from 'react-native';
import {OrderingQuestion} from '@components';
import styles from './style';
import {useStores} from '@mobx/hooks';

const OrderingCorrectAnswer = props => {
  const {response: responseobj, onSoundBtnClicked} = props;
  let {responseValidation, response} = responseobj;

  let identifier = responseValidation?.validResponse?.identifier;
  let rightAnswers = [];
  const store = useStores();
  let options = response?.choices.map((option, index) => {
    option.index = index;
    option.store = store;
    return option;
  });

  identifier?.map(rightAnswerItr => {
    options.map((choiceMap, choiceIndex) => {
      if (rightAnswerItr === choiceMap.identifier) {
        rightAnswers.push({
          ...choiceMap,
        });
      }
    });
  });

  return (
    <Fragment>
      <View style={styles.container}>
        {rightAnswers &&
          rightAnswers.map(item => {
            return (
              <OrderingQuestion
                item={item}
                index={item?.index}
                dragType={false}
                onSoundBtnClicked={onSoundBtnClicked}
                isExplaination={true}
              />
            );
          })}
      </View>
    </Fragment>
  );
};

export default OrderingCorrectAnswer;
