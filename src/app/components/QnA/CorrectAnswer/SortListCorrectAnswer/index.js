import React, {Fragment} from 'react';
import {View} from 'react-native';
import {Base64} from 'js-base64';
import styles from './style';
import {MyAutoHeightWebView} from '@components';
import {useStores} from '@mobx/hooks';
import getHtmlTemplate from '@utils/getHtmlTemplate';

const SortListCorrectAnswer = props => {
  const store = useStores();
  const {response: responseobj, questionType} = props;
  let {responseValidation, response} = responseobj;
  let identifier = responseValidation?.validResponse?.identifier;

  const isRTL = store.uiStore?.isRTL;

  let renderAns = [];
  for (var key in identifier) {
    let rightAnswer = response?.choices.find(choiceItr => {
      let decodeValidResponse = Base64.decode(identifier[key]);
      let decodeOptionIdentifier = Base64.decode(choiceItr?.identifier);
      if (decodeValidResponse.indexOf(decodeOptionIdentifier) !== -1) {
        return choiceItr;
      }
    });

    renderAns.push(rightAnswer?.value);
  }

  let correctAnswerBody =
    questionType === 'word' ? renderAns.join('') : renderAns.join('&nbsp;');
  correctAnswerBody =
    correctAnswerBody && getHtmlTemplate(correctAnswerBody, false, isRTL);

  return (
    <Fragment>
      <View style={[styles.container, isRTL ? styles.RTLContainer : '']}>
        <MyAutoHeightWebView
          style={styles.webviewContainer}
          onSizeUpdated={size => {
            console.log(size.height);
          }}
          source={{html: correctAnswerBody}}
          zoomable={false}
        />
      </View>
    </Fragment>
  );
};

export default SortListCorrectAnswer;
