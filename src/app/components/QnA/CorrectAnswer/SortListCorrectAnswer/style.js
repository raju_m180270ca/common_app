import {Dimensions, StyleSheet} from 'react-native';
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen';
import {COLORS} from '@constants';
import {getWp} from '@utils';

export default StyleSheet.create({
  container: {
    flexDirection: 'row',
    borderRadius: wp(5),
    backgroundColor: COLORS.white,
    marginHorizontal: getWp(5),
    // flex: 1,
    flexWrap: 'wrap',
    padding: getWp(5),
    alignItems: 'center',
    justifyContent: 'center',
  },
  RTLContainer: {
    flexDirection: 'row-reverse',
  },
  webviewContainer: {
    width: getWp(Dimensions.get('window').width/2 + Dimensions.get('window').width/3-1),
  },
  textStyle: {
    padding: getWp(5),
  },
});
