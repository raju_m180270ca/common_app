import React from 'react';
import {View, FlatList, TouchableOpacity} from 'react-native';
import Modal from 'react-native-modal';
import {MyAutoHeightWebView} from '@components';
import styles from './indexCss';
import getHtmlTemplate from '@utils/getHtmlTemplate';
import {useStores} from '@mobx/hooks';

const ChoiceList = props => {
  const {choices, show, selectChoiceHandler, onBackdropHandler} = props;
  const {uiStore} = useStores();
  const isRTL = uiStore.isRTL;

  const renderChoiceList = ({item, index}) => {
    return (
      <TouchableOpacity onPress={() => selectChoiceHandler(item)}>
        <View style={styles.choiceItem}>
          <MyAutoHeightWebView
            onMessage={() => {}}
            style={styles.webViewContainer}
            customScript={''}
            onSizeUpdated={size => {
              console.log(size.height);
            }}
            source={{html: getHtmlTemplate(item.value, false, isRTL)}}
            zoomable={false}
          />
        </View>
      </TouchableOpacity>
    );
  };

  return (
    <Modal isVisible={show} onBackdropPress={onBackdropHandler}>
      <View style={styles.container}>
        <FlatList
          scrollEnabled={true}
          data={choices}
          renderItem={renderChoiceList}
        />
      </View>
    </Modal>
  );
};

ChoiceList.propTypes = {};

ChoiceList.defaultProps = {};

export default ChoiceList;
