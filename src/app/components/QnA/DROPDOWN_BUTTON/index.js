/* eslint-disable react-native/no-inline-styles */
import React, {useState, useEffect, useRef} from 'react';
import {View, KeyboardAvoidingView} from 'react-native';
import {HintBox, MyAutoHeightWebView, ChoiceList} from '@components';

import {useStores} from '@mobx/hooks';
import {checkForAudio} from '@utils';
import {useLanguage} from '@hooks';
import styleSheet from './indexCss';

import getHtmlTemplate from '@utils/getHtmlTemplate';

const DROPDOWN_BUTTON = props => {
  const styles = styleSheet();
  const {
    showHint,
    setInputResponse,
    disableWebView,
    webref,
    onWebViewCallback,
  } = props;
  const [showHintBox, setShowHintBox] = useState(false);
  const [listChoices, setListChoices] = useState(null);
  const [showChoiceModal, setShowChoiceModal] = useState(false);
  const [trials, setTrials] = useState(-1);
  const [btnId, setBtnId] = useState(null);
  const {selectText} = useLanguage();

  var question = props.questionRes;
  const {qnaStore, uiStore} = useStores();
  const isRTL = uiStore.isRTL;

  useEffect(() => {
    console.log('Inside dropdown button ');
    setTrials(qnaStore.trialCount);
  }, [qnaStore.trialCount]);

  let questionBody = '';
  if (
    question?.data[0]?.questionBody &&
    question?.data[0]?.questionBody.length > 0
  ) {
    question.data[0].questionBody = question.data[0].questionBody.replace(
      'Dropdown',
      'dropdown',
    );
  }
  try {
    questionBody = decodeURI(question.data[0].questionBody);
  } catch (err) {
    questionBody = question.data[0].questionBody;
  }
  questionBody = questionBody.replace('\n', '').replace('\r', '');
  let choices = question.data[0].response;

  let fields = choices;
  let ddButton;
  for (var field in fields) {
    let userResponses =
      props.questionRes?.contentParams?.userAttemptData?.userResponse;

    let userAnswer = selectText;
    console.log('frop_down current userAnser/index text = select');
    if (userResponses?.hasOwnProperty(field)) {
      userResponse = parseInt(
        userResponses[field].hasOwnProperty('userAnswer')
          ? userResponses[field].userAnswer
          : -1,
      );

      if (
        userResponse !== null &&
        userResponse !== undefined &&
        userResponse !== -1 &&
        choices[field] !== null &&
        choices[field] !== undefined &&
        choices[field].choices[userResponse] !== null &&
        choices[field].choices[userResponse] !== undefined
      ) {
        userAnswer = choices[field].choices[userResponse].value;
      }
    }

    console.log(`userAnswer >>>>>>>>>>>>>>>>>>>>>>>>>>>>>${userAnswer}`);

    if (fields.hasOwnProperty(field)) {
      let fieldArr = field.split('_');
      switch (fieldArr[0]) {
        case 'dropdown':
          ddButton = `<button type="button" id="${field}" class="dropdown_button" onclick="ddButtonClickHandler(this)">${userAnswer}<i class="arrow down"></i></button>`;
          questionBody = questionBody.replace('[' + field + ']', ddButton);
          break;
      }
    }
  }

  const hintToggle = showBox => {
    setShowHintBox(showBox);
  };
  questionBody = checkForAudio(questionBody);
  questionBody += '';

  questionBody = getHtmlTemplate(questionBody, true, isRTL);
  const ddBtnWebViewMsgHandler = event => {
    console.log(event.nativeEvent.data);

    let msgData = JSON.parse(event.nativeEvent.data);
    console.log(msgData.type);
    if (msgData && msgData.type == 'select') {
      console.log(`dropdown response>>>>choices`);
      console.log(fields[msgData.id].choices);
      setBtnId(msgData.id);
      setListChoices(() => {
        let ddChoices = question.data[0].response[msgData.id];
        console.log('MAPPER:::::::::::::::' + ddChoices['mapper']);
        var tmpChoices = [];
        var mapper1 = ddChoices?.mapper;
        var localChoices = ddChoices?.choices;
        if (mapper1) {
          mapper1.forEach(function (val, index) {
            tmpChoices.push({
              id: val,
              value: localChoices[val].value,
            });
          });
        }
        return tmpChoices;
      });
      setShowChoiceModal(true);
    } else if (msgData && msgData.type == 'ContentService') {
      onWebViewCallback(event);
    }
  };

  const selectChoiceHandler = choiceItem => {
    setShowChoiceModal(false);
    console.log(`frop_down Selected choice item :`);
    console.log('frop_down choiceitem-', choiceItem);
    const run = `
    ddChoiceSelectHandler("${btnId}","${choiceItem.value}");
      `;
    webref.current.injectJavaScript(run);
    console.log(`Choiceitem >>>>>>>>>>>>>>>>>>>${choiceItem.id}`);
    setInputResponse(btnId, choiceItem.id);

    const updateHeightScript = `
    updateWebviewHeight();
      `;

    webref.current.injectJavaScript(updateHeightScript);
  };

  if (disableWebView) {
    const run = `
    disableWebView();
      `;
    webref && webref.current.injectJavaScript(run);
  }

  const onBackdropHandler = () => {
    setShowChoiceModal(false);
  };

  //console.log(questionBody);
  return (
    <View style={styles.container}>
      <View style={styles.innerContainer}>
        <KeyboardAvoidingView behavior="padding" enabled={true}>
          <MyAutoHeightWebView
            ref={webref}
            onMessage={ddBtnWebViewMsgHandler}
            style={styles.webViewContainer}
            customScript={''}
            // script={jsCode}
            onSizeUpdated={size => {
              console.log(size.height);
            }}
            source={{html: questionBody}}
            zoomable={false}
          />
          <HintBox
            hintList={question.data[0].hints}
            toggleCallback={hintToggle}
            showBtn={showHint}
            showHints={showHintBox}
            showHide={!showHintBox}
            trials={trials}
          />
          {listChoices && (
            <ChoiceList
              choices={listChoices}
              show={showChoiceModal}
              selectChoiceHandler={selectChoiceHandler}
              onBackdropHandler={onBackdropHandler}
            />
          )}
        </KeyboardAvoidingView>
      </View>
    </View>
  );
};

DROPDOWN_BUTTON.propTypes = {};

DROPDOWN_BUTTON.defaultProps = {};
export default DROPDOWN_BUTTON;
