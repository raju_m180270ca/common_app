import {StyleSheet} from 'react-native';
import {getHp, getWp} from '@utils/ViewUtils';

import {useStores} from '@mobx/hooks';

const styleSheetFunc = () => {
  const {uiStore} = useStores();
  const isRTL = uiStore.isRTL;

  return StyleSheet.create({
    container: {
      justifyContent: 'center',
      alignItems: 'center',
      //   width: getWp(373),
      flex: 1,
    },
    innerContainer: {
      backgroundColor: 'transparent',
      alignItems: 'center',
    },
    webViewContainer: {
      marginTop: getHp(27),
      marginBottom: getHp(45),
      //   marginStart: isRTL ? getWp(0) : getWp(16.5),
      marginEnd: isRTL ? getWp(16.5) : getWp(0),
      width: getWp(393),
    },
  });
};

// export default StyleSheet.create({
//   container: {
//     margin: 0,
//     paddingBottom: 0,
//     justifyContent: 'center',
//     flex: 1,
//     marginTop: hp('3'),
//   },

//   innderContainer: {
//     margin: 10,
//     paddingBottom: 0,
//   },
//   webViewContainer: {
//     width: wp('85%'),
//     marginStart: isRTL ? wp('0') : wp('4'),
//     marginEnd: isRTL ? wp('4') : wp('0'),
//   },
// });

export default styleSheetFunc;
