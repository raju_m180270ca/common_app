import React, {useState, useEffect} from 'react';
import {View, TouchableOpacity} from 'react-native';
import styles from './indexCss';
import {COLORS} from '@constants/COLORS';
import {ExplanationCorrect, ExplanationWrong, SoundSvg} from '@images';
import {
  SourceSansProBoldTextView,
  CorrectMCQ,
  MyAutoHeightWebView,
  CorrectMultiSelectMCQ,
  ClassificationCorrectAnswer,
  MatchListCorrectAnswer,
  OrderingCorrectAnswer,
  SortListCorrectAnswer,
} from '@components';
import {getWp} from '@utils';
import {useStores} from '@mobx/hooks';
import PropTypes from 'prop-types';

const Explanation = props => {
  const {
    testID,
    explanation,
    type,
    isCorrect,
    mcqData,
    audioData,
    onSoundBtnClicked,
    response,
  } = props;
  const {uiStore} = useStores();
  const languageData = uiStore.languageData;
  const explainationText =
    languageData && languageData?.hasOwnProperty('explanation')
      ? languageData?.explanation
      : 'Explanation';
  const isRTL = uiStore.isRTL;

  const renderTitleView = () => {
    return (
      <View style={isRTL ? styles.RTLtitleContainer : styles.titleContainer}>
        {audioData && audioData !== '' ? (
          <TouchableOpacity
            style={styles.soundIconContainer}
            onPress={() => onSoundBtnClicked(audioData)}>
            <SoundSvg width={getWp(20)} height={getWp(20)} />
          </TouchableOpacity>
        ) : null}
        {explanation ? (
          <SourceSansProBoldTextView style={styles.titleText}>
            {explainationText}
          </SourceSansProBoldTextView>
        ) : null}
      </View>
    );
  };

  const renderOptionItem = () => {
    switch (type) {
      case 'MCQ':
        if (response?.multiResponse) {
          return <CorrectMultiSelectMCQ response={response} />;
        } else {
          return (
            <CorrectMCQ
              isCorrect={isCorrect}
              response={response}
              mcqData={mcqData}
            />
          );
        }
      case 'Classification':
        return <ClassificationCorrectAnswer response={response} />;
      case 'MatchList':
        return <MatchListCorrectAnswer response={response} />;
      case 'Ordering':
        return (
          <OrderingCorrectAnswer
            response={response}
            onSoundBtnClicked={onSoundBtnClicked}
          />
        );
      case 'SortList':
        return (
          <SortListCorrectAnswer
            response={response}
            questionType={response?.questionType}
          />
        );
    }
  };

  return (
    <View
      accessible={true}
      testID={testID}
      accessibilityLabel={testID}
      key="container"
      style={[
        styles.container,
        {
          borderTopColor: isCorrect
            ? COLORS.explanationGreen
            : COLORS.explanationRed,
        },
      ]}>
      <View
        key="correctWrongLabel"
        style={
          isRTL
            ? styles.RTLCorrectWrongLabelContainer
            : styles.correctWrongLabelContainer
        }>
        {isCorrect === true ? (
          <ExplanationCorrect
            accessible={true}
            testID="ExplanationCorrect"
            accessibilityLabel="ExplanationCorrect"
          />
        ) : (
          <ExplanationWrong
            accessible={true}
            testID="ExplanationCorrect"
            accessibilityLabel="ExplanationCorrect"
          />
        )}
      </View>
      {renderOptionItem()}
      {renderTitleView()}

      <MyAutoHeightWebView
        showsHorizontalScrollIndicator={true}
        testID="MyAutoHeightWebViewExplanation"
        style={styles.webviewContainer}
        onSizeUpdated={size => {
          console.log(size.height);
        }}
        source={{html: explanation}}
        zoomable={false}
      />
      <View
        accessible={true}
        testID="ExplanationSeperateView"
        accessibilityLabel="ExplanationSeperateView"
        style={styles.separateView}
      />
    </View>
  );
};

Explanation.propTypes = {
  testID: PropTypes.string,
  enableMultiSelectMCQ: PropTypes.bool,
  onSoundBtnClicked: PropTypes.func,
};

Explanation.defaultProps = {
  testID: 'Explanation',
  enableMultiSelectMCQ: false,
  onSoundBtnClicked: () => {},
};
export default Explanation;
