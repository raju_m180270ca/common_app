import React from 'react';
import {Text, View, TouchableOpacity} from 'react-native';
import {MyAutoHeightWebView} from '@components';
import {SoundSvg} from '@images';
import {useStores} from '@mobx/hooks';
import {useLanguage} from '@hooks';
import styles from './indexCss';
import PropTypes from 'prop-types';
import {SimpleHtmlTemplate} from '@utils';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';
import {getWp} from '@utils';

const InstructorStimulusVO = props => {
  const {testID, showInsStVO, playSound} = props;
  const {qnaStore, uiStore} = useStores();
  const isRTL = uiStore.isRTL;

  let instStimulusVal = qnaStore?.currentQuestion?.instructorStimulus?.value;
  let voView = null;
  if (showInsStVO || instStimulusVal) {
    instStimulusVal = SimpleHtmlTemplate(instStimulusVal);
    voView = (
      <View
        accessible={true}
        testID={testID}
        accessibilityLabel={testID}
        style={
          isRTL
            ? styles.RTLInstructorStimulusContainer
            : styles.instructorStimulusContainer
        }>
        {showInsStVO && (
          <TouchableOpacity
            accessible={true}
            testID="InstructorStimulusSound"
            accessibilityLabel="InstructorStimulusSound"
            style={{
              ...styles.soundIconContainer,
              ...{marginRight: isRTL ? 0 : getWp(12)},
            }}
            onPress={() => playSound('instStimulusVO')}>
            <SoundSvg
              width={styles.soundIcon.width}
              height={styles.soundIcon.height}
            />
          </TouchableOpacity>
        )}
        <View
          accessible={true}
          testID="InstructorStimulusTextContainer"
          accessibilityLabel="InstructorStimulusTextContainer"
          style={
            isRTL
              ? styles.RTLInstructorStimulusTextContainer
              : styles.instructorStimulusTextContainer
          }>
          <MyAutoHeightWebView
            style={[
              isRTL
                ? styles.RTLInstructorStimulusText
                : styles.instructorStimulusText,
              {width: wp(80)},
            ]}
            files={[]}
            customScript={''}
            customStyle={``}
            onSizeUpdated={() => {}}
            source={{
              html: instStimulusVal,
            }}
            zoomable={true}
          />
        </View>
      </View>
    );
  }

  return voView;
};

InstructorStimulusVO.propTypes = {
  testID: PropTypes.string,
};

InstructorStimulusVO.defaultProps = {
  testID: 'InstructorStimulusVO',
};

export default InstructorStimulusVO;
