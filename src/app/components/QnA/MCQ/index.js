/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable react-native/no-inline-styles */
import React, {useState, useEffect, useImperativeHandle, useRef} from 'react';
import {
  View,
  FlatList,
  ScrollView,
  TouchableOpacity,
  PanResponder,
} from 'react-native';
import {Toast} from 'native-base';
import styles from './indexCss';
import {COLORS} from '@constants';
import {Base64} from 'js-base64';
import {
  MyAutoHeightWebView,
  SourceSansProBoldTextView,
  HintBox,
} from '@components';
import {getWp, validateUserResponseArrays, checkForAudio} from '@utils';
import {SoundSvg} from '@images';
import {useStores} from '@mobx/hooks';
import getHtmlTemplate from '@utils/getHtmlTemplate';

const MCQ = React.forwardRef((props, ref) => {
  const {
    isScreeningTest,
    isWorkOrHomeWorkTest,
    selectedChoice,
    setSelectedChoice,
    lockOptions,
    questionData,
  } = props;

  const [trials, setTrials] = useState(-1);
  const [showHint, setShowHint] = useState(false);
  const [showHintBox, setShowHintBox] = useState(false);
  const [disableClick, setDisable] = useState(false);
  const [isFetching, setIsFetching] = useState(false);

  const [hideQuestion, setHideQuestion] = useState(false);
  const [newHeight, setNewHeight] = useState(0);
  const store = useStores();
  const {qnaStore, uiStore} = store;
  const isRTL = uiStore.isRTL;

  const panResponder = React.useRef(
    PanResponder.create({
      // Ask to be the responder:
      onStartShouldSetPanResponder: (evt, gestureState) => true,
      onStartShouldSetPanResponderCapture: (evt, gestureState) => true,
      onMoveShouldSetPanResponder: (evt, gestureState) => true,
      onMoveShouldSetPanResponderCapture: (evt, gestureState) => true,

      onPanResponderGrant: (evt, gestureState) => {
        // The gesture has started. Show visual feedback so the user knows
        // what is happening!
        // gestureState.d{x,y} will be set to zero now
      },
      onPanResponderMove: (evt, gestureState) => {
        // The most recent move distance is gestureState.move{X,Y}
        // The accumulated gesture distance since becoming responder is
        // gestureState.d{x,y}
      },
      onPanResponderTerminationRequest: (evt, gestureState) => true,
      onPanResponderRelease: (evt, gestureState) => {
        // The user has released all touches while this view is the
        // responder. This typically means a gesture has succeeded
      },
      onPanResponderTerminate: (evt, gestureState) => {
        // Another component has become the responder, so this gesture
        // should be cancelled
        return true;
      },
      onShouldBlockNativeResponder: (evt, gestureState) => {
        // Returns whether this component should block native components from becoming the JS
        // responder. Returns true by default. Is currently only supported on android.
        return true;
      },
    }),
  ).current;

  useEffect(() => {
    setDisable(false);
    setTrials(questionData.trials);

    if (
      questionData.trials == 1 &&
      questionData.hints &&
      questionData.hints.length > 0
    ) {
      setShowHint(true);
    }

    return () => {
      setSelectedChoice(-1);
    };
  }, [questionData]);

  useEffect(() => {
    let hideTimer = null;
    if (questionData?.display?.hideQuesInSeconds) {
      hideTimer = setTimeout(() => {
        setHideQuestion(true);
      }, parseInt(questionData?.display?.hideQuesInSeconds) * 1000);
    } else {
      setHideQuestion(false);
    }

    return () => {
      hideTimer && clearTimeout(hideTimer);
    };
  }, [questionData]);

  useImperativeHandle(ref, () => ({
    evaluteAnswer(userInputData) {
      let isValidResponse = isValidUserResponse(userInputData);
      let validScore = questionData?.responseValidation?.validResponse?.score
        ? questionData?.responseValidation?.validResponse?.score
        : 1;

      let payload = {};
      payload.isDynamic = questionData?.isDynamic;
      payload.contentID = questionData?.contentID;
      payload.score = isValidResponse ? validScore : 0;
      payload.result = isValidResponse
        ? Base64.encode('pass')
        : Base64.encode('fail');
      payload.userResponse = {};
      payload.userResponse.mcqPattern = {};
      payload.userResponse.mcqPattern.type = questionData?.template;
      payload.userResponse.mcqPattern.userAnswer = userInputData.index;
      payload.userAttemptData = {
        trials: [
          {
            userResponse: {
              mcqPattern: {
                type: questionData?.template,
                userAnswer: userInputData.index,
              },
            },
            result: isValidResponse
              ? Base64.encode('true')
              : Base64.encode('false'),
            score: isValidResponse ? validScore : 0,
          },
        ],
      };
      let contentInfo = {};
      contentInfo.contentID = questionData?.contentID;
      contentInfo.contentVersionID = questionData?._id;
      contentInfo.contentType = questionData?.contentType;
      contentInfo.questionType = questionData?.template;
      contentInfo.revisionNum = questionData?.revisionNo;
      contentInfo.langCode = questionData?.langCode;
      payload.contentInfo = contentInfo;
      payload.remainingTime = 0;
      payload.nextContentSeqNum = null;

      return payload;
    },

    reset() {},
  }));

  const isValidUserResponse = data => {
    let isValidResponse = false;
    let userResponse = [data.item];
    if (
      questionData.responseValidation?.hasOwnProperty('validResponse') &&
      questionData.responseValidation?.validResponse &&
      questionData?.responseValidation?.validResponse?.identifier
    ) {
      let {identifier} = questionData.responseValidation?.validResponse;
      isValidResponse = validateUserResponseArrays(identifier, userResponse);
    } else if (
      questionData?.response?.mcqPattern?.hasOwnProperty('correctAnswer') &&
      questionData?.response?.mcqPattern?.correctAnswer &&
      questionData?.response?.mcqPattern?.correctAnswer !== ''
    ) {
      isValidResponse =
        data.index ===
        Number(Base64.decode(questionData.response.mcqPattern.correctAnswer))
          ? true
          : false;
    }

    return isValidResponse;
  };

  const checkAnswerStatus = data => {
    if (
      questionData.responseValidation?.hasOwnProperty('validResponse') &&
      questionData.responseValidation?.validResponse &&
      questionData?.responseValidation?.validResponse?.identifier
    ) {
      let {identifier} = questionData.responseValidation?.validResponse;
      let userIdentifier = data?.item?.identifier;
      if (identifier == userIdentifier) return true;
    } else if (
      questionData?.response?.mcqPattern?.hasOwnProperty('correctAnswer') &&
      questionData?.response?.mcqPattern?.correctAnswer &&
      questionData?.response?.mcqPattern?.correctAnswer !== ''
    ) {
      return (
        data.index ===
        Number(Base64.decode(questionData.response.mcqPattern.correctAnswer))
      );
    }
    return false;
  };
  const renderMcqOptionItem = data => {
    try {
      var backgroundColor = COLORS.blue;

      const isSelected = data.index === selectedChoice ? true : false;
      //console.log('------0');
      const resultBoolean = isValidUserResponse(data);
      //console.log('------1');

      if (isScreeningTest) {
        if (resultBoolean && isSelected) {
          backgroundColor = COLORS.green;
        } else if (!resultBoolean && isSelected) {
          backgroundColor = COLORS.red;
        }
      } else {
        if (resultBoolean && isSelected) {
          if (isWorkOrHomeWorkTest) {
            backgroundColor = COLORS.yellow;
          } else {
            backgroundColor = COLORS.green;
          }
        } else if (!resultBoolean && isSelected) {
          if (isWorkOrHomeWorkTest) {
            backgroundColor = COLORS.yellow;
          } else {
            backgroundColor = COLORS.red;
          }
        }
      }

      if (isScreeningTest && isFetching) {
        if (checkAnswerStatus(data)) {
          backgroundColor = COLORS.green;
        }
      }

      let optionVal = '';
      try {
        optionVal = decodeURI(data.item.value);
      } catch (err) {
        optionVal = data.item.value;
      }
      try {
        // optionVal = QHtmlTemplateForIframe(optionVal, getWp(230), store);
        optionVal = getHtmlTemplate(optionVal, false, isRTL);
      } catch (err) {
        console.log('ERRIR IN IFRAME:', optionVal);
      }
      //console.log('------3', optionVal);
      const isSoundEnabled =
        data.item.valueVoiceover && data.item.valueVoiceover !== '';

      // console.log(`MCQ Option HTML>>>>>${optionVal}`);

      return (
        <View
          style={isRTL ? styles.RTLOptionContainer : styles.optionContainer}>
          {isSoundEnabled && (
            <TouchableOpacity
              style={[
                styles.soundIconContainer,
                {
                  marginStart: isRTL ? getWp(16) : getWp(0),
                  marginEnd: !isRTL ? getWp(16) : getWp(0),
                },
              ]}>
              <SoundSvg width={getWp(20)} height={getWp(20)} />
            </TouchableOpacity>
          )}
          <View style={isRTL ? styles.RTLMcqItemBg : styles.mcqItemBg}>
            <TouchableOpacity
              key="mcqItemBg"
              activeOpacity={1}
              disabled={disableClick}
              style={isRTL ? styles.RTLMcqItemBg : styles.mcqItemBg}
              onPress={() => {
                if (!lockOptions) {
                  if (isScreeningTest) {
                    setIsFetching(true);
                  }
                  let remTrials = trials - 1;
                  console.log('Result:', resultBoolean);
                  console.log('Trial Count:', remTrials);
                  setTrials(remTrials);
                  qnaStore.setTrials(remTrials);
                  console.log('remaning trials', trials);
                  setSelectedChoice(data.index);
                  if (resultBoolean) {
                    props.trueSelectionMcq(data);
                  } else if (!resultBoolean && remTrials > 0) {
                    Toast.show({
                      text: `WRONG ANSWER, You have ${remTrials} trial left.`,
                      buttonText: 'Okay',
                      duration: 2000,
                    });
                    if (questionData.hints && questionData.hints.length > 0) {
                      setShowHint(true);
                    }
                  } else {
                    props.falseSelectionMcq(data);
                  }
                  if (isScreeningTest) {
                    props.submitQuestion();
                  }
                }
              }}>
              <View
                key="mcqTouchContainer"
                style={[
                  styles.mcqTouchContainer,
                  {backgroundColor: backgroundColor},
                ]}>
                <View>
                  <SourceSansProBoldTextView style={styles.optionIndex}>
                    {String.fromCharCode(data.index + 65)}
                  </SourceSansProBoldTextView>
                </View>
              </View>
              <View style={styles.optionHtmlContainer}>
                <MyAutoHeightWebView
                  key="mcqItemWebView"
                  style={{
                    width: isSoundEnabled ? getWp(160) : getWp(230),
                  }}
                  customScript={''}
                  scrollEnabled={false}
                  onSizeUpdated={() => {}}
                  source={{html: optionVal}}
                  zoomable={false}
                />
              </View>
            </TouchableOpacity>
          </View>
        </View>
      );
    } catch (err) {
      console.log('ERROR IN MCQ RENDER:', err);
    }
  };

  const hintToggle = showBox => {
    setShowHintBox(showBox);
  };

  if (questionData?.template === 'MCQ') {
    //console.log('Q>>', question.data[0].questionBody);
    let questionBody = '';
    try {
      questionBody = decodeURI(questionData?.questionBody);
      if (questionData?.display !=null && questionData?.display?.hideQuesInSeconds != null && 
            !questionData?.display?.hideQuesInSeconds) {
        setHideQuestion(false);
      }
    } catch (err) {
      questionBody = questionData?.questionBody;
    }
    questionBody = `${questionBody}<br>`;
    questionBody = checkForAudio(questionBody);

    // console.log(`MCQ Q body >>>> ${questionBody}`);
    questionBody = getHtmlTemplate(questionBody, false, isRTL);
    return (
      <View style={styles.container}>
        <ScrollView>
          <View>
            {!hideQuestion && (
              <MyAutoHeightWebView
                {...panResponder.panHandlers}
                style={{
                  ...styles.webViewContainer,
                  ...{height: newHeight + 30},
                }}
                onSizeUpdated={size => {
                  setNewHeight(size.height);
                  console.log('MCQ size', size.height);
                }}
                source={{html: questionBody}}
                zoomable={false}
                startInLoadingState={true}
                bounces={false}
              />
            )}
          </View>
          <HintBox
            hintList={questionData.hints}
            toggleCallback={hintToggle}
            showBtn={showHint}
            showHints={showHintBox}
            showHide={!showHintBox}
            trials={trials}
          />

          <FlatList
            scrollEnabled={true}
            contentContainerStyle={[{marginBottom: 0}]}
            data={questionData.response.mcqPattern.choices}
            renderItem={data => {
              return renderMcqOptionItem(data);
            }}
            horizontal={false}
            keyExtractor={data => {
              return data.value;
            }}
            extraData={isFetching}
            showsHorizontalScrollIndicator={false}
          />
        </ScrollView>
      </View>
    );
  }
});

MCQ.propTypes = {};

MCQ.defaultProps = {};
export default MCQ;
