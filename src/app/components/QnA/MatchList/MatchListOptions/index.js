import React from 'react';
import {Text, View} from 'react-native';
import {MatchQuestion, MatchAnswer} from '@components';
import styles from './indexCss';
import PropTypes from 'prop-types';

const MatchListOptions = props => {
  const {response} = props;
  console.log(`Rendering matchlist options in hoc/question`);
  const options =
    response?.response?.choices &&
    response?.response?.choices.map((item, index) => {
      item.index = index;
      item.isImage = item.value.indexOf('img') > 0;
      return item;
    });

  const stems =
    response?.stems &&
    response?.stems.map((item, index) => {
      item.isImage = options[index]?.isImage;
      return item;
    });
  return (
    <View style={styles.matchQuestionContainer}>
      <View style={styles.boxMargin}>
        {stems &&
          stems.map((item, index) => {
            return <MatchQuestion item={item} index={index} />;
          })}
      </View>
      <View>
        {options &&
          options.map((item, index) => {
            return <MatchAnswer item={item} index={index} drag={() => {}} />;
          })}
      </View>
    </View>
  );
};

MatchListOptions.propTypes = {};

MatchListOptions.defaultProps = {};

export default MatchListOptions;
