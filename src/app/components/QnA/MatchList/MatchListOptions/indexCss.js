import {StyleSheet} from 'react-native';
import { getWp} from '@utils/ViewUtils';

export default StyleSheet.create({
  matchQuestionContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    width: '100%',
    flexShrink: 1,
  },
  boxMargin: {
    marginRight: getWp(10),
  },
});
