import React from 'react';
import {View} from 'react-native';
import styles from './indexCss';
import {MyAutoHeightWebView} from '@components';
import PropTypes from 'prop-types';
import {getWp, getHp} from '@utils';
import {COLORS} from '@constants';
import getHtmlTemplate from '@utils/getHtmlTemplate';
import {useStores} from '@mobx/hooks';

const MatchQuestion = props => {
  const {testID, item, index, isExplaination} = props;
  const {uiStore} = useStores();
  const isRTL = uiStore.isRTL;

  let questionString = item.value && getHtmlTemplate(item.value, false, isRTL);

  return (
    <View
      accessible={true}
      testID={testID}
      accessibilityLabel={testID}
      style={[
        styles.container,
        {
          marginTop: item?.isImage && index > 0 ? getHp(55) : getWp(12),
          backgroundColor: isExplaination
            ? COLORS.white
            : COLORS.sortListQuestionBackgroundColor,
        },
      ]}>
      <View style={styles.arrangeLetterContainerChildView}>
        <MyAutoHeightWebView
          testID="MyAutoHeightWebViewMatchQuestion"
          key="mcqItemWebView"
          customScript={''}
          style={styles.webviewContainer}
          onSizeUpdated={() => {}}
          scrollEnabled={false}
          source={{html: questionString}}
          zoomable={false}
        />
      </View>
    </View>
  );
};

MatchQuestion.propTypes = {
  testID: PropTypes.string,
  item: PropTypes.object,
  index: PropTypes.number,
  isExplaination: PropTypes.bool,
};

MatchQuestion.defaultProps = {
  testID: 'MatchQuestion',
  isExplaination: false,
};

export default MatchQuestion;
