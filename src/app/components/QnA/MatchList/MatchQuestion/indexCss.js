import {StyleSheet} from 'react-native';
import {COLORS} from '@constants';
import {getWp, getHp} from '@utils';

export default StyleSheet.create({
  container: {
    width: getWp(170),
    minHeight: getHp(68),
    backgroundColor: COLORS.sortListQuestionBackgroundColor,
    borderRadius: getWp(5),
    alignItems: 'center',
    justifyContent: 'center',
  },

  arrangeLetterContainerChildView: {
    alignItems: 'center',
    justifyContent: 'center',
    width: getWp(140),
  },

  webviewContainer: {
    width: getWp(140),
    alignItems: 'center',
    justifyContent: 'center',
  },
});
