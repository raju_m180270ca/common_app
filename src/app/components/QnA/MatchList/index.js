import React, {useState, Fragment, useImperativeHandle, useEffect} from 'react';
import {View, FlatList} from 'react-native';
import DraggableFlatList from 'react-native-draggable-flatlist';
import styles from './indexCss';
import {QuestionContainer, MatchQuestion, MatchAnswer} from '@components';
import {Base64} from 'js-base64';
import {useStores} from '@mobx/hooks';

const MatchList = React.forwardRef(
  ({questionTree, dragAndDropCallback}, ref) => {
    const store = useStores();
    const [choices, setChoices] = useState([]);
    const [stems, setStems] = useState([]);
    const [hasUserChanged, setUserChanged] = useState(false);
    const [options, setOptions] = useState([]);
    const isRTL = store?.uiStore?.isRTL;

    let qStems;

    useEffect(() => {
      setOptions(() => {
        let userOptions = questionTree?.response?.choices.map((item, index) => {
          item.index = index;
          item.store = store;
          item.isImage = item.value.indexOf('img') > 0;
          return item;
        });

        return userOptions;
      });
      return () => {};
    }, [questionTree]);

    useEffect(() => {
      options && setChoices(options);
      qStems = questionTree?.stems.map((item, index) => {
        item.isImage = options[index]?.isImage;
        item.store = store;
        return item;
      });

      setStems(qStems);
      return () => {};
    }, [questionTree, options]);

    useImperativeHandle(ref, () => ({
      evaluteAnswer() {
        if (hasUserChanged) {
          let userInputData = {};
          questionTree.stems.map((matchStems, index) => {
            let key = matchStems.identifier;
            userInputData = {...userInputData, [key]: [choices[index].index]};
          });

          let isValidResponse = isValidUserResponse(userInputData);
          let payload = {};
          payload.isDynamic = questionTree?.isDynamic;
          payload.contentID = questionTree?.contentID;
          payload.score = isValidResponse
            ? questionTree?.responseValidation?.validResponse?.score
            : 0;
          payload.result = isValidResponse
            ? Base64.encode('pass')
            : Base64.encode('fail');
          payload.userResponse = {};
          payload.userResponse.type = questionTree?.template;
          payload.userResponse.MatchList = userInputData;
          payload.userAttemptData = {
            trials: [
              {
                userResponse: {
                  type: questionTree?.template,
                  MatchList: userInputData,
                },
                result: isValidResponse
                  ? Base64.encode('true')
                  : Base64.encode('false'),
                score: isValidResponse
                  ? questionTree?.responseValidation?.validResponse?.score
                  : 0,
              },
            ],
          };
          let contentInfo = {};
          contentInfo.contentID = questionTree?.contentID;
          contentInfo.contentVersionID = questionTree?._id;
          contentInfo.contentType = questionTree?.contentType;
          contentInfo.questionType = questionTree?.template;
          contentInfo.revisionNum = questionTree?.revisionNo;
          contentInfo.langCode = questionTree?.langCode;
          payload.contentInfo = contentInfo;
          payload.remainingTime = 0;
          payload.nextContentSeqNum = null;

          return payload;
        } else {
          store.uiStore.setLoader(false);
          // Toast.show({
          //   text: please_attempt_question_text,
          //   duration: 5000,
          //   buttonText: okayText,
          // });
          return;
        }

        return null;
      },

      reset() {
        setUserChanged(false);
      },
    }));

    const matchListQuestionHandler = ({item, index, drag, isActive}) => {
      return (
        <MatchAnswer
          item={item}
          index={index}
          drag={drag}
          isExplaination={isActive}
        />
      );
    };

    const matchListQuestion = ({item, index, drag, isActive}) => {
      return <MatchQuestion item={item} key={index} />;
    };

    const isValidUserResponse = userInputData => {
      const {scoringType, validResponse} = questionTree.responseValidation;
      const {identifier} = validResponse;

      let isValidResponse = false;

      if (scoringType === 'exact' && userInputData) {
        for (let i = 0; i < questionTree?.stems?.length; i++) {
          let matchStems = questionTree?.stems[i];
          let key = matchStems?.identifier;
          let decodeValidResponse = Base64.decode(identifier[key]);
          let userResponseIndex = userInputData[key][0];
          let decodeUserIdentifier = Base64.decode(
            options[userResponseIndex]?.identifier,
          );

          isValidResponse =
            decodeValidResponse.indexOf(decodeUserIdentifier) > 0;

          if (isValidResponse === false) {
            return false;
          }
        }
      }

      return isValidResponse;
    };
    return (
      <Fragment>
        <QuestionContainer questionTree={questionTree} />
        <View
          style={
            isRTL
              ? styles.RTLMatchTypeQuestionView
              : styles.matchTypeQuestionView
          }>
          <View>
            {stems && (
              <FlatList
                scrollEnabled={false}
                data={stems}
                keyExtractor={(item, index) => item.identifier}
                renderItem={matchListQuestion}
              />
            )}
          </View>
          <View
            style={
              isRTL
                ? styles.RTLDragOptionConatainer
                : styles.dragOptionConatainer
            }>
            {choices && (
              <DraggableFlatList
                scrollEnabled={false}
                data={choices}
                renderItem={matchListQuestionHandler}
                keyExtractor={(item, index) => item.identifier}
                onDragBegin={() => {
                  console.log('drag begins');
                  dragAndDropCallback(true);
                }}
                onDragEnd={({data}) => {
                  setChoices(data);
                  dragAndDropCallback(false);
                  setUserChanged(true);
                }}
              />
            )}
          </View>
        </View>
      </Fragment>
    );
  },
);

export default MatchList;
