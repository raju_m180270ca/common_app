/* eslint-disable no-sequences */
/* eslint-disable react-hooks/exhaustive-deps */
import React, {useState, useEffect, useImperativeHandle} from 'react';
import {
  View,
  TouchableWithoutFeedback,
  FlatList,
  TouchableOpacity,
} from 'react-native';
import {CheckBox} from 'native-base';
import PropTypes from 'prop-types';
import styles from './style';
import {
  MyAutoHeightWebView,
  QHtmlTemplateForIframe,
  HintBox,
  SourceSansProBoldTextView,
} from '@components';
import {getWp, validateUserResponseArrays} from '@utils';
import {COLORS, ALPHANUMMAP} from '@constants';
import {SoundSvg} from '@images';
import {Base64} from 'js-base64';
import {useStores} from '@mobx/hooks';
import {checkForAudio} from '@utils';
import getHtmlTemplate from '@utils/getHtmlTemplate';

const MultiSelectMCQ = React.forwardRef((props, ref) => {
  const {
    questionData,
    selectedChoice,
    setSelectedChoices,
    dragCallback,
    onSoundBtnClicked,
  } = props;

  const [trials, setTrials] = useState(-1);
  const [showHint, setShowHint] = useState(false);
  const [showHintBox, setShowHintBox] = useState(false);
  const [disableClick, setDisable] = useState(false);
  const store = useStores();
  const isRTL = store?.uiStore?.isRTL;

  useEffect(() => {
    setSelectedChoices([]);
    setDisable(false);
    setTrials(questionData.trials);
    if (questionData.trials === 1 && questionData.hints.length > 0) {
      setShowHint(true);
    }
  }, []);

  useImperativeHandle(ref, () => ({
    evaluteAnswer(userInputData) {
      if (userInputData && userInputData.length > 0) {
        let isValidResponse = isValidUserResponse(userInputData);

        let payload = {};
        payload.isDynamic = questionData?.isDynamic;
        payload.contentID = questionData?.contentID;
        payload.score = isValidResponse
          ? questionData?.responseValidation?.validResponse?.score
          : 0;
        payload.result = isValidResponse
          ? Base64.encode('pass')
          : Base64.encode('fail');
        payload.userResponse = {};
        payload.userResponse.type = questionData?.template;
        payload.userResponse.userAnswer = userInputData;
        payload.userAttemptData = {
          trials: [
            {
              userResponse: {
                mcqPattern: {
                  type: questionData?.template,
                  userAnswer: userInputData,
                },
              },
              result: isValidResponse
                ? Base64.encode('true')
                : Base64.encode('false'),
              score: isValidResponse
                ? questionData?.responseValidation?.validResponse?.score
                : 0,
            },
          ],
        };
        let contentInfo = {};
        contentInfo.contentID = questionData?.contentID;
        contentInfo.contentVersionID = questionData?._id;
        contentInfo.contentType = questionData?.contentType;
        contentInfo.questionType = questionData?.template;
        contentInfo.revisionNum = questionData?.revisionNo;
        contentInfo.langCode = questionData?.langCode;
        payload.contentInfo = contentInfo;
        payload.remainingTime = 0;
        payload.nextContentSeqNum = null;
        setDisable(true);

        return payload;
      }

      return null;
    },

    reset() {
      setTrials(-1);
      setSelectedChoices([]);
      setDisable(false);
      setShowHint(false);
      setShowHintBox(false);
    },
  }));

  const isValidUserResponse = userInputData => {
    let isValidResponse = false;
    if (userInputData) {
      let userResponse = [];
      let options = questionData?.response?.mcqPattern?.choices;
      for (var item of userInputData) {
        userResponse = userResponse.concat(options[item]);
      }

      if (
        questionData?.responseValidation?.hasOwnProperty('validResponse') &&
        questionData?.responseValidation?.validResponse
      ) {
        let responseValidattion =
          questionData?.responseValidation?.validResponse;
        isValidResponse = validateUserResponseArrays(
          responseValidattion?.identifier,
          userResponse,
        );
      } else if (
        questionData?.responseValidation?.hasOwnProperty('altResponses') &&
        questionData?.responseValidation?.altResponses.length > 0
      ) {
        for (var item of questionData?.responseValidation?.altResponses) {
          isValidResponse = validateUserResponseArrays(
            item?.identifier,
            userResponse,
          );
          if (isValidResponse) {
            break;
          }
        }
      }
    }

    return isValidResponse;
  };

  const hintToggle = showBox => {
    setShowHintBox(showBox);
  };

  let questionBody = '';
  if (questionData?.questionBody && questionData?.questionBody.length > 0) {
    try {
      questionBody = decodeURI(questionData.questionBody);
    } catch (err) {
      questionBody = questionData.questionBody;
    }
    questionBody = checkForAudio(questionBody);
    // questionBody = QHtmlTemplateForIframe(
    //   questionBody,
    //   getWp(310),
    //   store,
    //   null,
    //   null,
    //   true,
    // );
    questionBody = getHtmlTemplate(questionBody, false, isRTL);
  }

  const onSelectOption = data => {
    let remTrials = trials - 1;
    var selectedItem = [];
    if (selectedChoice) {
      if (selectedChoice.indexOf(data.index) >= 0) {
        selectedItem = selectedChoice.reduce(
          (p, c) => (c !== data.index && p.push(c), p),
          [],
        );
        setSelectedChoices(selectedItem);
      } else {
        selectedItem = selectedChoice.concat(data.index);
        setSelectedChoices(selectedItem);
        setTrials(remTrials);
      }
    } else {
      let choices = [];
      selectedItem = choices.concat(data.index);
      setSelectedChoices(selectedItem);
      setTrials(remTrials);
    }
    if (questionData.hints && questionData.hints.length > 0) {
      setShowHint(true);
    }
  };

  const renderMCQOptionItem = data => {
    var backgroundColor = COLORS.blue;
    let isImage = data?.item?.value.indexOf('img') > 0;

    let optionVal = '';
    try {
      optionVal = decodeURI(data.item.value);
    } catch (err) {
      optionVal = data.item.value;
    }
    // optionVal = QHtmlTemplateForIframe(
    //   optionVal,
    //   getWp(414),
    //   store,
    //   isImage ? true : false,
    // );
    optionVal = getHtmlTemplate(optionVal, false, isRTL);

    let enableSoundIcon =
      data?.item &&
      data?.item?.hasOwnProperty('optionsVoiceOver') &&
      data?.item?.optionsVoiceOver
        ? true
        : false;

    let MCQItemBGStyle = styles.mcqItemBg;
    if (isImage) {
      MCQItemBGStyle = isRTL
        ? styles.RTLImageMCQOptionContainer
        : styles.imageMCQOptionContainer;
    } else {
      MCQItemBGStyle = isRTL ? styles.RTLMcqItemBg : styles.mcqItemBg;
    }

    return (
      <View style={isRTL ? styles.RTLoptionContainer : styles.optionContainer}>
        {enableSoundIcon && (
          <TouchableOpacity
            style={styles.soundIconContainer}
            onPress={() => {
              onSoundBtnClicked(data?.item.optionsVoiceOver);
            }}>
            <SoundSvg width={getWp(20)} height={getWp(20)} />
          </TouchableOpacity>
        )}
        <TouchableOpacity
          key="mcqItemBg"
          disabled={disableClick}
          style={[
            MCQItemBGStyle,
            {marginLeft: enableSoundIcon ? getWp(16) : getWp(6)},
          ]}
          onPress={() => {
            onSelectOption(data);
          }}>
          <View key="mcsqCheckbox" style={styles.mcsqCheckbox}>
            <CheckBox
              onPress={() => {
                onSelectOption(data);
              }}
              color={COLORS.orangeBg}
              disabled={disableClick}
              checked={
                selectedChoice &&
                selectedChoice.length > 0 &&
                selectedChoice.indexOf(data.index) >= 0
                  ? true
                  : false
              }
            />
          </View>

          <View
            key="mcqTouchContainer"
            style={[
              styles.mcqTouchContainer,
              {backgroundColor: backgroundColor},
            ]}>
            <SourceSansProBoldTextView style={styles.optionIndex}>
              {ALPHANUMMAP[data.index]}
            </SourceSansProBoldTextView>
          </View>

          <View
            style={
              isImage ? styles.optionImageContainer : styles.optionHtmlContainer
            }
            accessible={false}
            pointerEvents={'none'}>
            <MyAutoHeightWebView
              key="mcqItemWebView"
              style={{width: enableSoundIcon ? getWp(140) : getWp(190)}}
              customScript={``}
              customStyle={``}
              onSizeUpdated={() => {}}
              source={{html: optionVal}}
              zoomable={false}
            />
          </View>
        </TouchableOpacity>
      </View>
    );
  };

  return (
    <View style={styles.mainContainer}>
      {questionBody && questionBody.length > 0 ? (
        <TouchableWithoutFeedback
          onLongPress={() => {
            console.log('Disabling the scroll');
            dragCallback(true);
          }}
          onPressOut={() => {
            dragCallback(false);
            console.log('Enabling the scroll');
          }}>
          <MyAutoHeightWebView
            style={styles.questionContainer}
            files={[
              {
                href: 'contentService',
                type: 'text/javascript',
                rel: 'script',
              },
            ]}
            customScript={''}
            customStyle={`

            `}
            onSizeUpdated={size => {
              console.log(size.height);
            }}
            source={{html: questionBody}}
            zoomable={true}
          />
        </TouchableWithoutFeedback>
      ) : null}
      <HintBox
        hintList={questionData.hints}
        toggleCallback={hintToggle}
        showBtn={showHint}
        showHints={showHintBox}
        showHide={!showHintBox}
        trials={trials}
      />
      <FlatList
        scrollEnabled={true}
        data={questionData?.response?.mcqPattern?.choices}
        renderItem={data => renderMCQOptionItem(data)}
        horizontal={false}
        keyExtractor={data => {
          return data.value;
        }}
        showsHorizontalScrollIndicator={false}
      />
    </View>
  );
});

MultiSelectMCQ.propTypes = {
  questionData: PropTypes.object,
  setSelectedChoices: PropTypes.func,
  dragCallback: PropTypes.func,
  onSoundBtnClicked: PropTypes.func,
};

MultiSelectMCQ.defaultProps = {
  setSelectedChoices: () => {},
  dragCallback: () => {},
  onSoundBtnClicked: () => {},
};

export default MultiSelectMCQ;
