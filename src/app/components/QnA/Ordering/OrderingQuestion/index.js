import React from 'react';
import {View} from 'react-native';
import styles from './indexCss';
import {
  MyAutoHeightWebView,
  SoundButton,
  SourceSansProBoldTextView,
} from '@components';

import PropTypes from 'prop-types';
import {TouchableOpacity} from 'react-native-gesture-handler';
import {COLORS} from '@constants';
import getHtmlTemplate from '@utils/getHtmlTemplate';
import {useStores} from '@mobx/hooks';

const OrderingQuestions = props => {
  const {item, index, drag, isExplaination, onSoundBtnClicked} = props;
  const {uiStore} = useStores();
  const isRTL = uiStore.isRTL;

  // let questionText = QHtmlTemplateForIframe(item.value, wp(40), item?.store);
  let questionText = getHtmlTemplate(item.value, false, isRTL);

  return (
    <View style={isRTL ? styles.RTLContainer : styles.container}>
      {item?.voiceover && item?.voiceover !== '' ? (
        <SoundButton
          onPress={() =>
            onSoundBtnClicked(`orderingVO${index}`, item?.voiceover)
          }
          isRTL={isRTL}
        />
      ) : null}
      <TouchableOpacity
        style={[
          isRTL ? styles.RTLoptionContainer : styles.optionContainer,
          {
            backgroundColor: isExplaination
              ? COLORS.white
              : COLORS.sortListQuestionBackgroundColor,
          },
        ]}
        onLongPress={drag}
        disabled={isExplaination}>
        <View style={styles.indexContainer}>
          <SourceSansProBoldTextView style={styles.indexText}>
            {String.fromCharCode(index + 65)}
          </SourceSansProBoldTextView>
        </View>
        <View
          style={styles.arrangeLetterContainerChildView}
          accessible={false}
          pointerEvents={'none'}>
          <MyAutoHeightWebView
            key="mcqItemWebView"
            style={styles.webviewContainer}
            customScript={''}
            onSizeUpdated={() => {}}
            source={{html: questionText}}
            zoomable={false}
          />
        </View>
      </TouchableOpacity>
    </View>
  );
};

OrderingQuestions.propTypes = {
  isExplaination: PropTypes.bool,
};

OrderingQuestions.defaultProps = {
  isExplaination: false,
};

export default OrderingQuestions;
