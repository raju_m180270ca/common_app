import React, { useState, useImperativeHandle } from 'react';
import { View } from 'react-native';
import { QuestionContainer, OrderingQuestion } from '@components';
import style from './indexCss';
import DraggableFlatList from 'react-native-draggable-flatlist';
import { Base64 } from 'js-base64';
import { useStores } from '@mobx/hooks';
import { getHp } from '@utils';

const Ordering = React.forwardRef(
	({ questionTree, dragAndDropCallback, onSoundBtnClicked }, ref) => {
		const store = useStores();

		const options = questionTree?.response?.choices.map((item, index) => {
			item.index = index;
			item.store = store;
			return item;
		});

		//Calculate the height of Draggable flatlist
		//Each item will be of getHp(68)
		let listHeight = getHp(80);
		options && (listHeight = getHp(80 * options.length + 150));

		const [orderingQuestions, setOrderingQuestions] = useState(options);
		const [hasUserChanged, setUserChanged] = useState(false);

		useImperativeHandle(ref, () => ({
			evaluteAnswer() {
				if (hasUserChanged) {
					const userInputData = orderingQuestions.map(item => {
						return item.index;
					});

					let payload = {};
					let isValidResponse = isValidUserResponse(orderingQuestions);
					payload.isDynamic = questionTree?.isDynamic;
					payload.contentID = questionTree?.contentID;
					payload.score = isValidResponse
						? questionTree?.responseValidation?.validResponse?.score
						: 0;
					payload.result = isValidResponse
						? Base64.encode('pass')
						: Base64.encode('fail');
					payload.userResponse = {};
					payload.userResponse.Ordering = {};
					payload.userResponse.Ordering.type = questionTree?.template;
					payload.userResponse.Ordering.userAnswer = userInputData;
					payload.userAttemptData = {
						trials: [
							{
								userResponse: {
									Ordering: {
										type: questionTree?.template,
										userAnswer: userInputData,
									},
								},
								result: isValidResponse
									? Base64.encode('true')
									: Base64.encode('false'),
								score: isValidResponse
									? questionTree?.responseValidation?.validResponse?.score
									: 0,
							},
						],
					};
					let contentInfo = {};
					contentInfo.contentID = questionTree?.contentID;
					contentInfo.contentVersionID = questionTree?._id;
					contentInfo.contentType = questionTree?.contentType;
					contentInfo.questionType = questionTree?.template;
					contentInfo.revisionNum = questionTree?.revisionNo;
					contentInfo.langCode = questionTree?.langCode;
					payload.contentInfo = contentInfo;
					payload.remainingTime = 0;
					payload.nextContentSeqNum = null;

					return payload;
				}

				return null;
			},

			reset() {
				setUserChanged(false);
			},
		}));

		function isValidUserResponse(userSelectedOption) {
			let aIdentifier = [];
			const { scoringType, validResponse } = questionTree.responseValidation;
			const { identifier } = validResponse;
			if (scoringType === 'exact') {
				userSelectedOption.map(singleOrderingQuestion => {
					aIdentifier.push(singleOrderingQuestion.identifier);
				});
			}

			if (JSON.stringify(aIdentifier) === JSON.stringify(identifier)) {
				return true;
			} else {
				return false;
			}
		}

		const orderingQuestionHandler = ({ item, index, drag, isActive }) => {
			return (
				<OrderingQuestion
					item={item}
					index={index}
					drag={drag}
					isExplaination={isActive}
					onSoundBtnClicked={onSoundBtnClicked}
				/>
			);
		};

		return (
			<View>
				{questionTree?.questionBody && questionTree?.questionBody !== '' ? (
					<QuestionContainer questionTree={questionTree} />
				) : null}
				<View style={{ ...style.dragTypeQuestionView, height: listHeight }}>
					<DraggableFlatList
						scrollEnabled={true}
						data={orderingQuestions}
						renderItem={orderingQuestionHandler}
						keyExtractor={(item, index) => item.identifier}
						onDragBegin={() => dragAndDropCallback(true)}
						onDragEnd={({ data }) => {
							dragAndDropCallback(false);
							setUserChanged(true);
							setOrderingQuestions(data);
						}}
					/>
				</View>
			</View>
		);
	}
);

export default Ordering;
