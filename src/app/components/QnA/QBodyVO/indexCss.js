import {StyleSheet} from 'react-native';
import {COLORS} from '@constants';
import {getHp, getWp} from '@utils/ViewUtils';

export default StyleSheet.create({
  RTLQbodyVoiceOverContainer: {
    // marginBottom: getWp(15),
    marginTop: getWp(16),
    marginLeft: getWp(16),
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-around',
    flex: 1,
  },

  QbodyVoiceOverContainer: {
    // marginBottom: getWp(15),
    marginTop: getWp(16),
    marginLeft: getWp(16),
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-start',
    // flex: 1,
  },
  soundIconContainer: {
    width: getWp(46),
    height: getWp(46),
    borderRadius: getWp(15),
    backgroundColor: COLORS.soundButtonBackgroundColor,
    borderWidth: getWp(3),
    borderColor: COLORS.white,
    alignItems: 'center',
    justifyContent: 'center',
    marginRight: getWp(12),
  },
  soundIcon: {
    width: getWp(20),
    height: getWp(20),
  },
  quesVOContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
});
