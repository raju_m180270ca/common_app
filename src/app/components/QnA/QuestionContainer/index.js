import React, {Fragment} from 'react';
import {View} from 'react-native';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';
import {QHtmlTemplateForIframe, MyAutoHeightWebView} from '@components';
import styles from './indexCss';
import {useStores} from '@mobx/hooks';
import {checkForAudio} from '@utils';

const QuestionContainer = ({questionTree}) => {
  const store = useStores();
  let questionBody = '';
  let questionView = '';
  if (questionTree?.questionBody && questionTree?.questionBody.length > 0) {
    questionView = questionTree.questionBody;
    questionView = checkForAudio(questionView);

    questionBody = QHtmlTemplateForIframe(
      questionView,
      wp(100),
      store,
      true,
      true,
      true,
    );
  }

  // console.log(`Q body>>>>${questionBody}`);

  return (
    <Fragment>
      {questionTree.questionBody.length > 0 && (
        <View style={styles.questionBodyContainer}>
          <MyAutoHeightWebView
            key="mcqItemWebView"
            style={{width: wp(100)}}
            files={[
              {
                href: 'contentService',
                type: 'text/javascript',
                rel: 'script',
              },
            ]}
            customScript={''}
            customStyle={{}}
            onSizeUpdated={size => {
              console.log(size.height);
            }}
            source={{html: questionBody}}
            zoomable={false}
          />
        </View>
      )}
    </Fragment>
  );
};

export default React.memo(QuestionContainer);
