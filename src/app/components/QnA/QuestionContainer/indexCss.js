import {StyleSheet} from 'react-native';
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen';

export default StyleSheet.create({
  questionBodyContainer: {
    width: wp(85),
    // marginTop: hp(2),
    marginLeft: wp(4),
  },
});
