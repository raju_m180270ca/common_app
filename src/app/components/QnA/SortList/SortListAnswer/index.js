import React from 'react';
import {View, Animated, TouchableOpacity,Text} from 'react-native';
import styles from './style';
import {MyAutoHeightWebView} from '@components';
import getHtmlTemplate from '@utils/getHtmlTemplate';
import {useStores} from '@mobx/hooks';

const SortListAnswer = ({
  item,
  resetArrangeLettersElement,
  dragAndDropCallback,
  disableClick,
}) => {
  const {uiStore} = useStores();

  const isRTL = uiStore.isRTL;

  return (
    <View
      ref={viewRef => {
        setTimeout(() => {
          if (item?.ref === null) {
            viewRef?.measure((fx, fy, width, height, px, py) => {
              item.ref = true;
              item.measures = {fx, fy, width, height, px, py};
            });
          }
        }, 4000);
      }}
      style={styles.arrangeLetterContainerParentView}>
      {item.items != null || item.isPrefilling ? (
        <Animated.View>
          <TouchableOpacity
            style={styles.elevatedContainer}
            disabled={disableClick}
            onPressIn={() => {
              console.log('Trigger - ');
              if (item.isPrefilling) {
                return false;
              }
              dragAndDropCallback(true);
              resetArrangeLettersElement(item);
            }}
            onPressOut={() => {
              dragAndDropCallback(false);
            }}>

            <View style={{flexDirection:"row"}}> 
              <View style={styles.arrangeLetterContainerChildView}>
                <MyAutoHeightWebView
                  key="mcqItemWebView"
                  style={styles.webviewContainer}
                  scrollEnabled={false}
                  customScript={''}
                  onSizeUpdated={() => {}}
                  source={{
                    html: getHtmlTemplate(
                      item.isPrefilling ? item.stemVal.value : item?.items?.value,
                      false,
                      isRTL,
                    ),
                  }}
                  zoomable={false}
                />
              </View>
              {item.isPrefilling?null:<Text style={{marginTop:-5,marginLeft:-17,height:'60%'}}>x</Text>}
            </View>
          </TouchableOpacity>
        </Animated.View>
      ) : null}
    </View>
  );
};

export default React.memo(SortListAnswer);
