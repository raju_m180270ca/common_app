import React from 'react';
import {View, Animated} from 'react-native';
import styles from './indexCss';
import SortListPan from '@hooks/SortListPan';
import {MyAutoHeightWebView} from '@components';
import {TouchableOpacity} from 'react-native-gesture-handler';
import getHtmlTemplate from '@utils/getHtmlTemplate';

import PropTypes from 'prop-types';
import {useStores} from '@mobx/hooks';

const SortListQuestion = props => {
  const {
    testID,
    item,
    index,
    arrangeTypeQuestionsAnswers,
    callBackForAssignAnswer,
    dragAndDropCallback,
    disableClick,
    updatedZIndex,
    setEnableScroll,
  } = props;
  const {uiStore} = useStores();

  const isRTL = uiStore.isRTL;

  const [animateValue, panResponder] = SortListPan(
    item,
    arrangeTypeQuestionsAnswers,
    callBackForAssignAnswer,
    updatedZIndex,
    setEnableScroll,
  );
  const animatedStyle = {
    transform: animateValue.getTranslateTransform(),
  };

  let questionTree = getHtmlTemplate(item.value, false, isRTL);

  return (
    <View
      accessible={true}
      testID={testID}
      accessibilityLabel={testID}
      style={{
        ...styles.arrangeLetterContainerParentView,
        zIndex: item.zIndex,
      }}>
      {item.isSelected === false ? (
        //<Animated.View style={{...animatedStyle}} {...panResponder.panHandlers}>
          <TouchableOpacity
            accessible={true}
            testID="SortListQuestionTouchableComp"
            accessibilityLabel="SortListQuestionTouchableComp"
            style={styles.elevatedContainer}
            disabled={disableClick}
            onLongPress={() => {
              console.log('pressed In');
              dragAndDropCallback(false);
            }}
            onPressOut={() => {
              dragAndDropCallback(true);
              callBackForAssignAnswer(
                    item,
                    index,//testID,
                    //arrangeTypeQuestionsAnswers,
                    callBackForAssignAnswer,
                    updatedZIndex);
            }}>
            <View
              pointerEvents={'none'}
              style={styles.arrangeLetterContainerChildView}>
              <MyAutoHeightWebView
                testID="MyAutoHeightWebViewSortListQuestion"
                key="mcqItemWebView"
                style={styles.webviewContainer}
                customScript={''}
                onSizeUpdated={() => {}}
                scrollEnabled={false}
                source={{html: questionTree}}
                zoomable={false}
              />
            </View>
          </TouchableOpacity>
        // </Animated.View>
      ) : null}
    </View>
  );
};

SortListQuestion.propTypes = {
  testID: PropTypes.string,
};

SortListQuestion.defaultProps = {
  testID: 'SortListQuestion',
};

export default SortListQuestion;
