import {StyleSheet} from 'react-native';
import {COLORS} from '@constants';
import {getWp, getHp} from '@utils';

export default StyleSheet.create({
  arrangeLetterContainerParentView: {
    backgroundColor: COLORS.sortListQuestionNormalBackgroundColor,
    marginTop: getWp(16),
    width: getWp(73),
    minHeight: getHp(60),
    alignItems: 'center',
    marginHorizontal: getWp(10),
    justifyContent: 'center',
  },

  elevatedContainer: {
    alignItems: 'center',
    justifyContent: 'center',
    width: getWp(73),
    minHeight: getHp(60),
    overflow: 'hidden',
    backgroundColor: COLORS.sortListQuestionBackgroundColor,
    borderWidth: getWp(1),
    borderStyle: 'dashed',
    borderRadius: getWp(5),
    borderColor: COLORS.dotLineColor,
  },

  arrangeLetterContainerChildView: {
    alignItems: 'center',
    justifyContent: 'center',
    width: getWp(73),
  },

  webviewContainer: {
    width: getWp(73),
    alignItems: 'center',
    justifyContent: 'center',
  },
});
