/* eslint-disable react-hooks/exhaustive-deps */
import React, {
  Fragment,
  useState,
  useCallback,
  useImperativeHandle,
  useEffect,
} from 'react';
import {View} from 'react-native';
import styles from './indexCss';
import {QuestionContainer, SortListQuestion} from '@components';
import SortListAnswer from './SortListAnswer';
import {Base64} from 'js-base64';
import {useStores} from '@mobx/hooks';
import {Toast} from 'native-base';
import {useLanguage} from '@hooks';

const SortList = React.forwardRef((props, ref) => {
  const {questionTree, dragAndDropCallback, setEnableScroll} = props;
  const store = useStores();

  const isRTL = store?.uiStore?.isRTL;
  
  const options = questionTree?.response?.choices.map((item, index) => {
    item.index = index;
    item.isImage = item.value.indexOf('img') > 0;
    return item;
  });

  const [sortListQuestion, setSortListQuestion] = useState([
    ...options.map(itr => ({...itr, isSelected: false, zIndex: 0})),
  ]);
  const [sortListAnswer, setSortListAnswer] = useState([
    ...questionTree?.stems.map(itr => ({
      identifier: itr.identifier,
      items: itr.value === '' ? null : itr,
      ref: null,
      isPrefilling: itr.value === '' ? false : true,
      stemVal: itr,
    })),
  ]);
  const [disableClick, setDisable] = useState(false);
  useEffect(() => {
    setSortListQuestion([
      ...options.map(itr => ({...itr, isSelected: false, zIndex: 0})),
    ]);
    setSortListAnswer([
      ...questionTree?.stems.map(itr => ({
        identifier: itr.identifier,
        items: itr.value === '' ? null : itr,
        ref: null,
        isPrefilling: itr.value === '' ? false : true,
        stemVal: itr,
      })),
    ]);
  }, [questionTree]);
  //callback for updating zIndex
  //udpate zIndex
  const updatedZIndex = useCallback(
    item => {
      let updatedSortListQuestionZIndex = sortListQuestion.map(
        singleContainer => {
          if (singleContainer.identifier === item.identifier) {
            return {...singleContainer, zIndex: 10};
          } else {
            return {...singleContainer, zIndex: 0};
          }
        },
      );
      setSortListQuestion(updatedSortListQuestionZIndex);
    },
    [sortListQuestion],
  );
 
  const getTotalFilledAnswerTillNow = () =>{
    var count = 0;
    for(var i in sortListAnswer){
      if(sortListAnswer[i].items !=null) count++;
    }
    return count;
  }

  const callBackForAssignLetterAnswer = useCallback(
    (item, indexTrigger, cb, sortedPoints) => {
      if(getTotalFilledAnswerTillNow() >= sortListAnswer.length) return;
      if(indexTrigger===undefined) indexTrigger = 0;
      if(sortListAnswer.length!=sortListQuestion.length) indexTrigger = 0;
      if (sortListAnswer[indexTrigger].isPrefilling) {
        //cb();
        indexTrigger=indexTrigger+1;
        //return false;
      } 
     
      let updateSortListQuestion = sortListQuestion.map(itr => {
        return itr.identifier === item.identifier
          ? {...itr, isSelected: true}
          : itr;
      });

      let Answer_updated=false;
      let updateSortListAnswer = sortListAnswer.map((itrs, index) => {
        if (itrs.items) return itrs;
        else {
          if(!Answer_updated){
            Answer_updated=true; 
            return  {...itrs, items: item};
          }else return itrs;
        }
      });

      dragAndDropCallback(false);
      setSortListAnswer(updateSortListAnswer);
      setSortListQuestion(updateSortListQuestion);
    },
    [sortListQuestion, sortListAnswer],
  );

  const resetArrangeLettersElement = useCallback(
    item => {
      let updateSortListQuestion = sortListQuestion.map(itr => {
        return itr.identifier === item.items.identifier
          ? {...itr, isSelected: false}
          : itr;
      });

      let updateSortListAnswer = sortListAnswer.map(itrs => {
        if (itrs.identifier === item.identifier) {
          return {...itrs, items: null};
        } else {
          return itrs;
        }
      });

      dragAndDropCallback(false);
      setSortListAnswer(updateSortListAnswer);
      setSortListQuestion(updateSortListQuestion);
    },
    [sortListQuestion, sortListAnswer],
  );

  useImperativeHandle(ref, () => ({
    evaluteAnswer() {
      if (checkAllAnswerSelected()) {
        let userInputData = {};
        questionTree.stems.map((matchStems, index) => {
          if (!matchStems.value) {
            let key = matchStems.identifier;
            userInputData = {
              ...userInputData,
              [key]: [sortListAnswer[index].items?.index],
            };
          }
        });

        let isValidResponse = isValidUserResponse(userInputData);
        let payload = {};
        payload.isDynamic = questionTree?.isDynamic;
        payload.contentID = questionTree?.contentID;
        payload.score = isValidResponse
          ? questionTree?.responseValidation?.validResponse?.score
          : 0;
        payload.result = isValidResponse
          ? Base64.encode('pass')
          : Base64.encode('fail');
        payload.userResponse = {};
        payload.userResponse.type = questionTree?.template;
        payload.userResponse.SortList = userInputData;
        payload.userAttemptData = {
          trials: [
            {
              userResponse: {
                type: questionTree?.template,
                SortList: userInputData,
              },
              result: isValidResponse
                ? Base64.encode('true')
                : Base64.encode('false'),
              score: isValidResponse
                ? questionTree?.responseValidation?.validResponse?.score
                : 0,
            },
          ],
        };
        let contentInfo = {};
        contentInfo.contentID = questionTree?.contentID;
        contentInfo.contentVersionID = questionTree?._id;
        contentInfo.contentType = questionTree?.contentType;
        contentInfo.questionType = questionTree?.template;
        contentInfo.revisionNum = questionTree?.revisionNo;
        contentInfo.langCode = questionTree?.langCode;
        payload.contentInfo = contentInfo;
        payload.remainingTime = 0;
        payload.nextContentSeqNum = null;
        setDisable(true);

        return payload;
      } else {
        store.uiStore.setLoader(false);
        // Toast.show({
        //   text: fillAllTheBoxes,
        //   duration: 5000,
        //   buttonText: okayBtnText,
        // });
        return;
      }
    },

    reset() {
      setDisable(false);
    },
  }));

  const checkAllAnswerSelected = () => {
    let isAllAnswerSelected = false;
    let ansRes = [];
    if (sortListAnswer && sortListAnswer.length > 0) {
      ansRes = sortListAnswer.filter(item => item?.items == null);
      // sortListAnswer.map(item => {
      //   if (!item?.items) {
      //     isAllAnswerSelected = false;
      //     return;
      //   } else {
      //     isAllAnswerSelected = true;
      //   }
      // });
      if (ansRes.length > 0) {
        isAllAnswerSelected = false;
      } else {
        isAllAnswerSelected = true;
      }
    }
    console.log(`isAllAnswerSelected Eval>>>>>>${isAllAnswerSelected}`);
    return isAllAnswerSelected;
  };

  const isValidUserResponse = userInputData => {
    const {scoringType, validResponse} = questionTree.responseValidation;
    const {identifier} = validResponse;
    let isValidResponse = false;

    if (scoringType === 'exact' && userInputData) {
      for (let i = 0; i < questionTree?.stems?.length; i++) {
        if (!questionTree?.stems[i].value) {
          let matchStems = questionTree?.stems[i];
          let key = matchStems.identifier;
          let decodeValidResponse =
            identifier && Base64.decode(identifier[key]);
          let userResponseIndex = userInputData[key][0];
          let decodeUserIdentifier = Base64.decode(
            options[userResponseIndex]?.identifier,
          );
          isValidResponse =
            decodeValidResponse &&
            decodeValidResponse.indexOf(decodeUserIdentifier) > 0;

          if (isValidResponse === false) {
            return false;
          }
        }
      }
    }

    return isValidResponse;
  };

  return (
    <Fragment>
      {questionTree?.questionBody && questionTree?.questionBody !== '' ? (
        <QuestionContainer questionTree={questionTree} />
      ) : null}
      <View
        style={
          isRTL
            ? styles.RTLDragContainQuestionParentView
            : styles.dragContainQuestionParentView
        }>
        {sortListQuestion.map(singleMap => {
          return (
            <SortListQuestion
              key={singleMap.identifier}
              item={singleMap}
              index={singleMap.index}
              arrangeTypeQuestionsAnswers={sortListAnswer}
              callBackForAssignAnswer={callBackForAssignLetterAnswer}
              dragAndDropCallback={dragAndDropCallback}
              disableClick={disableClick}
              updatedZIndex={updatedZIndex}
              setEnableScroll={setEnableScroll}
            />
          );
        })}
      </View>
      <View style={styles.separateView} />
      <View
        style={
          isRTL
            ? styles.RTLSortListAnswerContainer
            : styles.sortListAnswerContainer
        }>
        {sortListAnswer.map(singleArrangeType => {
          return (
            <SortListAnswer
              key={singleArrangeType.identifier}
              resetArrangeLettersElement={resetArrangeLettersElement}
              item={singleArrangeType}
              dragAndDropCallback={dragAndDropCallback}
              disableClick={disableClick}
            />
          );
        })}
      </View>
    </Fragment>
  );
});

export default SortList;
