import React, {useState, useContext} from 'react';
import {View, FlatList, Linking} from 'react-native';
import styles from './indexCss';
import {
  SourceSansProBoldTextView,
  SourceSansProRegTextView,
  CustomButton,
  InfoPopup,
  CustomCheckBox,
  TrustedDeviceCallout,
} from '@components';

import {Card, CardItem, Body} from 'native-base';
import {TouchableOpacity} from 'react-native-gesture-handler';
import {useLanguage, useAuth} from '@hooks';
import {useNavigation} from '@react-navigation/native';
import {useStores} from '@mobx/hooks';
import {AuthContext} from '@contexts/auth-context';
import {MixpanelCategories, MixpanelEvents, MixpanelActions} from '@constants';

const RenewUserList = props => {
  const {hangOnText, cantOpenUrl} = useLanguage();
  const navigation = useNavigation();
  const {loginStore} = useStores();
  const auth = useContext(AuthContext);
  const {isNewFlow} = props;

  let SUBSCRIPTION_PURCHASE = '';

  const [selectedItem, setSelectedItem] = useState(loginStore.renewStudents[0]);
  const [showInactiveModal, setShowInactiveModal] = useState(false);

  const {
    dispatch,
    TRUSTED_DEVICE,
    trustedDevice,
    trusetedDeviceConfirmed,
    TRUSTED_DEVICE_CONFIRMED,
  } = useAuth('text');

  const userItemPressed = item => {
    setSelectedItem(item);
  };

  const onSaveMySession = async () => {
    auth.trackEvent('mixpanel', MixpanelEvents.TRUSTED_POP_UP_YES, {
      Category: MixpanelCategories.TRUSTEDDEVICE,
      Action: MixpanelActions.CLICKED,
      Label: '',
    });
    dispatch({type: TRUSTED_DEVICE_CONFIRMED, value: !trusetedDeviceConfirmed});
    loginStore.setTrusted(true);
  };

  const showTrustedDevicePopUp = () => {
    auth.trackEvent('mixpanel', MixpanelEvents.TRUSTED_CHECKBOX, {
      Category: MixpanelCategories.TRUSTEDDEVICE,
      Action: MixpanelActions.CLICKED,
      Label: '',
    });

    if (trusetedDeviceConfirmed) {
      loginStore.setTrusted(false);
      dispatch({
        type: TRUSTED_DEVICE_CONFIRMED,
        value: !trusetedDeviceConfirmed,
      });
    }
    dispatch({type: TRUSTED_DEVICE, value: !trustedDevice});
  };

  const disableTrustedDevice = () => {
    auth.trackEvent('mixpanel', MixpanelEvents.TRUSTED_POP_UP_NO, {
      Category: MixpanelCategories.TRUSTEDDEVICE,
      Action: MixpanelActions.CLICKED,
      Label: '',
    });

    dispatch({type: TRUSTED_DEVICE, value: !trustedDevice});
  };

  const userListItem = itemData => {
    const {Name, class: classId, username} = itemData.item;
    let isSelected = selectedItem.username == username;
    const {item} = itemData;

    return (
      <TouchableOpacity onPress={() => userItemPressed(itemData.item)}>
        <Card style={{marginBottom: 20}}>
          <CardItem style={isSelected ? {backgroundColor: '#2875df'} : {}}>
            <Body style={styles.userListContainer}>
              <SourceSansProBoldTextView
                style={[styles.userName, isSelected ? {color: 'white'} : {}]}>
                {`${item.firstName} ${item.lastName}`}
              </SourceSansProBoldTextView>
              <SourceSansProRegTextView
                style={[styles.classText, isSelected ? {color: 'white'} : {}]}>
                {`Class ${classId ? classId : ''}`}
              </SourceSansProRegTextView>
            </Body>
          </CardItem>
        </Card>
      </TouchableOpacity>
    );
  };

  const proceedBtn = async () => {
    if (isNewFlow) {
      props.hideRenewModel();
      navigation.navigate('LoginScreen');
    } else {
      SUBSCRIPTION_PURCHASE = `https://www.mindspark.in/subscription?username=${selectedItem.username}&m_source=mobile`;
      const canOpenURL = await Linking.canOpenURL(SUBSCRIPTION_PURCHASE);
      if (canOpenURL) {
        await Linking.openURL(SUBSCRIPTION_PURCHASE);
        // Show Login
        navigation.navigate('LoginScreen', {});
      } else {
        setShowInactiveModal(true);
      }
    }
  };

  return (
    <View style={styles.container}>
      <InfoPopup
        testID="InfoPopupSubscriptionEndedPurchaseSubcription"
        show={showInactiveModal}
        svgText={hangOnText}
        desc={`${cantOpenUrl} ${SUBSCRIPTION_PURCHASE}`}
        onPress={() => setShowInactiveModal(false)}
      />
      <View style={styles.wrapper}>
        <SourceSansProBoldTextView style={styles.heading}>
          {isNewFlow
            ? 'Select a user to proceed with'
            : 'For which student you like to renew Mindspark?'}
        </SourceSansProBoldTextView>
        <View style={styles.tableWrapper}>
          <FlatList
            scrollEnabled={true}
            data={loginStore.renewStudents}
            renderItem={userListItem}
            keyExtractor={(item, index) => `${item.username}_${index}`}
          />
        </View>
        {/* {isNewFlow && <CustomCheckBox
                    label={'Remember me on this device'}
                    isSelected={trustedDevice}
                    setIsSelected={showTrustedDevicePopUp}
                />} */}
        <View style={styles.btnContainer}>
          <CustomButton
            disabled={false}
            testId={'proceedBtn'}
            btnText={'Proceed'}
            onSubmit={proceedBtn}
          />
        </View>
        {trustedDevice && !trusetedDeviceConfirmed && (
          <TrustedDeviceCallout
            onSaveMySession={onSaveMySession}
            disableTrustedDevice={disableTrustedDevice}
          />
        )}
      </View>
    </View>
  );
};

export default RenewUserList;
