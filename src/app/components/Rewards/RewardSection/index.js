import React, {Fragment} from 'react';
import {View, TouchableOpacity} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import {BalooThambiRegTextView} from '@components';
import styles from './indexCss';
import {useLanguage} from '@hooks';

const RewardSection = props => {
  const {
    children,
    title,
    rewardSectionChildContainer,
    contentLength,
    isViewAll,
    onViewAllPress,
  } = props;
  const {viewLess, viewAll} = useLanguage();
  return (
    <Fragment>
      <View style={styles.gradientContainer}>
        <LinearGradient
          style={styles.gradientStyle}
          start={{x: 0, y: 0}}
          end={{x: 1, y: 0}}
          colors={['#FFFFFF', '#FFFFFF00']}
        />
        <View style={styles.gradientContainerStyle}>
          <BalooThambiRegTextView style={styles.gradientTextStyle}>
            {title}
          </BalooThambiRegTextView>
          {contentLength > 3 && (
            <TouchableOpacity onPress={onViewAllPress}>
              <BalooThambiRegTextView style={styles.gradientViewButtonStyle}>
                {isViewAll ? `${viewLess}` : `${viewAll}`}
              </BalooThambiRegTextView>
            </TouchableOpacity>
          )}
        </View>
      </View>
      <View
        style={{
          ...styles.rewardSectionChildContainer,
          ...rewardSectionChildContainer,
        }}>
        {/* {children} */}
        {children}
      </View>
    </Fragment>
  );
};

export default RewardSection;
