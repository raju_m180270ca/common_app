import { StyleSheet } from 'react-native';
import { COLORS, TEXTFONTSIZE } from '@constants';
import { getWp, getHp } from '@utils/ViewUtils';
import { heightPercentageToDP as hp, widthPercentageToDP as wp } from 'react-native-responsive-screen';
export default StyleSheet.create({ 
  gradientStyle: {
    height: getHp(42),
    justifyContent: "center",
    opacity: .17, 
    position:'absolute',
    width: "100%"
  },
  gradientContainerStyle: {
    width: "90%",
    alignSelf: "center",
    flexDirection: "row",
    justifyContent:"space-between",
    alignItems: "center",
    height: getHp(42)
  },
  gradientViewButtonStyle: {
    color: `#F8651F`,
    fontSize: TEXTFONTSIZE.Text13,
    marginRight:20
  },
  gradientContainer: {
    justifyContent: "center",
    alignItems: "center", 
  },
  gradientTextStyle: {
    color: 'white',
    opacity: 1 ,  
    fontSize: TEXTFONTSIZE.Text15 
  },
  rewardSectionChildContainer: {
    marginBottom: getHp(15)
  }
});
