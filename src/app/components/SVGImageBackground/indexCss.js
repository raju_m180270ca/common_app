import {StyleSheet} from 'react-native';
import {COLORS, TEXTFONTSIZE} from '@constants';
import {getWp, getHp} from '@utils';

export default StyleSheet.create({
  container: {
    flex: 1,
  },
  screenBg: {
    width: '100%',
    height: '100%',
  },
});
