import {StyleSheet} from 'react-native';
import {COLORS, TEXTFONTSIZE} from '@constants';
import {getWp, getHp} from '@utils';

export default StyleSheet.create({
  container: {
    width: '100%',
    height: getHp(30),
    position: 'absolute',
    top: getHp(85),
    // backgroundColor: 'red',
  },
  imgStyle: {
    width: '100%',
    height: getHp(30),
  },
});
