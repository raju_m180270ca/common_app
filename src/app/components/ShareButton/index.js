import React from 'react';
import {Share, TouchableOpacity, View} from 'react-native';
import {ShareImg} from '@images';
import styles from './styles';
import {useStores} from '@mobx/hooks';

const onShareClick = async store => {
  const option = {
    subject: '',
    dialogTitle: '',
  };
  const content = {
    title: 'App link',
    message:
      'Accept my invite for EI Mindspark app & get 2 topics in Mindspark for free! Get it from: https://play.google.com/store/apps/details?id=com.mindspark.edicine',
    url: 'https://apps.apple.com/in/app/ei-mindspark-learning-app/id1541664545',
  };
  try {
    const result = await Share.share(content, option);
    if (result.action === Share.sharedAction) {
      if (result.activityType) {
        // shared with activity type of result.activityType
      } else {
        // shared
      }
    } else if (result.action === Share.dismissedAction) {
      // dismissed
    }
  } catch (error) {}
};

const ShareButton = () => {
  const store = useStores();
  return (
    <View style={styles.container}>
      <View>
        <TouchableOpacity onPress={() => onShareClick(store)}>
          <ShareImg
            accessible={true}
            testID="ShareButton"
            accessibilityLabel="ShareButton"
          />
        </TouchableOpacity>
      </View>
    </View>
  );
};

export default ShareButton;
