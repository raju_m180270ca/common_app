import { StyleSheet,Dimensions} from 'react-native';
import {getWp, getHp} from '@utils';

const styles = StyleSheet.create({
      container:{
       position:'absolute',
        width: getWp(Dimensions.get('window').width),
        bottom: getHp(160),
        justifyContent:'flex-end',
        flexDirection:'row'
      }
});
export default styles;