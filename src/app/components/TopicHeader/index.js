import React from 'react';
import {View, Text} from 'react-native';
import {BalooThambiRegTextView, SVGImageBackground} from '@components';
import {Back} from '@images';
import {getWp, getHp} from '@utils';
import styles from './indexCss';

const TopicHeader = props => {
  const {onClick, title, desc} = props;

  return (
    <View>
      <SVGImageBackground
        testID="SVGImageBackgroundHeaderSVGImage"
        SvgImage="bgHeader"
        themeBased
        screenBg>
        <View style={styles.mainContainer}>
          <View style={styles.mainWrapper}>
            <Back onPress={onClick} width={getWp(56)} height={getHp(52)} />
            <View style={styles.textContainer}>
              <View>
                <BalooThambiRegTextView
                  testID="HeaderTitleText"
                  style={styles.titleText}
                  numberOfLines={1}>
                  {title}
                </BalooThambiRegTextView>
                <Text style={styles.descText}>{desc}</Text>
              </View>
            </View>
          </View>
        </View>
      </SVGImageBackground>
    </View>
  );
};

export default TopicHeader;
