import {StyleSheet, Platform} from 'react-native';

import {getWp, getHp} from '@utils';

export default StyleSheet.create({
  mainContainer: {
    height: Platform.OS === 'ios' ? getHp(90) : getHp(104),
  },
  mainWrapper: {alignItems: 'center', flex: 1, flexDirection: 'row'},
  textContainer: {
    justifyContent: 'center',
    flex: 1,
    marginHorizontal: getWp(10),
  },
  titleText: {
    textAlign: 'center',
  },
  descText: {
    fontSize: 14,
    textAlign: 'center',
  },
});
