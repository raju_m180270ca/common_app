/**
 * Topic list item
 * Upper Tag is in attempts or due date
 * isPriority and isRevice are upper left top strip
 */
import React, {useContext} from 'react';
import {Text, View, Image} from 'react-native';
import {TouchableOpacity} from 'react-native-gesture-handler';
import {ButtonBG, SvgCardTagPriority, ReviceTag, Lock} from '@images';
import {
  SVGImageBackground,
  BalooThambiRegTextView,
  SimpleLottie,
} from '@components';
import {SvgUri} from 'react-native-svg';
import {ThemeContext} from '@contexts/theme-context';
import {themeSvg} from '@themeSvgs';
import compStyles from './indexCss';
import PropTypes from 'prop-types';
import {useLanguage} from '@hooks';
import DeviceInfo from 'react-native-device-info';

const ListItem = props => {
  // console.log('is tablet ?',DeviceInfo,DeviceInfo.isTablet())
  const theme = useContext(ThemeContext);
  const {
    title,
    desc,
    leftImage,
    SvgRight,
    percentage,
    rightJson,
    isActive,
    isLocked,
    isActiveLocked,
    showAttempt,
    isPriority,
    isRevice,
    isUpperTag,
    upperTagText,
    type,
    textStyle,
    onClickCallback,
    isDisabled,
  } = props;

  const {reviseLabel, priorityLabel} = useLanguage();

  const Progress0 = themeSvg[theme.name].progress0;
  const Progress30 = themeSvg[theme.name].progress30;
  const Progress60 = themeSvg[theme.name].progress60;

  let cardTag = null;
  let cardTagText = null;
  let cardUpperTag = null;
  let disabledStyle = {};
  let rightSvgText = null;
  let RightImage = null;
  let percentageComplete = parseInt(percentage);

  if (isPriority || isRevice) {
    let SvgCardTag = isPriority ? SvgCardTagPriority : ReviceTag;

    cardTagText = isPriority ? priorityLabel : reviseLabel;
    cardTag = (
      <View style={compStyles.cardTag}>
        <SVGImageBackground
          SvgImage={SvgCardTag}
          style={compStyles.cardTagSvgStyle}>
          <BalooThambiRegTextView style={compStyles.cardTagText}>
            {cardTagText}
          </BalooThambiRegTextView>
        </SVGImageBackground>
      </View>
    );
  }

  if (isUpperTag) {
    cardUpperTag = (
      <View style={compStyles.cardUpperTag}>
        <BalooThambiRegTextView style={compStyles.cardUpperTagText}>
          {upperTagText}
        </BalooThambiRegTextView>
      </View>
    );
  }

  if (percentage) {
    let percentStr = `${percentage}%`;
    rightSvgText = (
      <BalooThambiRegTextView style={compStyles.rightSvgText}>
        {percentStr}
      </BalooThambiRegTextView>
    );
  }

  if (!isActive) {
    disabledStyle = {...compStyles.disabledStyle};
  }

  switch (true) {
    case percentageComplete == 0:
      RightImage = (
        <Progress0
          width={compStyles.rightSvgStyle.width}
          height={compStyles.rightSvgStyle.height}
        />
      );
      break;
    case percentageComplete > 0 && percentageComplete <= 30:
      RightImage = (
        <Progress30
          width={compStyles.rightSvgStyle.width}
          height={compStyles.rightSvgStyle.height}
        />
      );
      break;
    case percentageComplete > 30 && percentageComplete < 90:
      RightImage = (
        <Progress60
          width={compStyles.rightSvgStyle.width}
          height={compStyles.rightSvgStyle.height}
        />
      );
      break;
    case percentageComplete > 90:
      RightImage = (
        <SimpleLottie
          jsonFileName="progress100"
          theme={theme.name}
          styles={compStyles.rightLottieStyle}
        />
      );
      break;
    default:
      break;
  }
  if (isLocked) {
    RightImage = <Lock />;
    rightSvgText = null;
  }

  return (
    <TouchableOpacity style={compStyles.topicItem} onPress={onClickCallback}>
      <SVGImageBackground SvgImage={ButtonBG} style={compStyles.svgBgContainer}>
        <View style={compStyles.svgbg}>
          <View key="btnContainer" style={compStyles.btnContainer}>
            <View key="btnLeftContainer" style={compStyles.btnLeftContainer}>
              <View style={compStyles.imageHolder}>
                {/* <Image
                  source={{
                    uri: leftImage,
                  }}
                  style={{maxWidth: '100%', maxHeight: '100%'}}
                /> */}
                {leftImage.includes('.svg') && (
                  <SvgUri width="100%" height="100%" uri={leftImage} />
                )}
              </View>
            </View>
            <View key="btnMidContainer" style={compStyles.btnMidContainer}>
              <BalooThambiRegTextView
                style={{...compStyles.btnMidUpperText, ...disabledStyle}}>
                {title}
              </BalooThambiRegTextView>
              <BalooThambiRegTextView
                style={{...compStyles.btnMidLowerText, ...disabledStyle}}>
                {desc}
              </BalooThambiRegTextView>
            </View>
            <View key="btnRightContainer" style={compStyles.btnRightContainer}>
              <View style={{alignItems: 'center', justifyContent: 'center'}}>
                {RightImage}
                {rightSvgText}
              </View>
            </View>
          </View>
          {cardTag}
          {cardUpperTag}
        </View>
      </SVGImageBackground>
    </TouchableOpacity>
  );
};

ListItem.propTypes = {
  title: PropTypes.string.isRequired,
  desc: PropTypes.string,
  leftImage: PropTypes.string,
  SvgRight: PropTypes.number,
  percentage: PropTypes.number,
  rightJson: PropTypes.string,
  isActive: PropTypes.bool,
  showAttempt: PropTypes.string,
  isPriority: PropTypes.bool,
  isRevice: PropTypes.bool,
  isUpperTag: PropTypes.bool,
  upperTagText: PropTypes.string,
  type: PropTypes.string,
  textStyle: PropTypes.object,
  onClickCallback: PropTypes.func.isRequired,
  isDisabled: PropTypes.bool,
};

ListItem.defaultProps = {
  isDisabled: false,
};

export default React.memo(ListItem);
