/* eslint-disable react-native/no-inline-styles */
import React, { useState, useEffect } from 'react';
import { Text, View, alert } from 'react-native';
import styles from './indexCss';
import PropTypes from 'prop-types';
import { COLORS } from '@constants/COLORS';
import { BalooThambiRegTextView, SimpleLottie } from '@components';
import { FinishFlag } from '@images';

const MapNumber = props => {
  const [starName, setstarName] = useState();
  var {
    testID,
    bgColor,
    textColor,
    text,
    position,
    percentage,
    isActive,
    containerStyle,
    showFlag,
  } = props;
  const isLeft = position == 'left' ? true : false;

  useEffect(() => {
    if (percentage) {
      setstarName(getStarName(percentage));
    }
  }, [percentage]);
  let numContainer = styles.numContainer;
  let numText = styles.numText;
  if (isActive) {
    numContainer = { ...styles.numContainer, ...styles.activeNumContainer };
    textColor = { ...styles.numText, ...styles.activeNumText };
  }
  return (
    <View accessible={true} testID={testID} accessibilityLabel={testID} key="container" style={{ ...styles.container, ...containerStyle }}>
      {isLeft && starName && (
        <View style={styles.leftStarContainer}>
          {isActive && (
            <SimpleLottie
              testID="SimpleLottieMapNumberStarName"
              jsonFileName={starName}
              theme="generic"
              styles={styles.leftStarLottie}
            />
          )}
        </View>
      )}
      <View style={{}}>
        {showFlag && <FinishFlag accessible={true} testID="MapNumberFinishFlag" accessibilityLabel="MapNumberFinishFlag" style={styles.finishFlag} />}
        <View key="bg" style={numContainer}>
          <BalooThambiRegTextView testID="MapNumberText" style={numText}>
            {text}
          </BalooThambiRegTextView>
        </View>
      </View>
      {!isLeft && starName && (
        <View style={styles.rightStarContainer}>
          {isActive && (
            <SimpleLottie
              accessible={true}
              testID="MapNumberStarName"
              testID="MapNumberStarName"
              jsonFileName={starName}
              theme="generic"
              styles={styles.rightStarLottie}
            />
          )}
        </View>
      )}
    </View>
  );
};

const getStarName = percentage => {
  console.log('GET STAR NAME');
  if (percentage > 90) {
    return 'three_stars';
  } else if (percentage > 50) {
    return 'two_stars';
  } else if (percentage > 35) {
    return 'one_star';
  } else {
    return null;
  }
};

MapNumber.propTypes = {
  testID: PropTypes.string
};

MapNumber.defaultProps = {
  testID: 'MapNumber',
  bgColor: COLORS.white,
  textColor: COLORS.mapTextDarkBlue,
  text: 'GO',
  position: 'left',
  percentage: 95,
  isActive: false,
  showFlag: false,
};
export default MapNumber;
