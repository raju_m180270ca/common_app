/* eslint-disable react-native/no-inline-styles */
import React, {useState, useEffect} from 'react';
import {Text, View, Image} from 'react-native';
import styles from './indexCss';
import PropTypes from 'prop-types';
import {COLORS} from '@constants/COLORS';
import {TEXTFONTSIZE} from '@constants';
import {SafeAreaView} from 'react-native-safe-area-context';
import {Back} from '@images';
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen';
import {useNavigation} from '@react-navigation/native';
import {BalooThambiRegTextView} from '@components';
import {SvgUri} from 'react-native-svg';

const TopicMapHeader = props => {
  const {title, description, svgUrl} = props;
  const navigation = useNavigation();
  return (
    <View key="container" style={styles.container}>
      <View style={styles.btnContainer}>
        <Back
          width={styles.backBtn.width}
          onPress={() => {
            navigation.navigate('TopicListingScreen');
          }}
        />
      </View>
      <View style={styles.subjectContainer}>
        {svgUrl.includes('.svg') && (
          <SvgUri
            width={styles.topicIcon.width}
            height={styles.topicIcon.height}
            uri={svgUrl}
          />
        )}
        <View style={styles.textContainer}>
          <BalooThambiRegTextView style={styles.title}>
            {title}
          </BalooThambiRegTextView>
          <BalooThambiRegTextView style={styles.description}>
            {description}
          </BalooThambiRegTextView>
        </View>
      </View>
      {/* <View style={{width: wp('20')}} /> */}
    </View>
  );
};

TopicMapHeader.propTypes = {
  title: PropTypes.string,
  description: PropTypes.string,
  svgUrl: PropTypes.string,
};

TopicMapHeader.defaultProps = {
  title: 'Fractions',
  description: '4 Units',
  svgUrl:
    'https://ei-asset.s3-ap-southeast-1.amazonaws.com/mindspark3.0/masterTopicIcons/fractions.svg',
};
export default TopicMapHeader;
