import {StyleSheet} from 'react-native';
import {COLORS} from '@constants/COLORS';
import {TEXTFONTSIZE} from '@constants';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {getWp, getHp} from '@utils';

export default StyleSheet.create({
  container: {
    flexDirection: 'row',
    justifyContent: 'center',
    paddingHorizontal: getWp(18),
    height: getHp(144),
    // backgroundColor: 'red',
    paddingBottom: getHp(8),
  },
  btnContainer: {
    justifyContent: 'flex-end',
    paddingRight: getWp(10),
    position: 'absolute',
    bottom: getHp(30),
    left: getWp(18),
  },
  backBtn: {
    width: getWp(56),
    height: getHp(52),
  },
  subjectContainer: {
    alignItems: 'center',
    marginTop: getHp(9),
    justifyContent: 'center',
    flex: 1,
  },
  textContainer: {
    marginStart: getWp(60),
    marginEnd: getWp(60),
    alignItems: 'center',
  },
  description: {
    color: COLORS.infoMessageGray,
    fontSize: TEXTFONTSIZE.Text12,
    marginTop: -getHp(9),
    textAlign: 'center',
  },
  title: {
    fontSize: TEXTFONTSIZE.Text18,
    textAlign: 'center',
    lineHeight: getHp(20),
    paddingTop: getHp(6),
  },
  topicIcon: {
    width: getWp(40),
    height: getHp(40),
  },
});
