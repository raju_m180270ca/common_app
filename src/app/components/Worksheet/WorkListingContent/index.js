/* eslint-disable react-native/no-inline-styles */
import React, { useContext } from 'react';
import { View, SectionList,SafeAreaView } from 'react-native';
// import { SafeAreaView } from 'react-navigation';
import {
  BalooThambiRegTextView,
  SearchInput,
  WorksheetListItem,
  RoundedButton
} from '@components';
import moment from 'moment';
import { getWp } from '@utils'
import { useNavigation } from '@react-navigation/native';
import { useStores } from '@mobx/hooks';
import { runInAction } from 'mobx';
import styles from './indexCss';
import PropTypes from 'prop-types';
import { ApiEndPoint } from '@constants';
import { API } from '@api';
import { Toast } from 'native-base';
import {WorkSheetSearchEmpty5,WorksheetEmty2} from '@images';
import { useLanguage } from '@hooks';
import { AuthContext } from '@contexts/auth-context';
import { MixpanelCategories, MixpanelEvents, MixpanelActions } from '@constants';

const WorkListingContent = props => {
  const {
    testID,
    sectionList,
    onSearch,
    searchQuery,
    permissions,
    showEmptyMessage,
    showSearchQueryEmptyMessage,
    loading
  } = props;
  const navigation = useNavigation();
  const store = useStores();
  const { sessionTimedOut, submittedOnText, questionText, dueOnText, worksheetSearchEmpty, worksheetEmptyState, goHomeBtnText, searchWorksheetText } = useLanguage();
  const auth = useContext(AuthContext);

  const callOpenTopic = async worksheet => {
    if (worksheet.actions.actionText == "start") {
      auth.trackEvent('mixpanel', MixpanelEvents.WORKSHEET_OPEN, { "Category": MixpanelCategories.WORKSHEET, "Action": MixpanelActions.CLICKED, "Label": '' });
    } else {
      auth.trackEvent('mixpanel', MixpanelEvents.WORKSHEET_CONTINUE, { "Category": MixpanelCategories.WORKSHEET, "Action": MixpanelActions.CLICKED, "Label": '' });
    }

    let req = {
      body: {
        worksheetID: worksheet.contentID,
        mode: 'test',
      },
      store: store,
    };
    try {
      let res = await API(ApiEndPoint.OPEN_WORKSHEET, req);
      console.log('\n\nOPEN TOPIC RESPONSE:', JSON.stringify(res.data));
      if (
        res.data.resultCode === 'C004' &&
        res.data.redirectionCode === 'ContentPage'
      ) {
        runInAction(() => {
          store.qnaStore.worksheetID = worksheet.contentID;
          store.qnaStore.setWorksheetInfo(worksheet);
        });
        navigation.navigate('WorksheetQnAScreen', { worksheet: worksheet });
      } else if (
        res.data.resultCode == 'C004' &&
        res.data.resultMessage == 'redirect' &&
        res.data.redirectionData.sessionTimeExceededFlag == true
      ) {
        store.uiStore.apiErrorInit({
          code: '200',
          message: sessionTimedOut,
        });
      } else if (
        res.data.resultCode === 'C004' &&
        res.data.resultMessage === 'redirect'
      ) {
        store.uiStore.apiErrorInit({
          code: '200',
          message: res.data.error[0],
        });
      } else if (res.data.resultCode === 'C900') {
        Toast.show({
          text: res.data.resultMessage,
          duration: 2000,
        });
      }
    } catch (error) { }
  };

  const renderTopicItem = data => {
    if (data.item.isEmpty) {
      return (
        <BalooThambiRegTextView testID="WorkListingContentItemTitle" style={styles.subHeading}>
          {data.item.title}
        </BalooThambiRegTextView>
      );
    }
    let desc = null;
    let upperTagText = null;
    let questionAttempted = data.item.questionAttempted;
    let totalQuestions = data.item.totalQuestions;
    let questionString = totalQuestions > 1 ? 's' : '';
    if (questionAttempted === 0) {
      if (
        data.item.actions.actionText === 'expired' ||
        data.item.actions.actionText === 'in-progress'
      ) {
        desc =
          data.item &&
          `${questionAttempted} out of ${totalQuestions} question${questionString} completed`;
      } else {
        desc = data.item && ` ${totalQuestions} ${questionText}${questionString}`;
      }
    } else if (
      data.item.actions.actionText === 'complete' &&
      data.item.submittedOn &&
      permissions.submittedOn
    ) {
      desc = `${submittedOnText} ${moment(data.item.submittedOn).format('DD MMM, hh:mm A')}`;
    } else {
      desc =
        data.item &&
        `${data.item.questionAttempted} out of ${totalQuestions} completed`;
    }

    if (data.item.actions.actionText !== 'expired' && data.item.endDateTime && data.item.endDateTime != ' ') {
      upperTagText = `${dueOnText} ${moment(data.item.endDateTime).format(
        'DD MMM, hh:mm A',
      )}`;
    } else if (data.item.submittedOn) {
      upperTagText = `${submittedOnText} ${moment(data.item.submittedOn).format(
        'DD MMM YYYY',
      )}`;
    }

    const onScrollAction = () => {
      auth.trackEvent('mixpanel', MixpanelEvents.WORKSHEET_SEE_REPORT, { "Category": MixpanelCategories.WORKSHEET, "Action": MixpanelActions.CLICKED, "Label": '' });
      if (data.item.actions.actionText === 'expired' || data?.item?.contentStatus === 'deactive') {
        navigation.navigate('WorksheetReportScreen', { worksheet: data.item });
      } else if (data.item.actions.actionText !== 'complete') {
        callOpenTopic(data.item);
      }
    };

    return (
      <View style={styles.itemContainer}>
        <WorksheetListItem
          testID="WorksheetListItemWorkSheetListingContent"
          upperTagText={upperTagText}
          isRecentlyAnnounced={data?.item?.recentlyAnnounced ? data?.item?.recentlyAnnounced : false}
          title={data?.item?.contentName}
          desc={desc}
          actionText={data?.item?.actions?.actionText}
          isExpired={data?.item?.actions?.actionText === 'expired' || data?.item?.contentStatus === 'deactive'}
          isSubmitted={Boolean(data?.item?.submittedOn)}
          isCompleted={data?.item?.actions?.actionText === 'complete'}
          isStart={data?.item?.actions?.actionText === 'start' && data?.item?.contentStatus !== 'deactive'}
          isInProgress={data?.item?.actions?.actionText === 'in-progress'}
          onClickCallback={onScrollAction}
          leftImage={data?.item?.worksheetIcon}
          permissions={permissions}
        />
      </View>
    );
  };

  let emptyListMessage = null;
  if (showSearchQueryEmptyMessage) {
    emptyListMessage = (
      <View style={styles.emptyStateView}>
        <WorkSheetSearchEmpty5 accessible={true} testID="WorkSheetListingContentSearchEmpty5Svg" accessibilityLabel="WorkSheetListingContentSearchEmpty5Svg" width={getWp(150)} style={styles.searchIcon} />
        <BalooThambiRegTextView testID="WorkSheetListingContentSearchEmptTxt" style={styles.emptyData}>
          {worksheetSearchEmpty}
        </BalooThambiRegTextView>
      </View>
    );
  }

  if (showEmptyMessage) {
    emptyListMessage = (
      <View style={styles.emptyStateView1}>
        <WorksheetEmty2 accessible={true} testID="WorkSheetListingContentSearchEmpty2Svg" accessibilityLabel="WorkSheetListingContentSearchEmpty2Svg" width={getWp(150)} style={styles.searchIcon} />
        {loading === false && <BalooThambiRegTextView testID="WorksheetEmptyStateWorksheetEmptyState" style={styles.emptyData}>
          {worksheetEmptyState}
        </BalooThambiRegTextView>}
        {loading === false && <RoundedButton
          testID="RoundedButtonWorkSheetListingContentGoHomeBtnText"
          onPress={() => { navigation.navigate('DashboardScreen') }}
          type="primaryOrange"
          text={goHomeBtnText}
          width={150}
          containerStyle={{ ...styles.goHomeBtnContainer }} />}
      </View>

    );
  }

  return (
    <>
      <SafeAreaView accessible={true} testID={testID} accessibilityLabel={testID} style={styles.container}>
        <View style={styles.subContainer}>
          {permissions.worksheetSearch && !showEmptyMessage && (
            <View key="searchContainer" style={styles.searchStyle}>
              <SearchInput
                testID="SearchInputWorkListingContent"
                onChangeText={val => {
                  onSearch(val);
                }}
                placeholder={searchWorksheetText}
                value={searchQuery}
              />
            </View>
          )}

          <SectionList
            accessible={true}
            testID="WorkListingCotentSectionList"
            accessibilityLabel="WorkListingCotentSectionList"
            sections={sectionList}
            keyExtractor={(item, index) => item + index}
            renderItem={renderTopicItem}
            renderSectionHeader={({ section: { title } }) => (
              <BalooThambiRegTextView testID="WorkListingContentTitleTxt" style={styles.heading}>
                {title}
              </BalooThambiRegTextView>
            )}
            ListEmptyComponent={emptyListMessage}
            showsVerticalScrollIndicator={false}
            stickySectionHeadersEnabled={false}
          />
        </View>
      </SafeAreaView>
    </>
  );
};

WorkListingContent.propTypes = {
  testID: PropTypes.string,
  sectionList: PropTypes.array,
  onSearch: PropTypes.func,
  searchQuery: PropTypes.string,
  permissions: PropTypes.array,
};

WorkListingContent.defaultProps = {
  testID: 'WorkListingContent'
};

export default WorkListingContent;
