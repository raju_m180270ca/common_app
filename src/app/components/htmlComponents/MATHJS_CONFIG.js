// import {useLanguage} from '@hooks';
//https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.5/MathJax.js?config=TeX-MML-AM_CHTML
//https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.1/MathJax.js?config=default
//Framework/Microservice/ContentService/template/libraries/MathJax-2.7.5/MathJax.js?config=TeX-MML-AM_CHTML

// const MATHJAXOFFLINEV3 = `
// <script>
//   MathJax = {
//     tex: {
//       inlineMath: [['$', '$'], ['\\{', '\\}'], ['<equ>', '</equ>']],
//      displayMath:[["$$", "$$"]]
//     },
//   };
// </script>
// <script
// type="text/javascript"
// id="MathJax-script"
// async
// src="${
//   config.JS_BASE_URL
// }/Framework/Microservice/ContentService/template/libraries/mathjax/tex-mml-chtml.js"
// ></script>
// <script>
// if (MathJax && typeof MathJax.typesetPromise !== 'undefined') {
// MathJax.typesetPromise().then(() => {
//   // modify the DOM here
//   MathJax.typesetPromise();
// }).catch((err) => console.log(err.message));
// }
// </script>
// `;

const MATHJAXONLINEV3 = `
<script>
MathJax = {
  tex: {
    inlineMath: [['$', '$'], ['\\{', '\\}'], ['<equ>', '</equ>']],
    displayMath:[["$$", "$$"]]
  },
};
</script>
<script
type="text/javascript"
id="MathJax-script"
async
src="https://cdn.jsdelivr.net/npm/mathjax@3/es5/tex-mml-chtml.js"
></script>
<script>
MathJax.typesetPromise().then(() => {
  // modify the DOM here
  MathJax.typesetPromise();
}).catch((err) => console.log(err.message));
</script>
`;

//MATHJAXONLINEV3 & MATHJAXOFFLINEV3- works

export const MATHJS_CONFIG = MATHJAXONLINEV3;

export function HandleTheMathJax(data) {
  var optionVal = data !== null && typeof data !== 'undefined' ? data : '';
  if (optionVal.search(/<equ>/g) >= 0) {
    optionVal = optionVal
      .replace(new RegExp('<equ>', 'g'), '<span class="math">$')
      .replace(new RegExp('</equ>', 'g'), '$</span>');
    // .replace(new RegExp('{', 'g'), '')
    // .replace(new RegExp('}', 'g'), '');
  }
  // optionVal = optionVal.replace(/class='math'/gi, 'class="math"');
  // const isMath = optionVal.indexOf('class="math"');
  // if (isMath > 0) {
  const rex = /(<([^>]+)>)/gi;

  const fracModify = str => {
    var res = str.match(/\{frac\((.*?)\|(.*?)\)\}/g);
    if (res) {
      var res1;
      for (let val of res) {
        res1 = val.split('frac')[1].replace(/[()}]/g, '');
        res1 = res1.split('|');
        res1[0] = isNaN(res1[0]) ? res1[0] : `\\text{${res1[0]}}`;
        res1[1] = isNaN(res1[1]) ? res1[1] : `\\text{${res1[1]}}`;
        res1 = `<span class="math">$\\frac {${res1[0]}}{${res1[1]}}$</span>`;
        str = str.replace(val, res1);
      }
    }
    return str;
  };

  if (optionVal.search(/frac/g) > 0) {
    optionVal = fracModify(optionVal);
  }

  /**
   * For Urdu, Math expressions will be ltr(html render direction) only
   */
  const isMath = optionVal.indexOf('class="math"');
  if (true && isMath > 0) {
    optionVal = optionVal.replace(
      new RegExp('<span class="math">$', 'g'),
      '<span class="math" dir="ltr">$',
    );
    optionVal = optionVal.replace(new RegExp('$</span>', 'g'), '$</span>');
  }
  return optionVal;
}

export function HandleTheMathJaxEdicine(data) {
  var optionVal = data !== null && typeof data !== 'undefined' ? data : '';
  optionVal = optionVal.replace(/class='math'/gi, 'class="math"');
  const isMath = optionVal.indexOf('class="math"');
  const isFrac = optionVal.indexOf('\\over');
  if (isMath > 0 && isFrac > 0) {
    optionVal = optionVal?.split(/<span class="math">(.*?)<\/span>/g);
    const rex = /(<([^>]+)>)/gi;

    if (optionVal?.length) {
      optionVal?.forEach(function (val, key) {
        if (key % 2 === 1) {
          let numerator = val.replace(rex, '').split('\\over')[0];
          let denominator = val.replace(rex, '').split('\\over')[1];

          if (isNaN(numerator)) {
            numerator = `\\text{${numerator}}`;
          }
          if (isNaN(denominator)) {
            denominator = `\\text{${denominator}}`;
          }

          console.log('Numberator:', numerator);
          console.log('Denominator:', denominator);
          let newVal = `${numerator}\\over${denominator}`;
          console.log('New Val:', newVal);
          //val = '<span class="math">$' + val.replace(rex, '') + '$</span>';
          val = '<span class="math">$' + newVal + '$</span>';
        }

        optionVal[key] = val;
      });

      optionVal = optionVal.join(' ');
    }
  }
  //console.log('After:', optionVal);
  return optionVal;
}
