import {getWp} from '@utils';

export default {
  Text32: getWp(32), // for 32px
  Text35: getWp(35), // for 35px
  Text28: getWp(28), // for 28px
  Text27: getWp(27), // for 27px
  Text26: getWp(26), // for 26px
  Text25: getWp(25), // for 25px
  Text24: getWp(24), // for 24px
  Text23: getWp(23), // for 24px
  Text22: getWp(22), // for 22px
  Text20: getWp(20), // for 20px
  Text19: getWp(19), // for 18px
  Text18: getWp(18), // for 18px
  Text17: getWp(17), // for 17px
  Text16: getWp(16), // for 16px
  Text15: getWp(15), // for 15px
  Text14: getWp(14), // for 14px
  Text13: getWp(13), // for 13px
  Text12: getWp(12), // for 12px
  Text11: getWp(11), // for 11px
  Text10: getWp(10), // for 10px
};
