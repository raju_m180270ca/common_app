const REWARD_TYPES = {
  BADGES: 'badges',
  TITLES: 'titles',
};

const REWARD_TYPES_CATEGORY = {
  EARNED: 'earned',
  ONGOING: 'onGoing',
  UPCOMING: 'upComing',
};

const GET_SORTED_REWARD_TYPES = rewardData => {
  let sortedArrayKeys = [];
  for (let keys in rewardData) {
    sortedArrayKeys.push(keys);
  }
  sortedArrayKeys.sort();
  let updatedRewardData = {};
  for (let ukeys of sortedArrayKeys) {
    updatedRewardData[ukeys] = rewardData[ukeys];
  }
  return updatedRewardData;
};
const REWARD_TYPES_SECTION = {
  [REWARD_TYPES.BADGES]: {
    [REWARD_TYPES_CATEGORY.EARNED]: 'Earned Badges',
    [REWARD_TYPES_CATEGORY.ONGOING]: 'Ongoing Badges',
    [REWARD_TYPES_CATEGORY.UPCOMING]: 'Upcoming Badges',
  },
  [REWARD_TYPES.TITLES]: {
    [REWARD_TYPES_CATEGORY.EARNED]: 'Earned Titles',
    [REWARD_TYPES_CATEGORY.ONGOING]: 'Ongoing Titles',
    [REWARD_TYPES_CATEGORY.UPCOMING]: 'Upcoming Titles',
  },
};
export {
  REWARD_TYPES,
  GET_SORTED_REWARD_TYPES,
  REWARD_TYPES_SECTION,
  REWARD_TYPES_CATEGORY,
};
