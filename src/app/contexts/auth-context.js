import {createContext} from 'react';

export const AuthContext = createContext({
  isAuth: false,
  didTryAutoLogin: false,
  login: () => {},
  logout: () => {},
  setDidTryAutoLogin: () => {},
  trackIdentity: () => {},
  setUserProfile: () => {},
  trackEvent: () => {},
  showToast: () => {},
  hideToast: () => {},
});
