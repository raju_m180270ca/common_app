/* eslint-disable react-hooks/exhaustive-deps */
// External Imports
import React, {useEffect, Fragment} from 'react';
import {FlatList, View, Image, LayoutAnimation} from 'react-native';
import moment from 'moment';
import {observer} from 'mobx-react';
import {useStores} from '@mobx/hooks';
import {SvgUri} from 'react-native-svg';

// Internal Imports
import {BalooThambiRegTextView} from '@components';
import styles from './style';
import {Gold, Silver, Bronze, SelectedTitleSVG, SparkyIcon} from '@images';
import {getWp, getHp} from '@utils/ViewUtils';
import {getFormatedDate, getTimeDiff} from '@utils';
import {useLanguage} from '@hooks';
import PropTypes from 'prop-types';

import DeviceInfo from 'react-native-device-info';

const Leaderboard = props => {
  const {testID, permissions, onEndReached, type} = props;
  const {sparkieEarned, sparkieLeaderBoard} = useLanguage();

  const {appStore, leaderBoardStore} = useStores();
  const {profileDetails} = appStore.userData;

  const [loaded, setLoaded] = React.useState(false);
  const [userDetails, setUserDetails] = React.useState({index: -1, user: null});

  useEffect(() => {
    LayoutAnimation.configureNext({
      duration: 300,
      create: {
        type: LayoutAnimation.Types.easeInEaseOut,
        property: LayoutAnimation.Properties.opacity,
      },
      update: {
        type: LayoutAnimation.Types.easeInEaseOut,
      },
    });

    let userDetails = {
      index: -1,
      user: null,
    };
    leaderBoardStore?.groupSparkieData.map((item, index) => {
      if (item.thisUser && userDetails.user === null) {
        userDetails.index = index;
        userDetails.user = item;
      }
    });
    setUserDetails(userDetails);
    setTimeout(() => {
      setLoaded(() => true);
    }, 200);
  }, []);

  const getRankItem = (index, thisUser) => {
    if (index === 1 || index === 2 || index === 3) {
      let Svg = index === 1 ? Gold : index === 2 ? Silver : Bronze;
      return (
        <View style={styles.rankContainer}>
          <Svg width={'100%'} height={getHp(40)} style={styles.medalStyle} />
          <BalooThambiRegTextView
            style={[
              styles.rankStyle,
              userDetails?.user?.rank === index &&
                thisUser &&
                styles.whiteTextColor,
            ]}>
            {index}
          </BalooThambiRegTextView>
        </View>
      );
    } else {
      return (
        <BalooThambiRegTextView
          style={[
            styles.rankStyle,
            userDetails?.user?.rank === index &&
              thisUser &&
              styles.whiteTextColor,
          ]}>
          {index}
        </BalooThambiRegTextView>
      );
    }
  };

  const renderItem = ({item, index}, show = false) => {
    if (item.thisUser && item.rank >= 6 && !show) {
      return null;
    }

    if (!show && item.count == 0) {
      return null;
    }

    return (
      <View
        style={[
          styles.listItemContainer,
          item.thisUser && styles.stickyListItemContainer,
        ]}>
        {getRankItem(item.rank, item?.thisUser)}
        {permissions.profilePicture && (
          <View>
            {item?.badgeIcon && item?.badgeIcon?.length > 0 && (
              <SvgUri
                accessible={true}
                testID={`LeaderboardSvgUri${item.upid}`}
                accessibilityLabel={`LeaderboardSvgUri${item.upid}`}
                uri={item?.badgeIcon}
                width={getWp(20)}
                height={getWp(20)}
                style={styles.badgeStyle}
              />
            )}
            <Image
              accessible={true}
              testID={`LeaderboardImage${item.upid}`}
              style={styles.avatar}
              source={{uri: item.avatar}}
            />
          </View>
        )}
        {permissions.profileName && (
          <View
            style={{
              height: '100%',
              marginRight: getWp(4),
              marginLeft: getWp(5),
              justifyContent: 'center',
              flex: 1,
            }}>
            <BalooThambiRegTextView
              testID={`Leaderboard${item.upid}`}
              numberOfLines={2}
              ellipsizeMode="tail"
              style={[
                styles.titleStyle,
                item.thisUser && styles.whiteTextColor,
              ]}>
              {item.name}
            </BalooThambiRegTextView>
            {item?.title?.length > 0 && (
              <View
                style={{
                  justifyContent: 'center',
                  alignItems: 'center',
                  marginLeft: -10,
                }}>
                <SelectedTitleSVG
                  accessible={true}
                  testID={`Leaderboard${item.upid}`}
                  accessibilityLabel={`Leaderboard${item.upid}`}
                  width={'90%'}
                  height={
                    String(item.title).length > 12 ? getWp(35) : getWp(30)
                  }
                  preserveAspectRatio="none"
                />
                <BalooThambiRegTextView
                  testID={`Leaderboard${item.upid}`}
                  numberOfLines={1}
                  ellipsizeMode="tail"
                  style={[
                    styles.titleTextStyle,
                    //item?.thisUser && { color: '#46494b' }
                  ]}>
                  {item.title}
                </BalooThambiRegTextView>
              </View>
            )}
          </View>
        )}

        {permissions.mySparkies && (
          <View style={styles.sparkeyContainer}>
            <View style={styles.row}>
              <Image
                accessible={true}
                testID={`LeaderBoardMySparkies${item.upid}`}
                accessibilityLabel={`LeaderBoardMySparkies${item.upid}`}
                source={SparkyIcon}
                style={styles.iconStyle}
              />
              <BalooThambiRegTextView
                testID={`LeaderboardItemCount${item.upid}`}
                style={[
                  styles.pointStyle,
                  item.thisUser && styles.whiteTextColor,
                ]}>
                {item.count < 10 ? `0${item.count}` : item.count}
              </BalooThambiRegTextView>
            </View>
            <BalooThambiRegTextView
              testID={`LeaderBoardSparkiesEarned${item.upid}`}
              style={[
                styles.titleStyle,
                item.thisUser && styles.whiteTextColor,
              ]}>
              {sparkieEarned}
            </BalooThambiRegTextView>
          </View>
        )}
      </View>
    );
  };

  const getDate = () => {
    if (leaderBoardStore?.currentMonth) {
      const array = leaderBoardStore?.currentMonth.split('-');
      return (
        moment()
          .month(array[1] - 1)
          .format('MMMM') +
        ' ' +
        array[0]
      );
    }
  };

  const leaderBoardDateSection = () => {
    let element = null;
    if (type == 'yourSection') {
      element = (
        <BalooThambiRegTextView
          testID="LeaderboardDate"
          style={styles.subTitle}>
          {getDate()}
        </BalooThambiRegTextView>
      );
    } else {
      if (leaderBoardStore?.startDate && leaderBoardStore?.endDate) {
        let updatedTime = getTimeDiff(leaderBoardStore?.lastUpdatedOn);

        element = (
          <Fragment>
            <BalooThambiRegTextView
              testID="LeaderboardFormattedDate"
              style={styles.subTitle}>
              {getFormatedDate(leaderBoardStore?.startDate)} -{' '}
              {getFormatedDate(leaderBoardStore?.endDate)}
            </BalooThambiRegTextView>

            <BalooThambiRegTextView
              testID="LeaderBoardUpdated"
              style={styles.subTitle1}>
              {`Updated ${updatedTime.timeDifference} ${updatedTime.unit} ago`}
            </BalooThambiRegTextView>
          </Fragment>
        );
      }
    }
    return element;
  };

  return (
    <View
      accessible={true}
      testID={testID}
      accessibilityLabel={testID}
      style={styles.container}>
      <View style={styles.titleContainer}>
        <BalooThambiRegTextView
          testID="LeaderboardSparkieLeaderBoard"
          style={styles.title}>
          {sparkieLeaderBoard}
        </BalooThambiRegTextView>
        {leaderBoardDateSection()}
      </View>
      <View style={styles.listContainer}>
        {loaded == true &&
        leaderBoardStore?.groupSparkieData !== null &&
        leaderBoardStore?.groupSparkieData.length > 0 ? (
          <FlatList
            style={styles.flexOne}
            data={leaderBoardStore?.groupSparkieData}
            renderItem={item => renderItem(item)}
            onEndReached={() => {
              onEndReached();
            }}
            onEndReachedThreshold={0.7}
            keyExtractor={item => item?.upid}
            removeClippedSubviews={false}
            stickyHeaderIndices={
              userDetails.index != null ? [userDetails.index] : []
            }
          />
        ) : null}
        {/* {loaded == true &&
          userDetails?.user?.rank > 5 &&
          renderItem({item: userDetails?.user}, true)} */}
        <View style={{flex: DeviceInfo.isTablet() ? 0.38 : 0.18}}>
          {loaded == true &&
            leaderBoardStore?.myRankDetails &&
            (isNaN(leaderBoardStore?.myRankDetails.sparkieRank)
              ? true
              : leaderBoardStore?.myRankDetails.sparkieRank >= 4) &&
            renderItem(
              {
                item: {
                  name: leaderBoardStore?.myRankDetails.name,
                  avatar: leaderBoardStore?.myRankDetails.avatar,
                  count: leaderBoardStore?.myRankDetails?.sparkieCount,
                  rank: leaderBoardStore?.myRankDetails?.sparkieRank,
                  badgeIcon: leaderBoardStore?.myRankDetails?.badgeIcon,
                  title: leaderBoardStore?.myRankDetails?.title,
                  thisUser: true,
                },
              },
              true,
            )}
        </View>
      </View>
    </View>
  );
};

Leaderboard.propTypes = {
  testID: PropTypes.string,
};

Leaderboard.defaultProps = {
  testID: 'Leaderboard',
};

export default observer(Leaderboard);
