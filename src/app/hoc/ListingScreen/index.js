/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable react-native/no-inline-styles */
/**
|--------------------------------------------------
| Dashboard Screen
|--------------------------------------------------
*/
import React, {useState, useContext, useEffect} from 'react';
import {View} from 'react-native';

import {SurpriseGiftImg} from '@images';
import {
  NavMenu,
  NewMessageModal,
  Header,
  DashboardFooter,
  SuccessPopup,
  SVGImageBackground,
  SimpleLottie,
  Shade,
  ShareButton,
} from '@components';
import {useStores} from '@mobx/hooks';
import {observer} from 'mobx-react';
import {ThemeContext} from '@contexts/theme-context';
import PropTypes from 'prop-types';
import {NotificationListModal} from '@hoc';
import styles from './indexCss';
import {useLanguage} from '@hooks';
import {useBackHandler} from '@react-native-community/hooks';
import {copilot} from 'react-native-copilot';

const ListingScreen = props => {
  const theme = useContext(ThemeContext);
  const {uiStore, loginStore} = useStores();
  const [showMessage, setShowMessage] = useState(false);
  const [showSuccessPopup, setShowSuccessPopup] = useState(false);
  const [showNotification, setShowNotification] = useState(false);
  const {msgSuccessText, thisIsSchoolDeviceBtnText} = useLanguage();

  const {
    testID,
    headerBtnType,
    showSideDrawer,
    headerBtnClick,
    topShadeView,
    bottomShadeView,
    shadeTopContainer,
    shadeBottomContainer,
    footerOnPress,
    fromHome,
  } = props;

  useEffect(() => {
    if (uiStore.showHomepageOverlay === true) {
      props.start();
    }
    props.copilotEvents.on('stop', () => {
      uiStore.setShowHomepageOverlay(false);
    });
    return props.copilotEvents.off('stop');
  }, []);

  useBackHandler(() => {
    console.log('Entered BAck Handlerr : ', showSideDrawer);
    headerBtnClick();
  });

  const clickedMenuItemHandler = item => {
    switch (item) {
      case 'message':
        setShowMessage(true);
        break;
      case 'notification':
        setShowNotification(true);
        break;
    }
  };

  const DaseboardScreenVisible = () => {
    if (fromHome) {
      return null;
    } else {
      return (
        <DashboardFooter
          footerOnPress={footerOnPress}
          permissions={
            Object.keys(uiStore.menuDataPermissions).length > 0
              ? uiStore.menuDataPermissions.home
              : {}
          }
        />
      );
    }
  };

  const GetShareButton = () => {
    if (fromHome) {
      return <ShareButton />;
    } else {
      return null;
    }
  };

  return (
    <View
      accessible={true}
      testID={testID}
      accessibilityLabel={testID}
      key="container"
      style={styles.container}>
      <SVGImageBackground
        testID="SVGImageBackgroundListingDashboard"
        SvgImage="bgDashboard"
        themeBased
        screenBg>
        {/* <SVGImageBackground
          testID="SVGImageBackgroundListingLeftNav"
          SvgImage="bgLeftNav"
          themeBased
          screenBg
          containerStyle={styles.leftNavStyle}
        /> */}
        {/* <View style={styles.renewCallout}>
          <SourceSansProRegTextView >
            Your subscription is
            expiring in 3 days!
        </SourceSansProRegTextView>
          <CustomButton
            disabled={false}
            width={96}
            textSize={10}
            style={{width: 90}}
            testId={"renewButton"}
            onPress={() => {

            }}
            btnText={"Renew Subscription"}
          />
        </View> */}
        <Header
          testID="HeaderListing"
          type={headerBtnType}
          onClick={headerBtnClick}
          fromHome={fromHome}
        />
        <View key="innerContainer" style={styles.innerContainer}>
          <View style={styles.subContainer}>
            {/* <View style={styles.sideMenuContainer}>
              <SideMenu
                testID="SideMenuListing"
                permissions={
                  Object.keys(uiStore.menuDataPermissions).length > 0
                    ? uiStore.menuDataPermissions.menu
                    : {}
                }
                clickedMenuItem={clickedMenuItemHandler}
              />
            </View> */}

            {props.children}
            {topShadeView && <Shade containerStyle={shadeTopContainer} />}
            {bottomShadeView && (
              <Shade
                containerStyle={shadeBottomContainer}
                imageName="shadeBottom"
              />
            )}
          </View>
        </View>
        <View style={styles.buddy}>
          {/* <Buddy width={wp('16.5')} height={hp('12')} /> */}
        </View>

        {/* <DashboardFooter
          footerOnPress={footerOnPress}
          permissions={
            Object.keys(uiStore.menuDataPermissions).length > 0
              ? uiStore.menuDataPermissions.home
              : {}
          }
        /> */}
        <GetShareButton />
        <DaseboardScreenVisible />

        <View style={styles.btmLeftAnimContainer}>
          <SimpleLottie theme={theme.name} jsonFileName="dashboardAnimation" />
        </View>
      </SVGImageBackground>
      <NewMessageModal
        isVisible={showMessage}
        pageId={'contentPage'}
        onSuccess={() => {
          setShowMessage(false);
        }}
        onHide={() => {
          setShowSuccessPopup(true);
        }}
        onclose={() => {
          setShowMessage(false);
        }}
      />
      <NotificationListModal
        isVisible={showNotification}
        onPress={() => setShowNotification(false)}
      />
      <SuccessPopup
        isVisible={!showMessage && showSuccessPopup}
        text={msgSuccessText}
        onPress={() => {
          setShowSuccessPopup(false);
        }}
      />
      <NavMenu
        permissions={
          Object.keys(uiStore.menuDataPermissions).length > 0
            ? uiStore.menuDataPermissions.menu
            : {}
        }
        isModalVisible={showSideDrawer}
        toggleModal={headerBtnClick}
        TopSvg={SurpriseGiftImg}
        mascotName="eye_blink"
        buttonText={thisIsSchoolDeviceBtnText}
        clickedMenuItem={clickedMenuItemHandler}
      />
    </View>
  );
};

ListingScreen.propTypes = {
  testID: PropTypes.string,
  headerBtnType: PropTypes.string,
};

ListingScreen.defaultProps = {
  testID: 'ListingScreen',
  headerBtnType: 'menu',
  topShadeView: false,
  bottomShadeView: false,
  footerOnPress: () => {
    console.log('Add footerOnPress()');
  },
};

export default observer(
  copilot({
    verticalOffset: 23,
  })(ListingScreen),
);
