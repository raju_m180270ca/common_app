import React from 'react';
import {
  View,
  ScrollView,
  SafeAreaView,
  KeyboardAvoidingView,
} from 'react-native';
import {observer, PropTypes} from 'mobx-react';
import {
  SVGImageBackground,
  QBodyVO,
  BalooThambiRegTextView,
  InstructorStimulusVO,
  SourceSansProBoldTextView,
  Loader,
  QuestionLabel,
  Timer,
  TimedTestLiveStats,
  WorksheetTimer,
  DashboardFooter,
  WordMeaning,
  Buddy,
} from '@components';
import {APP_VERSION} from '@env';
import {useStores} from '@mobx/hooks';
import {runInAction} from 'mobx';
import styles from './indexCss';

const QnAScreen = props => {
  const {
    testID,
    renderHeader,
    renderPassageView,
    renderBottomButtons,
    renderSearchFields,
    searchQScreen,
    enableScroll,
    scrollViewRef,
    renderQuestionsItem,
    renderExplanation,
    showQuesVO,
    showInsStVO,
    playSound,
    qBodyVoiceOver,
    timerRef,
    setShowTimeTestModal,
    setShowTimesUp,
    getTimeTestPopup,
    renderHomeworkSolutionView,
    renderHomeworkInstructionView,
    isBuddyVisible,
  } = props;

  const {qnaStore, uiStore} = useStores();
  const isRTL = uiStore.isRTL;

  let loader = null;

  if (qnaStore.loader) {
    loader = <Loader />;
  }

  let scrollBehaviour = 'height';
  if (
    qnaStore?.isSkipBtnVisible ||
    qnaStore?.currentQuestion?.template === 'Interactive'
  ) {
    scrollBehaviour = null;
  }
  let instStimulusVal = '';
  if (
    qnaStore?.currentQuestion?.instructorStimulus?.value &&
    qnaStore?.currentQuestion?.instructorStimulus?.value.length > 0
  ) {
    instStimulusVal = qnaStore?.currentQuestion?.instructorStimulus?.value;
  } else {
    instStimulusVal = qnaStore?.currentQuestion?.instructorStimulus?.voiceover;
  }

  return (
    <SafeAreaView
      accessible={true}
      testID={testID}
      accessibilityLabel={testID}
      style={styles.mainContainer}>
      <SVGImageBackground
        testID="SVGImageBackgroundQnAScreen"
        SvgImage="bgCommon"
        themeBased
        screenBg>
        <View style={styles.flexOne}>
          <DashboardFooter
            testID="DashboardFooterQnAScreen"
            detailPage
            containerStyle={styles.footerContainerStyle}
          />
          <KeyboardAvoidingView
            style={styles.keyboard}
            behavior={scrollBehaviour}
            enabled>
            <ScrollView
              key="qnaScrollView"
              // disableScrollViewPanResponder={true}
              horizontal={false}
              persistentScrollbar={false}
              showsVerticalScrollIndicator={true}
              scrollEnabled={enableScroll}
              nestedScrollEnabled={true}
              style={
                qnaStore?.isSkipBtnVisible
                  ? styles.skipQuestionScrollContainer
                  : styles.scrollviewContainer
              }
              keyboardDismissMode="interactive"
              keyboardShouldPersistTaps="never"
              automaticallyAdjustContentInsets={false}
              onResponderMove={() => {
                console.log('outer responding');
              }}
              ref={scrollViewRef}>
              <View>
                {qnaStore?.currentQuestion?._id && (
                  <View style={styles.cardContainer}>
                    <View
                      style={
                        isRTL
                          ? styles.RTLQuestionCardHeaderContainer
                          : styles.questionCardHeaderContainer
                      }>
                      {qnaStore?.isViewQuestionBtnVisible === false && (
                        <View
                          accessible={true}
                          testID="QnAScreenisViewQuestionBtnVisible"
                          accessibilityLabel="QnAScreenisViewQuestionBtnVisible"
                          style={[styles.questionTagContainer]}>
                          <QuestionLabel />
                        </View>
                      )}
                      {/* <QBodyVO
                    testID="QBodyVOQnAScreen"
                    show={showQuesVO}
                    playSound={playSound}
                    type="quesVo"
                  /> */}
                      <View style={styles.soundIconContainer}>
                        <BalooThambiRegTextView
                          testID="QnAScreenContentSeqNum"
                          style={styles.questionNumberText}>
                          {qnaStore.contentData?.contentSeqNum}
                        </BalooThambiRegTextView>
                      </View>

                      {qnaStore.isTimeTest && (
                        <Timer
                          testID="TimerQnAScreen"
                          start={qnaStore.timeTestData.durationClass}
                          ref={timerRef}
                          onTimeUp={() => {
                            runInAction(() => {
                              qnaStore.isTimeUp = true;
                              qnaStore.timeTaken =
                                qnaStore.timeTestData.durationClass * 60;
                            });
                            setShowTimeTestModal(true);
                          }}
                          containerStyle={styles.timerContainer}
                        />
                      )}
                      {qnaStore?.timed && !qnaStore?.isOpenedFirstTime && (
                        <WorksheetTimer
                          testID="WorksheetTimerQnAScreen"
                          start={
                            qnaStore?.contentHeaderInfo?.remainingTime
                              ? qnaStore?.contentHeaderInfo?.remainingTime
                              : qnaStore?.contentHeaderInfo?.totalTime
                          }
                          ref={timerRef}
                          onTimeUp={() => {
                            setShowTimesUp(true);
                          }}
                          containerStyle={styles.timerContainer}
                        />
                      )}
                    </View>
                    <View>
                      <View style={styles.questionContainer}>
                        {qnaStore.isTimeTest && (
                          <View style={styles.timeTestDetailsContainer}>
                            <BalooThambiRegTextView
                              testID="QnAScreenTimeTitle"
                              style={styles.timeTestTitle}>
                              {qnaStore.timeTestData.title}
                            </BalooThambiRegTextView>
                            <TimedTestLiveStats
                              testID="TimedTestStatQnAScreen"
                              attempted={qnaStore.timeTestUserAnswers.length}
                              correct={qnaStore.correctAnswerCount}
                              total={qnaStore.timeTextQuestions.length}
                            />
                          </View>
                        )}
                        {renderHomeworkInstructionView &&
                          typeof renderHomeworkInstructionView !==
                            'undefined' &&
                          renderHomeworkInstructionView()}
                        {instStimulusVal && instStimulusVal !== '' ? (
                          <InstructorStimulusVO
                            testID="InstructorStimulusVOQnAScreenQuestionBtnNotVisible"
                            showInsStVO={showInsStVO}
                            playSound={playSound}
                          />
                        ) : null}
                        {qnaStore?.isViewQuestionBtnVisible === false && (
                          <View style={styles.voiceOverContainer}>
                            <QBodyVO
                              testID="QBodyVOQnAScreen"
                              show={showQuesVO}
                              playSound={playSound}
                              type="quesVo"
                            />
                          </View>
                        )}
                        <QBodyVO
                          testID="QBodyVOQnAScreenqBodyVoiceOver"
                          show={qBodyVoiceOver}
                          playSound={playSound}
                        />
                        {qnaStore?.enableWordMeaning ? (
                          <WordMeaning />
                        ) : qnaStore?.isViewQuestionBtnVisible ? (
                          renderPassageView()
                        ) : (
                          renderQuestionsItem(qnaStore.currentQuestion.template)
                        )}

                        {typeof renderHomeworkSolutionView !== 'undefined' &&
                          renderHomeworkSolutionView()}
                      </View>
                      <View style={styles.explanationContainer}>
                        {qnaStore?.isViewQuestionBtnVisible === false &&
                          renderExplanation &&
                          renderExplanation()}
                      </View>
                    </View>
                  </View>
                )}
              </View>
            </ScrollView>
            {renderBottomButtons()}
          </KeyboardAvoidingView>
          {isBuddyVisible && <Buddy isAnimated={true} />}

          <SourceSansProBoldTextView
            testID="QnAScreenSessionID"
            style={styles.sessionIDText}>
            {APP_VERSION + '|'}
            {qnaStore.sessionInformation?.sessionID} |{' '}
            {qnaStore.currentQuestion?.qcode}
            {/* {getFormatedDateTime(qnaStore.sessionInformation?.sessionStartAt)} */}
          </SourceSansProBoldTextView>

          {/* {renderCSHtmlView()} */}
          {!searchQScreen && renderHeader()}
          {searchQScreen && renderSearchFields()}
          {qnaStore.isTimeTest && getTimeTestPopup()}
          {props.children}
        </View>
        {loader}
      </SVGImageBackground>
    </SafeAreaView>
  );
};

QnAScreen.propTypes = {
  testID: PropTypes.string,
};

QnAScreen.defaultProps = {
  testID: 'QnAScreen',
};

export default observer(QnAScreen);
