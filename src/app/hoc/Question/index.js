import React from 'react';
import {View} from 'react-native';
import styles from './style';
import PropTypes from 'prop-types';
import {
  MyAutoHeightWebView,
  MCQOption,
  SortListQuestion,
  ClassificationQuestion,
  OrderingQuestion,
  MatchListOptions,
} from '@components';
import {replaceInput, replaceInputWithAnswer} from '@utils';
import getHtmlTemplate from '@utils/getHtmlTemplate';
import {useStores} from '@mobx/hooks';

const Question = props => {
  const {
    testID,
    question,
    style,
    showAns,
    userAnswer,
    optionContainerStyle,
    optionTextContainerStyle,
    optionTextStyle,
    webContentStyle,
    qresponse,
  } = props;

  const {uiStore} = useStores();

  const isRTL = uiStore.isRTL;

  let userResponse;
  if (qresponse?.userAttemptData) {
    ({userResponse} = qresponse?.userAttemptData);
  }

  let questionText = '';
  if (showAns && userResponse && Object.keys(userResponse).length > 0) {
    questionText = replaceInputWithAnswer(qresponse);
  } else if (
    question?.questionBody !== null &&
    typeof question?.questionBody !== 'undefined'
  ) {
    questionText = replaceInput(qresponse);
  }

  let questionBody = getHtmlTemplate(questionText, false, isRTL);

  const renderOptions = () => {
    switch (question.template) {
      case 'MCQ':
        return question?.response?.mcqPattern?.choices.map((item, index) => {
          return (
            <MCQOption
              testID="MCQOptionQuestion"
              index={index}
              option={item?.value}
              key={index}
              showAns={showAns}
              userAnswer={userAnswer}
              answer={
                qresponse?.data?.response?.mcqPattern?.correctAnswer ==
                undefined
                  ? qresponse?.data?.responseValidation?.validResponse
                      ?.identifier[0]
                  : qresponse?.data?.response?.mcqPattern?.correctAnswer
              }
              containerStyle={optionContainerStyle}
              optionContainerStyle={optionTextContainerStyle}
              optionTextStyle={optionTextStyle}
              webContentStyle={webContentStyle}
              resultFlag={qresponse?.userAttemptData?.result}
              isRTL={isRTL}
            />
          );
        });
      case 'SortList':
        const sortListQuestion = [
          ...question.response.choices.map(itr => ({
            ...itr,
            isSelected: false,
          })),
        ];
        return sortListQuestion.map(item => {
          return (
            <SortListQuestion
              testID="SortListQuestionQuestion"
              key={item.identifier}
              item={item}
              arrangeTypeQuestionsAnswers={[]}
              callBackForAssignAnswer={() => {}}
              dragType={false}
              dragAndDropCallback={() => {}}
            />
          );
        });
      case 'Classification':
        const classificationContainer = [
          ...question.response.choices.map(itr => ({
            ...itr,
            isSelected: false,
          })),
        ];
        return classificationContainer.map(item => {
          return (
            <ClassificationQuestion
              testID="ClassificationQuestionQuestion"
              dragToContainer1={() => {}}
              dragToContainer2={() => {}}
              item={item}
              dragType={false}
              dragAndDropCallback={() => {}}
            />
          );
        });

      case 'Ordering':
        return question?.response?.choices.map((item, index) => {
          return (
            <OrderingQuestion
              testID="OrderingQuestionQuestionItem"
              item={item}
              index={index}
              dragType={false}
              containerStyle={styles.orderOptionContainer}
              dragAndDropCallback={() => {}}
            />
          );
        });
      case 'MatchList':
        return (
          <MatchListOptions
            testID="MatchListOptionsQuestion"
            response={question}
          />
        );
    }
  };

  // function MatchListOptions({response}) {
  //   const options =
  //     response?.response?.choices &&
  //     response?.response?.choices.map((item, index) => {
  //       item.index = index;
  //       item.isImage = item.value.indexOf('img') > 0;
  //       return item;
  //     });

  //   const stems =
  //     response?.stems &&
  //     response?.stems.map((item, index) => {
  //       item.isImage = options[index]?.isImage;
  //       return item;
  //     });
  //   return (
  //     <View style={styles.matchQuestionContainer}>
  //       <View>
  //         {stems &&
  //           stems.map((item, index) => {
  //             return <MatchQuestion item={item} index={index} />;
  //           })}
  //       </View>
  //       <View>
  //         {options &&
  //           options.map((item, index) => {
  //             return <MatchAnswer item={item} index={index} drag={() => {}} />;
  //           })}
  //       </View>
  //     </View>
  //   );
  // }

  return (
    <View
      accessible={true}
      testID={testID}
      accessibilityLabel={testID}
      style={[styles.parentContainer, style]}>
      {questionBody && questionBody !== '' ? (
        <View style={styles.container}>
          <MyAutoHeightWebView
            testID="MyAutoHeightWebViewQuestion"
            style={styles.webViewStyle}
            files={[]}
            customScript={''}
            customStyle={`
            `}
            onSizeUpdated={() => {}}
            source={{
              html: questionBody,
            }}
            zoomable={true}
          />
        </View>
      ) : null}
      <View
        style={[
          styles.answerContainer,
          question.template == 'SortList' ? {flexDirection: 'row'} : '',
        ]}>
        {renderOptions()}
      </View>
    </View>
  );
};

Question.propTypes = {
  testID: PropTypes.string,
  question: PropTypes.object,
  optionContainerStyle: PropTypes.any,
  optionTextContainerStyle: PropTypes.any,
  optionTextStyle: PropTypes.any,
  webContentStyle: PropTypes.any,
};

Question.defaultProps = {
  testID: 'Question',
  option: [],
  showAns: true,
};
export default Question;
