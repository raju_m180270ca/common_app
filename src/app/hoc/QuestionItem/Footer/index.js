import React, {useState, Fragment} from 'react';
import {View} from 'react-native';
import Collapsible from 'react-native-collapsible';
import {DownGreyArrow, UpGreyArrow} from '@images';
// import base64 from 'react-native-base64';
import styles from './indexCss';
import {
  MyAutoHeightWebView,
  BalooThambiRegTextView,
  ClassificationCorrectAnswer,
  MatchListCorrectAnswer,
  OrderingCorrectAnswer,
  SortListCorrectAnswer,
} from '@components';

import {TouchableOpacity} from 'react-native-gesture-handler';
import {useStores} from '@mobx/hooks';
import {useLanguage} from '@hooks';
import getHtmlTemplate from '@utils/getHtmlTemplate';

const Footer = props => {
  let [isCollapsed, setIsCollapssed] = useState(true);
  const {
    type,
    userAttemptData,
    explanation,
    qdata,
    response,
    howIDid,
    onSoundBtnClicked,
  } = props;
  const {
    viewExplanationLabel,
    yourAnswerLabel,
    correctAnswerText,
    hideExplanationLabel,
  } = useLanguage();
  const store = useStores();
  const isRTL = store?.uiStore?.isRTL;

  const char = 'A';
  let explanationBody = null;

  if (explanation) {
    explanationBody = getHtmlTemplate(explanation, false, isRTL);
  }

  let showYourAnswer = true;
  let showCorrectAnswer = true;

  const viewExplainationText = viewExplanationLabel;

  const yourAnswerText = yourAnswerLabel;

  const rightAnswerText = correctAnswerText;

  // For Challenge type question

  if (howIDid) {
    if (response.contentMode === 'Challenge') {
      // If Skipped
      if (Object.keys(userAttemptData.userResponse).length === 0) {
        if (store.topicTrailsStore.topicDetails.totaltAttempts === 1) {
          showYourAnswer = false;
          showCorrectAnswer = false;
        } else if (store.topicTrailsStore.topicDetails.totaltAttempts > 1) {
          showYourAnswer = false;
          showCorrectAnswer = true;
        }
      } else {
        //If answered
        if (
          userAttemptData.result == 'fail' &&
          store.topicTrailsStore.topicDetails.totaltAttempts === 1
        ) {
          showCorrectAnswer = false;
        } else {
          if (userAttemptData.result == 'pass') {
            showYourAnswer = false;
            showCorrectAnswer = true;
          } else {
            showCorrectAnswer = true;
          }
        }
      }
    } else {
      if (userAttemptData.result == 'pass') {
        showYourAnswer = false;
        showCorrectAnswer = true;
      } else {
        showYourAnswer = true;
        showCorrectAnswer = true;
      }
    }
  }

  const showAnswer = () => {
    if (type === 'MCQ' && userAttemptData?.result) {
      return (
        <View style={styles.generic.answerContainer}>
          {showYourAnswer && (
            <View style={styles.generic.answerSubContainer}>
              <BalooThambiRegTextView style={styles.generic.answerHeading}>
                {yourAnswerText}
              </BalooThambiRegTextView>
              <View
                style={[
                  styles.generic.optionContainer,
                  userAttemptData.result == 'pass'
                    ? null
                    : styles.generic.optionContainerWrong,
                ]}>
                <BalooThambiRegTextView
                  style={[
                    styles.generic.option,
                    userAttemptData.result == 'pass'
                      ? null
                      : styles.generic.optionWrong,
                  ]}>
                  {String.fromCharCode(
                    char.charCodeAt(0) +
                      userAttemptData?.userResponse?.mcqPattern?.userAnswer,
                  )}
                </BalooThambiRegTextView>
              </View>
            </View>
          )}
          {showCorrectAnswer && type != 'Interactive' && (
            <View style={styles.generic.answerSubContainer}>
              <BalooThambiRegTextView>
                {userAttemptData.result == 'pass'
                  ? yourAnswerText
                  : rightAnswerText}
              </BalooThambiRegTextView>
              <View
                style={[
                  styles.generic.optionContainer,
                  styles.generic.optionContainerRight,
                ]}>
                <BalooThambiRegTextView
                  style={[styles.generic.option, styles.generic.optionRight]}>
                  {String.fromCharCode(
                    char.charCodeAt(0) +
                      (qdata?.mcqPattern?.correctAnswer !== undefined
                        ? qdata?.mcqPattern?.correctAnswer
                        : response?.data?.responseValidation?.validResponse
                            ?.identifier[0]),
                  )}
                </BalooThambiRegTextView>
              </View>
            </View>
          )}
        </View>
      );
    } else if (qdata != null) {
      let correct_answers = null;
      switch (type) {
        case 'Blank':
          correct_answers = Object.keys(qdata).map(ans => {
            return qdata[ans]?.correctAnswers[0];
          });
          break;
        case 'Dropdown':
          correct_answers = Object.keys(qdata).map(ans => {
            return qdata[ans].choices[qdata[ans].correctAnswer].value;
          });
          break;
        case 'Blank_Dropdown':
          let template;
          correct_answers = Object.keys(qdata).map(ans => {
            template = qdata[ans].type;
            if (template == 'Blank') {
              return qdata[ans]?.correctAnswers[0];
            } else {
              return qdata[ans].choices[qdata[ans].correctAnswer].value;
            }
          });
          break;
        case 'SortList':
          correct_answers = Object.keys(
            response?.data?.responseValidation?.validResponse?.identifier,
          ).map(key => {
            let userAnswer =
              response?.data?.responseValidation?.validResponse?.identifier[
                key
              ][0];
            return String.fromCharCode(userAnswer + 65);
          });

          break;
        case 'MatchList':
          correct_answers = Object.keys(
            response?.data?.responseValidation?.validResponse?.identifier,
          ).map(key => {
            let userAnswer =
              response?.data?.responseValidation?.validResponse?.identifier[
                key
              ][0];
            return String.fromCharCode(userAnswer + 65);
          });
          break;
        case 'Ordering':
          correct_answers = Object.keys(
            userAttemptData?.userResponse?.Ordering,
          ).map(key => {
            let userAnswer = userAttemptData?.userResponse?.Ordering[key][0];
            return String.fromCharCode(userAnswer + 65);
          });
          break;
        case 'Classification':
          correct_answers = Object.keys(
            userAttemptData?.userResponse?.Classification,
          ).map(key => {
            let userAnswer =
              userAttemptData?.userResponse?.Classification[key][0];
            return String.fromCharCode(userAnswer + 65);
          });
          break;
        case 'Interactive':
          correct_answers = '';
          break;
        default:
          correct_answers = Object.keys(userAttemptData?.userResponse).map(
            ans => {
              return userAttemptData?.userResponse[ans]?.userAnswer;
            },
          );
          break;
      }

      if (correct_answers) {
        return (
          <Fragment>
            {showCorrectAnswer && type != 'Interactive' && (
              <View style={styles.generic.answerContainer}>
                <BalooThambiRegTextView style={styles.generic.answers}>
                  {userAttemptData.result == 'pass'
                    ? yourAnswerText
                    : rightAnswerText}
                  <BalooThambiRegTextView style={styles.generic.answerVal}>
                    {`: ${correct_answers.join(', ')}`}
                  </BalooThambiRegTextView>
                </BalooThambiRegTextView>
              </View>
            )}
          </Fragment>
        );
      }
    }

    return <View />;
  };

  const renderOptionItem = () => {
    console.log(`rendering matchlist explanation footer >>>>${type}`);
    switch (type) {
      case 'Classification':
        return <ClassificationCorrectAnswer response={response} />;
      case 'MatchList':
        return <MatchListCorrectAnswer response={response?.data} />;
      case 'Ordering':
        return (
          <OrderingCorrectAnswer
            response={response}
            onSoundBtnClicked={onSoundBtnClicked}
          />
        );
      case 'SortList':
        return <SortListCorrectAnswer response={response?.data} />;
    }
  };

  return (
    <View key="container" style={styles.generic.container}>
      <View key="innerContainer" style={styles.generic.innerContainer}>
        <View
          key="innerLeftContainer"
          style={{
            ...styles.generic.innerLeftContainer,
          }}>
          <TouchableOpacity
            style={styles.generic.row}
            transparent
            onPress={() => {
              setIsCollapssed(!isCollapsed);
            }}>
            {isCollapsed ? (
              <DownGreyArrow
                width={styles.generic.innerLeftSvgStyle.width}
                height={styles.generic.innerLeftSvgStyle.height}
                style={styles.generic.innerLeftSvg}
              />
            ) : (
              <UpGreyArrow
                width={styles.generic.innerLeftSvgStyle.width}
                height={styles.generic.innerLeftSvgStyle.height}
                style={styles.generic.innerLeftSvg}
              />
            )}
            <BalooThambiRegTextView styles={styles.generic.innerLeftText}>
              {isCollapsed ? viewExplainationText : hideExplanationLabel}
            </BalooThambiRegTextView>
          </TouchableOpacity>
        </View>
        {howIDid && showAnswer()}
      </View>
      <View
        key="collapsibleContainer"
        style={styles.generic.collapsibleContainer}>
        <Collapsible collapsed={isCollapsed}>
          <View style={styles.generic.explanationView}>
            {isCollapsed == false && renderOptionItem()}
            {explanationBody !== null && (
              <MyAutoHeightWebView
                source={{html: explanationBody}}
                style={styles.generic.webViewStyle}
                files={[
                  {
                    href: 'contentService',
                    type: 'text/javascript',
                    rel: 'script',
                  },
                ]}
                customScript={''}
                customStyle={''}
                onSizeUpdated={size => {}}
                zoomable={true}
              />
            )}
          </View>
        </Collapsible>
      </View>
    </View>
  );
};

Footer.propTypes = {};

Footer.defaultProps = {};

export default Footer;
