import React from 'react';
import PropTypes from 'prop-types';
import Modal from 'react-native-modal';
import styles from './indexCss';
import {View, TouchableOpacity} from 'react-native';
import {RoundedButton, BalooThambiRegTextView} from '@components';
import {HeaderBackground, SoundSvg} from '@images';
import {getWp, getAsValue} from '@utils';
import {useStores} from '@mobx/hooks';
import {API} from '@api';
import {observer} from 'mobx-react';
import {Toast} from 'native-base';
import {ApiEndPoint} from '@constants';

const ScreenTestDialog = props => {
  const {isStart, onPress} = props;
  const store = useStores();
  const languageData = store.uiStore.languageData;

  const [showAudio] = React.useState(false);

  console.log(`is start>>>>${isStart}`);

  const startScreenTest = async () => {
    const reqBody = {
      jwt: getAsValue('jwt'),
      store: store,
      body: {
        screeningTestID: store.appStore.screeningData.pedagogyID,
        mode: store.appStore.screeningData.mode,
      },
    };

    try {
      const response = await API(ApiEndPoint.OPEN_SCREENING_TEST, reqBody);
      if (response.data.resultCode === 'C004') {
        if (response.data.redirectionCode === 'ContentPage') {
          // set userData in appStore
          console.log(JSON.stringify(response.data));
          store.uiStore.setScreenTestDialog(false);
          onPress();
        } else if (
          response.data.resultMessage === 'redirect' &&
          response.data.redirectionData.sessionTimeExceededFlag === true
        ) {
          store.uiStore.setScreenTestDialog(false);
          console.log('SESSION TIME EXCEEDED4');
          Toast.show({
            text: 'OPEN Screen API:  Session Time Exceeded',
            duration: 5000,
          });
          store.uiStore.setSessionExceeded(true);
        } else {
          let errorMessage =
            response?.data?.error && response?.data?.error.length > 0
              ? response?.data?.error[0]
              : '';
          if (errorMessage && errorMessage !== '') {
            Toast.show({
              text: errorMessage,
              duration: 3000,
            });
            store?.uiStore.setScreenTestDialog(false);
          }
        }
      } else {
        console.log('OPEN Screen test API ERROR');
      }
    } catch (e) {
      console.log(`Open screening test error>>>${e}`);
    }
  };

  let startText =
    languageData &&
    languageData?.hasOwnProperty('btn') &&
    languageData?.btn?.hasOwnProperty('start')
      ? languageData?.btn?.start
      : 'Start';
  if (!isStart) {
    startText =
      languageData &&
      languageData?.hasOwnProperty('btn') &&
      languageData?.btn?.hasOwnProperty('continue')
        ? languageData?.btn?.continue
        : 'Continue';
  }

  let titleIdentifier = 'screening_test';
  let defaultTitleText = 'Level Check';

  let instructionIdentifier = 'screeningtest_instruction';
  let instructionDefaultText =
    'It looks like you are doing Mindspark for the first time this year, Let s do a small screening test to understand your skill level in this subject. Click on start test from below.';

  if (!store.appStore.isScreenTestActive) {
    titleIdentifier = 'level_test';
    defaultTitleText = 'Quarterly Test';

    instructionIdentifier = 'leveltest_instruction';
    instructionDefaultText =
      "Let's take a small level check to understand how much you already know. Answer your questions carefully.";
  }
  let titleText =
    languageData && languageData?.hasOwnProperty(titleIdentifier)
      ? languageData[titleIdentifier]
      : defaultTitleText;

  let testInstuction =
    languageData && languageData?.hasOwnProperty(instructionIdentifier)
      ? languageData[instructionIdentifier]
      : instructionDefaultText;

  return (
    <View>
      <Modal isVisible={true}>
        <View style={styles.container}>
          <View style={styles.titleContainer}>
            <BalooThambiRegTextView style={styles.titleText}>
              {titleText}
            </BalooThambiRegTextView>
            <HeaderBackground style={styles.svgBackgroundImage} />
          </View>
          <BalooThambiRegTextView style={styles.descriptionText}>
            {testInstuction}
          </BalooThambiRegTextView>
          <RoundedButton
            onPress={() => {
              startScreenTest();
            }}
            type="elevatedOrange"
            text={startText}
          />
          {showAudio && (
            <TouchableOpacity style={styles.soundIconContainer}>
              <SoundSvg
                width={getWp(30)}
                height={getWp(30)}
                onPress={() => {
                  Toast.show({
                    text: 'Work In Progress',
                  });
                }}
              />
            </TouchableOpacity>
          )}
        </View>
      </Modal>
    </View>
  );
};

ScreenTestDialog.propTypes = {
  onPress: PropTypes.func,
};

ScreenTestDialog.defaultProps = {
  onPress: () => {},
  isStart: false,
};

export default observer(ScreenTestDialog);
