import React from 'react';
import PropTypes from 'prop-types';
import Modal from 'react-native-modal';
import styles from './indexCss';
import {View} from 'react-native';
import {RoundedButton} from '@components';
import {HeaderBackground} from '@images';
import {BalooThambiRegTextView} from '@components';
import {useStores} from '@mobx/hooks';
import {setAsValue} from '@utils';
import {observer} from 'mobx-react';
import {useLanguage} from '@hooks';

const SessionTimeOutDialog = props => {
  const store = useStores();
  const {
    sessionTimeOutModalText,
    sessionTimeOutModalMsg,
    yourSessionEndedText,
    yourSessionExpiryMsgText,
    loginAgainBtnText,
  } = useLanguage();

  const onPressHandler = async () => {
    await setAsValue('jwt', '');
    store.loginStore.setIsAuth(false);
    store.appStore.setJwt(null);
    store.loginStore.setSkipOnBoardingScreen(true);
    store.uiStore.reset();
  };

  return (
    <View
      accessible={true}
      testID={props.testID}
      accessibilityLabel={props.testID}>
      <Modal
        isVisible={store.uiStore.sessionExceeded}
        animationIn="fadeIn"
        animationOut="fadeOut">
        <View style={styles.container}>
          <View style={styles.titleContainer}>
            <BalooThambiRegTextView
              testID="SessionTimeOutDialogTitleText"
              style={styles.titleText}>
              {yourSessionEndedText}
            </BalooThambiRegTextView>
            <HeaderBackground
              accessible={true}
              testID="SessionTimeOutDialogHeaderBg"
              accessibilityLabel="SessionTimeOutDialogHeaderBg"
              style={styles.svgBackgroundImage}
            />
          </View>
          <BalooThambiRegTextView
            testID="SessionTimeOutDialogDescriptionTextt"
            style={styles.descriptionText}>
            {yourSessionExpiryMsgText}
          </BalooThambiRegTextView>
          <RoundedButton
            testID="RoundedButtonSessionTimeOutDialogCloseText"
            onPress={onPressHandler}
            type="elevatedOrange"
            text={loginAgainBtnText}
          />
        </View>
      </Modal>
    </View>
  );
};

SessionTimeOutDialog.propTypes = {
  testID: PropTypes.string,
  onPress: PropTypes.func,
};

SessionTimeOutDialog.defaultProps = {
  testID: 'SessionTimeOutDialog',
  onPress: () => {},
};

export default observer(SessionTimeOutDialog);
