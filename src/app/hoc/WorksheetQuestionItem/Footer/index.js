import React, {useState, Fragment} from 'react';
import {Text, View} from 'react-native';
// import base64 from 'react-native-base64';
import styles from './indexCss';
import {BalooThambiRegTextView} from '@components';

import PropTypes from 'prop-types';
import {useStores} from '@mobx/hooks';
import {useLanguage} from '@hooks';

const Footer = props => {
  const {testID, type, userAttemptData, qdata} = props;
  const char = 'A';

  const {yourAnswerLabel, correctAnswerText} = useLanguage();

  let showYourAnswer = true;
  let showCorrectAnswer = true;

  const yourAnswerText = yourAnswerLabel;
  const rightAnswerText = correctAnswerText;

  if (userAttemptData.result == 'pass') {
    showYourAnswer = false;
    showCorrectAnswer = true;
  } else {
    showYourAnswer = userAttemptData?.userResponse && true;
    showCorrectAnswer = true;
  }

  const showAnswer = () => {
    if (type === 'MCQ') {
      return (
        <View style={styles.generic.answerContainer}>
          {showYourAnswer && (
            <View style={styles.generic.answerSubContainer}>
              <BalooThambiRegTextView
                testID="FooterYourAnswerText"
                style={styles.generic.answerHeading}>
                {yourAnswerText}
              </BalooThambiRegTextView>
              <View
                style={[
                  styles.generic.optionContainer,
                  userAttemptData?.result != 'pass' &&
                    styles.generic.optionContainerWrong,
                ]}>
                <BalooThambiRegTextView
                  testID="FooterUserAttemptData"
                  style={[
                    styles.generic.option,
                    userAttemptData?.result != 'pass' &&
                      styles.generic.optionWrong,
                  ]}>
                  {String.fromCharCode(
                    char.charCodeAt(0) +
                      userAttemptData?.userResponse?.mcqPattern?.userAnswer,
                  )}
                </BalooThambiRegTextView>
              </View>
            </View>
          )}
          {showCorrectAnswer && (
            <View style={styles.generic.answerSubContainer}>
              <BalooThambiRegTextView testID="FooterUserAttemptData">
                {userAttemptData?.result == 'pass'
                  ? yourAnswerText
                  : rightAnswerText}
              </BalooThambiRegTextView>
              <View
                style={[
                  styles.generic.optionContainer,
                  styles.generic.optionContainerRight,
                ]}>
                <BalooThambiRegTextView
                  testID="FooterUserCorrectAnswer"
                  style={[styles.generic.option, styles.generic.optionRight]}>
                  {String.fromCharCode(
                    char.charCodeAt(0) + qdata?.mcqPattern?.correctAnswer,
                  )}
                </BalooThambiRegTextView>
              </View>
            </View>
          )}
        </View>
      );
    } else if (qdata != null) {
      let correct_answers = null;
      switch (type) {
        case 'Blank':
          correct_answers = Object.keys(qdata).map(ans => {
            return qdata[ans]?.correctAnswers.join(', ');
          });
          break;
        case 'Dropdown':
          correct_answers = Object.keys(qdata).map(ans => {
            return qdata[ans].choices[qdata[ans].correctAnswer].value;
          });
          break;
        case 'Blank_Dropdown':
          let template;
          correct_answers = Object.keys(qdata).map(ans => {
            template = qdata[ans].type;
            if (template == 'Blank') {
              return qdata[ans]?.correctAnswers.join(', ');
            } else {
              return qdata[ans].choices[qdata[ans].correctAnswer].value;
            }
          });
          break;
        default:
          break;
      }

      if (correct_answers) {
        return (
          <Fragment>
            {showCorrectAnswer && (
              <View style={styles.generic.answerContainer}>
                <BalooThambiRegTextView
                  testID="FooterYourAnswerText"
                  style={styles.generic.answers}>
                  {userAttemptData.result == 'pass'
                    ? yourAnswerText
                    : rightAnswerText}
                  <BalooThambiRegTextView
                    testID="FooterCoorectAns"
                    style={styles.generic.answerVal}>
                    {`: ${correct_answers.join(', ')}`}
                  </BalooThambiRegTextView>
                </BalooThambiRegTextView>
              </View>
            )}
          </Fragment>
        );
      }
    }

    return <View />;
  };

  return (
    <View
      accessible={true}
      testID={testID}
      accessibilityLabel={testID}
      key="container"
      style={styles.generic.container}>
      <View key="innerContainer" style={styles.generic.innerContainer}>
        {showAnswer()}
      </View>
    </View>
  );
};

Footer.propTypes = {
  testID: PropTypes.string,
};

Footer.defaultProps = {
  testID: 'Footer',
};

export default Footer;
