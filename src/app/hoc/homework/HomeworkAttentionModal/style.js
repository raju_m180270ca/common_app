import { StyleSheet } from 'react-native';
import { COLORS, TEXTFONTSIZE } from '@constants';
import { getWp, getHp } from '@utils';

export default StyleSheet.create({
    container: {
        backgroundColor: COLORS.white,
        borderRadius: getWp(60),
        borderWidth: getWp(5),
        borderColor: COLORS.infoContainerBdr,
        alignItems: 'center',
        width: getWp(340),
        height: getHp(460),
        alignSelf: 'center',
        paddingBottom: getHp(40),
    },

    titleContainer: {
        flexDirection: 'row',
        width: getWp(271),
        height: getHp(128),
        marginTop: getWp(25),
        alignItems: 'center',
        justifyContent: 'center',
    },

    svgBackgroundImage: {
        position: 'absolute',
    },

    titleText: {
        fontSize: TEXTFONTSIZE.Text35,
        color: COLORS.statTextColor,
        textAlign: 'center',
        lineHeight: 47,
    },

    messageText: {
        fontSize: TEXTFONTSIZE.Text16,
        color: COLORS.leaderBoardTitleColor,
        marginTop: getHp(40),
        marginStart: getWp(57),
        marginEnd: getWp(57),
        textAlign: 'center',
    },

    buttonContainer: {
        position: 'absolute',
        height: getHp(60),
        left: getWp(20),
        right: getWp(20),
        bottom: getHp(48),
        alignItems: 'center',
    },

    buttonSkipText: {
        color: COLORS.orange,
        fontSize: TEXTFONTSIZE.Text20,
        fontFamily: 'BalooThambi-Regular',
    },

    buttonSeparator: {
        height: getHp(18),
    }
});