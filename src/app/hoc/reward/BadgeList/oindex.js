/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable no-shadow */
/* eslint-disable no-sparse-arrays */
import React, {useEffect} from 'react';
import {View, FlatList} from 'react-native';
import styles from './indexCss';
import {BalooThambiRegTextView} from '@components';
import {COLORS} from '@constants';
import {RewardBadge, RoundedButton} from '@components';
import {getWp, getHp} from '@utils/ViewUtils';
import PropTypes from 'prop-types';
import {TouchableOpacity} from 'react-native-gesture-handler';
import {observer} from 'mobx-react';

const BadgeList = props => {
  const {badges, onItemSelected, appliedBadge} = props;
  const itemWidth = getWp(328) / 4;

  const buttons = [
    {
      id: 'bd7acbea-c1b1-46c2-aed5-3ad53abb28ba',
      name: 'starter',
      backgroundColor: COLORS.rewardBadgeBackgroundColor,
      backgroundColorDark: COLORS.darkButtonBlueColor,
    },
    {
      id: 'bd7acbea-c1b1-46c2-aef5-3ad53abb28ba',
      name: 'champion',
      backgroundColor: COLORS.rewardButtonBackgroundColor,
      backgroundColorDark: COLORS.darkGreenColor,
    },
    {
      id: 'bd7acbea-c1b1-46c2-a4d5-3ad53abb28ba',
      name: 'legend',
      backgroundColor: COLORS.rewardBadgeButtonColor,
      backgroundColorDark: COLORS.darkBlueColor,
    },
  ];

  const [selected, setSelected] = React.useState(
    new Map().set(buttons[0].id, true),
  );
  const [selectedButton, setButton] = React.useState(buttons[0].name);

  const [bagdeSelected, setBadgeSelected] = React.useState(new Map());

  const filteredBadges = badges.filter(
    item => item.badgeType === selectedButton,
  );

  useEffect(() => {
    if (badges.length > 0) {
      let item = badges[0];
      setBadgeSelected(new Map().set(item.name, true));
    }
  }, []);

  function ButtonItem({item, selected}) {
    return (
      <View style={styles.buttonItemContainer}>
        <RoundedButton
          width={getWp(94)}
          height={getHp(40)}
          text={item.name}
          raiseLevel={selected ? 0 : getWp('5')}
          borderRadius={getWp(4)}
          backgroundColor={item.backgroundColor}
          backgroundDarker={item.backgroundColorDark}
          onPress={() => {
            setSelected(new Map().set(item.id, true));
            setButton(item.name);
          }}
        />
        <View style={{width: getWp(16)}} />
      </View>
    );
  }

  function BagdeItem({item, selectedBadge}) {
    return (
      <TouchableOpacity
        onPress={() => {
          setBadgeSelected(new Map().set(item.name, true));
          onItemSelected(item);
        }}
        style={[styles.badgeItemContainer, , {width: itemWidth}]}>
        <RewardBadge
          svgURI={item?.badgeIcon}
          count={item?.count}
          isApplied={appliedBadge != null && appliedBadge?.name === item?.name}
          conatinerStyle={
            selectedBadge
              ? {borderColor: COLORS.btnColor}
              : {borderColor: COLORS.rewardBadgeBackgroundColor}
          }
        />
        <BalooThambiRegTextView style={styles.badgeItemText}>
          {item.name}
        </BalooThambiRegTextView>
      </TouchableOpacity>
    );
  }

  function EmptyComponent({}) {
    return (
      <View style={styles.emptyBadgeContainer}>
        <BalooThambiRegTextView style={styles.emptyBadgeText}>
          No Badges Available
        </BalooThambiRegTextView>
      </View>
    );
  }

  return (
    <View style={styles.container}>
      <View style={styles.subContainer}>
        <BalooThambiRegTextView style={styles.titleText}>
          Badges
        </BalooThambiRegTextView>
        <FlatList
          style={styles.buttonListContainer}
          data={buttons}
          renderItem={({item, index}) => (
            <ButtonItem
              item={item}
              selected={selected.get(item?.id)}
              index={index}
            />
          )}
          horizontal={true}
          keyExtractor={item => item.id}
          extraData={selected}
        />
        <View style={{height: getHp(16)}} />
        <FlatList
          style={styles.badgeListContainer}
          data={filteredBadges}
          ListEmptyComponent={<EmptyComponent />}
          showsVerticalScrollIndicator={false}
          renderItem={({item}) => (
            <BagdeItem
              item={item}
              selectedBadge={bagdeSelected.get(item?.name)}
            />
          )}
          numColumns={4}
          keyExtractor={item => item.name}
          extraData={bagdeSelected}
        />
      </View>
    </View>
  );
};

BadgeList.propTypes = {
  badges: PropTypes.array.isRequired,
  onItemSelected: PropTypes.func,
  appliedBadge: PropTypes.object,
};

BadgeList.defaultProps = {
  badges: [],
  onItemSelected: () => {},
  appliedBadge: null,
};

export default observer(BadgeList);
