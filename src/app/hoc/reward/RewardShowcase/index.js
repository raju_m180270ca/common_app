import React, {Fragment} from 'react';
import {View} from 'react-native';

import {RewardBadge, BalooThambiRegTextView, SimpleLottie} from '@components';
import styles from './indexCss';
import {getWp, getHp} from '@utils/ViewUtils';
import {RoundedButton} from '@components';
import {REWARD_TYPES, REWARD_TYPES_CATEGORY} from '@constants';
import {SvgUri} from 'react-native-svg';
import {CorrectAnswer, SelectedTitleSVG, StarBadge} from '@images';
import {useLanguage} from '@hooks';

const RewardShowcase = props => {
  const {
    testID,
    rewardShowCaseDetails: {type, item},
    onSetRewardsClick,
  } = props;
  const {level, applyBtnText, appliedBtnText} = useLanguage();

  const BadgeShowCase = () => {
    const injectProps = {};
    const conatinerStyle = {};
    if (item.category == 'earned') {
      injectProps.svgURI = item?.badgeIcon;
      console.log('badgeTypeTEst - ', item?.badgeIcon);
      //injectProps.svgURI = SVG_TEMP;
      conatinerStyle.borderColor = `#FFFFFF`;
    } else {
      injectProps.Svg = StarBadge;
      //conatinerStyle.borderColor = `#256F94`;
    }
    return (
      <Fragment>
        {item?.badgeIcon && (
          <RewardBadge
            testID="RewardBadgeRewardShowCase"
            {...injectProps}
            //svgURI={item?.badgeIcon}
            count={item?.count}
            conatinerStyle={
              item.category == 'earned'
                ? styles.rewardBadgeConatinerStyle
                : styles.rewardBadgeConatinerStyle1
            }
            countContainerStyle={styles.rewardBadgeCountContainerStyle}
            badgeWidth={getHp(114)}
            progress={item?.progress}
          />
        )}
        {item.name && (
          <BalooThambiRegTextView
            testID="RewardShowCaseItemName1"
            style={styles.rewardNameTextStyle}>
            {item.name}
          </BalooThambiRegTextView>
        )}
        {item.badgeType && (
          <BalooThambiRegTextView
            testID="RewardShowCaseItemBadgeType"
            style={styles.rewardTypeTextStyle}>
            {level} {item.badgeType}
          </BalooThambiRegTextView>
        )}
        {item.description && (
          <BalooThambiRegTextView
            testID="RewardShowCaseItemDescription"
            style={styles.rewardDescriptionTextStyle}>
            {item.description}
          </BalooThambiRegTextView>
        )}
      </Fragment>
    );
  };
  const TitleShowCase = () => {
    return (
      <View>
        <View style={styles.rewardTitleContainer}>
          <View style={styles.rewardTitleSVGContainer}>
            {!item?.titleIcon ? (
              <SelectedTitleSVG />
            ) : (
              <SvgUri
                accessible={true}
                testID="RewardShowCaseSVGUri"
                accessibilityLabel="RewardShowCaseSVGUri"
                uri={item.titleIcon}
                style={styles.titleIconSvgStyle}
                width={styles.titleIconSvgStyle.width}
                height={styles.titleIconSvgStyle.height}
                preserveAspectRatio={'none'}
              />
            )}
          </View>
          <BalooThambiRegTextView
            testID="RewardShowCaseItemName2"
            style={styles.rewardTitleNameStyle}>
            {item?.name}
          </BalooThambiRegTextView>
        </View>
        <BalooThambiRegTextView
          testID="RewardShowCaseTopicName"
          style={styles.rewardTitleTextStyle}>
          {item.category == REWARD_TYPES_CATEGORY.EARNED
            ? 'You earned '
            : 'Earn '}
          this title by completing {`${item?.topicName}`}
        </BalooThambiRegTextView>
      </View>
    );
  };
  function RenderBGRays() {
    return (
      <View style={styles.rewardsBgRaysContainer}>
        <SimpleLottie
          testID="SimpleLottieRenderBGRays"
          theme={`rewardLotties`}
          jsonFileName="rewardBGRays"
          speed={0.4}
        />
      </View>
    );
  }
  const RenderAppliedButton = () => {
    if (item.category != REWARD_TYPES_CATEGORY.EARNED) {
      return null;
    }
    let isContentApplied = item.isApplied;
    return (
      <RoundedButton
        testID="RoundedButtonRewardShowCase"
        onPress={onSetRewardsClick}
        type="secondaryWhite"
        text={isContentApplied ? appliedBtnText : applyBtnText}
        width={styles.applyReward.width}
        height={styles.applyReward.height}
        containerStyle={{marginTop: getHp(10)}}
        textStyle={styles.applyRewardTextStyle}
        SvgImage={isContentApplied && CorrectAnswer}
        isRTLSvg={true}
        disabled={isContentApplied}
        //iconStyle={}
      />
    );
  };
  const RenderShowCase = () => {
    let showCaseContent = null;
    switch (type) {
      case REWARD_TYPES.BADGES:
        showCaseContent = <BadgeShowCase />;
        break;
      case REWARD_TYPES.TITLES:
        showCaseContent = <TitleShowCase />;
        break;
      default:
        showCaseContent = null;
    }
    return (
      <View
        accessible={true}
        testID={testID}
        accessibilityLabel={testID}
        style={styles.rewardShowCaseContainer}>
        <RenderBGRays />
        {showCaseContent}
        <RenderAppliedButton />
      </View>
    );
  };
  return item != undefined ? <RenderShowCase /> : null;
};

export default RewardShowcase;
