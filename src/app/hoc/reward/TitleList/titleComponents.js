import React from 'react';
import {TouchableOpacity, View} from 'react-native';
import styles from './indexCss';
import {RewardTitle, BalooThambiRegTextView} from '@components';
import {CorrectAnswer, RewardEmptyState} from '@images';
import {getWp, getHp} from '@utils/ViewUtils';
import {useLanguage} from '@hooks';
export const TitleItem = props => {
  const {item, titleRewardSectionType} = props;
  return (
    <View style={[styles.titleItemContainer]}>
      <RewardTitle
        item={item}
        titleRewardSectionType={titleRewardSectionType}
      />
      {item.isApplied != undefined && item.isApplied == true && (
        <CorrectAnswer
          width={getWp(21)}
          height={getWp(21)}
          style={styles.checkMarkConatiner}
        />
      )}
    </View>
  );
};

export const EmptyComponent = ({}) => {
  const {rewardEmptyState} = useLanguage();
  return (
    <View style={styles.emptyTitleContainer}>
      <RewardEmptyState width={getWp(150)} style={styles.searchIcon} />
      <BalooThambiRegTextView style={styles.emptyTitleText}>
        {rewardEmptyState}
      </BalooThambiRegTextView>
    </View>
  );
};
