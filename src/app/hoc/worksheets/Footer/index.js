import React, {useState} from 'react';
import {View, TouchableOpacity} from 'react-native';
import Collapsible from 'react-native-collapsible';
import {DownGreyArrow, UpGreyArrow} from '@images';
import styles from './indexCss';
import {getWp, getHp} from '@utils';
import {
  MyAutoHeightWebView,
  BalooThambiRegTextView,
  QHtmlTemplateForIframe,
  ClassificationCorrectAnswer,
  MatchListCorrectAnswer,
  OrderingCorrectAnswer,
  SortListCorrectAnswer,
} from '@components';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';
import {useStores} from '@mobx/hooks';
import {COLORS} from '@constants';

const Footer = props => {
  let [isCollapsed, setIsCollapssed] = useState(true);
  const {
    type,
    userAttemptData,
    explanation,
    question,
    onSoundBtnClicked,
  } = props;

  // console.log(`Footer question obj>>>`);
  // console.log(question);
  const char = 'A';
  const store = useStores();
  let explanationBody = QHtmlTemplateForIframe(explanation, wp('85%'), store);

  // console.log(`Expln Footer body>>>>>${explanationBody}`);

  const languageData = store.uiStore.languageData;
  const viewExplainationText =
    languageData && languageData?.hasOwnProperty('view_explanation')
      ? languageData?.view_explanation
      : 'View Explanation';
  const hideExplainationText =
    languageData && languageData?.hasOwnProperty('hide_explanation')
      ? languageData?.hide_explanation
      : 'Hide Explanation';
  const yourAnswerText =
    languageData && languageData?.hasOwnProperty('your_answer')
      ? languageData?.your_answer
      : 'Your Answer';
  const isRTL = store?.uiStore?.isRTL;

  const showAnswer = () => {
    if (userAttemptData != null && userAttemptData?.userResponse != null) {
      if (type === 'MCQ') {
        if (question?.multiResponse) {
          let correctAnswer = userAttemptData?.userResponse?.mcqPattern?.userAnswer.map(
            item => {
              return String.fromCharCode(item + 65);
            },
          );

          let isCorrectAnswer =
            userAttemptData?.result && userAttemptData?.result !== ''
              ? userAttemptData?.result === 'pass'
              : false;

          return renderAnswerView(correctAnswer, isCorrectAnswer);
        } else {
          let isCorrectAnswer =
            userAttemptData?.result && userAttemptData?.result !== ''
              ? userAttemptData?.result === 'pass'
              : false;

          return (
            <View
              style={
                isRTL
                  ? styles.generic.RTLanswerContainer
                  : styles.generic.answerContainer
              }>
              <BalooThambiRegTextView
                style={{marginLeft: isRTL ? getWp(10) : getWp(0)}}>
                {yourAnswerText}
              </BalooThambiRegTextView>
              <View
                style={[
                  styles.generic.optionContainer,
                  {
                    backgroundColor: isCorrectAnswer
                      ? COLORS.worksheetCorrectCountBackgroundColor
                      : COLORS.worksheetWrongCountBackgroundColor,
                  },
                ]}>
                <BalooThambiRegTextView style={styles.generic.option}>
                  {String.fromCharCode(
                    char.charCodeAt(0) +
                      userAttemptData?.userResponse?.mcqPattern?.userAnswer,
                  )}
                </BalooThambiRegTextView>
              </View>
            </View>
          );
        }
      } else if (type === 'SortList') {
        let correctAnswer = Object.keys(
          userAttemptData?.userResponse?.SortList,
        ).map(key => {
          let userAnswer = userAttemptData?.userResponse?.SortList[key][0];
          return String.fromCharCode(userAnswer + 65);
        });

        let isCorrectAnswer =
          userAttemptData?.result && userAttemptData?.result !== ''
            ? userAttemptData?.result === 'pass'
            : false;

        return renderAnswerView(correctAnswer, isCorrectAnswer);
      } else if (type === 'MatchList') {
        let correctAnswer = Object.keys(
          userAttemptData?.userResponse?.MatchList,
        ).map(key => {
          let userAnswer = userAttemptData?.userResponse?.MatchList[key][0];
          return String.fromCharCode(userAnswer + 65);
        });

        let isCorrectAnswer =
          userAttemptData?.result && userAttemptData?.result !== ''
            ? userAttemptData?.result === 'pass'
            : false;

        return renderAnswerView(correctAnswer, isCorrectAnswer);
      } else if (type === 'Ordering') {
        let correctAnswer = Object.keys(
          userAttemptData?.userResponse?.Ordering,
        ).map(key => {
          let userAnswer = userAttemptData?.userResponse?.Ordering[key][0];
          return String.fromCharCode(userAnswer + 65);
        });

        let isCorrectAnswer =
          userAttemptData?.result && userAttemptData?.result !== ''
            ? userAttemptData?.result === 'pass'
            : false;

        return renderAnswerView(correctAnswer, isCorrectAnswer);
      } else if (type === 'Classification') {
        let correctAnswer = Object.keys(
          userAttemptData?.userResponse?.Classification,
        ).map(key => {
          let userAnswer =
            userAttemptData?.userResponse?.Classification[key][0];
          return String.fromCharCode(userAnswer + 65);
        });

        let isCorrectAnswer =
          userAttemptData?.result && userAttemptData?.result !== ''
            ? userAttemptData?.result === 'pass'
            : false;

        return renderAnswerView(correctAnswer, isCorrectAnswer);
      } else {
        let correct_answers = Object.keys(userAttemptData?.userResponse).map(
          ans => {
            return userAttemptData?.userResponse[ans]?.userAnswer;
          },
        );
        if (correct_answers) {
          const yourAnswer = isRTL
            ? correct_answers.join(', ') + ' ' + yourAnswerText
            : yourAnswerText + ' ' + correct_answers.join(', ');

          return (
            <View
              style={
                isRTL
                  ? styles.generic.RTLanswerContainer
                  : styles.generic.answerContainer
              }>
              <BalooThambiRegTextView style={styles.generic.answers}>
                {yourAnswer}
              </BalooThambiRegTextView>
            </View>
          );
        }
      }
    }

    return <View />;
  };

  const renderAnswerView = (correctAnswer, isCorrectAnswer) => {
    if (correctAnswer) {
      return (
        <View
          style={
            isRTL
              ? styles.generic.RTLanswerContainer
              : styles.generic.answerContainer
          }>
          <BalooThambiRegTextView
            style={{marginLeft: isRTL ? getWp(10) : getWp(0)}}>
            {yourAnswerText}
          </BalooThambiRegTextView>
          <View
            style={[
              styles.generic.optionOtherContainer,
              {
                backgroundColor: isCorrectAnswer
                  ? COLORS.worksheetCorrectCountBackgroundColor
                  : COLORS.worksheetWrongCountBackgroundColor,
              },
            ]}>
            <BalooThambiRegTextView style={styles.generic.option}>
              {correctAnswer.join(', ')}
            </BalooThambiRegTextView>
          </View>
        </View>
      );
    }
  };

  const renderOptionItem = () => {
    switch (type) {
      case 'Classification':
        return <ClassificationCorrectAnswer response={question} />;
      case 'MatchList':
        return <MatchListCorrectAnswer response={question} />;
      case 'Ordering':
        return (
          <OrderingCorrectAnswer
            response={question}
            onSoundBtnClicked={onSoundBtnClicked}
          />
        );
      case 'SortList':
        return <SortListCorrectAnswer response={question} />;
    }
  };

  return (
    <View key="container" style={styles.generic.container}>
      <View
        key="innerContainer"
        style={
          isRTL
            ? styles.generic.RTLInnerContainer
            : styles.generic.innerContainer
        }>
        <View
          key="innerLeftContainer"
          style={
            isRTL
              ? styles.generic.RTLInnerLeftContainer
              : styles.generic.innerLeftContainer
          }>
          <TouchableOpacity
            style={isRTL ? styles.generic.RTLRow : styles.generic.row}
            transparent
            onPress={() => {
              setIsCollapssed(!isCollapsed);
            }}>
            {isCollapsed ? (
              <DownGreyArrow
                width={getHp(24)}
                height={getWp(24)}
                style={
                  isRTL
                    ? styles.generic.RTLinnerLeftSvg
                    : styles.generic.innerLeftSvg
                }
              />
            ) : (
              <UpGreyArrow
                width={getHp(24)}
                height={getWp(24)}
                style={
                  isRTL
                    ? styles.generic.RTLinnerLeftSvg
                    : styles.generic.innerLeftSvg
                }
              />
            )}
            <BalooThambiRegTextView styles={styles.generic.innerLeftText}>
              {isCollapsed ? viewExplainationText : hideExplainationText}
            </BalooThambiRegTextView>
          </TouchableOpacity>
        </View>
        {showAnswer()}
      </View>
      <View
        key="collapsibleContainer"
        style={styles.generic.collapsibleContainer}>
        <Collapsible collapsed={isCollapsed}>
          <View style={styles.generic.explanationView}>
            {renderOptionItem()}
            <MyAutoHeightWebView
              source={{html: explanationBody}}
              style={{
                width: wp('75%'),
              }}
              files={[
                {
                  href: 'contentService',
                  type: 'text/javascript',
                  rel: 'script',
                },
              ]}
              customScript={''}
              customStyle={''}
              onSizeUpdated={() => {}}
              zoomable={true}
            />
          </View>
        </Collapsible>
      </View>
    </View>
  );
};

Footer.propTypes = {};

Footer.defaultProps = {};

export default Footer;
