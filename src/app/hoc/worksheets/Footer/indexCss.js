import {StyleSheet} from 'react-native';
import {COLORS, TEXTFONTSIZE} from '@constants';
import DIMEN from '@constants/DIMEN';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';

import {getWp, getHp} from '@utils';

export default {
  generic: {
    container: {
      justifyContent: 'center',
      alignItems: 'center',
      backgroundColor: COLORS.howIdidFooter,
      width: '100%',
      borderBottomLeftRadius: wp('4.8'),
      borderBottomRightRadius: wp('4.8'),
    },

    innerContainer: {
      flexDirection: 'row',
      justifyContent: 'space-between',
      alignItems: 'center',
      width: '100%',
      height: hp('8.7'),
      paddingRight: getWp(8),
      paddingLeft: getWp(8),
      paddingVertical: hp('0.5'),
    },

    RTLInnerContainer: {
      flexDirection: 'row-reverse',
      justifyContent: 'space-between',
      alignItems: 'center',
      width: '100%',
      height: hp('8.7'),
      paddingRight: wp('4.3'),
      paddingLeft: wp('3.1'),
      paddingVertical: hp('0.5'),
    },

    innerLeftContainer: {
      flex: 1,
      flexDirection: 'row',
      justifyContent: 'flex-start',
      alignItems: 'center',
    },

    RTLInnerLeftContainer: {
      flex: 1,
      flexDirection: 'row-reverse',
      alignItems: 'center',
    },

    row: {
      flexDirection: 'row',
      flex: 1,
      alignItems: 'center',
    },

    RTLRow: {
      flexDirection: 'row-reverse',
      flex: 1,
      alignItems: 'center',
    },

    innerLeftSvg: {
      marginRight: getWp(2),
    },

    RTLinnerLeftSvg: {
      marginLeft: getWp(16),
    },

    innerLeftText: {
      fontSize: TEXTFONTSIZE.Text20,
      color: COLORS.topicCardTitle,
      textAlign: 'left',
    },
    innerRightContainer: {flex: 1},
    yourAnswer: {
      flexDirection: 'row',
      justifyContent: 'flex-end',
      alignItems: 'center',
      marginBottom: hp('0.6'),
    },
    answerText: {
      fontSize: wp('3.8'),
      color: COLORS.answerText,
      marginRight: wp('1.9'),
    },
    wrongAnswerMCQOption: {
      backgroundColor: COLORS.btnColor,
      color: COLORS.wrongAnswerText,
    },
    rightAnswerMCQOption: {
      backgroundColor: COLORS.mapTextBlue,
      color: COLORS.rightAnswerText,
    },
    collapsibleContainer: {
      alignItems: 'flex-start',
      justifyContent: 'center',
      width: '100%',
    },
    explanationView: {
      // paddingLeft: wp('10.6'),
      paddingRight: wp('2.4'),
      paddingTop: hp('0.5'),
      paddingBottom: hp('2.6'),
    },
    webViewStyle: {
      width: wp('75%'),
    },

    optionContainer: {
      minWidth: getWp(26),
      height: getWp(26),
      marginLeft: getWp(8),
      borderRadius: getWp(13),
      backgroundColor: COLORS.worksheetCorrectCountBackgroundColor,
      alignItems: 'center',
      justifyContent: 'center',
    },

    optionOtherContainer: {
      minWidth: getWp(26),
      height: getWp(26),
      marginLeft: getWp(8),
      borderRadius: getWp(13),
      backgroundColor: COLORS.worksheetCorrectCountBackgroundColor,
      alignItems: 'center',
      paddingLeft: getWp(5),
      paddingRight: getWp(5),
      justifyContent: 'center',
    },

    option: {
      fontSize: TEXTFONTSIZE.Text14,
      color: COLORS.white,
    },
    answerContainer: {
      flexDirection: 'row',
      flex: 1,
      justifyContent: 'flex-end',
    },

    RTLanswerContainer: {
      flexDirection: 'row-reverse',
      alignItems: 'center',
      justifyContent: 'center',
    },

    answers: {
      textAlign: 'right',
    },
  },
};
