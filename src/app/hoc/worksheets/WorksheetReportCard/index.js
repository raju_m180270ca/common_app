import React from 'react';
import {View} from 'react-native';
import {
  BalooThambiRegTextView,
  SourceSansProBoldTextView,
  RoundedButton,
} from '@components';
import {CorrectAnswer, Clock, WrongAnswer} from '@images';
import styles from './style';
import PropTypes from 'prop-types';
import {NaandiQuestion, NaandiFooter, PassageDialog} from '@hoc';
import {useStores} from '@mobx/hooks';
import {getHp, getWp} from '@utils';

const WorksheetReportCard = props => {
  const {response} = props;
  const {appStore, uiStore} = useStores();
  const [isVisible, setVisibility] = React.useState(false);

  const languageData = uiStore.languageData;
  const questionsText =
    languageData && languageData?.hasOwnProperty('question')
      ? languageData?.question
      : 'Question';

  const timeTakenText =
    languageData && languageData?.hasOwnProperty('time_taken')
      ? languageData?.time_taken
      : 'Time Taken';

  const viewPassageText =
    languageData &&
    languageData.hasOwnProperty('btn') &&
    languageData?.btn.hasOwnProperty('view_passage')
      ? languageData?.btn?.view_passage
      : 'View Passage';
  const isRTL = uiStore.isRTL;

  let passageData = null;
  if (response?.passageData && response?.passageData?._id) {
    passageData = response?.passageData;
  }

  return (
    <View style={styles.questionCardContainer}>
      {passageData && (
        <PassageDialog
          isVisible={isVisible}
          passageData={passageData}
          onPress={() => setVisibility(false)}
        />
      )}
      <View
        style={
          isRTL
            ? styles.RTLQuestionCardHeaderContainer
            : styles.questionCardHeaderContainer
        }>
        <View
          style={
            isRTL
              ? styles.RTLQuestionCardHeaderSubContainer
              : styles.questionCardHeaderSubContainer
          }>
          <SourceSansProBoldTextView style={styles.questionNumberText}>
            {`${questionsText} ${response?.contentSeqNum}`}
          </SourceSansProBoldTextView>
          {response?.userAttemptData?.result === 'pass' ? (
            <CorrectAnswer />
          ) : response?.userAttemptData?.result ? (
            <WrongAnswer />
          ) : null}
        </View>
        {response?.userAttemptData?.result && (
          <View
            style={
              isRTL
                ? styles.RTLQuestionCardHeaderSubContainer
                : styles.questionCardHeaderSubContainer
            }>
            <Clock />
            <BalooThambiRegTextView style={styles.timeText}>
              {`${timeTakenText} ${response?.userAttemptData?.timeTaken} sec`}
            </BalooThambiRegTextView>
          </View>
        )}
      </View>
      {passageData ? (
        <RoundedButton
          type="elevatedOrange"
          text={viewPassageText}
          textStyle={styles.footerButtonText}
          containerStyle={isRTL ? styles.RTLFooterButton : styles.footerButton}
          width={getWp(180)}
          height={getHp(50)}
          onPress={() => setVisibility(true)}
        />
      ) : null}
      <NaandiQuestion
        userAnswer={
          response?.data?.template === 'MCQ'
            ? response?.userAttemptData?.userResponse?.mcqPattern?.userAnswer
            : 0
        }
        response={response}
        resultFlag={response?.userAttemptData?.result}
        question={response.data}
        optionContainerStyle={
          isRTL ? styles.RTLOptionContainer : styles.optionContainer
        }
        optionTextContainerStyle={styles.optionTextContainer}
        optionTextStyle={styles.optionText}
        webContentStyle={styles.webView}
      />

      <NaandiFooter
        type={response?.data?.template}
        userAttemptData={response?.userAttemptData}
        explanation={response?.data?.explanation}
        question={response?.data}
        onSoundBtnClicked={() => {}}
      />
    </View>
  );
};

WorksheetReportCard.propTypes = {
  response: PropTypes.object,
};

WorksheetReportCard.defaultProps = {
  response: null,
};

export default WorksheetReportCard;
