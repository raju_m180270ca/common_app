import {StyleSheet} from 'react-native';
import {COLORS, TEXTFONTSIZE} from '@constants';
import {getWp} from '@utils/ViewUtils';

export default StyleSheet.create({
  questionCardContainer: {
    flexDirection: 'column',
    backgroundColor: COLORS.white,
    borderRadius: getWp(23),
    marginTop: getWp(20),
    minHeight: getWp(200),
    width: getWp(370),
    overflow: 'hidden',
  },

  RTLQuestionCardContainer: {
    flexDirection: 'column',
    backgroundColor: COLORS.white,
    borderRadius: getWp(23),
    marginTop: getWp(20),
    minHeight: getWp(200),
    width: getWp(370),
    alignItems: 'flex-end',
    overflow: 'hidden',
  },

  questionCardHeaderContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    marginLeft: getWp(24),
    marginRight: getWp(24),
    marginTop: getWp(32),
    marginBottom: getWp(24),
  },

  RTLQuestionCardHeaderContainer: {
    flexDirection: 'row-reverse',
    alignItems: 'center',
    justifyContent: 'space-between',
    marginLeft: getWp(24),
    marginRight: getWp(24),
    marginTop: getWp(32),
    marginBottom: getWp(24),
  },

  questionCardHeaderSubContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },

  RTLQuestionCardHeaderSubContainer: {
    flexDirection: 'row-reverse',
    alignItems: 'center',
    justifyContent: 'center',
  },

  questionNumberText: {
    fontSize: TEXTFONTSIZE.Text20,
    color: COLORS.screenTestDescriptionTextColor,
    marginRight: getWp(6),
    marginLeft: getWp(6),
  },

  timeText: {
    fontSize: TEXTFONTSIZE.Text11,
    color: COLORS.screenTestDescriptionTextColor,
    marginLeft: getWp(6),
    marginRight: getWp(6),
  },

  optionContainer: {
    flexDirection: 'row',
    minHeight: getWp(68),
    borderRadius: getWp(36),
    padding: getWp(14),
    backgroundColor: COLORS.worksheetReportQuestionOptionColor,
  },

  RTLOptionContainer: {
    flexDirection: 'row-reverse',
    minHeight: getWp(68),
    borderRadius: getWp(36),
    padding: getWp(14),
    backgroundColor: COLORS.worksheetReportQuestionOptionColor,
  },

  optionTextContainer: {
    width: getWp(40),
    height: getWp(40),
    borderRadius: getWp(20),
    backgroundColor: COLORS.statTextColor,
    alignItems: 'center',
    justifyContent: 'center',
    marginRight: getWp(14),
  },

  optionText: {
    color: COLORS.white,
    fontSize: TEXTFONTSIZE.Text22,
    fontWeight: 'bold',
  },

  webView: {paddingTop: getWp(10)},

  footerButton: {
    backgroundColor: 'transparent',
    marginLeft: getWp(16),
  },

  RTLFooterButton: {
    backgroundColor: 'transparent',
    marginRight: getWp(16),
    alignSelf: 'flex-end',
  },

  footerButtonText: {
    fontFamily: 'BalooThambi-Regular',
    fontSize: TEXTFONTSIZE.Text24,
    color: COLORS.white,
  },
});
