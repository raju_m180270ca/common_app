import {Animated, PanResponder, Dimensions} from 'react-native';
const SortListPan = (
  item,
  arrangeTypeQuestionsAnswers,
  callBackForAssignAnswer,
  updatedZIndex,
  setEnableScroll,
) => {
  let animatedValue = new Animated.ValueXY();
  let _value = {x: 0, y: 0};
  animatedValue.addListener((value) => (_value = value));

  let panResponder = PanResponder.create({
    onStartShouldSetPanResponder: (evt, gestureState) => true,
    onMoveShouldSetPanResponder: (evt, gestureState) => true,
    onPanResponderGrant: (e, gestureState) => {
      animatedValue.setOffset({
        x: _value.x,
        y: _value.y,
      });
      animatedValue.setValue({x: 0, y: 0});
      //  setScrollEnabled(false);
      //cb run here for updating zIndex
      setEnableScroll(false);
      updatedZIndex(item);
    },
    onPanResponderMove: Animated.event([
      null,
      {
        dx: animatedValue.x,
        dy: animatedValue.y,
      },
    ]),
    onPanResponderRelease: (e, gestureState) => {
      animatedValue.flattenOffset();
      Animated.decay(animatedValue, {
        deceleration: 0.5,
        velocity: {x: gestureState.vx, y: gestureState.vy},
      }).start();
      const {pageX} = e.nativeEvent;
      const {pageY} = e.nativeEvent;
      let itemStatus = {
        selected: false,
        item: null,
        index: 0,
      };
      let plottingPoints = [];

      for (let i = 0; i < arrangeTypeQuestionsAnswers.length; i++) {
        let {fx, fy, width, height, px, py} = arrangeTypeQuestionsAnswers[
          i
        ].measures;

        let equation =
          Math.pow(pageX - (px + width / 2), 2) + Math.pow(pageY - py, 2);
        let plottingPoint = Math.sqrt(equation);

        plottingPoints.push({point: plottingPoint, index: i});
      }
      let sortedPoints = plottingPoints.sort((a, b) => a.point - b.point);
      itemStatus.selected = true;
      itemStatus.item = arrangeTypeQuestionsAnswers[sortedPoints[0].index];
      itemStatus.index = sortedPoints[0].index;
      setEnableScroll(true);

      if (itemStatus.selected) {
        callBackForAssignAnswer(
          item,
          itemStatus.index,
          () => {
            animatedValue.setValue({x: 0, y: 0});
          },
          sortedPoints,
        );
      } else {
        animatedValue.setValue({x: 0, y: 0});
      }
    },
  });

  return [animatedValue, panResponder];
};

export default SortListPan;

// let minPoint = Math.min(...plottingPoints.map((p) => p.point));
//       let findMinIndex = plottingPoints.findIndex(
//         (ptr) => ptr.point == minPoint,
//       );
//       console.log(
//         `Minpoint Value - ${minPoint} - Minpoint Index - ${findMinIndex}`,
//       );

// animatedValue.setValue({x: 0, y: 0});
//       return false;
// if (pageX < px + width) {
//   if (pageY < py + height) {
//     itemStatus.selected = true;
//     itemStatus.item = arrangeTypeQuestionsAnswers[i];
//     itemStatus.index = i;
//     break;
//   }
// }
// if (pageY > (py - height/6) && pageY < py + height) {
//   if (pageX > px && pageX < px + width) {
//     itemStatus.selected = true;
//     itemStatus.item = arrangeTypeQuestionsAnswers[i];
//     itemStatus.index = i;
//     break;
//   }
// } else {
//   itemStatus.selected = false;
// }
// console.log(
//   'Test [' +
//     i +
//     '] --   ' +
//     JSON.stringify({ans: px + width, fx, fy, width, height, px, py}),
// );
// console.log('ANS [' + i + '] - ', plottingPoint);
