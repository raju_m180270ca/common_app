import {useSound} from './useSound';
import {useQnA} from './useQnA';
import {useLanguage} from './useLanguage';
import useAuth from './useAuth';
import useUI from './useUI';
import useStarQuestion from './useStarQuestion';
import {useTopic} from './useTopic';
import useToggleSection from './Rewards/useToggleSection';

export {
  useSound,
  useQnA,
  useLanguage,
  useAuth,
  useUI,
  useStarQuestion,
  useTopic,
  useToggleSection,
};
