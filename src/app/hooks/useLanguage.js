import React from 'react';
import {useStores} from '@mobx/hooks';
import {TEXTFONTSIZE} from '@constants';

export const useLanguage = () => {
  const {uiStore, appStore} = useStores();

  const languageData = uiStore.languageData;

  const worksheetText =
    languageData && languageData?.hasOwnProperty('worksheets')
      ? languageData?.worksheets
      : 'Worksheets';

  const topicsText =
    languageData && languageData?.hasOwnProperty('topics')
      ? languageData?.topics
      : 'Topics';

  const challengeQuestionText =
    languageData && languageData?.hasOwnProperty('challenge_question')
      ? languageData?.challenge_question
      : 'Challenge Question';

  const submitText =
    languageData &&
    languageData?.hasOwnProperty('btn') &&
    languageData?.btn?.hasOwnProperty('submit')
      ? languageData?.btn?.submit
      : 'Submit';

  let please_enter_the_answer_text =
    languageData && languageData?.hasOwnProperty('please_enter_the_answer')
      ? languageData?.please_enter_the_answer
      : 'Please enter the answer.';

  let please_select_an_option_text =
    languageData && languageData?.hasOwnProperty('please_select_an_option')
      ? languageData?.please_select_an_option
      : 'Please select an option';

  let closeText =
    languageData &&
    languageData?.hasOwnProperty('btn') &&
    languageData?.btn?.hasOwnProperty('close')
      ? languageData?.btn?.close
      : 'Close';

  let nextText =
    languageData &&
    languageData?.hasOwnProperty('btn') &&
    languageData?.btn?.hasOwnProperty('next')
      ? languageData?.btn?.next
      : 'Next';

  let prevText =
    languageData &&
    languageData?.hasOwnProperty('btn') &&
    languageData?.btn?.hasOwnProperty('previous')
      ? languageData?.btn?.previous
      : 'Previous';

  let idontknowText =
    languageData &&
    languageData?.hasOwnProperty('btn') &&
    languageData?.btn?.hasOwnProperty('i_dont_know')
      ? languageData?.btn?.i_dont_know
      : 'I Don’t Know';

  let titleIdentifier = 'screening_test';
  let defaultTitleText = 'Screening Test';

  if (!appStore.isScreenTestActive) {
    titleIdentifier = 'level_test';
    defaultTitleText = 'Quarterly Test';
  }

  let titleText =
    languageData && languageData?.hasOwnProperty(titleIdentifier)
      ? languageData[titleIdentifier]
      : defaultTitleText;
  // let seeEnglishText =
  //   languageData &&
  //   languageData?.hasOwnProperty('btn') &&
  //   languageData?.btn?.hasOwnProperty('see_english')
  //     ? languageData?.btn?.see_english
  //     : 'See English';
  let sessionIDText =
    languageData && languageData?.hasOwnProperty('session_id')
      ? languageData?.session_id
      : 'SESSION ID';
  let viewQuestionText =
    languageData &&
    languageData?.hasOwnProperty('btn') &&
    languageData?.btn?.hasOwnProperty('view_question')
      ? languageData?.btn?.view_question
      : 'View Question';
  let readPassageText =
    languageData &&
    languageData?.hasOwnProperty('btn') &&
    languageData?.btn?.hasOwnProperty('read_passage')
      ? languageData?.btn?.view_question
      : 'Read Passage';
  let viewPassageText =
    languageData &&
    languageData?.hasOwnProperty('btn') &&
    languageData?.btn?.hasOwnProperty('view_passage')
      ? languageData?.btn?.view_passage
      : 'View Passage';

  let error_in_audio_file_text =
    languageData && languageData?.hasOwnProperty('error_in_audio_file')
      ? languageData?.error_in_audio_file
      : 'Error in audio file';

  let please_select_answer_text =
    languageData && languageData?.hasOwnProperty('please_select_answer')
      ? languageData?.please_select_answer
      : 'Please select answer';

  let some_answers_have_not_been_answered_text =
    languageData &&
    languageData?.hasOwnProperty('some_answers_have_not_been_answered')
      ? languageData?.some_answers_have_not_been_answered
      : 'Some answers have not been answered';

  let audio_is_not_available_text =
    languageData && languageData?.hasOwnProperty('audio_is_not_available')
      ? languageData?.audio_is_not_available
      : 'Audio is not available';

  const isTamilLang =
    appStore?.userLanguage && appStore?.userLanguage === 'ta'
      ? TEXTFONTSIZE.Text12
      : TEXTFONTSIZE.Text16;

  const yesbtnText =
    languageData &&
    languageData?.hasOwnProperty('btn') &&
    languageData?.btn?.hasOwnProperty('yes')
      ? languageData?.btn?.yes
      : 'Yes';
  const nobtnText =
    languageData &&
    languageData?.hasOwnProperty('btn') &&
    languageData?.btn?.hasOwnProperty('no')
      ? languageData?.btn?.no
      : 'No';

  const confirmationText =
    languageData && languageData?.hasOwnProperty('confirmation')
      ? languageData?.confirmation
      : 'Confirmation';
  let viewWordMeaningText =
    languageData &&
    languageData?.hasOwnProperty('btn') &&
    languageData?.btn?.hasOwnProperty('view_word_meaning')
      ? languageData?.btn?.view_word_meaning
      : 'View Word Meaning';

  const sessionEndingConfirmMessage =
    languageData &&
    languageData?.hasOwnProperty('session_ending_confirmation_message')
      ? languageData?.session_ending_confirmation_message
      : 'Do you want exit from the session?';
  const sparkiesText =
    languageData && languageData?.hasOwnProperty('sparkies')
      ? languageData?.sparkies
      : 'Sparkies';

  const worksheetBtnText =
    languageData &&
    languageData?.hasOwnProperty('btn') &&
    languageData?.btn?.hasOwnProperty('worksheets')
      ? languageData?.btn?.worksheets
      : 'Worksheets';

  const topicBtnText =
    languageData &&
    languageData?.hasOwnProperty('btn') &&
    languageData?.btn?.hasOwnProperty('topics')
      ? languageData?.btn?.topics
      : 'Topics';
  const gameBtnText =
    languageData &&
    languageData?.hasOwnProperty('btn') &&
    languageData?.btn?.hasOwnProperty('games')
      ? languageData?.btn?.games
      : 'Games';

  let enterPasswordText =
    languageData && languageData?.hasOwnProperty('enter_password')
      ? languageData?.enter_password
      : 'Enter Password';

  let choosePasswordText =
    languageData && languageData?.hasOwnProperty('set_new_password')
      ? languageData?.set_new_password
      : 'Set New Password';

  let userNameText =
    languageData && languageData?.hasOwnProperty('hey')
      ? languageData?.hey.toString()
      : 'Hey {{userName}}';

  let loginBtnName =
    languageData?.btn && languageData?.btn.login
      ? languageData?.btn.login
      : 'Login';

  let selectText =
    languageData?.btn && languageData?.btn.select
      ? languageData?.btn.select
      : 'Select';

  const startText =
    languageData &&
    languageData?.hasOwnProperty('btn') &&
    languageData?.btn?.hasOwnProperty('start')
      ? languageData?.btn?.start
      : 'Start';
  const continueText =
    languageData &&
    languageData?.hasOwnProperty('btn') &&
    languageData?.btn?.hasOwnProperty('continue')
      ? languageData?.btn?.continue
      : 'Continue';

  const seeReportText =
    languageData &&
    languageData?.hasOwnProperty('btn') &&
    languageData?.btn?.hasOwnProperty('see_report')
      ? languageData?.btn?.see_report
      : 'See Report';

  const learnBtnText =
    languageData &&
    languageData?.hasOwnProperty('btn') &&
    languageData?.btn?.hasOwnProperty('learn')
      ? languageData?.btn?.learn
      : 'Learn';

  const liveWorksheetText =
    languageData && languageData?.hasOwnProperty('live_worksheets')
      ? languageData?.live_worksheets
      : 'Live Worksheets';
  const olderWorksheetText =
    languageData && languageData?.hasOwnProperty('older_worksheets')
      ? languageData?.older_worksheets
      : 'Older Worksheets';
  const completedText =
    languageData && languageData?.hasOwnProperty('worksheet_question_completed')
      ? languageData?.worksheet_question_completed
      : '{{completed}} of {{total}} completed';

  const workSheetNumberOfQuestions =
    languageData && languageData?.hasOwnProperty('worksheet_total_questions')
      ? languageData?.worksheet_total_questions
      : 'You have a total of {{total}} Questions \n to answer';

  const emptyText =
    languageData && languageData?.hasOwnProperty('no_worksheets_are_available')
      ? languageData?.no_worksheets_are_available
      : 'No Worksheets are available';

  const timeTakenText =
    languageData && languageData?.hasOwnProperty('time_taken')
      ? languageData?.time_taken
      : 'Time Taken';

  const sparkieEarnedText =
    languageData && languageData?.hasOwnProperty('sparkies_earned')
      ? languageData?.sparkies_earned
      : 'Sparkies earned';

  const youDidQuestionText =
    languageData && languageData?.hasOwnProperty('you_did_questions')
      ? languageData?.you_did_questions
      : 'You did {{questions_attempt}} questions';

  const viewMapText =
    languageData && languageData?.hasOwnProperty('view map')
      ? languageData['view map']
      : 'View my learning map';

  const correctText =
    languageData && languageData?.hasOwnProperty('correct')
      ? languageData?.correct
      : 'Correct';
  const wrongText =
    languageData && languageData?.hasOwnProperty('wrong')
      ? languageData?.wrong
      : 'Wrong';

  const howIdidText =
    languageData &&
    languageData?.hasOwnProperty('btn') &&
    languageData?.btn?.hasOwnProperty('how_i_did')
      ? languageData?.btn?.how_i_did
      : 'See How I Did';

  const gameEmptyText =
    languageData && languageData?.hasOwnProperty('no_games_are_available')
      ? languageData?.no_games_are_available
      : 'No games are available';

  const homeWorkBtnText =
    languageData &&
    languageData?.hasOwnProperty('btn') &&
    languageData?.btn?.hasOwnProperty('homework')
      ? languageData?.btn?.homework
      : 'Homework';

  const profileProgressText =
    languageData && languageData?.hasOwnProperty('profile_progress')
      ? languageData?.profile_progress
      : 'Profile Progress';

  const profileCompleteText =
    languageData && languageData?.hasOwnProperty('complete_your_profile')
      ? languageData?.complete_your_profile
      : 'Complete your Profile';

  const homeText =
    languageData &&
    languageData?.hasOwnProperty('btn') &&
    languageData?.btn?.hasOwnProperty('home')
      ? languageData?.btn?.home
      : 'Home';

  const staredPularText =
    languageData && languageData?.hasOwnProperty('starred_plural_questions')
      ? languageData?.starred_plural_questions
      : 'Starred Questions';

  const messageLabelText =
    languageData && languageData?.hasOwnProperty('messages')
      ? languageData?.messages
      : 'Messages';

  const leaderboardLabelText =
    languageData && languageData?.hasOwnProperty('leaderboard')
      ? languageData?.leaderboard
      : 'Leaderboard';

  const rewardLabelText =
    languageData && languageData?.hasOwnProperty('my_rewards')
      ? languageData?.my_rewards
      : 'My Rewards';

  const profileLabelText =
    languageData && languageData?.hasOwnProperty('profile')
      ? languageData?.profile
      : 'Profile';

  const logoutLableText =
    languageData && languageData?.hasOwnProperty('logout')
      ? languageData?.logout
      : 'Logout';

  const starredSingularText =
    languageData && languageData?.hasOwnProperty('starred_question')
      ? languageData?.starred_question
      : 'Starred Question';

  const goHomeBtnText =
    languageData &&
    languageData?.hasOwnProperty('btn') &&
    languageData?.btn?.hasOwnProperty('go_to_home')
      ? languageData?.btn?.go_to_home
      : 'Go to Home';

  const attentionPleaseText =
    languageData && languageData?.hasOwnProperty('attention_please')
      ? languageData?.attention_please
      : 'Attention please';

  const noCancelBtnText =
    languageData &&
    languageData?.hasOwnProperty('btn') &&
    languageData?.btn?.hasOwnProperty('no_cancel')
      ? languageData?.btn?.no_cancel
      : 'No, cancel';

  const yesRemoveBtnText =
    languageData &&
    languageData?.hasOwnProperty('btn') &&
    languageData?.btn?.hasOwnProperty('yes_remove')
      ? languageData?.btn?.yes_remove
      : 'Yes, remove';

  const thisWillRemoveDescText =
    languageData &&
    languageData?.hasOwnProperty('this_will_remove_the_question')
      ? languageData?.this_will_remove_the_question
      : 'This will remove the question from the bookmarks page. Do you want to remove?';

  const newMsgBtnText =
    languageData &&
    languageData?.hasOwnProperty('btn') &&
    languageData?.btn?.hasOwnProperty('new_message')
      ? languageData?.btn?.new_message
      : 'New Message';

  const searchMsgText =
    languageData && languageData?.hasOwnProperty('search_messages')
      ? languageData?.search_messages
      : 'Search messages';

  const singularMsgText =
    languageData && languageData?.hasOwnProperty('singular_msg')
      ? languageData?.singular_msg
      : '1 message';

  const pluralMsgText =
    languageData && languageData?.hasOwnProperty('plural_msg')
      ? languageData?.plural_msg
      : 'messages';

  const singularMsgCapitalText =
    languageData && languageData?.hasOwnProperty('message')
      ? languageData?.message
      : 'Messages';

  const rateConvoText =
    languageData && languageData?.hasOwnProperty('rate_ur_message')
      ? languageData?.rate_ur_message
      : 'How useful was this conversation?';

  const typeReplyText =
    languageData && languageData?.hasOwnProperty('type_your_reply')
      ? languageData?.type_your_reply
      : 'Type your reply';

  const sendText =
    languageData && languageData?.hasOwnProperty('send')
      ? languageData?.send
      : 'Send';

  const msgSuccessText =
    languageData && languageData?.hasOwnProperty('message_sent_successfully')
      ? languageData?.message_sent_successfully
      : 'Your message has been sent successfully.';

  const maxFileSizeText =
    languageData && languageData?.hasOwnProperty('max_upload_filesize')
      ? languageData?.max_upload_filesize
      : 'Maximum size allowed is 2 MB.';

  const maxFileErrorText =
    languageData && languageData?.hasOwnProperty('max_file_error')
      ? languageData?.max_file_error
      : 'You can upload a maximum of 5 files.';

  const fileFormatErrorText =
    languageData && languageData?.hasOwnProperty('file_format_error')
      ? languageData?.file_format_error
      : 'File format not supported';

  const failedText =
    languageData && languageData?.hasOwnProperty('failed')
      ? languageData?.failed
      : 'Failed';

  const writeYourMsg =
    languageData && languageData?.hasOwnProperty('write_ur_message')
      ? languageData?.write_ur_message
      : 'Write your message';

  const attachFileText =
    languageData && languageData?.hasOwnProperty('attach_file')
      ? languageData?.attach_file
      : 'Attach file';

  const storagePermissionText =
    languageData && languageData?.hasOwnProperty('storage_permission')
      ? languageData?.storage_permission
      : 'Storage permission';

  const storageDescText =
    languageData && languageData?.hasOwnProperty('storage_dialogue')
      ? languageData?.storage_dialogue
      : 'Mindspark needs access to your storage to download a file.';

  const downloadFailedText =
    languageData && languageData?.hasOwnProperty('donwload_failed')
      ? languageData?.donwload_failed
      : 'Download failed';

  const yourSection =
    languageData && languageData?.hasOwnProperty('your_section')
      ? languageData?.your_section
      : 'Your section';

  const yourCity =
    languageData && languageData?.hasOwnProperty('your_city')
      ? languageData?.your_city
      : 'Your city';

  const yourCountry =
    languageData && languageData?.hasOwnProperty('your_country')
      ? languageData?.your_country
      : 'World Ranking';

  const sectionLeaderBoardEmpty =
    languageData && languageData?.hasOwnProperty('section_leader_board_empty')
      ? languageData?.section_leader_board_empty
      : 'No one here, yet. Run, get started with questions to be on your section leaderboard!';

  const cityLeaderBoardEmpty =
    languageData && languageData?.hasOwnProperty('city_leader_board_empty')
      ? languageData?.city_leader_board_empty
      : 'No one here, yet. Run, get started with questions to be on your city ranking leaderboard!';

  const worldLeaderBoardEmpty =
    languageData && languageData?.hasOwnProperty('world_leader_board_empty')
      ? languageData?.world_leader_board_empty
      : 'No one here, yet. Run, get started with questions to be on your world ranking leaderboard!';
  const sparkieEarned =
    languageData && languageData?.hasOwnProperty('sparkie_earned')
      ? languageData?.sparkie_earned
      : 'Sparkies earned';

  const sparkieLeaderBoard =
    languageData && languageData?.hasOwnProperty('sparkie_leader_board')
      ? languageData?.sparkie_leader_board
      : 'Sparkie Leaderboard';

  const badgesLabel =
    languageData && languageData?.hasOwnProperty('badges')
      ? languageData?.badges
      : 'Badges';

  const titleLabel =
    languageData && languageData?.hasOwnProperty('titles')
      ? languageData?.titles
      : 'Titles';

  const earnedBadges =
    languageData && languageData?.hasOwnProperty('earned_badges')
      ? languageData?.earned_badges
      : 'Earned Badges';

  const ongoingBadges =
    languageData && languageData?.hasOwnProperty('ongoing_badges')
      ? languageData?.ongoing_badges
      : 'Ongoing Badges';

  const upcomingBadges =
    languageData && languageData?.hasOwnProperty('upcoming_badges')
      ? languageData?.upcoming_badges
      : 'Upcoming Badges';

  const viewAll =
    languageData && languageData?.hasOwnProperty('view_all')
      ? languageData?.view_all
      : 'View all';

  const viewLess =
    languageData && languageData?.hasOwnProperty('view_less')
      ? languageData?.view_less
      : 'View less';

  const level =
    languageData && languageData?.hasOwnProperty('level')
      ? languageData?.level
      : 'Level';

  const applyBtnText =
    languageData &&
    languageData?.hasOwnProperty('btn') &&
    languageData?.btn?.hasOwnProperty('apply')
      ? languageData?.btn?.apply
      : 'Apply';

  const appliedBtnText =
    languageData &&
    languageData?.hasOwnProperty('btn') &&
    languageData?.btn?.hasOwnProperty('applied')
      ? languageData?.btn?.applied
      : 'Applied';

  const copyiedToClipboard =
    languageData && languageData?.hasOwnProperty('copied_to_clipboard')
      ? languageData?.copied_to_clipboard
      : 'Copied to clipboard!';

  const parentCodeText =
    languageData && languageData?.hasOwnProperty('parent_Code')
      ? languageData?.parent_Code
      : 'Parent Code:';

  const changePassBtnText =
    languageData &&
    languageData?.hasOwnProperty('btn') &&
    languageData?.btn?.hasOwnProperty('change_password')
      ? languageData?.btn?.change_password
      : 'Change \npassword';

  const viewSubscriptionBtnText =
    languageData &&
    languageData?.hasOwnProperty('btn') &&
    languageData?.btn?.hasOwnProperty('view_subscription')
      ? languageData?.btn?.view_subscription
      : 'View\nSubscription';

  const imText =
    languageData && languageData?.hasOwnProperty('im_a')
      ? languageData?.im_a
      : 'I’m a';

  const boyText =
    languageData && languageData?.hasOwnProperty('boy')
      ? languageData?.boy
      : 'Boy';

  const girlText =
    languageData && languageData?.hasOwnProperty('girl')
      ? languageData?.girl
      : 'Girl';

  const neutralText =
    languageData && languageData?.hasOwnProperty('neutral')
      ? languageData?.neutral
      : 'Neutral';

  const nameText =
    languageData && languageData?.hasOwnProperty('name')
      ? languageData?.name
      : 'Name';

  const classText =
    languageData && languageData?.hasOwnProperty('class')
      ? languageData?.class
      : 'Class';

  const sectionText =
    languageData && languageData?.hasOwnProperty('section')
      ? languageData?.section
      : 'Section';

  const schoolText =
    languageData && languageData?.hasOwnProperty('school')
      ? languageData?.school
      : 'School';

  const cityText =
    languageData && languageData?.hasOwnProperty('city')
      ? languageData?.city
      : 'City';

  const dobText =
    languageData && languageData?.hasOwnProperty('date_of_birth')
      ? languageData?.date_of_birth
      : 'Date of Birth';

  const saveChangesBtnText =
    languageData && languageData?.hasOwnProperty('save_changes')
      ? languageData?.save_changes
      : 'Save changes';

  const parent1Detail =
    languageData && languageData?.hasOwnProperty('parent1_details')
      ? languageData?.parent1_details
      : 'Parent 1 Details';

  const parent2Detail =
    languageData && languageData?.hasOwnProperty('parent2_details')
      ? languageData?.parent2_details
      : 'Parent 2 Details';

  const parent1Name =
    languageData && languageData?.hasOwnProperty('parent1_name')
      ? languageData?.parent1_name
      : 'Parent 1 Name';

  const parent2Name =
    languageData && languageData?.hasOwnProperty('parent2_name')
      ? languageData?.parent2_name
      : 'Parent 2 Name';

  const alphabetsAllowedText =
    languageData && languageData?.hasOwnProperty('only_aphabets_allowed')
      ? languageData?.only_aphabets_allowed
      : 'Only alphabets and spaces allowed.';

  const parent1PhoneLabel =
    languageData && languageData?.hasOwnProperty('parent1_phone')
      ? languageData?.parent1_phone
      : 'Parent 1 Phone';

  const parent2PhoneLabel =
    languageData && languageData?.hasOwnProperty('parent2_phone')
      ? languageData?.parent2_phone
      : 'Parent 2 Phone';

  const validPhoneNumText =
    languageData && languageData?.hasOwnProperty('enter_valid_number')
      ? languageData?.enter_valid_number
      : 'Please enter a valid phone number.';

  const parent1EmailLabel =
    languageData && languageData?.hasOwnProperty('parent1_email')
      ? languageData?.parent1_email
      : 'Parent 1 Email';

  const parent2EmailLabel =
    languageData && languageData?.hasOwnProperty('parent2_email')
      ? languageData?.parent2_email
      : 'Parent 2 Email';

  const validEmailIDText =
    languageData && languageData?.hasOwnProperty('enter_valid_email')
      ? languageData?.enter_valid_email
      : 'Please enter a valid email ID.';

  const startDateText =
    languageData && languageData?.hasOwnProperty('start_date')
      ? languageData?.start_date
      : 'Start date :';

  const endDateText =
    languageData && languageData?.hasOwnProperty('end_date')
      ? languageData?.end_date
      : 'End date :';

  const notifySetText =
    languageData && languageData?.hasOwnProperty('notification settings')
      ? languageData['notification settings']
      : 'Notification Settings';

  const notificationPlural =
    languageData && languageData?.hasOwnProperty('notification')
      ? languageData?.notification
      : 'Notifications';

  const soundText =
    languageData && languageData?.hasOwnProperty('sounds')
      ? languageData?.sounds
      : 'Sounds';

  const trustedDevicesText =
    languageData && languageData?.hasOwnProperty('trusted_devices')
      ? languageData?.trusted_devices
      : 'Trusted Devices';

  const markAsTrustedProfile =
    languageData && languageData?.hasOwnProperty('mark_your_device_text')
      ? languageData?.mark_your_device_text
      : 'Mark your device as trusted to always stay\n logged in to your Mindspark account.';

  const thisDeviceText =
    languageData && languageData?.hasOwnProperty('this_device')
      ? languageData?.this_device
      : 'This Device';

  const markTrustedBtnText =
    languageData &&
    languageData?.hasOwnProperty('btn') &&
    languageData?.btn?.hasOwnProperty('mark_truted')
      ? languageData?.btn?.mark_truted
      : 'Mark As Trusted';

  const subsciptionSingular =
    languageData && languageData?.hasOwnProperty('subscription')
      ? languageData?.subscription
      : 'Subscription';

  const profileUpdatedText =
    languageData && languageData?.hasOwnProperty('profile_updated_successfully')
      ? languageData?.profile_updated_successfully
      : 'Your profile is successfully updated!';

  const trustedDeviceEmptyState =
    languageData && languageData?.hasOwnProperty('trusted_device_emptyState')
      ? languageData?.trusted_device_emptyState
      : "No trusted devices! If you mark a device as trusted, we'll keep you logged in on it and save your time. Mark this device as trusted?";

  const daysRemainingLabel =
    languageData && languageData?.hasOwnProperty('days_remianing')
      ? languageData?.days_remianing
      : 'days remaining';

  const notYourPasswordMsg =
    languageData && languageData?.hasOwnProperty('not_your_password')
      ? languageData?.not_your_password
      : 'This is not your password. Please try again.';

  const somethingWentWrong =
    languageData && languageData?.hasOwnProperty('something_went_wrong')
      ? languageData?.something_went_wrong
      : 'Something went wrong! Please try again.';

  const onlyAplhaPassRules =
    languageData && languageData?.hasOwnProperty('only_alpaha_passrule_msg')
      ? languageData?.only_alpaha_passrule_msg
      : 'Only alphabets {a-z and A-Z}, numbers {0-9} and special characters {. - _  @} are allowed in a password. Do not use other characters.';

  const minLengthFour =
    languageData && languageData?.hasOwnProperty('min_length_is_four')
      ? languageData?.min_length_is_four
      : 'Minimum length is 4 characters!';

  const thisFieldIsRequired =
    languageData && languageData?.hasOwnProperty('this_field_is_required')
      ? languageData?.this_field_is_required
      : 'This field is required.';

  const changeYourPassMsg =
    languageData && languageData?.hasOwnProperty('change_password_msg')
      ? languageData?.change_password_msg
      : 'Change your password';

  const currentPassText =
    languageData && languageData?.hasOwnProperty('current_password')
      ? languageData?.current_password
      : 'Current password';

  const newPassText =
    languageData && languageData?.hasOwnProperty('new_password')
      ? languageData?.new_password
      : 'New password';

  const reEnterNewPass =
    languageData && languageData?.hasOwnProperty('retype_new_password')
      ? languageData?.retype_new_password
      : 'Re-enter your new password';

  const passwordNotMatchText =
    languageData && languageData?.hasOwnProperty('the_paswword_notmatch')
      ? languageData?.the_paswword_notmatch
      : 'Your password does not match.';

  const changePassNolineBreak =
    languageData && languageData?.hasOwnProperty('changepass_nolinebreak')
      ? languageData?.changepass_nolinebreak
      : 'Change Password';

  const yourAndConfirmPassNotMatch =
    languageData && languageData?.hasOwnProperty('the_pass_confirm_notmatch')
      ? languageData?.the_pass_confirm_notmatch
      : 'The password and confirmation password do not match.';

  const passChangedSuccessfully =
    languageData &&
    languageData?.hasOwnProperty('password_changed_successfully')
      ? languageData?.password_changed_successfully
      : 'Password successfully changed.';

  const incorrectPassLabel =
    languageData && languageData?.hasOwnProperty('incorrect_password')
      ? languageData?.incorrect_password
      : 'Incorrect Password!';

  const tryAgainBtnText =
    languageData &&
    languageData?.hasOwnProperty('btn') &&
    languageData?.btn?.hasOwnProperty('try_again')
      ? languageData?.btn?.try_again
      : 'Try Again';

  const uhHoText =
    languageData && languageData?.hasOwnProperty('uh_oh')
      ? languageData?.uh_oh
      : 'Uh-oh!';

  const askMathOrMindspark =
    languageData &&
    languageData?.hasOwnProperty('ask_anything_on_mathematics_or_mindspark')
      ? languageData?.ask_anything_on_mathematics_or_mindspark
      : 'Ask us anything about mathematics or Mindspark and we will reply to you. Your name, class and school details will be recorded when you send a message.';

  const playedGamesLabel =
    languageData && languageData?.hasOwnProperty('played_games')
      ? languageData?.played_games
      : 'Played games';

  const latestGamesLabel =
    languageData && languageData?.hasOwnProperty('lates_games')
      ? languageData?.lates_games
      : 'Latest games';

  const lockedGamesLabel =
    languageData && languageData?.hasOwnProperty('locked_games')
      ? languageData?.locked_games
      : 'Locked games';

  const noGamesAssignedText =
    languageData && languageData?.hasOwnProperty('no_game_assigned')
      ? languageData?.no_game_assigned
      : 'Oops. Level Zero! Finish a few topics to unlock a game.';

  const sessionTimedOut =
    languageData && languageData?.hasOwnProperty('session_time_over')
      ? languageData?.session_time_over
      : "You cannot attempt more topics or games today. But there's a lot more you can do on Mindspark, go explore!";

  const seeMore =
    languageData && languageData?.hasOwnProperty('show_more')
      ? languageData?.show_more
      : 'See more';

  const showQuestion =
    languageData && languageData?.hasOwnProperty('show_question')
      ? languageData?.show_question
      : 'Show Question';

  const hideQuestion =
    languageData && languageData?.hasOwnProperty('hide_question')
      ? languageData?.hide_question
      : 'Hide Question';

  const seeLess =
    languageData && languageData?.hasOwnProperty('show_less')
      ? languageData?.show_less
      : 'See less';

  const letsCompleteTopc =
    languageData && languageData?.hasOwnProperty('lets_complete_the_topic')
      ? languageData?.lets_complete_the_topic
      : "Let's complete the topic";

  const toUnlockGame =
    languageData && languageData?.hasOwnProperty('to_unlock_the_game')
      ? languageData?.to_unlock_the_game
      : 'to unlock this game.';

  const doneBtnText =
    languageData &&
    languageData?.hasOwnProperty('btn') &&
    languageData?.btn?.hasOwnProperty('done')
      ? languageData?.btn?.done
      : 'Done';

  const skipBtnText =
    languageData &&
    languageData?.hasOwnProperty('btn') &&
    languageData?.btn?.hasOwnProperty('skip')
      ? languageData?.btn?.skip
      : 'Skip';

  const submittedOnText =
    languageData && languageData?.hasOwnProperty('submitted_on')
      ? languageData?.submitted_on
      : 'Submitted on';

  const questionText =
    languageData && languageData?.hasOwnProperty('question')
      ? languageData?.question
      : 'Question';

  const questionTextPlural =
    languageData && languageData?.hasOwnProperty('questions_plural')
      ? languageData?.questions_plural
      : 'Questions';

  const dueOnText =
    languageData && languageData?.hasOwnProperty('due_on')
      ? languageData?.due_on
      : 'Due on';

  const wasdueOnText =
    languageData && languageData?.hasOwnProperty('due_on')
      ? languageData?.due_on
      : 'Was Due on';

  const worksheetSearchEmpty =
    languageData && languageData?.hasOwnProperty('worksheet_search_emptyState')
      ? languageData?.worksheet_search_emptyState
      : "Blank sheet! We couldn't find the Worksheet you're looking for. Make sure you're using the right spellings or ask an elder for help.";

  const worksheetEmptyState =
    languageData && languageData?.hasOwnProperty('worksheet_emptyState')
      ? languageData?.worksheet_emptyState
      : 'Worksheet absent! Ask your teacher to assign you a Worksheet.';

  const searchWorksheetText =
    languageData && languageData?.hasOwnProperty('search_worsheets')
      ? languageData?.search_worsheets
      : 'Search worksheets';

  const recentlyAnnouncedText =
    languageData && languageData?.hasOwnProperty('recently_announced')
      ? languageData?.recently_announced
      : 'Recently announced';

  const pleaseCompleteAllQuestion =
    languageData &&
    languageData?.hasOwnProperty('please_complete_all_questions')
      ? languageData?.please_complete_all_questions
      : 'Please complete all questions.';

  const worksheetLabel =
    languageData && languageData?.hasOwnProperty('worksheetlabel')
      ? languageData?.worksheetlabel
      : 'Worksheet';

  const effortLabel =
    languageData && languageData?.hasOwnProperty('effort_label')
      ? languageData?.effort_label
      : 'Effort';

  const higherLevelLabel =
    languageData && languageData?.hasOwnProperty('higherlevel_label')
      ? languageData?.higherlevel_label
      : 'Higher Level';

  const homeWorkLabel =
    languageData && languageData?.hasOwnProperty('homework_label')
      ? languageData?.homework_label
      : 'Homework';

  const reviseLabel =
    languageData && languageData?.hasOwnProperty('revise_label')
      ? languageData?.revise_label
      : 'Revise';

  const daLabel =
    languageData && languageData?.hasOwnProperty('da_label')
      ? languageData?.da_label
      : 'DA Question';

  const worksheetReportText =
    languageData && languageData?.hasOwnProperty('worksheet_report')
      ? languageData?.worksheet_report
      : 'Worksheet Report';

  const acuracyText =
    languageData && languageData?.hasOwnProperty('accuracy')
      ? languageData?.accuracy
      : 'Accuracy';

  const exerciseWiseSumaryText =
    languageData && languageData?.hasOwnProperty('exercise_wise_summary')
      ? languageData?.exercise_wise_summary
      : 'Exercise-wise Summary';

  const topicWiseSumaryText =
    languageData && languageData?.hasOwnProperty('topic_wise_summary')
      ? languageData?.topic_wise_summary
      : 'Topic-wise Summary';

  const filterNoQuestionFound =
    languageData && languageData?.hasOwnProperty('no_quetions_found_filter')
      ? languageData?.no_quetions_found_filter
      : "Uh-oh! Looks like there's nothing here.";

  const emptyNotificationText =
    languageData && languageData?.hasOwnProperty('no_notifications')
      ? languageData?.no_notifications
      : 'You do not have any notifications yet.';

  const homeWorkEmptyStateText =
    languageData && languageData?.hasOwnProperty('homework_absent')
      ? languageData?.homework_absent
      : 'Homework absent! Ask your teacher to assign you Homework.';

  const homeWorkSearchEmptyText =
    languageData && languageData?.hasOwnProperty('homework_search_notfound')
      ? languageData?.homework_search_notfound
      : "Blank sheet! We couldn't find the Homework you're looking for. Make sure you're using the right spellings or ask an elder for help.";

  const teacherCommentText =
    languageData && languageData?.hasOwnProperty('teacher_comments')
      ? languageData?.teacher_comments
      : 'Teacher comments';

  const noCommentsText =
    languageData && languageData?.hasOwnProperty('no_comments')
      ? languageData?.no_comments
      : 'No comments';

  const letsGoBtnText =
    languageData &&
    languageData?.hasOwnProperty('btn') &&
    languageData?.btn?.hasOwnProperty('lets_go')
      ? languageData?.btn?.lets_go
      : 'Let’s go!';

  const liveHomeworkTitleText =
    languageData && languageData?.hasOwnProperty('live_homework_title')
      ? languageData?.live_homework_title
      : 'Live \nHomework';

  const completeNowBtnText =
    languageData &&
    languageData?.hasOwnProperty('btn') &&
    languageData?.btn?.hasOwnProperty('complete_now')
      ? languageData?.btn?.complete_now
      : 'Complete now';

  const attemptWoksheetOnego =
    languageData && languageData?.hasOwnProperty('attempt_worksheet_onego')
      ? languageData?.attempt_worksheet_onego
      : 'Attempt\nWorksheet\nin one go!';

  const submitWorksheetModalText =
    languageData && languageData?.hasOwnProperty('submit_worksheet_modal_text')
      ? languageData?.submit_worksheet_modal_text
      : 'Submit\nWorksheet';

  const timedWorksheetoneGoMessage =
    languageData &&
    languageData?.hasOwnProperty('you_often_perform_timedworksheet_msg')
      ? languageData?.you_often_perform_timedworksheet_msg
      : 'You often perform better if you \n attempt timed worksheet in one go.';

  const worksheetWillBeSubmited =
    languageData && languageData?.hasOwnProperty('worksheet_will_be_sumbitted')
      ? languageData?.worksheet_will_be_sumbitted
      : 'Your worksheet will be submitted\n to the teacher.';

  const completeLaterBtnText =
    languageData &&
    languageData?.hasOwnProperty('btn') &&
    languageData?.btn?.hasOwnProperty('complete_later')
      ? languageData?.btn?.complete_later
      : 'Complete later';

  const timeLimitText =
    languageData && languageData?.hasOwnProperty('time_limit')
      ? languageData?.time_limit
      : 'Time Limit';

  const searchHomeWorkText =
    languageData && languageData?.hasOwnProperty('search_homework')
      ? languageData?.search_homework
      : 'Search Homework';

  const liveHwLabel =
    languageData && languageData?.hasOwnProperty('live_homework')
      ? languageData?.live_homework
      : 'Live homework';

  const completedHomeWorkLabel =
    languageData && languageData?.hasOwnProperty('completed_homework')
      ? languageData?.completed_homework
      : 'Completed homework';

  const seeOlderHwBtnText =
    languageData &&
    languageData?.hasOwnProperty('btn') &&
    languageData?.btn?.hasOwnProperty('see_older_homework')
      ? languageData?.btn?.see_older_homework
      : 'See older homework';

  const submitHwModalText =
    languageData && languageData?.hasOwnProperty('submit_hw_modal')
      ? languageData?.submit_hw_modal
      : 'Submit\n Homework';

  const hwWillBeSubmitedText =
    languageData && languageData?.hasOwnProperty('hw_will_be_submitted')
      ? languageData?.hw_will_be_submitted
      : 'Your homework will be submitted to the teacher.';

  const attemptHwInOneGo =
    languageData && languageData?.hasOwnProperty('attempt_homework_onego')
      ? languageData?.attempt_homework_onego
      : 'Attempt \nHomework \nin one go';

  const youPerformbetterInOneGo =
    languageData &&
    languageData?.hasOwnProperty('you_perfom_better_if_you_attempt_hw_in_onego')
      ? languageData?.you_perfom_better_if_you_attempt_hw_in_onego
      : 'You often perform better if you attempt homework in one go.';

  const subjectiveText =
    languageData && languageData?.hasOwnProperty('subjective')
      ? languageData?.subjective
      : 'Subjective';

  const vieReportBtnText =
    languageData &&
    languageData?.hasOwnProperty('btn') &&
    languageData?.btn?.hasOwnProperty('view_report')
      ? languageData?.btn?.view_report
      : 'View report';

  const homeWorkReportLabel =
    languageData && languageData?.hasOwnProperty('homework_report')
      ? languageData?.homework_report
      : 'Homework Report';

  const activeTopicsLabel =
    languageData && languageData?.hasOwnProperty('active_topics')
      ? languageData?.active_topics
      : 'Active topics';

  const otherTopicsLabel =
    languageData && languageData?.hasOwnProperty('other_topics')
      ? languageData?.other_topics
      : 'Other topics';

  const topicSearchEmptystateText =
    languageData && languageData?.hasOwnProperty('topic_search_emptystate')
      ? languageData?.topic_search_emptystate
      : "Blank sheet! We couldn't find the Topic you're looking for. Make sure you're using the right spellings or ask an elder for help.";

  const topicEmptyStateText =
    languageData && languageData?.hasOwnProperty('topic_emptystate')
      ? languageData?.topic_emptystate
      : 'Uh-oh! No topics here!';

  const thisIsSchoolDeviceBtnText =
    languageData &&
    languageData?.hasOwnProperty('btn') &&
    languageData?.btn?.hasOwnProperty('this_is_school_device')
      ? languageData?.btn?.this_is_school_device
      : 'This is a school device.';

  const priorityTopicActiveText =
    languageData && languageData?.hasOwnProperty('priority_topic_active')
      ? languageData?.priority_topic_active
      : 'A priority topic is active.';

  const youHaveToCompleteText =
    languageData && languageData?.hasOwnProperty('you_have_to_first_complete')
      ? languageData?.you_have_to_first_complete
      : 'You have to first complete';

  const toAccessTopicText =
    languageData && languageData?.hasOwnProperty('to_access_topic')
      ? languageData?.to_access_topic
      : 'to access this topic.';

  const hangOnText =
    languageData && languageData?.hasOwnProperty('hang_on')
      ? languageData?.hang_on
      : 'Hang on';

  const letsDoActiveTopicText =
    languageData &&
    languageData?.hasOwnProperty('lets_do_the_active_topics_now')
      ? languageData?.lets_do_the_active_topics_now
      : 'Let’s do the active topics now.';

  const lockedText =
    languageData && languageData?.hasOwnProperty('locked')
      ? languageData?.locked
      : 'Locked';

  const upgradeToGetAccess =
    languageData &&
    languageData?.hasOwnProperty('upgrade_to_full_version_to_access')
      ? languageData?.upgrade_to_full_version_to_access
      : 'This topic is locked. You need to upgrade to the full version to access this topic.';

  const searchTopicText =
    languageData && languageData?.hasOwnProperty('search_topics')
      ? languageData?.search_topics
      : 'Search topics';

  const unitCompleteText =
    languageData && languageData?.hasOwnProperty('units_completed')
      ? languageData?.units_completed
      : 'Units Completed';

  const attemptNoInitCapText =
    languageData && languageData?.hasOwnProperty('attempt_no_initcap')
      ? languageData?.attempt_no_initcap
      : 'attempt';

  const priorityLabel =
    languageData && languageData?.hasOwnProperty('priority_label')
      ? languageData?.priority_label
      : 'Priority';

  const secondAttemptText =
    languageData && languageData?.hasOwnProperty('second_attempt')
      ? languageData?.second_attempt
      : '2nd attempt';

  const thirdAttemptText =
    languageData && languageData?.hasOwnProperty('third_attempt')
      ? languageData?.third_attempt
      : '3rd attempt';

  const thAttemptText =
    languageData && languageData?.hasOwnProperty('th_attempt')
      ? languageData?.th_attempt
      : 'th attempt';

  const stAttemptText =
    languageData && languageData?.hasOwnProperty('st_attempt')
      ? languageData?.st_attempt
      : 'st attempt';

  const forAttemptNumber =
    languageData && languageData?.hasOwnProperty('for_attempt_no')
      ? languageData?.for_attempt_no
      : 'For attempt no.';

  const howIDidEmptyStateText =
    languageData && languageData?.hasOwnProperty('how_i_did_emptystate')
      ? languageData?.how_i_did_emptystate
      : 'Psssst. You are yet to start the race. Complete a few questions and come back!';

  const attemptText =
    languageData && languageData?.hasOwnProperty('attempt')
      ? languageData?.attempt
      : 'Attempt';

  const questionDoneText =
    languageData && languageData?.hasOwnProperty('questions_done')
      ? languageData?.questions_done
      : 'Question done';

  const conceptText =
    languageData && languageData?.hasOwnProperty('concept')
      ? languageData?.concept
      : 'Concept';

  const exitBtnText =
    languageData &&
    languageData?.hasOwnProperty('btn') &&
    languageData?.btn?.hasOwnProperty('exit')
      ? languageData?.btn?.exit
      : 'Exit';

  const attempTimedTestOneGo =
    languageData && languageData?.hasOwnProperty('attemp_timed_test_in_one_go')
      ? languageData?.attemp_timed_test_in_one_go
      : 'Attempt the \nTimed Test \nin one go';

  const youOftenPerformTimedTestOneGo =
    languageData &&
    languageData?.hasOwnProperty('you_often_perform_timedtest_msg')
      ? languageData?.you_often_perform_timedtest_msg
      : 'You often perform better if \nyou attempt a Timed Test in one go.';

  const timedTestText =
    languageData && languageData?.hasOwnProperty('timed_test')
      ? languageData?.timed_test
      : 'Timed\nTest';

  const tryAgainLaterText =
    languageData && languageData?.hasOwnProperty('try_again_next_time')
      ? languageData?.try_again_next_time
      : 'Try again next time!';

  const excellentWorkText =
    languageData && languageData?.hasOwnProperty('excellent_work')
      ? languageData?.excellent_work
      : 'Excellent Work!';

  const yourTimeText =
    languageData && languageData?.hasOwnProperty('your_time')
      ? languageData?.your_time
      : 'Your time:';

  const tryHarderText =
    languageData && languageData?.hasOwnProperty('try_harder')
      ? languageData?.try_harder
      : 'Try harder!';

  const youNeedToGetMoreQuetionRight =
    languageData &&
    languageData?.hasOwnProperty('you_neeed_more_question_right')
      ? languageData?.you_neeed_more_question_right
      : 'You need to get more questions right.';

  const timeUpText =
    languageData && languageData?.hasOwnProperty('time_up')
      ? languageData?.time_up
      : "Time's\nUp!";

  const timeWorkSheetOverText =
    languageData && languageData?.hasOwnProperty('time_up_msg')
      ? languageData?.time_up_msg
      : 'Your time for this worksheet is over.\nThe results will be out after the due date.';

  const okayBtnText =
    languageData && languageData?.hasOwnProperty('okay')
      ? languageData?.okay
      : 'Okay';

  const selectSolutionImage =
    languageData && languageData?.hasOwnProperty('select_solution_image')
      ? languageData?.select_solution_image
      : 'Select solution image';

  const maxSizeHwSolutionAllowed =
    languageData &&
    languageData?.hasOwnProperty('max_filesize_allowed_hw_solution')
      ? languageData?.max_filesize_allowed_hw_solution
      : 'Maximum size allowed is 5 MB.';

  const writeYourSolutionText =
    languageData && languageData?.hasOwnProperty('write_your_solution')
      ? languageData?.write_your_solution
      : 'Write your solution.';

  const uploadYourSolutionText =
    languageData && languageData?.hasOwnProperty('upload_your_solution')
      ? languageData?.upload_your_solution
      : 'Upload your solution.';

  const uploadBtnText =
    languageData &&
    languageData?.hasOwnProperty('btn') &&
    languageData?.btn?.hasOwnProperty('upload')
      ? languageData?.btn?.upload
      : 'Upload';

  const takePhotoBtnText =
    languageData &&
    languageData?.hasOwnProperty('btn') &&
    languageData?.btn?.hasOwnProperty('take_photo')
      ? languageData?.btn?.take_photo
      : 'Take photo';

  const youCanOnlyUploadOneImage =
    languageData &&
    languageData?.hasOwnProperty('remember_you_can_add_one_image')
      ? languageData?.remember_you_can_add_one_image
      : 'Remember, you can only upload one image per solution.';

  const sessionTimeOutModalText =
    languageData && languageData?.hasOwnProperty('session_timeout')
      ? languageData?.session_timeout
      : 'Session time out';

  const sessionTimeOutModalMsg =
    languageData && languageData?.hasOwnProperty('it_look_like_sessiontimeout')
      ? languageData?.it_look_like_sessiontimeout
      : 'You cannot attempt more topics or games. Please Login again to use Mindspark';

  const welcomeText =
    languageData && languageData?.hasOwnProperty('welcome')
      ? languageData?.welcome
      : 'Welcome';

  const lookslikeFirstTimeUseText =
    languageData && languageData?.hasOwnProperty('looks_like_you_are_msg')
      ? languageData?.looks_like_you_are_msg
      : 'Looks like you are using Mindspark for the first time on this device.';

  const markThisAsTrusted =
    languageData && languageData?.hasOwnProperty('mark_this_as_trusted')
      ? languageData?.mark_this_as_trusted
      : 'Mark this as a trusted device?';

  const skipNowBtnText =
    languageData &&
    languageData?.hasOwnProperty('btn') &&
    languageData?.btn?.hasOwnProperty('skip_now')
      ? languageData?.btn?.skip_now
      : 'Skip for now';

  const enterYourPhoneEmail =
    languageData && languageData?.hasOwnProperty('enter_parent_phone_email')
      ? languageData?.enter_parent_phone_email
      : 'Enter your parent’s phone number or email ID.';

  const enterYourPhoneEmailMsgText =
    languageData &&
    languageData?.hasOwnProperty('enter_phone_email_trusted_msg')
      ? languageData?.enter_phone_email_trusted_msg
      : 'Enter your parent’s phone number or email ID to mark this device as trusted.';

  const phoneEmaiPlaceHolder =
    languageData && languageData?.hasOwnProperty('phone_email_placeholder')
      ? languageData?.phone_email_placeholder
      : 'Parent’s email ID / phone number';

  const enterOtpText =
    languageData && languageData?.hasOwnProperty('enter_otp')
      ? languageData?.enter_otp
      : 'Enter the OTP';

  const wrongInformationText =
    languageData && languageData?.hasOwnProperty('wrong_information')
      ? languageData?.wrong_information
      : 'Wrong information?';

  const clickHereToChange =
    languageData && languageData?.hasOwnProperty('click_here_to_change')
      ? languageData?.click_here_to_change
      : 'Click here to change';

  const verifyBtnText =
    languageData &&
    languageData?.hasOwnProperty('btn') &&
    languageData?.btn?.hasOwnProperty('verify')
      ? languageData?.btn?.verify
      : 'Verify';

  const stillWaitingText =
    languageData && languageData?.hasOwnProperty('resend_otp')
      ? languageData?.resend_otp
      : 'Still waiting...\nTry resending the OTP';

  const autoDetectingOtpText =
    languageData && languageData?.hasOwnProperty('auto_detect_otp')
      ? languageData?.auto_detect_otp
      : 'Auto detecting the OTP...\nPlease wait';

  const resendBtnText =
    languageData &&
    languageData?.hasOwnProperty('btn') &&
    languageData?.btn?.hasOwnProperty('resend')
      ? languageData?.btn?.resend
      : 'Resend OTP';

  const verificationCompleteText =
    languageData && languageData?.hasOwnProperty('verification_complete')
      ? languageData?.verification_complete
      : 'Verification Complete';

  const youWillNotBeloggedOut =
    languageData && languageData?.hasOwnProperty('you_will_not_loggedout_msg')
      ? languageData?.you_will_not_loggedout_msg
      : 'You will not be automatically logged out from trusted devices.';

  const youCanRemoveInProfile =
    languageData &&
    languageData?.hasOwnProperty('you_can_remove_this_in_profile')
      ? languageData?.you_can_remove_this_in_profile
      : 'You can remove this device from the list of trusted devices from your profile page.';

  const viewExplanationLabel =
    languageData && languageData?.hasOwnProperty('view_explanation')
      ? languageData?.view_explanation
      : 'View explanation';

  const hideExplanationLabel =
    languageData && languageData?.hasOwnProperty('hide_explanation')
      ? languageData?.hide_explanation
      : 'Hide explanation';

  const yourAnswerLabel =
    languageData && languageData?.hasOwnProperty('your_answer')
      ? languageData?.your_answer
      : 'Your answer';

  const correctAnswerText =
    languageData && languageData?.hasOwnProperty('right_answer')
      ? languageData?.right_answer
      : 'Correct answer';

  const uploadedSolutionLabel =
    languageData && languageData?.hasOwnProperty('uploaded_solution')
      ? languageData?.uploaded_solution
      : 'Uploaded solution';

  const answerExplantaionLabel =
    languageData && languageData?.hasOwnProperty('answer_explantion')
      ? languageData?.answer_explantion
      : 'Answer explanation';

  const notAttemptedLabel =
    languageData && languageData?.hasOwnProperty('not_attempted')
      ? languageData?.not_attempted
      : 'Not attempted';

  const skippedLabel =
    languageData && languageData?.hasOwnProperty('skipped')
      ? languageData?.skipped
      : 'Skipped';

  const mindaSparkLabel =
    languageData && languageData?.hasOwnProperty('to_default')
      ? languageData?.to_default
      : 'Mindspark';

  const attachmentslabel =
    languageData && languageData?.hasOwnProperty('attachments')
      ? languageData?.attachments
      : 'Attachments';

  const toText =
    languageData && languageData?.hasOwnProperty('to')
      ? languageData?.to
      : 'To:';

  const appUpdateAlertMsg =
    languageData && languageData?.hasOwnProperty('app_update_alert')
      ? languageData?.app_update_alert
      : 'Your app has an update';

  const appInstallInstructionMsg =
    languageData && languageData?.hasOwnProperty('app_install_instruction')
      ? languageData?.app_install_instruction
      : 'Click below to install the app';

  const appInstallMSG =
    languageData && languageData?.hasOwnProperty('app_install_message')
      ? languageData?.app_install_message
      : 'app_install_message';

  const updateLabel =
    languageData && languageData?.hasOwnProperty('update')
      ? languageData?.update
      : 'Update';

  const laterLabel =
    languageData && languageData?.hasOwnProperty('later')
      ? languageData?.later
      : 'Later';

  const unableToOpenUrlText =
    languageData && languageData?.hasOwnProperty('unable_to_open_url')
      ? languageData?.unable_to_open_url
      : 'unable to open URL';

  const thisUserNotRegisteredText =
    languageData && languageData?.hasOwnProperty('this_username_notregistered')
      ? languageData?.this_username_notregistered
      : 'This username is not registered.\n Please create guest account to continue.';

  const helloLoginText =
    languageData && languageData?.hasOwnProperty('hello_login')
      ? languageData?.hello_login
      : 'Hello Login';

  const createGuestAccountText =
    languageData && languageData?.hasOwnProperty('create_guestaccount')
      ? languageData?.create_guestaccount
      : 'Create Guest Account';

  const startFreeTrialText =
    languageData && languageData?.hasOwnProperty('start_free_trial')
      ? languageData?.start_free_trial
      : 'Start a Free Trial';

  const register =
    languageData && languageData?.hasOwnProperty('register')
      ? languageData?.register
      : 'Register';

  const permissionToUseCamera =
    languageData && languageData?.hasOwnProperty('permission_to_use_camera')
      ? languageData?.permission_to_use_camera
      : 'Permission to use camera';

  const cameraPermissionMsg =
    languageData && languageData?.hasOwnProperty('pemission_camera_msg')
      ? languageData?.pemission_camera_msg
      : 'We need your permission to use your camera.';

  const snapText =
    languageData && languageData?.hasOwnProperty('snap')
      ? languageData?.snap
      : 'SNAP';

  const tapAnyWhereToClose =
    languageData && languageData?.hasOwnProperty('tap_anywhere_to_close')
      ? languageData?.tap_anywhere_to_close
      : 'Tap anywhere to close';

  const phoneNoLabel =
    languageData && languageData?.hasOwnProperty('telephone_number')
      ? languageData?.telephone_number
      : 'Phone number';

  const loggedOutText =
    languageData && languageData?.hasOwnProperty('logged_out')
      ? languageData?.logged_out
      : 'Logged Out';

  const sessionLoggedOutMsg =
    languageData && languageData?.hasOwnProperty('session_logged_out_msg')
      ? languageData?.session_logged_out_msg
      : 'You were logged out because you were inactive for a while or tried logging in from a different place.';

  const idleLoggedOutMsg =
    languageData && languageData?.hasOwnProperty('idle_logged_out_msg')
      ? languageData?.idle_logged_out_msg
      : 'You has been idle for long time?,Please login again to continue';

  const loginAgainBtnText =
    languageData &&
    languageData?.hasOwnProperty('btn') &&
    languageData?.btn?.hasOwnProperty('login_again')
      ? languageData?.btn?.login_again
      : 'Login Again';

  const noInternetLabel =
    languageData && languageData?.hasOwnProperty('no_internet')
      ? languageData?.no_internet
      : 'No internet';

  const notConnectedToNet =
    languageData && languageData?.hasOwnProperty('not_connected_to_network')
      ? languageData?.not_connected_to_network
      : 'Not connected to a network.';

  const forgotPasswordText =
    languageData && languageData?.hasOwnProperty('forgot_password')
      ? languageData?.forgot_password
      : 'Forgot your password?';

  const selectYourFavouriteAnimal =
    languageData && languageData?.hasOwnProperty('select_your_favorite_animal')
      ? languageData?.select_your_favorite_animal
      : 'Select your favourite animal';

  const selectYourFavouriteFruit =
    languageData && languageData?.hasOwnProperty('select_your_favorite_fruit')
      ? languageData?.select_your_favorite_fruit
      : 'Select your favourite fruit';

  const selectYourFavoriteFood =
    languageData && languageData?.hasOwnProperty('select_your_favorite_food')
      ? languageData?.select_your_favorite_food
      : 'Select your favourite food';

  const confirYourPasswodText =
    languageData && languageData?.hasOwnProperty('confirm_your_password')
      ? languageData?.confirm_your_password
      : 'Confirm your password';

  const newAndConfirmPassMissMatch =
    languageData &&
    languageData?.hasOwnProperty('newpassword_not_matching_confirmpassword')
      ? languageData?.newpassword_not_matching_confirmpassword
      : 'New password is not matching with confirm password.';

  const previousHintText =
    languageData &&
    languageData?.hasOwnProperty('btn') &&
    languageData?.btn?.hasOwnProperty('previous_hint')
      ? languageData?.btn?.previous_hint
      : 'Previous Hint';

  const nextHintText =
    languageData &&
    languageData?.hasOwnProperty('btn') &&
    languageData?.btn?.hasOwnProperty('next_hint')
      ? languageData?.btn?.next_hint
      : 'Next Hint';

  const showHintText =
    languageData &&
    languageData?.hasOwnProperty('btn') &&
    languageData?.btn?.hasOwnProperty('show_hint')
      ? languageData?.btn?.show_hint
      : 'Show Hint';

  const hintText =
    languageData &&
    languageData?.hasOwnProperty('btn') &&
    languageData?.btn?.hasOwnProperty('hint')
      ? languageData?.btn?.hint
      : 'Hint';

  const fillAllTheBoxes =
    languageData && languageData?.hasOwnProperty('fill_all_boxes')
      ? languageData?.fill_all_boxes
      : 'Fill all the boxes';

  const attemptedLabel =
    languageData && languageData?.hasOwnProperty('attempted_text')
      ? languageData?.attempted_text
      : 'Attempted';

  const totalLabel =
    languageData && languageData?.hasOwnProperty('total')
      ? languageData?.total
      : 'Total';

  const cancelLabel =
    languageData && languageData?.hasOwnProperty('cancel_text')
      ? languageData?.cancel_text
      : 'Cancel';

  const typeSomethingLabel =
    languageData && languageData?.hasOwnProperty('type_something')
      ? languageData?.type_something
      : 'Type something...';

  const progressLabel =
    languageData && languageData?.hasOwnProperty('progress')
      ? languageData?.progress
      : 'Progress';

  const removeLabel =
    languageData && languageData?.hasOwnProperty('remove_text')
      ? languageData?.remove_text
      : 'Remove';

  const rewardEmptyState =
    languageData && languageData?.hasOwnProperty('no_badge_availble')
      ? languageData?.no_badge_availble
      : 'Empty shelf! Start doing questions to win rewards and to see them displayed here. ';

  const notifiedTeacherToResetPass =
    languageData &&
    languageData?.hasOwnProperty('we_have_notified_teacher_for_password_reset')
      ? languageData?.we_have_notified_teacher_for_password_reset
      : 'We have notified your teacher to help you reset your password.';

  const parentDetailsMissing =
    languageData && languageData?.hasOwnProperty('parent_details_missing')
      ? languageData?.parent_details_missing
      : 'We do not have your parent’s contact details where we can send the OTP.';

  const useOtherOption =
    languageData && languageData?.hasOwnProperty('use_other_option')
      ? languageData?.use_other_option
      : 'Please use the other options this time, and do add your details once you are logged in.';

  const notifiedParentToResetPass =
    languageData &&
    languageData?.hasOwnProperty('we_have_notified_parent_for_password_reset')
      ? languageData?.we_have_notified_parent_for_password_reset
      : 'We have notified your parent to help you reset your password.';

  const teacherBtnText =
    languageData &&
    languageData?.hasOwnProperty('btn') &&
    languageData?.btn?.hasOwnProperty('teacher')
      ? languageData?.btn?.teacher
      : 'Teacher';

  const parentBtnText =
    languageData &&
    languageData?.hasOwnProperty('btn') &&
    languageData?.btn?.hasOwnProperty('parent')
      ? languageData?.btn?.parent
      : 'Parent';

  const notifiedMindsparkToResetPass =
    languageData &&
    languageData?.hasOwnProperty(
      'we_have_sent_mail_to_mindspark_for_password_reset',
    )
      ? languageData?.we_have_sent_mail_to_mindspark_for_password_reset
      : 'We have notified the Mindspark team to help you reset your password.';

  const weWillGetBackText =
    languageData && languageData?.hasOwnProperty('we_will_getback')
      ? languageData?.we_will_getback
      : 'We’ll get back to you in 24 hours.';

  const passResetApprovalExpiredText =
    languageData &&
    languageData?.hasOwnProperty('password_reset_approval_expired')
      ? languageData?.password_reset_approval_expired
      : 'Password reset approval has expired!';

  const passResetExpiredMessage =
    languageData &&
    languageData?.hasOwnProperty('whoops_ur_password_reset_expired_msg')
      ? languageData?.whoops_ur_password_reset_expired_msg
      : 'Whoops! Your approval to reset  your password has expired!';

  const requestAgainPlease =
    languageData &&
    languageData?.hasOwnProperty('request_again_to_create_new_password')
      ? languageData?.request_again_to_create_new_password
      : 'Please put a new request to create a new password.';

  const maxAttemptReachedText =
    languageData && languageData?.hasOwnProperty('you_have_reached_max_attempt')
      ? languageData?.you_have_reached_max_attempt
      : 'You have reached the maximum number of attempts to log in into your account';

  const youAlreadyHavePendingPassRequest =
    languageData &&
    languageData?.hasOwnProperty('you_already_have_reset_request_msg')
      ? languageData?.you_already_have_reset_request_msg
      : 'You already have a pending password reset request within the last 24 hours. Your teacher will help you with the password.';

  const resetPassLabel =
    languageData && languageData?.hasOwnProperty('reset_password')
      ? languageData?.reset_password
      : 'Reset using OTP';

  const askHelpFromYourLabel =
    languageData && languageData?.hasOwnProperty('ask_help_from')
      ? languageData?.ask_help_from
      : 'Ask help from your';

  const emailUsAtLabel =
    languageData && languageData?.hasOwnProperty('email_us_at')
      ? languageData?.email_us_at
      : 'Email us at';

  const orLabel =
    languageData && languageData?.hasOwnProperty('or')
      ? languageData?.or
      : 'or';

  const userNamePlaceHolder =
    languageData && languageData?.hasOwnProperty('Username')
      ? languageData?.Username
      : 'Username';

  const newToMindSparkText =
    languageData && languageData?.hasOwnProperty('new_to_mindspark')
      ? languageData?.new_to_mindspark
      : 'New to Mindspark?\n';

  const pleaseEnterValidOtp =
    languageData && languageData?.hasOwnProperty('please_enter_valid_otp')
      ? languageData?.please_enter_valid_otp
      : 'Please enter a valid OTP';

  const sentOtpToParent =
    languageData && languageData?.hasOwnProperty('we_have_sent_otp_to_parent')
      ? languageData?.we_have_sent_otp_to_parent
      : 'We have sent an OTP to your parent’s\nphone. Please enter it below to reset your password.';

  const yourSubEndedMsg =
    languageData && languageData?.hasOwnProperty('subscription_ended_msg')
      ? languageData?.subscription_ended_msg
      : 'Oh no! Your subscription period has ended! Mindspark really wants you back!';

  const cantOpenUrl =
    languageData && languageData?.hasOwnProperty('cant_open_url')
      ? languageData?.cant_open_url
      : "Can't open URl =>";

  const starredQuestionEmptyState =
    languageData && languageData?.hasOwnProperty('no_starred_question')
      ? languageData?.no_starred_question
      : 'No bookmarked questions. Click on the "star" next to questions to save the tough ones or your favourite ones here.';

  const messageEmptyState =
    languageData && languageData?.hasOwnProperty('messages_emptystate')
      ? languageData?.messages_emptystate
      : `It's pindrop silence here! Click on "Write a Message" to message Mindspark. We can help you with any issues or doubts you may have.`;

  const choseSubjectTxt =
    languageData && languageData?.hasOwnProperty('choose_subject')
      ? languageData?.choose_subject
      : 'Choose Subject';

  const preciousTreasureText =
    languageData && languageData?.hasOwnProperty('precious_treasures_text')
      ? languageData?.precious_treasures_text
      : 'Precious treasures lay ahead.\n Choose a subject to start the journey.';

  const mathsSubject =
    languageData && languageData?.hasOwnProperty('Mindspark')
      ? languageData?.Mindspark
      : 'Maths';

  const englishSubjectText =
    languageData && languageData?.hasOwnProperty('english')
      ? languageData?.english
      : 'English';

  const scienceSubjectText =
    languageData && languageData?.hasOwnProperty('science')
      ? languageData?.science
      : 'Science';

  const mathematicians_dont_give_up =
    languageData && languageData?.hasOwnProperty('mathematicians_dont_give_up')
      ? languageData?.mathematicians_dont_give_up
      : "Mathematicians don't give up, just like you! Let's continue learning.";

  const scientists_dont_give_up =
    languageData && languageData?.hasOwnProperty('scientists_dont_give_up')
      ? languageData?.scientists_dont_give_up
      : "Scientists don't give up, just like you! Let's continue learning.";

  const your_extra_effort_really_helped =
    languageData &&
    languageData?.hasOwnProperty('your_extra_effort_really_helped')
      ? languageData?.your_extra_effort_really_helped
      : 'This was quite a challenge, and your extra effort really helped. Great job!';
  const you_meet_all_challenges =
    languageData && languageData?.hasOwnProperty('you_meet_all_challenges')
      ? languageData?.you_meet_all_challenges
      : 'Superb! You meet all your challenges with a smile.';
  const great_to_see_you_work_hard =
    languageData && languageData?.hasOwnProperty('great_to_see_you_work_hard')
      ? languageData?.great_to_see_you_work_hard
      : "Brilliant! It's great to see you work hard at these challenges!";
  const you_can_do_better =
    languageData && languageData?.hasOwnProperty('you_can_do_better')
      ? languageData?.you_can_do_better
      : 'Read the answer carefully.';
  const ask_doubts_to_leave_behind =
    languageData && languageData?.hasOwnProperty('ask_doubts_to_leave_behind')
      ? languageData?.ask_doubts_to_leave_behind
      : 'Take time to read the answer.';
  const try_carefully_to_overcome =
    languageData && languageData?.hasOwnProperty('try_carefully_to_overcome')
      ? languageData?.try_carefully_to_overcome
      : 'Reading the answer will help you learn.';

  const doubleLoginHeader =
    languageData && languageData?.hasOwnProperty('double_login_header')
      ? languageData?.double_login_header
      : 'Oops! Logged-out due to double-login';

  const doubleLoginMessage =
    languageData && languageData?.hasOwnProperty('double_login_message')
      ? languageData?.double_login_message
      : 'Hey, we think you have logged into your account from a different place.';

  const doubleLoginNotYouMessage =
    languageData && languageData?.hasOwnProperty('double_login_not_you')
      ? languageData?.double_login_not_you
      : 'Not you? Login again and change your password now.';

  const goBackText =
    languageData && languageData?.hasOwnProperty('go_back_text')
      ? languageData?.go_back_text
      : 'Go Back';

  const students_find_this_tough =
    languageData && languageData?.hasOwnProperty('students_find_this_tough')
      ? languageData?.students_find_this_tough
      : "Many students find this tough, so don't worry. You will get another try on this challenge question. You will see the answer only after your second try.";

  const server_down_error =
    languageData && languageData?.hasOwnProperty('server_down_error')
      ? languageData?.server_down_error
      : 'Sorry, our servers are not available at the moment. Please try later.';

  const connectOnWhatsApp =
    languageData &&
    languageData?.hasOwnProperty('btn') &&
    languageData?.btn?.hasOwnProperty('connect_on_whatsApp')
      ? languageData?.btn?.connect_on_whatsApp
      : 'Connect on WhatsApp';

  const askADoubt =
    languageData && languageData?.hasOwnProperty('ask_a_doubt')
      ? languageData?.ask_a_doubt
      : 'Ask A Doubt';

  const feedback =
    languageData && languageData?.hasOwnProperty('feedback')
      ? languageData?.feedback
      : 'Feedback';

  const bookmarks =
    languageData && languageData?.hasOwnProperty('bookmarks')
      ? languageData?.bookmarks
      : 'Bookmarks';
  const bookmarkedQuestion =
    languageData && languageData?.hasOwnProperty('bookmarked_question')
      ? languageData?.bookmarked_question
      : 'Bookmarked Question';
  const changeSubject =
    languageData && languageData?.hasOwnProperty('change_subject')
      ? languageData?.change_subject
      : 'Change Subject';

  const trustedDeviceMaxLimitMsg =
    languageData && languageData?.hasOwnProperty('trusted_max_limit')
      ? languageData?.trusted_max_limit
      : 'You can only save user ID and password in 3 devices, this seems to be your 4th Device.';

  const helpNeedQuery =
    languageData && languageData?.hasOwnProperty('help_need_query')
      ? languageData.help_need_query
      : 'Send your query to';
  const helpNeedTimeQuery =
    languageData && languageData?.hasOwnProperty('help_need_query_time')
      ? languageData.help_need_query_time
      : 'Or call us on (Mon to Fri 10am to 6pm)';
  const About_mindspark =
    languageData && languageData?.hasOwnProperty('about_mindspark')
      ? languageData.about_mindspark
      : 'About Ei Mindspark';
  const minSpark_disc =
    languageData && languageData?.hasOwnProperty('minSpark_disc')
      ? languageData.minSpark_disc
      : "Ei Mindspark is a personalised learning platform that allows children to effectively advance at their own pace. Every day, Ei Mindspark delivers millions of questions, and the data collected is used to enhance the child's learning pathway.";
  const mindspark_disc2 =
    languageData && languageData?.hasOwnProperty('mindspark_disc2')
      ? languageData.mindspark_disc2
      : 'Independent evaluations by J-PAL, IDinsight and Gray Matters have demonstrated learning outcomes to improve dramatically.';

  //Future of minspark
  const Future1 =
    languageData && languageData?.hasOwnProperty('future1')
      ? languageData.future1
      : 'Question-based self-learning platform';
  const Future2 =
    languageData && languageData?.hasOwnProperty('future2')
      ? languageData.future2
      : 'Makes children independent learners';
  const Future3 =
    languageData && languageData?.hasOwnProperty('future3')
      ? languageData.future3
      : 'Adapts to each child’s individual learning pace';
  const Future4 =
    languageData && languageData?.hasOwnProperty('future4')
      ? languageData.future4
      : 'Intelligently curated questions';
  const Future5 =
    languageData && languageData?.hasOwnProperty('future5')
      ? languageData.future5
      : 'Fun-based learning with games and rewards';
  const Future6 =
    languageData && languageData?.hasOwnProperty('future6')
      ? languageData.future6
      : 'Detailed explanation after every question';
  const Future7 =
    languageData && languageData?.hasOwnProperty('future7')
      ? languageData.future7
      : 'Aligned to CBSE, ICSE & IGCSE syllabus';
  const Future8 =
    languageData && languageData?.hasOwnProperty('future8')
      ? languageData.future8
      : 'Topic-wise accuracy reports';
  const futureOfMindSpark =
    languageData && languageData?.hasOwnProperty('futureOfMindSpark')
      ? languageData.futureOfMindSpark
      : 'Features of Ei Mindspark';
  const ReviewedRecognised =
    languageData && languageData?.hasOwnProperty('ReviewedRecognised')
      ? languageData.ReviewedRecognised
      : 'Reviewed and Recognised by';

  const haveAnAccount =
    languageData && languageData?.hasOwnProperty('have_an_account')
      ? languageData?.have_an_account
      : 'Have an account?';

  const loginWithExistingMindsparkId =
    languageData &&
    languageData?.hasOwnProperty('login_with_existing_mindspark_id')
      ? languageData?.login_with_existing_mindspark_id
      : 'Login with Existing Mindspark Id';

  const yourSessionEndedText =
    languageData && languageData?.hasOwnProperty('your_session_ended')
      ? languageData?.your_session_ended
      : 'Oops! Your session has ended.';

  const yourSessionExpiryMsgText =
    languageData && languageData?.hasOwnProperty('your_session_ended_msg')
      ? languageData?.your_session_ended_msg
      : 'Hey, we think your session has expired or you have logged into your account from another device.\nNot you? Login again and change your password now.';

  const accuracyText =
    languageData && languageData?.hasOwnProperty('accuracy')
      ? languageData?.accuracy
      : 'Accuracy';
  const passagesAttemptedText =
    languageData && languageData?.hasOwnProperty('passages_attempted')
      ? languageData?.passages_attempted
      : 'Passages Attempted';
  const questionsAttemptedText =
    languageData && languageData?.hasOwnProperty('questions_attempted')
      ? languageData?.questions_attempted
      : 'Questions Attempted';

  const genericEmptyText =
    languageData && languageData?.hasOwnProperty('no_data_available')
      ? languageData?.no_data_available
      : 'No Data Available';

  const profileClass =
    languageData && languageData?.hasOwnProperty('profile_class')
      ? languageData?.profile_class
      : 'Class';

  const totalSparkieEarneText =
    languageData && languageData?.hasOwnProperty('total sparkies earned')
      ? languageData['total sparkies earned']
      : 'Total Sparkies earned';

  const myProfileText =
    languageData && languageData?.hasOwnProperty('my profile')
      ? languageData['my profile']
      : 'My Profile';

  const hoorayText =
    languageData && languageData?.hasOwnProperty('hooray')
      ? languageData?.hooray
      : 'Hooray!';
  const anotherAudioPlayingText =
    languageData && languageData?.hasOwnProperty('another_audio_playing_text')
      ? languageData?.another_audio_playing_text
      : 'Another Audio Is Already Playing';

  const invalidAudioFileText =
    languageData && languageData?.hasOwnProperty('invalidAudioFileText')
      ? languageData?.invalidAudioFileText
      : 'Invalid Audio File';

  const audioInitialisingText =
    languageData && languageData?.hasOwnProperty('audioInitialisingText')
      ? languageData?.audioInitialisingText
      : 'Audio Is Initilising ';

  const outOfUnitsCompleted =
    languageData && languageData?.hasOwnProperty('out_of_units_completed')
      ? languageData?.out_of_units_completed
      : '{{unitsCleared}} out of {{unitsOverall}} units completed';

  const outOfUnitCompleted =
    languageData && languageData?.hasOwnProperty('out_of_unit_completed')
      ? languageData?.out_of_unit_completed
      : '{{unitsCleared}} out of {{unitsOverall}} unit completed';
  const saveMyLoginIdAndPassword =
    languageData && languageData?.hasOwnProperty('saveMyLoginIdAndPassword')
      ? languageData?.saveMyLoginIdAndPassword
      : 'Save my login Id and password on this device';

  const activityTxt =
    languageData && languageData?.hasOwnProperty('activity')
      ? languageData?.activity
      : 'Activity';

  const UnitTxt =
    languageData && languageData?.hasOwnProperty('unit')
      ? languageData?.activity
      : 'Unit';

  const UnitsTxt =
    languageData && languageData?.hasOwnProperty('units')
      ? languageData?.activity
      : 'Units';

  const teacherLoginError =
    languageData && languageData?.hasOwnProperty('teacher_login_error')
      ? languageData?.teacher_login_error
      : 'Please login with a valid student user';

  return {
    doubleLoginHeader,
    doubleLoginMessage,
    doubleLoginNotYouMessage,
    worksheetText,
    challengeQuestionText,
    submitText,
    please_enter_the_answer_text,
    please_select_an_option_text,
    closeText,
    nextText,
    idontknowText,
    titleText,
    sessionIDText,
    viewQuestionText,
    readPassageText,
    error_in_audio_file_text,
    please_select_answer_text,
    some_answers_have_not_been_answered_text,
    audio_is_not_available_text,
    isTamilLang,
    yesbtnText,
    nobtnText,
    confirmationText,
    sessionEndingConfirmMessage,
    sparkiesText,
    worksheetBtnText,
    topicBtnText,
    gameBtnText,
    enterPasswordText,
    choosePasswordText,
    userNameText,
    loginBtnName,
    selectText,
    startText,
    continueText,
    seeReportText,
    liveWorksheetText,
    olderWorksheetText,
    completedText,
    emptyText,
    timeTakenText,
    sparkieEarnedText,
    youDidQuestionText,
    correctText,
    wrongText,
    howIdidText,
    gameEmptyText,
    homeWorkBtnText,
    profileProgressText,
    homeText,
    staredPularText,
    messageLabelText,
    leaderboardLabelText,
    rewardLabelText,
    profileLabelText,
    logoutLableText,
    starredSingularText,
    goHomeBtnText,
    attentionPleaseText,
    noCancelBtnText,
    yesRemoveBtnText,
    thisWillRemoveDescText,
    newMsgBtnText,
    searchMsgText,
    singularMsgText,
    pluralMsgText,
    singularMsgCapitalText,
    rateConvoText,
    typeReplyText,
    sendText,
    msgSuccessText,
    maxFileSizeText,
    maxFileErrorText,
    fileFormatErrorText,
    failedText,
    writeYourMsg,
    attachFileText,
    storagePermissionText,
    storageDescText,
    downloadFailedText,
    yourSection,
    yourCity,
    yourCountry,
    cityLeaderBoardEmpty,
    sectionLeaderBoardEmpty,
    worldLeaderBoardEmpty,
    sparkieEarned,
    sparkieLeaderBoard,
    badgesLabel,
    titleLabel,
    earnedBadges,
    ongoingBadges,
    upcomingBadges,
    viewAll,
    viewLess,
    level,
    applyBtnText,
    appliedBtnText,
    copyiedToClipboard,
    parentCodeText,
    totalSparkieEarneText,
    changePassBtnText,
    viewSubscriptionBtnText,
    imText,
    boyText,
    girlText,
    neutralText,
    nameText,
    classText,
    sectionText,
    schoolText,
    cityText,
    dobText,
    saveChangesBtnText,
    parent1Detail,
    parent2Detail,
    parent1Name,
    parent2Name,
    alphabetsAllowedText,
    parent1PhoneLabel,
    parent2PhoneLabel,
    validPhoneNumText,
    parent1EmailLabel,
    parent2EmailLabel,
    validEmailIDText,
    startDateText,
    endDateText,
    notifySetText,
    notificationPlural,
    soundText,
    trustedDevicesText,
    markAsTrustedProfile,
    thisDeviceText,
    markTrustedBtnText,
    myProfileText,
    subsciptionSingular,
    profileUpdatedText,
    trustedDeviceEmptyState,
    daysRemainingLabel,
    notYourPasswordMsg,
    somethingWentWrong,
    onlyAplhaPassRules,
    minLengthFour,
    thisFieldIsRequired,
    changeYourPassMsg,
    currentPassText,
    newPassText,
    reEnterNewPass,
    passwordNotMatchText,
    changePassNolineBreak,
    yourAndConfirmPassNotMatch,
    passChangedSuccessfully,
    incorrectPassLabel,
    tryAgainBtnText,
    uhHoText,
    askMathOrMindspark,
    playedGamesLabel,
    latestGamesLabel,
    lockedGamesLabel,
    noGamesAssignedText,
    sessionTimedOut,
    seeMore,
    seeLess,
    letsCompleteTopc,
    toUnlockGame,
    doneBtnText,
    skipBtnText,
    submittedOnText,
    questionText,
    dueOnText,
    worksheetSearchEmpty,
    worksheetEmptyState,
    searchWorksheetText,
    recentlyAnnouncedText,
    pleaseCompleteAllQuestion,
    worksheetLabel,
    effortLabel,
    higherLevelLabel,
    homeWorkLabel,
    reviseLabel,
    daLabel,
    worksheetReportText,
    questionTextPlural,
    acuracyText,
    exerciseWiseSumaryText,
    topicWiseSumaryText,
    filterNoQuestionFound,
    emptyNotificationText,
    homeWorkEmptyStateText,
    homeWorkSearchEmptyText,
    teacherCommentText,
    letsGoBtnText,
    liveHomeworkTitleText,
    noCommentsText,
    completeNowBtnText,
    attemptWoksheetOnego,
    submitWorksheetModalText,
    timedWorksheetoneGoMessage,
    worksheetWillBeSubmited,
    completeLaterBtnText,
    timeLimitText,
    searchHomeWorkText,
    liveHwLabel,
    completedHomeWorkLabel,
    seeOlderHwBtnText,
    submitHwModalText,
    hwWillBeSubmitedText,
    attemptHwInOneGo,
    youPerformbetterInOneGo,
    subjectiveText,
    vieReportBtnText,
    homeWorkReportLabel,
    activeTopicsLabel,
    otherTopicsLabel,
    topicSearchEmptystateText,
    topicEmptyStateText,
    thisIsSchoolDeviceBtnText,
    priorityTopicActiveText,
    youHaveToCompleteText,
    toAccessTopicText,
    hangOnText,
    letsDoActiveTopicText,
    searchTopicText,
    unitCompleteText,
    attemptNoInitCapText,
    priorityLabel,
    secondAttemptText,
    thirdAttemptText,
    thAttemptText,
    stAttemptText,
    howIDidEmptyStateText,
    attemptText,
    questionDoneText,
    conceptText,
    exitBtnText,
    attempTimedTestOneGo,
    youOftenPerformTimedTestOneGo,
    timedTestText,
    tryAgainLaterText,
    excellentWorkText,
    yourTimeText,
    tryHarderText,
    youNeedToGetMoreQuetionRight,
    timeUpText,
    okayBtnText,
    timeWorkSheetOverText,
    selectSolutionImage,
    maxSizeHwSolutionAllowed,
    writeYourSolutionText,
    uploadYourSolutionText,
    uploadBtnText,
    takePhotoBtnText,
    youCanOnlyUploadOneImage,
    sessionTimeOutModalText,
    sessionTimeOutModalMsg,
    welcomeText,
    lookslikeFirstTimeUseText,
    markThisAsTrusted,
    skipNowBtnText,
    enterYourPhoneEmail,
    enterYourPhoneEmailMsgText,
    phoneEmaiPlaceHolder,
    enterOtpText,
    wrongInformationText,
    clickHereToChange,
    verifyBtnText,
    stillWaitingText,
    autoDetectingOtpText,
    resendBtnText,
    verificationCompleteText,
    youWillNotBeloggedOut,
    youCanRemoveInProfile,
    viewExplanationLabel,
    hideExplanationLabel,
    yourAnswerLabel,
    correctAnswerText,
    uploadedSolutionLabel,
    answerExplantaionLabel,
    notAttemptedLabel,
    skippedLabel,
    mindaSparkLabel,
    attachmentslabel,
    toText,
    appUpdateAlertMsg,
    appInstallInstructionMsg,
    appInstallMSG,
    updateLabel,
    laterLabel,
    unableToOpenUrlText,
    thisUserNotRegisteredText,
    helloLoginText,
    createGuestAccountText,
    startFreeTrialText,
    register,
    permissionToUseCamera,
    cameraPermissionMsg,
    snapText,
    tapAnyWhereToClose,
    phoneNoLabel,
    loggedOutText,
    sessionLoggedOutMsg,
    loginAgainBtnText,
    noInternetLabel,
    notConnectedToNet,
    forgotPasswordText,
    selectYourFavouriteAnimal,
    selectYourFavouriteFruit,
    selectYourFavoriteFood,
    forAttemptNumber,
    confirYourPasswodText,
    newAndConfirmPassMissMatch,
    previousHintText,
    nextHintText,
    showHintText,
    hintText,
    fillAllTheBoxes,
    attemptedLabel,
    totalLabel,
    cancelLabel,
    typeSomethingLabel,
    progressLabel,
    removeLabel,
    rewardEmptyState,
    notifiedTeacherToResetPass,
    notifiedParentToResetPass,
    teacherBtnText,
    parentBtnText,
    notifiedMindsparkToResetPass,
    weWillGetBackText,
    passResetApprovalExpiredText,
    passResetExpiredMessage,
    requestAgainPlease,
    maxAttemptReachedText,
    youAlreadyHavePendingPassRequest,
    resetPassLabel,
    askHelpFromYourLabel,
    emailUsAtLabel,
    orLabel,
    userNamePlaceHolder,
    newToMindSparkText,
    pleaseEnterValidOtp,
    sentOtpToParent,
    yourSubEndedMsg,
    cantOpenUrl,
    starredQuestionEmptyState,
    messageEmptyState,
    choseSubjectTxt,
    preciousTreasureText,
    mathsSubject,
    englishSubjectText,
    scienceSubjectText,
    mathematicians_dont_give_up,
    scientists_dont_give_up,
    your_extra_effort_really_helped,
    you_meet_all_challenges,
    great_to_see_you_work_hard,
    you_can_do_better,
    ask_doubts_to_leave_behind,
    try_carefully_to_overcome,
    parentDetailsMissing,
    useOtherOption,
    goBackText,
    students_find_this_tough,
    upgradeToGetAccess,
    lockedText,
    profileCompleteText,
    server_down_error,
    connectOnWhatsApp,
    askADoubt,
    bookmarks,
    bookmarkedQuestion,
    feedback,
    learnBtnText,
    topicsText,
    accuracyText,
    questionsAttemptedText,
    passagesAttemptedText,
    viewPassageText,
    viewWordMeaningText,
    prevText,
    genericEmptyText,
    profileClass,
    hoorayText,
    anotherAudioPlayingText,
    invalidAudioFileText,
    audioInitialisingText,
    workSheetNumberOfQuestions,
    outOfUnitsCompleted,
    outOfUnitCompleted,
    viewMapText,
    changeSubject,
    helpNeedQuery,
    helpNeedTimeQuery,
    About_mindspark,
    minSpark_disc,
    mindspark_disc2,
    Future1,
    Future2,
    Future3,
    Future4,
    Future5,
    Future6,
    Future7,
    Future8,
    futureOfMindSpark,
    ReviewedRecognised,
    haveAnAccount,
    loginWithExistingMindsparkId,
    trustedDeviceMaxLimitMsg,
    idleLoggedOutMsg,
    yourSessionEndedText,
    yourSessionExpiryMsgText,
    wasdueOnText,
    showQuestion,
    hideQuestion,
    saveMyLoginIdAndPassword,
    activityTxt,
    UnitTxt,
    UnitsTxt,
    teacherLoginError,
  };
};
