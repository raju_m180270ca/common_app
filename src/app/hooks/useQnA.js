/**
 * This is common QnA logic which is shared between NAANDI and EDICINE Question screens
 * Change logic here will affect all screens
 *
 */

import React, {useState, useEffect, useContext, useRef} from 'react';
import {View, findNodeHandle, Image, Platform} from 'react-native';
import {useNavigation} from '@react-navigation/native';
import {useStores} from '@mobx/hooks';
// import base64 from 'react-native-base64';
import {Base64} from 'js-base64';
import moment from 'moment';
import {Toast} from 'native-base';
import {runInAction, toJS} from 'mobx';
import ImagePicker from 'react-native-image-picker';
import * as mime from 'react-native-mime-types';
import {
  QHtmlTemplateForIframe,
  MyAutoHeightWebView,
  MCQ,
  BLANK,
  Explanation,
  MultiSelectMCQ,
  Ordering,
  MatchList,
  Classification,
  SortList,
  BLANK_DROPDOWN,
  InteractiveN,
  Game,
  DROPDOWN_BUTTON,
  SourceSansProRegTextView,
  BalooThambiRegTextView,
  CustomTextInput,
  TextInteraction,
} from '@components';
import {
  getWp,
  getHp,
  createValidURL,
  checkForAudio,
  getAsValue,
  replaceString,
} from '@utils';
import {useSound, useLanguage} from '@hooks';
import {ApiEndPoint, ContentIDs} from '@constants';
import {API} from '@api';
import {
  SessionTimeOutDialog,
  TimedWorksheetModal,
  TimeTestModal,
  HomeworkSolutionImageModal,
  HomeworkAttentionModal,
  RewardCollectionModal,
} from '@hoc';
import {TouchableOpacity} from 'react-native-gesture-handler';
import {CameraWhite, Upload, ErrorClose} from '@images';
import homeworkStyle from '../screens/homework/HomeworkQnAScreen/style';
import getHtmlTemplate from '@utils/getHtmlTemplate';
import {AuthContext} from '@contexts/auth-context';
import {MixpanelCategories, MixpanelEvents, MixpanelActions} from '@constants';
import ImageResizer from 'react-native-image-resizer';

const SEARCH = 'SEARCH';
const SCREEN_TEST = 'SCREEN_TEST';
const WORKSHEET = 'WORKSHEET';
const TOPICS = 'TOPICS';
const WORKSHEET_EDICINE = 'WORKSHEET_EDICINE';
const HOMEWORK_EDICINE = 'HOMEWORK_EDICINE';
const DISCRETE_SKILL = 'DISCRETE_SKILL';

export const useQnA = (screenType = SEARCH) => {
  const navigation = useNavigation();
  const store = useStores();
  const {
    playSound,
    initializeAudioSection,
    audioCleanup,
    showInsStVO,
    showQuesVO,
    qBodyVoiceOver,
    stopAudio,
  } = useSound();
  const {qnaStore, uiStore} = store;
  const [lockOptions, setLockOptions] = useState(false);
  const [selectedMcq, setSelectedMcq] = useState({});
  const [startTime, setStartTime] = useState(moment());
  const [enableScroll, setEnableScroll] = useState(false);
  const [isSubmitClicked, setIsSubmitClicked] = useState(false);
  const [isVisible, showLoaderVisbility] = useState(false);
  const [inputResponse, setInputResponse] = useState({});
  // const [selectedChoice, setSelectedChoice] = useState(-1);
  const [skipQuestion, setskipQuestion] = useState(false);
  const [showTimeTestModal, setShowTimeTestModal] = useState(false);
  const [showSubmitPopup, setShowSubmitPopup] = useState(false);
  const [showClosePopup, setShowClosePopup] = useState(false);
  const [showTimesUp, setShowTimesUp] = useState(false);
  const [disableWebView, setDisableWebView] = useState(false);
  const [rewardModal, setRewardModal] = useState(false);
  const [rewardItem, setRewardItem] = useState({});
  const [solutionUploadError, setSolutionUploadError] = useState('');
  const [iframeSubmitStatus, setIframeSubmitStatus] = useState(2);
  const auth = useContext(AuthContext);

  const {
    tryAgainLaterText,
    excellentWorkText,
    yourTimeText,
    tryHarderText,
    youNeedToGetMoreQuetionRight,
    timeUpText,
    okayBtnText,
    timeWorkSheetOverText,
    selectSolutionImage,
    maxSizeHwSolutionAllowed,
    writeYourSolutionText,
    uploadYourSolutionText,
    uploadBtnText,
    takePhotoBtnText,
    youCanOnlyUploadOneImage,
    please_enter_the_answer_text,
    please_select_an_option_text,
    your_extra_effort_really_helped,
    you_meet_all_challenges,
    great_to_see_you_work_hard,
    you_can_do_better,
    ask_doubts_to_leave_behind,
    try_carefully_to_overcome,
    students_find_this_tough,
    liveWorksheetText,
    letsGoBtnText,
    workSheetNumberOfQuestions,
    please_select_answer_text,
    mathematicians_dont_give_up,
    scientists_dont_give_up
  } = useLanguage();

  const isRTL = uiStore.isRTL;

  //Create references
  const questionRef = useRef();
  const interactiveRef = useRef(null);
  const webref = useRef(null);
  const scrollViewRef = useRef();
  const parentScrollRef = useRef();
  const viewRef = useRef(null);
  const timerRef = useRef();

  useEffect(() => {
    console.log(`Running QnA Custom hook>>>>>>`);
    (async () => {
      await reset();
    })();
    return () => {};
  }, []);
  useEffect(() => {
    if (
      qnaStore?.trialCount == 1 &&
      qnaStore?.currentQuestion?.hints?.length > 0
    ) {
      qnaStore.showHint();
    }
  }, [qnaStore]);
  useEffect(() => {
    let errorHideTimer = null;
    if (
      solutionUploadError !== null &&
      solutionUploadError !== '' &&
      solutionUploadError !== undefined
    ) {
      errorHideTimer = setTimeout(() => {
        setSolutionUploadError('');
      }, 3000);
    }

    return () => {
      errorHideTimer && clearTimeout(errorHideTimer);
    };
  }, [solutionUploadError]);

  const reset = async () => {
    return new Promise(resolve => {
      qnaStore.reset();
      setEnableScroll(true);
      setIsSubmitClicked(false);
      setStartTime(moment());
      setInputResponse({});
      // setSelectedChoice(-1);
      setLockOptions(false);
      setShowSubmitPopup(false);
      setShowClosePopup(false);
      setShowTimesUp(false);
      setDisableWebView(false);
      audioCleanup();
      resolve();
    });
  };

  const trueSelectionMcq = data => {
    console.log('DATA Selected True:', JSON.stringify(data));
    runInAction(() => {
      qnaStore.inputData = [{value: data.index}];
    });
    //setLockOptions(true);
    if (screenType === HOMEWORK_EDICINE || screenType === WORKSHEET_EDICINE) {
      setLockOptions(false);
    } else {
      setLockOptions(true);
    }

    // Evaluate Answer
    console.log('TRUE SELECTION SELECTED:', data.index);
    data.correct = true;
    data.score =
      qnaStore.currentQuestion?.responseValidation &&
      qnaStore.currentQuestion?.responseValidation?.validResponse &&
      qnaStore.currentQuestion?.responseValidation?.validResponse?.score
        ? qnaStore.currentQuestion?.responseValidation?.validResponse?.score
        : 1;

    setSelectedMcq(data);
    if (
      screenType != SCREEN_TEST &&
      screenType != WORKSHEET_EDICINE &&
      screenType !== HOMEWORK_EDICINE
    ) {
      let response = questionRef.current.evaluteAnswer(data);
      createSubmitRequest(response);
    } else {
      uiStore.setLoader(false);
    }
  };

  const falseSelectionMcq = data => {
    console.log('DATA Selected False:', JSON.stringify(data));
    runInAction(() => {
      qnaStore.inputData = [{value: data.index}];
    });
    if (SCREEN_TEST)
      if (screenType === HOMEWORK_EDICINE || screenType === WORKSHEET_EDICINE) {
        setLockOptions(false);
      } else {
        setLockOptions(true);
      }

    console.log('False SELECTION SELECTED:', data.index);
    data.correct = false;
    data.score =
      qnaStore.currentQuestion?.responseValidation &&
      qnaStore.currentQuestion?.responseValidation?.unscored
        ? qnaStore.currentQuestion?.responseValidation?.unscored
        : 0;

    setSelectedMcq(data);
    if (
      screenType != SCREEN_TEST &&
      screenType != WORKSHEET_EDICINE &&
      screenType !== HOMEWORK_EDICINE
    ) {
      let response = questionRef.current.evaluteAnswer(data);
      createSubmitRequest(response);
    } else {
      uiStore.setLoader(false);
    }
  };

  const createSubmitRequest = async submitData => {
    if (submitData || screenType === HOMEWORK_EDICINE) {
      let resultDecoded = Base64.decode(submitData?.result);
      if (qnaStore?.passageData && qnaStore?.passageData?._id) {
        submitData.contentID = qnaStore?.passageData?.contentID;
        submitData.childContentID = qnaStore?.currentQuestion?.contentID;

        submitData.contentInfo.contentID = qnaStore?.passageData?.contentID;
        submitData.contentInfo.childContentID =
          qnaStore?.contentData?.childContentId;
        submitData.contentInfo.contentVersionID = qnaStore?.passageData?._id;
        submitData.contentInfo.childContentVersionID =
          qnaStore?.currentQuestion?._id;
        submitData.contentInfo.childContentType =
          qnaStore?.contentData?.childContentType;
        submitData.contentInfo.groupType = qnaStore?.contentData?.contentType;
        submitData.contentInfo.revisionNum = qnaStore?.passageData?.revisionNo;
        submitData.contentInfo.childRevisionNum =
          qnaStore.currentQuestion?.revisionNo;
      }

      submitData.timeTaken = moment().diff(startTime, 'seconds');
      submitData.pedagogyID = qnaStore?.contentHeaderInfo?.pedagogyID;
      submitData.userAttemptData.hintTaken =
        screenType == SCREEN_TEST ? false : qnaStore.isHintVisible;
      submitData.userAttemptData.trialCount = qnaStore.trialCount;
      submitData.contentSeqNum = qnaStore.contentData?.contentSeqNum;
      if (submitData.userAttemptData && submitData.userAttemptData.trials) {
        submitData.userAttemptData.trials.map(item => {
          item.timeTaken = moment().diff(startTime, 'seconds');
        });
      }

      submitData.mode = qnaStore.contentData.contentSubMode;
      submitData.contentSubMode = qnaStore.contentData.contentSubMode;
      if (
        qnaStore.contentData.contentSubMode === 'remediation' ||
        qnaStore.contentData.contentSubMode === 'challenge'
      ) {
        submitData.mode = 'learn';
      }

      if (screenType != SCREEN_TEST) {
        if (resultDecoded === 'pass') {
          runInAction(() => {
            qnaStore.isAnswerCorrect = true;
            qnaStore.showDefaultBuddy = false;
            if (qnaStore.isTimeTest) {
              qnaStore.correctAnswerCount = qnaStore.correctAnswerCount + 1;
            }
          });
        } else {
          runInAction(() => {
            qnaStore.isAnswerCorrect = false;
            qnaStore.showDefaultBuddy = false;
          });
        }
      }

      /**
       * Check Trials
       */
      let count = qnaStore.trialCount - 1;
      console.log(`COunt >>>>>${count}`);
      qnaStore.setTrials(count);
      if (
        screenType != SCREEN_TEST &&
        screenType !== HOMEWORK_EDICINE &&
        resultDecoded !== 'pass' &&
        count > 0
      ) {
        uiStore.setLoader(false);
        qnaStore.setDisableBTn(false);
        // Toast.show({
        //   text: `WRONG ANSWER, You have ${count} trial left.`,
        //   buttonText: 'Okay',
        //   duration: 5000,
        // });

        auth.showToast({
          title: '',
          description: `WRONG ANSWER, You have ${count} trial left.`,
          bgcolor: 'lightpink',
          timeout: 5000,
        });

        let lengthVal = 0;
        if (qnaStore.currentQuestion.hints != null) {
          console.log('test1');
          lengthVal = qnaStore.currentQuestion.hints.length;
          if (count > 0 && lengthVal > 0) {
            console.log('\n\nhintQA1\n', qnaStore.isHintVisible);
            qnaStore.showHint();
            console.log('\n\nhintQA2\n', qnaStore.isHintVisible);
          }
        }

        return;
      }

      /**
       * Ends
       */

      /**
       * Worksheet EDICINE Timed related
       */
      if (qnaStore.timed) {
        submitData.remainingTime = qnaStore.timed
          ? qnaStore.contentHeaderInfo?.remainingTime - submitData.timeTaken
          : 0;
        submitData.nextContentSeqNum =
          qnaStore.nextContentSeqNum !== 0
            ? qnaStore.nextContentSeqNum
            : qnaStore.contentHeaderInfo.pedagogyProgress.totalUnits ===
              qnaStore.contentData.contentSeqNum
            ? 1
            : qnaStore.contentData.contentSeqNum + 1;
      }

      /**
       * Ends
       */

      /**
       * Time test related
       */
      console.log(`cmg before is time test`);
      if (qnaStore.isTimeTest) {
        if (qnaStore.questionIndex !== qnaStore.timeTextQuestions.length - 1) {
          console.log('\nbefore saving');
          setLockOptions(false);
          uiStore.setLoader(false);
          qnaStore.saveTimeTestResponseAndShowNext(submitData);
          qnaStore.setDisableBTn(false);
          console.log('saved');
          return;
        } else if (
          qnaStore.questionIndex ===
          qnaStore.timeTextQuestions.length - 1
        ) {
          qnaStore.saveTimeTestResponse(submitData);
          var timeTaken = timerRef.current?.pause();
          qnaStore.setTimeTaken(qnaStore.timeTaken + timeTaken - 3);
          console.log('Time Taken', qnaStore.timeTaken);
          setShowTimeTestModal(true);
          console.log('completed');
          return;
        }
      }
      /**
       * Ends
       */

      // Edicine Homework Related
      if (screenType === HOMEWORK_EDICINE) {
        submitData.nextContentSeqNum = qnaStore?.nextContentSeqNum;
        let uploadedData = [null];
        if (qnaStore.homeworkSolutionAttachment !== null) {
          uploadedData = [
            {
              uri: qnaStore?.homeworkSolutionAttachment?.uri,
              fileName: qnaStore?.homeworkSolutionAttachment?.fileName,
            },
          ];
        }
        const textInteractionData = {};
        textInteractionData.type = 'TextInteraction';
        textInteractionData.userAnswer = qnaStore?.homeworkSolution;

        if (submitData != null && submitData.userResponse !== null) {
          submitData.userResponse.uploads = uploadedData;
          submitData.userResponse.textInteraction = textInteractionData;
        }

        if (submitData.userAttemptData && submitData.userAttemptData.trials) {
          submitData.userAttemptData.trials.map(item => {
            item.timeTaken = moment().diff(startTime, 'seconds');
            if (item?.userResponse !== null) {
              item.userResponse.uploads = uploadedData;
              item.userResponse.textInteraction = textInteractionData;
            }
          });
        }
      }

      // End

      console.log('Submit Data --->>>' + JSON.stringify(submitData));
      if (screenType != SCREEN_TEST) {
        setDisableWebView(true);
      }

      const reqBody = {
        jwt: await getAsValue('jwt'),
        store: store,
        body: submitData,
      };
      if (screenType === HOMEWORK_EDICINE) {
        if (
          qnaStore.homeworkSolutionAttachment !== null &&
          qnaStore?.homeworkSolutionAttachment?.uri &&
          (qnaStore?.homeworkSolutionAttachment?.uri.indexOf('https') === -1 ||
            qnaStore?.homeworkSolutionAttachment?.uri.indexOf('http') === -1)
        ) {
          callUpdateSolutionAPI(reqBody);
        } else {
          callSubmitHomeworkQuestionAPI(reqBody);
        }
      }
      if (screenType == WORKSHEET || screenType == WORKSHEET_EDICINE) {
        callSubmitQuestionAttemptAPI(reqBody);
      }
      if (screenType == SCREEN_TEST) {
        callSubmitWorkSheetAPI(reqBody);
      }
      if (screenType == SEARCH) {
        uiStore.setLoader(false);
        runInAction(() => {
          qnaStore.showExplanation = true;
        });
      }
      if (screenType == TOPICS) {
        /**messages for challenge questions */

        let passMessage = [
          your_extra_effort_really_helped,
          you_meet_all_challenges,
          great_to_see_you_work_hard,
        ];

        if (qnaStore.currentQuestion.encrypted) {
          reqBody.body.result = resultDecoded;
          let showExplantionFlag =
            qnaStore.contentData.contentSubMode === 'challenge' &&
            qnaStore.contentHeaderInfo.pedagogyMessages[0] ==
              'challengeAttempt1'
              ? false
              : true;
          if (
            resultDecoded == 'pass' &&
            qnaStore.contentData.contentSubMode === 'challenge'
          ) {
            showExplantionFlag = true;
            // Toast.show({
            //   text: passMessage[Math.floor(Math.random() * passMessage.length)],
            //   duration: 8000,
            //   buttonText: 'OK',
            // });
            auth.showToast({
              title: '',
              description:
                passMessage[Math.floor(Math.random() * passMessage.length)],
              bgcolor: 'lightgreen',
              timeout: 8000,
            });
          } else {
            if (
              qnaStore.contentData.contentSubMode === 'challenge' &&
              qnaStore.contentHeaderInfo.pedagogyMessages[0] ==
                'challengeAttempt1'
            ) {
              showExplantionFlag = false;
              // Toast.show({
              //   text: students_find_this_tough,
              //   duration: 8000,
              //   buttonText: 'OK',
              // });
              auth.showToast({
                title: '',
                description: students_find_this_tough,
                bgcolor: 'lightgreen',
                timeout: 8000,
              });
            }
            if (
              qnaStore.contentData.contentSubMode === 'challenge' &&
              qnaStore.contentHeaderInfo.pedagogyMessages[0] ==
                'challengeAttempt2'
            ) {
              showExplantionFlag = true;
              var secondFailMessage = [
                you_can_do_better,
                ask_doubts_to_leave_behind,
                try_carefully_to_overcome,
              ];
              const subjectName = await getAsValue('subjectName');
              if (subjectName && subjectName === 'Science') {
                secondFailMessage.push(scientists_dont_give_up);
              } else {
                secondFailMessage.push(mathematicians_dont_give_up);
              }

              auth.showToast({
                title: '',
                description:
                  secondFailMessage[
                    Math.floor(Math.random() * secondFailMessage.length)
                  ],
                bgcolor: 'lightgreen',
                timeout: 8000,
              });
            }
          }
          callSubmitTopicQuestionAttemptAPI(reqBody, showExplantionFlag);
        }
      }

      if (screenType == DISCRETE_SKILL) {
        callSubmitDiscreteSkillQuestionAttemptAPI(reqBody);
      }
    } else {
      uiStore.setLoader(false);
      qnaStore.setDisableBTn(false);
      // Toast.show({
      //   text: please_select_answer_text,
      //   duration: 5000,
      //   buttonText: 'OK',
      // });
      auth.showToast({
        title: '',
        description: please_select_answer_text,
        bgcolor: 'lightpink',
        timeout: 5000,
      });
    }
  };

  const callUpdateSolutionAPI = async submitData => {
    try {
      const requestBody = new FormData();
      requestBody.append('solutionimage', {
        uri: qnaStore?.homeworkSolutionAttachment?.uri,
        name: qnaStore?.homeworkSolutionAttachment?.fileName,
        type: mime.lookup(qnaStore?.homeworkSolutionAttachment?.fileName),
      });
      requestBody.append('attemptID', qnaStore?.contentData?.attemptId);

      const req = {
        store: store,
        body: requestBody,
      };

      const response = await API(ApiEndPoint.UPLOAD_HOMEWORK_SOLUTION, req);
      if (response.data.resultCode === 'C001') {
        const uploadedData = [
          {
            uri: response?.data?.imagepath,
            fileName: response?.data?.imagename,
          },
        ];
        if (
          submitData != null &&
          submitData.body &&
          submitData.body.userResponse !== null
        ) {
          submitData.body.userResponse.uploads = uploadedData;
        }

        if (
          submitData.body.userAttemptData &&
          submitData.body.userAttemptData.trials
        ) {
          submitData.body.userAttemptData.trials.map(item => {
            item.timeTaken = moment().diff(startTime, 'seconds');
            if (item?.userResponse !== null) {
              item.userResponse.uploads = uploadedData;
            }
          });
        }
        callSubmitHomeworkQuestionAPI(submitData);
      } else {
        store.uiStore.apiErrorInit({
          code: response?.status,
          message: response?.data?.resultMessage,
        });
      }
    } catch (e) {
      console.log('Update Solution API Error', e);
    }
  };

  const callSubmitHomeworkQuestionAPI = async request => {
    try {
      const req = {
        body: request.body,
        store: store,
      };
      console.log(
        'frop_down screen-req.body ************************************ ',
        req.body,
      );
      const response = await API(ApiEndPoint.SUBMIT_HOMEWORK_QUESTIONS, req);
      if (response?.data?.resultCode === 'C001') {
        if (screenType == HOMEWORK_EDICINE) {
          await reset();
          if (showClosePopup) {
            navigation.goBack();
            return;
          }
          qnaStore.init(response.data);
          updateInputResponse();
        } else {
          if (
            response?.data?.contentData?.hasOwnProperty('contentParams') &&
            response?.data?.contentData?.contentParams &&
            response?.data?.contentData?.contentParams?.userAttemptData &&
            response?.data?.contentData?.contentParams?.userAttemptData?.hasOwnProperty(
              'userResponse',
            )
          ) {
            runInAction(() => {
              qnaStore.showExplanation = true;
              qnaStore.isLastQuestion = true;
            });
            qnaStore.showNextBtn();
          } else if (response?.data?.contentData?.length === 0) {
            runInAction(() => {
              qnaStore.showExplanation = true;
              qnaStore.isLastQuestion = true;
            });
            navigation.navigate('HomeworkSummaryScreen');
          } else {
            setskipQuestion(response?.data?.contentData?.skipQuestion);
            qnaStore.setNextQuestionData(response.data);
            runInAction(() => {
              qnaStore.showExplanation = true;
            });
            qnaStore.showNextBtn();
          }
        }
      } else if (
        response?.data?.resultCode === 'C004' &&
        response?.data?.redirectionCode === 'ContentPage'
      ) {
        if (screenType == HOMEWORK_EDICINE) {
          navigation.replace('HomeworkListScreen');
        } else {
          navigation.replace('HomeworkSummaryScreen');
        }
      } else {
        store.uiStore.apiErrorInit({
          code: response.status,
          message: response.data?.resultMessage,
        });
      }
    } catch (ex) {
      console.log('Submit Homweork API Error', eX);
    }
  };

  const csInit = q => {
    let data = q.data;
    // data[0].conditions = [];
    const run = `
    initContentService(${JSON.stringify(data)});
    `;
    console.log('SS initContentService');
    if (interactiveRef.current) {
      interactiveRef.current.injectJavaScript(run);
    }
    if (webref.current) {
      webref.current.injectJavaScript(run);
    }
  };

  const csCheckIframe = () => {
    console.log('anime coming inside cscHECKiFRame');
    try {
      console.log('cs Check Iframe');
      console.log('\n\nDATA:' + JSON.stringify(qnaStore.contentData));
      console.log('\n\nInput Data:' + JSON.stringify(qnaStore.inputData));
      let data = qnaStore.contentData.data;
      // data[0].conditions = [];
      csInit(qnaStore.contentData);
      setTimeout(() => {
        const run = ` 
          checkIframe();
        `;
        interactiveRef.current.injectJavaScript(run);
      }, 500);
    } catch (err) {
      Toast.show({text: 'ERROR CSCHECK '});
    }
  };

  const csEvaluateAnswer = (type, identifier) => {
    console.log('anime coming inside csEvaluateasnwer');
    console.log('\n\nDATA:' + JSON.stringify(qnaStore.contentData.data));
    console.log('\n\nInput Data:' + JSON.stringify(qnaStore.inputData));
    console.log('\n\nEVALUATION TYPE:' + type);
    let data = qnaStore.contentData.data;
    //TODO:Added this for check
    // data[0].conditions = [];

    if (type === 'Interactive') {
      console.log('SS Evaluate Answer:Interactive');
      const run = `
      evaluateAnswer(${JSON.stringify(data)},${null},${JSON.stringify({
        id: identifier,
      })});
      `;
      interactiveRef.current.injectJavaScript(run);
    } else if (type === 'dontknow') {
      console.log('SS Evaluate Answer:Dontknow');
      const run = `
      evaluateAnswer(${JSON.stringify(data)},${null},${JSON.stringify({
        id: identifier,
      })});
      `;
      webref.current.injectJavaScript(run);
    } else {
      console.log('SS Evaluate Answer:Others');
      const run = `
      evaluateAnswer(${JSON.stringify(data)},${JSON.stringify(
        qnaStore.inputData,
      )},${JSON.stringify({id: identifier})} );
      `;
      webref.current.injectJavaScript(run);
    }
  };

  const submitFunction = () => {
    console.log('TRIAL LEFT here:' + qnaStore.trialCount);
    setEnableScroll(true);
    if (qnaStore.currentQuestion.template === 'Interactive') {
      // isSubmitClicked = true;
      setIsSubmitClicked(true);
      csCheckIframe();
    } else if (qnaStore.currentQuestion.template === 'MCQ') {
      if (qnaStore.inputData.length === 0) {
        if (
          screenType == WORKSHEET_EDICINE ||
          screenType === HOMEWORK_EDICINE
        ) {
          dontKnow(false);
        } else {
          uiStore.setLoader(false);
          qnaStore.setDisableBTn(false);
          // Toast.show({
          //   text: please_select_an_option_text,
          //   duration: 5000,
          //   buttonText: 'OK',
          // });
          auth.showToast({
            title: '',
            description: please_select_an_option_text,
            bgcolor: 'lightpink',
            timeout: 5000,
          });
          return;
        }
      } else if (qnaStore.currentQuestion?.multiResponse) {
        let count = qnaStore.trialCount - 1;
        qnaStore.setTrials(count);
        const submitData = questionRef.current.evaluteAnswer(
          qnaStore?.inputData,
        );
        createSubmitRequest(submitData);
      } else {
        let response = questionRef.current.evaluteAnswer(selectedMcq);
        createSubmitRequest(response);
      }
    } else if (
      qnaStore.currentQuestion.template === 'Blank' ||
      qnaStore.currentQuestion.template === 'Blank_Dropdown' ||
      qnaStore.currentQuestion.template === 'Dropdown'
    ) {
      console.log(
        'Total Answers:',
        Object.keys(qnaStore.currentQuestion.response),
      );
      let allBlanksAnswered = true;
      let ipData = [];

      Object.keys(qnaStore.currentQuestion.response).forEach(key => {
        if (
          !inputResponse.hasOwnProperty(key) ||
          (inputResponse.hasOwnProperty(key) && inputResponse[key] === '')
        ) {
          const correctAnswers = Base64.decode(
            qnaStore.currentQuestion.response[key].correctAnswers,
          );
          if (
            qnaStore.currentQuestion.template == 'Blank' &&
            (correctAnswers.includes('0') || correctAnswers.includes(''))
          ) {
            ipData.push({name: key, value: ''});
          } else {
            allBlanksAnswered = false;
          }
          allBlanksAnswered = false;
        } else {
          ipData.push({name: key, value: inputResponse[key]});
        }
      });

      if (allBlanksAnswered) {
        runInAction(() => {
          qnaStore.inputData = ipData;
        });

        csInit(qnaStore.contentData);
        showLoaderVisbility(true);
        setTimeout(() => {
          csEvaluateAnswer('blank', 'blank');
        }, 500);
        console.log('InputData:', JSON.stringify(ipData));
        setInputResponse({});
        // if (qnaStore.isTimeTest) {
        //   setInputResponse({});
        // }
      } else {
        if (
          screenType == WORKSHEET_EDICINE ||
          screenType === HOMEWORK_EDICINE
        ) {
          dontKnow(false);
        } else {
          uiStore.setLoader(false);
          qnaStore.setDisableBTn(false);
          // Toast.show({
          //   text: please_enter_the_answer_text,
          //   buttonText: 'Okay',
          //   duration: 10000,
          // });
          auth.showToast({
            title: '',
            description: please_enter_the_answer_text,
            bgcolor: 'lightpink',
            timeout: 5000,
          });

          return;
        }
      }
    } else if (qnaStore?.currentQuestion?.template === 'TextInteraction') {
      if (hasValidUserResponse()) {
        const submitData = questionRef.current.evaluteAnswer(
          qnaStore?.inputData,
        );
        createSubmitRequest(submitData);
      } else {
        dontKnow(false);
      }
    } else {
      const submitData = questionRef.current.evaluteAnswer(qnaStore?.inputData);
      createSubmitRequest(submitData);
    }
  };

  const hasValidUserResponse = () => {
    let isValidResponse = false;
    if (
      qnaStore?.homeworkSolution &&
      typeof qnaStore?.homeworkSolution !== 'undefined' &&
      qnaStore?.homeworkSolution !== ''
    ) {
      isValidResponse = true;
    }

    if (
      qnaStore?.homeworkSolutionAttachment &&
      typeof qnaStore?.homeworkSolutionAttachment !== 'undefined'
    ) {
      isValidResponse = true;
    }

    return isValidResponse;
  };

  const onWebViewMessage = async event => {
    var iframeResponse = null;
    console.log(
      'Message received from webview:::',
      JSON.parse(event.nativeEvent.data),
    );

    let msgData;
    let count;
    try {
      msgData = JSON.parse(event.nativeEvent.data);
      console.log('---', msgData);

      if (msgData.hasOwnProperty('type') && msgData.type === 'IframeResponse') {
        iframeResponse = msgData.response;
        console.log('iframeResponse--->' + iframeResponse[0]);
        if (
          isSubmitClicked ||
          iframeResponse[0] == '1' ||
          iframeResponse[0] == '0'
        ) {
          setIsSubmitClicked(false);
          // isSubmitClicked = false;
          // console.log(`This is Iframe 0 val>>>>>${iframeResponse[0]}`);
          switch (iframeResponse[0]) {
            case '0':
              setIframeSubmitStatus(0);
              count = qnaStore.trialCount - 1;
              qnaStore.setTrials(qnaStore.trialCount - 1);
              if (screenType != SCREEN_TEST) {
                if (count > 0) {
                  uiStore.setLoader(false);
                  qnaStore.setDisableBTn(false);
                  // Toast.show({
                  //   text: `WRONG ANSWER, You have ${count} trial left.`,
                  //   buttonText: 'Okay',
                  //   duration: 2000,
                  // });
                  auth.showToast({
                    title: '',
                    description: `WRONG ANSWER, You have ${count} trial left.`,
                    bgcolor: 'lightpink',
                    timeout: 5000,
                  });
                  let lengthVal = 0;
                  if (qnaStore.currentQuestion.hints != null) {
                    console.log('test1');
                    lengthVal = qnaStore.currentQuestion.hints.length;
                    if (count > 0 && lengthVal > 0) {
                      console.log('\n\nhintQA1\n', qnaStore.isHintVisible);
                      qnaStore.showHint();
                      console.log('\n\nhintQA2\n', qnaStore.isHintVisible);
                    }
                  }
                  return;
                }
              }

              csEvaluateAnswer('Interactive', 'interactive');
              break;
            case '1':
              count = qnaStore.trialCount - 1;
              qnaStore.setTrials(count);
              csEvaluateAnswer('Interactive', 'interactive');
              setIframeSubmitStatus(1);
              break;
            case '2':
              uiStore.setLoader(false);
              qnaStore.setDisableBTn(false);
              // Toast.show({
              //   text: please_enter_the_answer_text,
              // });
              auth.showToast({
                title: '',
                description: please_enter_the_answer_text,
                bgcolor: 'lightpink',
                timeout: 5000,
              });

              break;
            default:
              uiStore.setLoader(false);
              break;
          }
        }
      } else if (
        msgData.hasOwnProperty('type') &&
        msgData.hasOwnProperty('function') &&
        msgData.type === 'ContentService'
      ) {
        switch (msgData.function) {
          case 'evaluateAnswer':
            // console.log('Received evaluateAnswer=', msgData.evaluatedResult);
            let submitData = msgData.submitData;

            // console.log('submitData>>>>>>>>>>>', submitData);
            if (msgData.id === 'idontknow') {
              console.log('GOT IDONT KNOW');
              submitData.result = Base64.encode('skip');
            } else {
              if (iframeSubmitStatus != 2) {
                submitData.result = Base64.encode(
                  iframeSubmitStatus == 1 ? 'pass' : submitData.result,
                );
                setIframeSubmitStatus(2);
              } else {
                submitData.result = Base64.encode(submitData.result);
              }
            }

            if (screenType != SCREEN_TEST) {
              if (msgData.evaluatedResult) {
                if (msgData.evaluatedResult) {
                  //setEvaluatedResult(msgData.evaluatedResult);
                  if (
                    msgData.evaluatedResult.alertMessage &&
                    msgData.evaluatedResult.alertMessage != '' &&
                    qnaStore.userAttempt == 1
                  ) {
                    // let count = qnaStore.trialCount - 1;
                    // qnaStore.setTrials(count);

                    let userAttempt = qnaStore.userAttempt + 1;
                    qnaStore.updateUserAttempt(userAttempt);
                    qnaStore.setDisableBTn(false);

                    auth.showToast({
                      title: '',
                      description: msgData.evaluatedResult.alertMessage,
                      bgcolor: 'lightgreen',
                      timeout: 8000,
                    });

                    return;
                  }
                } else {
                  let lengthVal = 0;
                  if (qnaStore.currentQuestion.hints != null) {
                    lengthVal = qnaStore.currentQuestion.length;
                  }

                  if (qnaStore.trialCount === 1 && lengthVal > 0) {
                    qnaStore.hideHint();
                  }
                }
              }

              let userAttempt = qnaStore.userAttempt + 1;
              qnaStore.updateUserAttempt(userAttempt);

              // runInAction(() => {
              //   qnaStore.showExplanation = true;
              // });

              // uiStore.setLoader(false);
            }
            createSubmitRequest(submitData);
            break;
        }
      } else {
        let key = Object.keys(msgData)[0];
        let val = msgData[key];
        let newResponse = inputResponse;
        newResponse[key] = val;
        setInputResponse(newResponse);
        // uiStore.setLoader(false);
        console.log('NEW RESPONSE>', inputResponse);
      }
    } catch (err) {
      console.warn(err);
      return;
    }
  };

  const renderCSHtmlView = () => {
    let content = QHtmlTemplateForIframe('<p></p>', getWp('1'), store);
    return (
      <MyAutoHeightWebView
        ref={webref}
        onMessage={onWebViewMessage}
        style={{
          width: getWp('1'),
          height: getWp('1'),
        }}
        source={{html: content}}
        zoomable={false}
      />
    );
  };

  const dragAndDropCallback = isLongPressed => {
    if (isLongPressed) {
      setEnableScroll(false);
      // parentScrollRef.current.scrollEnabled = false;
      scrollViewRef.current.scrollEnabled = false;
      console.log(`Disabling the scroll`);
      // setTimeout(() => {
      //   // setEnableScroll(true);
      //   console.log('Enabling the scroll after 10 seconds');
      //   // parentScrollRef.current.scrollEnabled = true;
      //   scrollViewRef.current.scrollEnabled = true;
      // }, 10000);
    } else {
      // parentScrollRef.current.scrollEnabled = true;
      scrollViewRef.current.scrollEnabled = true;
      console.log('Enabling the scroll');
      setEnableScroll(true);
    }
  };

  const classificationDragAndDrop = isLongPressed => {
    if (isLongPressed) {
      setEnableScroll(false);
      // parentScrollRef.current.scrollEnabled = false;
      scrollViewRef.current.scrollEnabled = false;
      setTimeout(() => {
        setEnableScroll(true);
        console.log('Enabling the scroll after 3 seconds');
        // parentScrollRef.current.scrollEnabled = true;
        scrollViewRef.current.scrollEnabled = true;
      }, 10000);
    } else {
      setEnableScroll(true);
      // parentScrollRef.current.scrollEnabled = true;
      scrollViewRef.current.scrollEnabled = true;
      console.log('Enabling the scroll');
    }
  };

  const setInputResponseHandler = (id, value) => {
    setInputResponse(prevState => {
      let updState = {
        ...prevState,
        [id]: value,
      };
      return updState;
    });
  };

  const renderQuestionsItem = templateType => {
    console.log('renderQuestionsItem=====>' + templateType);
    switch (templateType) {
      case 'MCQ':
        if (
          qnaStore?.multiResponse === 'true' ||
          qnaStore?.multiResponse === true
        ) {
          return (
            <MultiSelectMCQ
              ref={questionRef}
              selectedChoice={qnaStore?.inputData}
              setSelectedChoices={data => {
                console.log('DATA Selected True:', JSON.stringify(data));
                runInAction(() => {
                  qnaStore.inputData = data;
                });
                // setSelectedChoice(data);
                qnaStore.setSelectedChoice(data);
              }}
              questionData={qnaStore.currentQuestion}
              dragCallback={() => {}}
              onSoundBtnClicked={data => {
                playSound('multiSelectMCQVO', createValidURL(data));
              }}
            />
          );
        } else {
          return (
            <MCQ
              ref={questionRef}
              lockOptions={lockOptions}
              selectedChoice={qnaStore.selectedChoice}
              setSelectedChoice={data => {
                qnaStore.setSelectedChoice(data);
              }}
              isScreeningTest={
                screenType == SCREEN_TEST
                  ? true
                  : false
              }
              isWorkOrHomeWorkTest = {
                screenType == WORKSHEET_EDICINE ||
                screenType == HOMEWORK_EDICINE ? true : false
              }
              questionData={qnaStore.currentQuestion}
              submitQuestion={async () => {
                submitFunction();
              }}
              trueSelectionMcq={data => trueSelectionMcq(data)}
              falseSelectionMcq={data => falseSelectionMcq(data)}
              dragCallback={status => {
                // setEnableScroll(status);
              }}
            />
          );
        }
      case 'Blank':
        return (
          <BLANK
            webref={webref}
            questionRes={qnaStore.contentData}
            onWebViewCallback={onWebViewMessage}
            showHint={
              screenType == SCREEN_TEST || screenType == WORKSHEET_EDICINE
                ? false
                : qnaStore.isHintVisible
            }
            dragCallback={dragAndDropCallback}
            userResponse={qnaStore.userResponse}
            disableWebView={disableWebView}
          />
        );
      case 'Blank_Dropdown':
        return (
          <BLANK_DROPDOWN
            webref={webref}
            questionRes={qnaStore.contentData}
            onWebViewCallback={onWebViewMessage}
            showHint={
              screenType == SCREEN_TEST || screenType == WORKSHEET_EDICINE
                ? false
                : qnaStore.isHintVisible
            }
            userResponse={qnaStore.userResponse}
            disableWebView={disableWebView}
            setInputResponse={setInputResponseHandler}
          />
        );
      case 'Dropdown':
        return (
          // <DROPDOWN
          //   questionRes={qnaStore.contentData}
          //   onWebViewCallback={onWebViewMessage}
          //   showHint={
          //     screenType == SCREEN_TEST || screenType == WORKSHEET_EDICINE
          //       ? false
          //       : qnaStore.isHintVisible
          //   }
          //   userResponse={qnaStore.userResponse}
          // />
          <DROPDOWN_BUTTON
            webref={webref}
            questionRes={qnaStore.contentData}
            onWebViewCallback={onWebViewMessage}
            showHint={
              screenType == SCREEN_TEST || screenType == WORKSHEET_EDICINE
                ? false
                : qnaStore.isHintVisible
            }
            setInputResponse={setInputResponseHandler}
            userResponse={qnaStore.userResponse}
            disableWebView={disableWebView}
          />
        );
      case 'Interactive':
        return (
          <InteractiveN
            interactiveRef={interactiveRef}
            onWebViewCallback={onWebViewMessage}
            showHint={
              screenType == SCREEN_TEST ? false : qnaStore.isHintVisible
            }
            dragCallback={dragAndDropCallback}
            questionRes={qnaStore.contentData}
            disableWebView={disableWebView}
          />
        );
      case 'Ordering':
        return (
          <Ordering
            questionTree={qnaStore.currentQuestion}
            ref={questionRef}
            onSoundBtnClicked={playSound}
            dragAndDropCallback={dragAndDropCallback}
          />
        );
      case 'MatchList':
        return (
          <MatchList
            questionTree={qnaStore.currentQuestion}
            ref={questionRef}
            dragAndDropCallback={dragAndDropCallback}
          />
        );
      case 'Classification':
        return (
          <Classification
            questionTree={qnaStore.currentQuestion}
            ref={questionRef}
            dragAndDropCallback={classificationDragAndDrop}
          />
        );
      case 'SortList':
        return (
          <SortList
            questionTree={qnaStore.currentQuestion}
            ref={questionRef}
            dragAndDropCallback={dragAndDropCallback}
            setEnableScroll={() => {}}
          />
        );

      case 'Game':
      case 'Remedial':
        return (
          <View style={{alignSelf: 'center', flex: 1, marginTop: getHp(80)}}>
            <Game
              image={qnaStore.currentQuestion.thumbImg}
              title={qnaStore.currentQuestion.name}
              onPress={() => {
                if (screenType == 'SEARCH') {
                  navigation.navigate('GamePlayArenaScreen', {
                    file: qnaStore?.currentQuestion?.file,
                  });
                } else {
                  callOpenActivity();
                }
              }}
              isActive={true}
            />
          </View>
        );
      case 'TextInteraction':
        return (
          <TextInteraction
            ref={questionRef}
            questionData={qnaStore?.currentQuestion}
          />
        );
    }
  };

  const scrollToExplanation = () => {
    // console.log('HEIGHT BEFORE:', this.state.scrollViewHeight);

    setTimeout(() => {
      try {
        requestAnimationFrame(() => {
          //   console.log('VIEW REF:', viewRef.current);
          //   console.log('SCROLL REF:', scrollViewRef.current);
          if (viewRef.current && scrollViewRef.current) {
            viewRef.current.measureLayout(
              findNodeHandle(scrollViewRef.current),
              (x, y) => {
                //console.log('EXPLANATION NODE:', y);
                scrollViewRef.current.scrollTo({x: 0, y: y, animated: true});
              },
            );
          } else {
            console.log('REF ERROR scrollToExplanation>>');
          }
        });
      } catch (e) {
        // this.SCROLLVIEW_REF.scrollToEnd();
      }
    }, 200);
  };

  const renderExplanation = () => {
    try {
      console.log(`Render expln flag in store >>>>${qnaStore.showExplanation}`);
      if (qnaStore.showExplanation) {
        let explanation = '';
        if (
          qnaStore.currentQuestion.explanation &&
          qnaStore.currentQuestion.explanation !== ''
        ) {
          let explanationText = qnaStore.currentQuestion.explanation;
          if (qnaStore.currentQuestion.encrypted) {
            explanationText = Base64.decode(
              qnaStore.currentQuestion.explanation,
            );
          }
          explanationText = checkForAudio(explanationText);
          const isMath = explanationText.indexOf('<equ>');
          let isIframe = false;
          if (explanationText.indexOf('<iframe') > -1) {
            isIframe = true;
          }

          if (isMath > 0 || isIframe) {
            explanation = QHtmlTemplateForIframe(explanationText);
          } else {
            explanation = getHtmlTemplate(
              explanationText,
              false,
              isRTL,
              null,
              false,
              true,
            );
          }
        }
        let explainationAudio =
          qnaStore.currentQuestion?.hasOwnProperty('explVoiceover') &&
          qnaStore.currentQuestion?.explVoiceover;

        setTimeout(() => {
          scrollToExplanation();
          //console.log('Ran Scroll:', selectedMcq);
        }, 200);

        return (
          <View ref={viewRef}>
            <Explanation
              type={qnaStore.currentQuestion.template}
              explanation={explanation}
              isCorrect={qnaStore.isAnswerCorrect}
              mcqData={selectedMcq}
              audioData={explainationAudio}
              response={qnaStore.currentQuestion}
              onSoundBtnClicked={data => {
                playSound('explantionVO', createValidURL(data));
              }}
            />
          </View>
        );
      }
    } catch (err) {
      console.log('ERROR IN RENDER EXPLANATION:', err);
    }
  };

  const renderTimeoutDialog = () => {
    if (store.uiStore.sessionExceeded === true) {
      store.loginStore.setSkipOnBoardingScreen(true);
      return (
        <>
          <SessionTimeOutDialog
            onPress={() => {
              setAsValue('jwt', '');
              store.uiStore.reset();
              store.loginStore.setIsAuth(false);
            }}
          />
        </>
      );
    }
  };

  const quitWorksheets = async () => {
    let req = {
      body: {
        worksheetID: qnaStore?.contentHeaderInfo?.pedagogyID,
      },
      store: store,
    };
    let res = await API(ApiEndPoint.QUIT_WORKSHEET, req);
    uiStore.setLoader(false);
    console.log('QUIT WORKSHEET RESPONSE:', JSON.stringify(res.data));
    if (res.data.resultCode === 'C004') {
      if (screenType == WORKSHEET) {
        if (res.data.redirectionCode === 'WorksheetList') {
          navigation.replace('WorksheetSessionReportScreen');
        } else if (res.data.redirectionCode === 'ContentPage') {
          navigation.replace('WorksheetSessionReportScreen');
        }
      } else {
        if (res.data.redirectionCode === 'ScreeningTestReport') {
          store.appStore.setScreeningTestStatus(true);
          navigation.replace('ScreeningTestReportScreen');
        }
      }
    } else {
      console.log('QUIT WORKSHEET ERROR' + JSON.stringify(res.data));
    }
  };

  const callSubmitWorkSheetAPI = async reqBody => {
    delete reqBody.body.evaluatedResult;

    try {
      let req = {
        body: reqBody.body,
        store: store,
      };

      const response = await API(ApiEndPoint.SUBMIT_WORKSHEET, req);

      if (response.data.resultCode === 'C001') {
        let questions = response.data.contentData;
        questions.contentHeaderInfo = response.data.contentHeaderInfo;
        if (
          questions.hasOwnProperty('contentParams') &&
          questions?.contentParams &&
          questions?.contentParams?.userAttemptData &&
          questions?.contentParams?.userAttemptData?.hasOwnProperty(
            'userResponse',
          )
        ) {
          stopAudio();
          quitWorksheets();
        } else {
          reset();
          stopAudio();

          setskipQuestion(response?.data?.contentData?.skipQuestion);
          qnaStore.init(response.data);
          setStartTime(moment());
          initializeAudioSection(response?.data?.contentData?.data[0]);
          console.log(
            `response?.data?.contentData?.data[0] ${JSON.stringify(
              response?.data?.contentData?.data[0],
            )}`,
          );
          //Disable scroll for Sortlist and Matchlist questions
          if (
            qnaStore?.currentQuestion?.template === 'SortList' ||
            qnaStore?.currentQuestion?.template === 'MatchList'
          ) {
            setEnableScroll(false);
          } else {
            setEnableScroll(true);
          }
        }
      } else if (
        response.data.resultCode === 'C004' &&
        response.data.contentHeaderInfo.hasOwnProperty('pedagogyMessages') &&
        (response.data.contentHeaderInfo.pedagogyMessages[0] ===
          'screeningTestComplete' ||
          response.data.contentHeaderInfo.pedagogyMessages[0] ===
            'levelTestComplete')
      ) {
        stopAudio();
        quitWorksheets();
      } else {
        console.log('API ERROR:' + JSON.stringify(response.data));
        uiStore.setLoader(false);
      }
    } catch (e) {
      console.log(`Submit Worksheet Questions error>>>${e}`);
      uiStore.setLoader(false);
    }
  };

  const dontKnow = async isQuit => {
    // uiStore.setLoader(true);
    let requestBody = {
      pedagogyID: qnaStore?.contentHeaderInfo?.pedagogyID,
      isDynamic: qnaStore.currentQuestion?.isDynamic,
      contentID: qnaStore.contentData?.contentId,
      result: Base64.encode('skip'),
      timeTaken: moment().diff(startTime, 'seconds'),
      userResponse: {},
      userAttemptData: {},
      contentInfo: {
        contentID: qnaStore.contentData?.contentId,
        contentVersionID: qnaStore.currentQuestion._id,
        contentType: qnaStore.contentData.contentType,
        questionType: qnaStore.currentQuestion.template,
        revisionNum: qnaStore.currentQuestion.revisionNo,
        langCode: qnaStore.currentQuestion.langCode,
      },
      contentSeqNum: qnaStore.contentData.contentSeqNum,
      mode:
        qnaStore.contentData.contentSubMode === 'remediation'
          ? 'learn'
          : qnaStore.contentData.contentSubMode,
      contentSubMode: qnaStore.contentData.contentSubMode,
      remainingTime: qnaStore.timed
        ? qnaStore.contentHeaderInfo?.remainingTime -
          moment().diff(startTime, 'seconds')
        : 0,
      nextContentSeqNum: isQuit
        ? qnaStore.contentData.contentSeqNum
        : qnaStore.nextContentSeqNum !== 0
        ? qnaStore.nextContentSeqNum
        : qnaStore.contentHeaderInfo.pedagogyProgress.totalUnits ===
          qnaStore.contentData.contentSeqNum
        ? 1
        : qnaStore.contentData.contentSeqNum + 1,
    };
    if (
      qnaStore.contentData.contentSubMode === 'remediation' ||
      qnaStore.contentData.contentSubMode === 'challenge'
    ) {
      requestBody.mode = 'learn';
    }

  if(screenType === WORKSHEET_EDICINE && qnaStore.currentQuestion.template === 'MCQ' && qnaStore?.userResponse != null ){  
       requestBody.userResponse = qnaStore?.userResponse;  
       requestBody.userAttemptData = {
        hintTaken:qnaStore?.isHintVisible,
        trialCount:qnaStore?.trialCount,  
        trials: [
          {
            userResponse: qnaStore?.userResponse,
            result: qnaStore?.contentData?.contentParams?.userAttemptData?.result === "true" || 
                    qnaStore?.contentData?.contentParams?.userAttemptData?.result ==  true ? true : false,
            timeTaken:qnaStore?.contentData?.contentParams?.userAttemptData?.timeTaken,
          },
        ],
      };
  }

    if (screenType === HOMEWORK_EDICINE)  {
      let uploadedData = [null];
      if (qnaStore.homeworkSolutionAttachment !== null) {
        uploadedData = [
          {
            uri: qnaStore?.homeworkSolutionAttachment?.uri,
            fileName: qnaStore?.homeworkSolutionAttachment?.fileName,
          },
        ];
      }
      const textInteractionData = {};
      textInteractionData.type = 'TextInteraction';
      textInteractionData.userAnswer = null;

      if (requestBody != null && requestBody.userResponse !== null) {
        requestBody.userResponse.uploads = uploadedData;
        requestBody.userResponse.textInteraction = textInteractionData;
      }

      if (requestBody.userAttemptData && requestBody.userAttemptData.trials) {
        requestBody.userAttemptData.trials.map(item => {
          item.timeTaken = moment().diff(startTime, 'seconds');
          if (item?.userResponse !== null) {
            item.userResponse.uploads = uploadedData;
            item.userResponse.textInteraction = textInteractionData;
          }
        });
      }
    }
    console.log('I Donot Know Request', requestBody);
    const reqBody = {
      jwt: await getAsValue('jwt'),
      store: store,
      body: requestBody,
    };
    if (screenType == TOPICS) {
      callSubmitTopicQuestionAttemptAPI(reqBody, false, true);
    } else if (screenType == SCREEN_TEST) {
      (reqBody.body.nextContentSeqNum = null), callSubmitWorkSheetAPI(reqBody);
    } else if (screenType === HOMEWORK_EDICINE) {
      callSubmitHomeworkQuestionAPI(reqBody);
    } else {
      callSubmitQuestionAttemptAPI(reqBody);
    }
  };

  const callSubmitQuestionAttemptAPI = async reqBody => {
    setStartTime(moment());

    try {
      let req = {
        body: reqBody.body,
        store: store,
      };
      const res = await API(ApiEndPoint.SUBMIT_WORKSHEET, req);
      if (res?.data?.resultCode === 'C001') {
        if (screenType == WORKSHEET_EDICINE) {
          await reset();
          if (showClosePopup) {
            navigation.goBack();
            return;
          }
          qnaStore.init(res.data);
          updateInputResponse();
        } else {
          if (
            res?.data?.contentData?.hasOwnProperty('contentParams') &&
            res?.data?.contentData?.contentParams &&
            res?.data?.contentData?.contentParams?.userAttemptData &&
            res?.data?.contentData?.contentParams?.userAttemptData?.hasOwnProperty(
              'userResponse',
            )
          ) {
            // Toast.show({text: 'Last Question1', duration: 3000});
            console.log(
              'THIS IS THE LAST QUESTION BECAUSE userResponse is available',
            );
            runInAction(() => {
              qnaStore.showExplanation = true;
              qnaStore.isLastQuestion = true;
            });
            qnaStore.showNextBtn();
          } else if (res?.data?.contentData?.length === 0) {
            Toast.show({text: 'Last Question3', duration: 3000});
            console.log(
              'THIS IS THE LAST QUESTION BECAUSE userResponse is available',
            );
            runInAction(() => {
              qnaStore.showExplanation = true;
              qnaStore.isLastQuestion = true;
            });
            navigation.navigate('WorksheetSessionReportScreen');
          } else {
            console.log('----------HAVE MORE QUESTIONS--------');
            setskipQuestion(res?.data?.contentData?.skipQuestion);
            qnaStore.setNextQuestionData(res.data);
            runInAction(() => {
              qnaStore.showExplanation = true;
            });
            qnaStore.showNextBtn();
          }
        }
      } else if (
        res?.data?.resultCode === 'C004' &&
        res?.data?.redirectionCode === 'ContentPage'
      ) {
        if (screenType == WORKSHEET_EDICINE) {
          navigation.replace('WorksheetListScreen');
        } else {
          navigation.replace('WorksheetSessionReportScreen');
        }
      } else {
        store.uiStore.apiErrorInit({
          code: res.status,
          message: res.data?.resultMessage,
        });
      }
    } catch (err) {
      console.log('ERROR IN SUBMIT API', err);
      uiStore.setLoader(false);
    }
  };

  const quitDiscreteSkill = async () => {
    let req = {
      body: {
        discreteSkillID: qnaStore?.contentHeaderInfo?.pedagogyID,
      },
      store: store,
    };
    let res = await API(ApiEndPoint.QUIT_DISCRETE_SKILL, req);
    if (res.data.resultCode === 'C001' || res.data.resultCode === 'C004') {
      switch (res?.data?.redirectionCode) {
        case 'ContentPage':
          navigation.replace('DiscreteSkillSessionReportScreen');
          break;
        case 'OpenDiscreteSkill':
          callOpenDiscreteSkill(
            res.data?.redirectionData?.activatedDiscreteSkillID,
          );
          break;
        default:
          break;
      }
    } else {
      store.uiStore.apiErrorInit({
        code: res.status,
        message: res.data?.resultMessage,
      });
    }
  };

  const callOpenDiscreteSkill = async discreteSkillID => {
    let req = {
      body: {
        discreteSkillID,
        mode: 'Test',
      },
      store: store,
    };
    try {
      let res = await API(ApiEndPoint.OPEN_DISCRETE_SKILL, req);
      if (
        res.data.resultCode == 'C004' &&
        res.data.redirectionCode == 'ContentPage'
      ) {
        runInAction(() => {
          store.qnaStore.topicId = discreteSkillID;
        });
        navigation.push('DiscreteSkillQnAScreen');
      } else {
        store.uiStore.apiErrorInit({
          code: response.status,
          message: res.data?.resultMessage,
        });
      }
    } catch (error) {}
  };

  const updateQuestion = async () => {
    if (qnaStore.isLastQuestion) {
      if (screenType == DISCRETE_SKILL) {
        quitDiscreteSkill();
      } else {
        quitWorksheets();
      }
    } else {
      await reset();
      qnaStore.init(qnaStore?.nextQuestionRes);

      let response = {data: qnaStore?.nextQuestionRes};
      initializeAudioSection(response?.data?.contentData?.data[0]);
      //Disable scroll for Sortlist and Matchlist questions
      if (
        qnaStore?.currentQuestion?.template === 'SortList' ||
        qnaStore?.currentQuestion?.template === 'MatchList'
      ) {
        setEnableScroll(false);
      } else {
        setEnableScroll(true);
      }
      uiStore.setLoader(false);
    }
  };

  const callUpdateQuestionAttemptAPI = async () => {
    console.log('callUpdateQuestionAttemptAPI');
    try {
      let req = {
        body: {
          contentID: qnaStore.contentData.contentId,
          userAttemptData: {
            explanationRead: true,
            explanationReadTime: moment().diff(startTime, 'seconds'),
            explanationRating: null,
            gaveExplanation: false,
            feedbackResponse: null,
            userExplanation: null,
          },
          contentInfo: {
            contentID: qnaStore.contentData.contentId,
            contentVersionID: qnaStore.currentQuestion._id,
            contentType: qnaStore.contentData
              ? qnaStore.contentData.contentType
              : '',
            questionType: qnaStore.currentQuestion.template,
            revisionNum: qnaStore.contentData.revisionNo,
            langCode: qnaStore.currentQuestion.langCode,
          },
        },
        store: store,
      };
      const res = await API(ApiEndPoint.UPDATE_QUESTION_ATTEMPT, req);
      console.log(
        ' QnaStore nextQuestionRes = ',
        JSON.stringify(qnaStore.nextQuestionRes),
      );
      if (res.data.resultCode === 'C001') {
        if (
          (qnaStore.nextQuestionRes.resultCode == 'C004' &&
            qnaStore.nextQuestionRes.redirectionCode == 'CloseContent') ||
          (qnaStore.nextQuestionRes.redirectionData &&
            qnaStore.nextQuestionRes.redirectionData.endTopicFlag)
        ) {
          navigation.replace('TopicSummaryScreen');
        } else {
          console.log(' initialize Audio Section ');
          await reset();
          qnaStore.init(qnaStore.nextQuestionRes);
          if (ContentIDs.includes(qnaStore.contentData.contentId)) {
            qnaStore.showSkip();
            qnaStore.skipQnaQuestion();
          }
          initializeAudioSection(qnaStore.nextQuestionRes.contentData?.data[0]);
          // setStarred(false);
        }
      } else {
        store.uiStore.apiErrorInit({
          code: res.status,
          message: res.data?.resultMessage,
        });
      }
    } catch (err) {
      console.log('UseQnA callUpdateQuestionAttemptAPI error = ', err);
      store.uiStore.apiErrorInit({
        code: false,
        message: 'Api Error',
      });
    }
  };

  const closeContent = async () => {
    try {
      console.log('closing content');
      let req = {
        body: {
          endTopicFlag: false,
          endTopicHigherLevel: false,
          userTriggered: true,
          sessionTimeExceededFlag: false,
        },
        store: store,
      };
      let res = await API(ApiEndPoint.CLOSE_CONTENT, req);
      console.log('close content response = ', res);
      if (res) {
        if (
          res.data.resultCode === 'C004' &&
          res.data.redirectionCode === 'TopicSessionReport'
        ) {
          //Check if Revice mode
          if (store.qnaStore.contentData.contentSubMode == 'revise') {
            navigation.replace('TopicListingScreen');
          } else {
            runInAction(() => {
              store.qnaStore.topicId = res.data.redirectionData.ID;
            });
            navigation.replace('TopicSummaryScreen');
          }
        } else {
          store.uiStore.apiErrorInit({
            code: res.status,
            message: res.data?.resultMessage,
          });
          console.log('CLOSE CONTENT ERROR' + JSON.stringify(res.data));
        }
      } else {
        store.uiStore.setLoader(false);
        store.uiStore.apiErrorInit({
          code: false,
          message: 'Api Error',
        });
      }
    } catch (err) {
      console.log('UseQnA close content error = ', err);
      store.uiStore.apiErrorInit({
        code: false,
        message: 'Api Error',
      });
    }
  };

  const callSubmitTopicQuestionAttemptAPI = async (
    reqBody,
    showExplination = true,
    dontKnow = false,
  ) => {
    auth.trackEvent('mixpanel', MixpanelEvents.QUESTION_ATTEMPTED, {
      Category: MixpanelCategories.TOPIC,
      Action: MixpanelActions.ATTEMPTED,
      Label: '',
    });

    console.log(`Submit Question attempt API Request>>>${moment()}`);
    setStartTime(moment());
    console.log('anime req old', reqBody.body);
    let interactiveBody;
    if (reqBody.body.contentInfo.questionType === 'Interactive') {
      interactiveBody = {
        contentID: reqBody.body.contentID,
        contentInfo: reqBody.body.contentInfo,
        contentSeqNum: reqBody.body.contentSeqNum,
        contentSubMode: reqBody.body.contentSubMode,
        isDynamic: reqBody.body.isDynamic,
        mode: reqBody.body.mode,
        result: reqBody.body.result,
        timeTaken: reqBody.body.timeTaken,
        userAttemptData: reqBody.body.userAttemptData,
        userResponse: reqBody.body.userResponse,
      };
    }

    console.log('anime req new', interactiveBody);
    try {
      let req = {
        body:
          reqBody.body.contentInfo.questionType === 'Interactive'
            ? interactiveBody
            : reqBody.body,
        store: store,
      };
      let apiUrl = '';
      apiUrl = ApiEndPoint.SUBMIT_QUESTION_ATTEMPT;

      // if (Config.ENV === 'edicine') {
      //   apiUrl = ApiEndPoint.SUBMIT_QUESTION_ATTEMPT;
      // } else {
      //   apiUrl = ApiEndPoint.SUBMIT_QUESTION_ATTEMPT_V3;
      // }

      const res = await API(apiUrl, req);
      console.log(`Submit Question attempt API response>>>`, reqBody.body);
      if (res.data.resultCode === 'C001') {
        if (reqBody?.isTimeTest) {
          await reset();
          qnaStore.init(res.data);
          // qnaStore.setNextQuestionData(res.data);
          // callUpdateQuestionAttemptAPI();
        } else if (
          reqBody.body.contentSubMode == 'challenge' &&
          dontKnow == true
        ) {
          if (
            qnaStore.contentHeaderInfo.pedagogyMessages[0] ==
            'challengeAttempt1'
          ) {
            qnaStore.setNextQuestionData(res.data);
            runInAction(() => {
              qnaStore.showExplanation = showExplination;
            });
            // qnaStore.showNextBtn();
            qnaStore.setDisableBTn(true);
            callUpdateQuestionAttemptAPI();
            // setStarred(false);
          }
          if (
            qnaStore.contentHeaderInfo.pedagogyMessages[0] ==
            'challengeAttempt2'
          ) {
            qnaStore.setNextQuestionData(res.data);
            runInAction(() => {
              qnaStore.showExplanation = true;
            });
            setLockOptions(true);
            qnaStore.showNextBtn();
          }
        } else {
          //qnaStore.showEffortpopup();
          if (
            res.data.contentHeaderInfo.pedagogyMessages.includes(
              'remedialFlow1_Remedial',
            ) ||
            res.data.contentHeaderInfo.pedagogyMessages.includes(
              'remedialFlow1_Cluster',
            ) ||
            res.data.contentHeaderInfo.pedagogyMessages.includes(
              'remedialFlow2',
            ) ||
            res.data.contentHeaderInfo.pedagogyMessages.includes(
              'remedialFlow3',
            )
          ) {
            //Enable State for effort popup
            qnaStore.showEffortpopup();
          }
          qnaStore.setNextQuestionData(res.data);
          runInAction(() => {
            qnaStore.showExplanation = showExplination;
          });
          qnaStore.showNextBtn();
        }
      } else if (res.data.resultCode == 'C004') {
        if (res.data.redirectionCode == 'CloseContent') {
          if (res.data?.contentHeaderInfo?.hasOwnProperty('alert')) {
            qnaStore.setRewardTitle(res.data);
          } else {
            navigation.replace('TopicSummaryScreen');
          }
        } else {
          qnaStore.setNextQuestionData(res.data);
          qnaStore.showNextBtn();
        }
      } else {
        store.uiStore.apiErrorInit({
          code: res.status,
          message: res.data?.resultMessage,
        });
      }
    } catch (err) {
      console.log('ERROR IN SUBMIT API', err);
    }
  };

  const callOpenActivity = async () => {
    runInAction(() => {
      store.appStore.activityID = qnaStore.currentQuestion.contentID;
    });
    const reqBody = {
      jwt: await getAsValue('jwt'),
      store: store,
      body: {
        activityID: qnaStore.currentQuestion.contentID,
      },
    };
    const res = await API(ApiEndPoint.OPEN_ACTIVITY, reqBody);
    if (res.data.resultCode === 'C004') {
      console.log(
        'disabled ActivityStartModal(Activity unlocked) qnaStore.setEnableAttachmentModal(false)}',
      );
      qnaStore.setEnableAttachmentModal(false);
      if (qnaStore.currentQuestion.template == 'Introduction') {
        navigation.navigate('IntroductionScreen');
      } else {
        navigation.navigate('GamePlayArenaScreen');
        qnaStore.setActivitySkipButton(true);
      }
    } else if (res.data.resultCode === 'C002') {
      console.warn('Res data>', JSON.stringify(res.data));
    }
  };

  const submitTimeTest = async () => {
    if (screenType == SEARCH) {
      navigation.navigate('PreviewQnASearchScreen');
    }
    var contentData = qnaStore.questionRes.contentData;
    var data = contentData.data[0];
    var contentInfo = {
      contentID: contentData.contentId,
      contentVersionID: data._id,
      contentType: contentData.contentType,
      groupType: data.type,
      revisionNum: data.revisionNo,
      langCode: data.langCode,
    };
    var submitData = {
      contentID: contentData.contentId,
      result: 'pass',
      timeTaken: qnaStore.timeTaken,
      timedTestInfo: {
        contentInfo: contentInfo,
      },
      userAttemptData: {
        userAttemptInfo: {
          totalCorrect: qnaStore.correctAnswerCount,
          totalAttempted: qnaStore.questionIndex,
          totalQuestions: qnaStore.timeTextQuestions.length,
          finished: !qnaStore.isTimeUp,
        },
        questionWiseData: qnaStore.timeTestUserAnswers,
      },
      contentInfo: contentInfo,
      contentSeqNum: contentData.contentSeqNum,
      mode: 'learn',
      contentSubMode: contentData.contentSubMode,
    };

    const reqBody = {
      jwt: await getAsValue('jwt'),
      store: store,
      body: submitData,
      isTimeTest: true,
    };
    console.log('RequestBody', JSON.stringify(reqBody.body));
    callSubmitTopicQuestionAttemptAPI(reqBody);
  };

  const getTimeTestPopup = () => {
    if (qnaStore.isTimeTestDone) {
      return (
        <TimeTestModal
          isVisible={showTimeTestModal}
          showAttemptLaterModal={true}
          onPress={() => {
            setShowTimeTestModal(false);
            runInAction(() => {
              timerRef?.current?.start();
              qnaStore.isTimeTestDone = false;
              qnaStore.isTimeUp = false;
            });
          }}
          onCompleteLater={() => {
            setShowTimeTestModal(false);
            // submitTimeTest();
            console.log('UseQnA close content called');
            closeContent();
          }}
        />
      );
    } else if (qnaStore.isTimeUp) {
      return (
        <TimeTestModal
          isVisible={showTimeTestModal}
          onPress={() => {
            setShowTimeTestModal(false);
            submitTimeTest();
          }}
          title={tryAgainLaterText}
          topic={qnaStore.timeTestData.title}
          isStatusShown={false}
          subtitle={`Your time Is up.\n You had ${
            qnaStore.timeTestData.durationClass
          }  minutes to do this test.`}
        />
      );
    } else {
      var correct = qnaStore.correctAnswerCount;
      var wrong = qnaStore.questionIndex - qnaStore.correctAnswerCount;
      var accuracy = ((correct / qnaStore.questionIndex) * 100).toFixed(0);
      if (accuracy >= 75) {
        return (
          <TimeTestModal
            isVisible={showTimeTestModal}
            onPress={() => {
              setShowTimeTestModal(false);
              submitTimeTest();
            }}
            title={excellentWorkText}
            topic={qnaStore.timeTestData.title}
            isStatusShown={true}
            correct={correct}
            wrong={wrong}
            accuracy={accuracy}
            infoMessage={`Given Time:${
              qnaStore.timeTestData.durationClass
            } minutes`}
            subtitle={`${yourTimeText} ${moment
              .utc(qnaStore.timeTaken * 1000)
              .format('mm [min] ss [sec]')}`}
          />
        );
      } else {
        return (
          <TimeTestModal
            isVisible={showTimeTestModal}
            onPress={() => {
              setShowTimeTestModal(false);
              submitTimeTest();
            }}
            title={tryHarderText}
            topic={qnaStore.timeTestData.title}
            isStatusShown={true}
            correct={correct}
            wrong={wrong}
            accuracy={accuracy}
            infoMessage={''}
            subtitle={youNeedToGetMoreQuetionRight}
          />
        );
      }
    }
  };

  const callNavigateFunc = async () => {
    timerRef?.current?.stop();
    await reset();
    navigation.replace('WorksheetListScreen');
  };

  const quitEdicineWorksheet = async () => {
    let req = {
      body: {
        worksheetID: qnaStore.contentHeaderInfo?.pedagogyID,
      },
      store: store,
    };
    let res = await API(ApiEndPoint.QUIT_WORKSHEET_EDICINE, req);
    if (res.data.resultCode === 'C001') {
      if (res.data.alert) {
        setShowTimesUp(false);
        setRewardItem(res.data.alert.badge);
        setRewardModal(true);
      } else {
        timerRef?.current?.stop();
        await reset();
        navigation.replace('WorksheetListScreen');
      }
    } else if (res.data.resultCode === 'C004') {
      timerRef?.current?.stop();
      await reset();
      navigation.replace('WorksheetListScreen');
    } else {
      store.uiStore.apiErrorInit({
        code: res.status,
        message: res.data?.resultMessage,
      });
      console.log('Quit  Worksheet ERROR' + JSON.stringify(res.data));
    }
  };

  const updateInputResponse = () => {
    console.log('qqna screen-updateInputResponse called');
    let answers = qnaStore.userResponse;
    //let answers = qnaStore.contentData?.contentParams?.userAttemptData?.userResponse;
    let newResponse = {};
    console.log('qqna screen-answers ', toJS(answers));
    for (var answer in answers) {
      console.log('qqna screen-answer ', answer);
      if (answers.hasOwnProperty(answer) && answers[answer]?.userAnswer !=null) {
        newResponse[answer] = answers[answer]?.userAnswer;
        // if (
        //   qnaStore.currentQuestion.template === 'Dropdown' &&
        //   qnaStore.currentQuestion.response.hasOwnProperty(answer)
        // ) {
        //   console.log(
        //     'qqna screen-inside if condtition true = template=dropdown and crrentqsn resp has prop',
        //     answer,
        //   );
        //   let mapper = qnaStore.currentQuestion.response[answer]?.mapper;
        //   console.log('qqna screen-mapper ', mapper);
        //   if (mapper.length > 0) {
        //     console.log(
        //       'qqna screen-mapper insdife if \nAnswer is ,',
        //       answers[answer]?.userAnswer,
        //       'mapper',
        //       mapper,
        //     );
        //     let index = mapper.findIndex(
        //       obj => obj == answers[answer]?.userAnswer,
        //     );
        //     console.log(
        //       'qqna screen- findindex ansers[answer] -',
        //       answers[answer],
        //     );
        //     console.log(
        //       'qqna screen- findindex ansers[answer].userAnswer -',
        //       answers[answer]?.userAnswer,
        //     );
        //     console.log(
        //       'qqna screen-  newResonse[answer] -',
        //       newResponse[answer],
        //     );
        //     console.log('qqna screen-  newResonse[answer] = index -', index);
        //     console.log('qqna screen-  answers[answer]', answers[answer]);
        //     console.log(
        //       'qqna screen-  answers[answer].useranswer',
        //       answers[answer].userAnswer,
        //     );
        //     console.log(
        //       'qqna screen-  answers[answer].useranswer = index',
        //       index,
        //     );
        //     newResponse[answer] = index;
        //     answers[answer].userAnswer = index;
        //   }
        // }
      }
    }
    console.log('qqna screen- outside if answers', toJS(answers));
    console.log(
      'qqna screen- outside if inputreposne/newresponse',
      newResponse,
    );
    qnaStore.currentQuestion.template === 'Dropdown' &&
      qnaStore.setUserResponse(answers);
    setInputResponse(newResponse);
    console.log('\n new Response', newResponse);
  };

  const getWorksheetPopup = () => {
    if (qnaStore?.isOpenedFirstTime) {
      let infoMsg = '';
      if (
        qnaStore.worksheetInfo.endDateTime &&
        qnaStore.worksheetInfo.endDateTime != ' '
      ) {
        infoMsg = `Due on ${moment(qnaStore.worksheetInfo.endDateTime).format(
          'DD MMM YY',
        )}`;
      }
      return (
        <TimedWorksheetModal
          onPress={() => {
            qnaStore.setIsOpenedFirstTime(false);
          }}
          isVisible={true}
          topic={qnaStore?.contentHeaderInfo?.pedagogyName}
          infoMessage={infoMsg}
          subtitle={replaceString(
            workSheetNumberOfQuestions,
            'total',
            qnaStore.contentHeaderInfo?.pedagogyProgress?.totalUnits,
          )}
          title={liveWorksheetText}
          isTimed={qnaStore.timed}
          buttonText={letsGoBtnText}
          time={qnaStore.contentHeaderInfo?.totalTime}
        />
      );
    } else if (showSubmitPopup) {
      return (
        <TimedWorksheetModal
          isVisible={true}
          showSubmitModal={true}
          onPress={() => {
            setShowSubmitPopup(false);
            quitEdicineWorksheet();
          }}
          onCompleteLater={() => {
            setShowSubmitPopup(false);
          }}
          title={liveWorksheetText}
          buttonText={letsGoBtnText}
        />
      );
    } else if (showClosePopup) {
      return (
        <TimedWorksheetModal
          isVisible={true}
          showAttemptLaterModal={true}
          onPress={() => {
            timerRef?.current?.start();
            setShowClosePopup(false);
          }}
          title={liveWorksheetText}
          onCompleteLater={() => {
            timerRef?.current?.stop();
            dontKnow(true);
            auth.trackEvent('mixpanel', MixpanelEvents.WORKSHEET_DONE, {
              Category: MixpanelCategories.WORKSHEET,
              Action: MixpanelActions.CLICKED,
              Label: '',
            });
          }}
          buttonText={letsGoBtnText}
        />
      );
    } else if (qnaStore.timed && showTimesUp) {
      return (
        <TimedWorksheetModal
          onPress={() => {
            quitEdicineWorksheet();
          }}
          isVisible={true}
          title={timeUpText}
          buttonText={okayBtnText}
          topic={qnaStore?.contentHeaderInfo?.pedagogyName}
          subtitle={timeWorkSheetOverText}
        />
      );
    } else if (rewardModal) {
      return (
        <RewardCollectionModal
          isVisible={rewardModal}
          item={rewardItem}
          onStartBtnPressed={() => {
            setRewardModal(false);
            callNavigateFunc();
          }}
        />
      );
    }
  };

  const options = {
    title: selectSolutionImage,
    storageOptions: {
      skipBackup: true,
      path: 'images',
    },
  };

  const launchImageLibrary = () => {
    ImagePicker.launchImageLibrary(options, response => {
      if (response.didCancel) {
        console.log('User cancelled photo picker');
      } else if (response.error) {
        console.log('ImagePicker Photo Library Error: ', response.error);
      } else {
        if (response.fileSize / 1000 < 5120) {
          let path = response.uri;
          if (Platform.OS === 'ios') {
            path = '~' + path.substring(path.indexOf('/Documents'));
          }
          if (!response.fileName) response.fileName = path.split('/').pop();
          console.log('image response', response.fileName);
          qnaStore.setHomeworkSolutionAttachment(response);
        } else setSolutionUploadError(maxSizeHwSolutionAllowed);
      }
    });
  };

  const launchCamera = () => {
    ImagePicker.launchCamera(options, response => {
      if (response.didCancel) {
        console.log('User cancelled photo picker');
      } else if (response.error) {
        console.log('ImagePicker Camera Error: ', response.error);
      } else {
        ImageResizer.createResizedImage(
          response.uri,
          600,
          600,
          'JPEG',
          100,
          0,
          undefined,
          false,
        )
          .then(resizedImage => {
            var imageData = {fileName: resizedImage.name, ...resizedImage};
            if (imageData.size / 1000 < 5120) {
              qnaStore.setHomeworkSolutionAttachment(imageData);
            } else setSolutionUploadError(maxSizeHwSolutionAllowed);
          })
          .catch(err => {
            console.log(err);
          });
      }
    });
  };

  const onAttachmentButtonClicked = isPhotoLibrary => {
    if (qnaStore.homeworkSolutionAttachment !== null) {
      qnaStore.setEnableHomeworkAttentionModal(true);
    } else if (isPhotoLibrary) {
      launchImageLibrary();
    } else {
      launchCamera();
    }
  };

  const renderHomeworkSolutionView = () => {
    return (
      <View style={homeworkStyle.solutionContainer}>
        <BalooThambiRegTextView style={homeworkStyle.solutionTitle}>
          {writeYourSolutionText}
        </BalooThambiRegTextView>
        <CustomTextInput
          style={homeworkStyle.solutionField}
          onChangeText={value => qnaStore?.setHomeworkSolution(value)}
          value={qnaStore?.homeworkSolution}
          multiline={true}
        />
        <BalooThambiRegTextView style={homeworkStyle.solutionTitle}>
          {uploadYourSolutionText}
        </BalooThambiRegTextView>
        <View style={homeworkStyle.solutionButtonContainer}>
          <TouchableOpacity
            onPress={() => onAttachmentButtonClicked(true)}
            style={[
              homeworkStyle.solutionButtonSubContainer,
              {width: getWp(100), marginEnd: getWp(8)},
            ]}>
            <BalooThambiRegTextView style={homeworkStyle.solutionButtonText}>
              {uploadBtnText}
            </BalooThambiRegTextView>
            <Upload width={getWp(16)} height={getWp(16)} />
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => onAttachmentButtonClicked(false)}
            style={homeworkStyle.solutionButtonSubContainer}>
            <BalooThambiRegTextView style={homeworkStyle.solutionButtonText}>
              {takePhotoBtnText}
            </BalooThambiRegTextView>
            <CameraWhite width={getWp(16)} height={getWp(16)} />
          </TouchableOpacity>
        </View>
        <HomeworkSolutionImageModal
          isVisible={qnaStore?.enableAttachmentModal}
          imageURL={qnaStore?.homeworkSolutionAttachment?.uri}
          onPress={() => qnaStore?.setEnableAttachmentModal(false)}
        />
        <HomeworkAttentionModal
          isVisible={qnaStore?.enableHomeworkAttentionModal}
          onOkayButtonPressed={() => {
            // qnaStore.setHomeworkSolutionAttachment(null);
            qnaStore.setEnableHomeworkAttentionModal(false);
            setTimeout(function() {
              launchImageLibrary();
            }, 500);
          }}
          onNoButtonPressed={() =>
            qnaStore.setEnableHomeworkAttentionModal(false)
          }
        />
        {qnaStore?.homeworkSolutionAttachment !== null && (
          <View style={homeworkStyle.solutionAttachmentContainer}>
            <TouchableOpacity
              onPress={() => qnaStore.setEnableAttachmentModal(true)}>
              <Image
                style={homeworkStyle.solutionAttachmentImage}
                source={{uri: qnaStore?.homeworkSolutionAttachment?.filePath}}
              />
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() => {
                console.log('uploading');
                qnaStore.setEnableAttachmentModal(true);
              }}>
              <BalooThambiRegTextView
                numberOfLines={1} //numberOflines seems to be not working here due to layout, thus to use ellipses I used substring and '...'
                ellipsizeMode="middle"
                style={homeworkStyle.solutionAttachmentText}>
                {qnaStore?.homeworkSolutionAttachment?.fileName}
              </BalooThambiRegTextView>
            </TouchableOpacity>
            <ErrorClose
              style={homeworkStyle.solutionCloseButton}
              width={getWp(20)}
              height={getWp(20)}
              onPress={() => qnaStore.setHomeworkSolutionAttachment(null)}
            />
          </View>
        )}
        {solutionUploadError !== null &&
          solutionUploadError !== '' &&
          solutionUploadError !== undefined && (
            <BalooThambiRegTextView
              style={homeworkStyle.solutionUploadErrorMsg}>
              {solutionUploadError}
            </BalooThambiRegTextView>
          )}
        <SourceSansProRegTextView style={homeworkStyle.solutionDescriptionText}>
          {youCanOnlyUploadOneImage}
        </SourceSansProRegTextView>
      </View>
    );
  };

  const passageBtnClickHandler = type => {
    switch (type) {
      case 'view_question':
        qnaStore.isReadPassageBtnVisible = true;
        qnaStore.isViewQuestionBtnVisible = false;
        if (qnaStore.wordMeaningData.length) {
          qnaStore.setWordMeaningEnabled(false);
          qnaStore.setViewMeaningBtnVisible(false);
        }
        initializeAudioSection(qnaStore.currentQuestion);
        break;
      case 'view_passage':
        qnaStore.isReadPassageBtnVisible = false;
        qnaStore.isViewQuestionBtnVisible = true;
        qnaStore.enableViewQuestionButton = true;
        if (qnaStore.wordMeaningData.length) {
          qnaStore.setViewMeaningBtnVisible(true);
          qnaStore.setViewWordMeaningEnabled(true);
          qnaStore.setWordMeaningEnabled(false);
        }
        audioCleanup();
        break;
      case 'view_word_meaing':
        qnaStore.isReadPassageBtnVisible = true;
        qnaStore.setWordMeaningEnabled(true);
        qnaStore.setViewMeaningBtnVisible(false);
        break;

      default:
        break;
    }
  };

  const exitQnARequest = async (isQuit = true) => {
    let requestBody = {
      isDynamic: qnaStore.currentQuestion?.isDynamic,
      contentID: qnaStore.contentData?.contentId,
      childContentID: qnaStore.contentData?.childContentId,
      userResponse: {},
      userAttemptData: {},
      contentInfo: {
        contentID: qnaStore.contentData?.contentId,
        childContentID: qnaStore.contentData?.childContentId,
        contentVersionID: qnaStore.contentData?.passageData._id,
        childContentVersionID: qnaStore.currentQuestion?._id,
        contentType: qnaStore.contentData.contentType,
        childContentType: qnaStore.contentData?.childContentType,
        groupType: 'passage',
        questionType: qnaStore.currentQuestion.template,
        revisionNum: qnaStore.contentData?.passageData?.revisionNo,
        childRevisionNum: qnaStore.currentQuestion.revisionNo,
        langCode: qnaStore.currentQuestion.langCode,
      },
      contentSeqNum: qnaStore.contentData.contentSeqNum,
    };

    /**
     * Add this if Wordmeaning data present
     */
    if (
      qnaStore?.wordMeaningData &&
      qnaStore?.wordMeaningData.length > 0 &&
      qnaStore?.wordMeaningData[qnaStore?.wordMeaningCurrentPosition]
    ) {
      requestBody.contentInfo.lastWordMeaningID =
        qnaStore?.wordMeaningData[
          qnaStore?.wordMeaningCurrentPosition
        ].contentID;
      requestBody.contentInfo.wordstatus = qnaStore?.wordMeaningStatus;
    }
    /**
     * Ends
     */

    console.log('I Donot Know Request', requestBody);

    const reqBody = {
      jwt: await getAsValue('jwt'),
      store: store,
      body: requestBody,
    };
    if (screenType == SCREEN_TEST) {
      reqBody.body.pedagogyID = qnaStore?.contentHeaderInfo?.pedagogyID;
      reqBody.body.contentInfo.contentVersionID = qnaStore.currentQuestion._id;
      reqBody.body.contentInfo.revisionNum =
        qnaStore.currentQuestion.revisionNo;

      callSubmitWorkSheetAPI(reqBody, isQuit);
    } else if (screenType === DISCRETE_SKILL) {
      callSubmitDiscreteSkillQuestionAttemptAPI(reqBody, isQuit);
    } else {
      callSubmitQuestionAttemptAPI(reqBody);
    }
  };

  const callSubmitDiscreteSkillQuestionAttemptAPI = async (
    reqBody,
    isQuit = false,
  ) => {
    setStartTime(moment());

    try {
      let req = {
        body: reqBody.body,
        store: store,
      };
      const res = await API(ApiEndPoint.SUBMIT_DISCRETE_SKILL_QUESTION, req);
      if (res?.data?.resultCode === 'C001') {
        if (isQuit) {
          navigation.navigate('DiscreteSkillSessionReportScreen');
        }
        if (res?.data?.contentHeaderInfo?.submitButton?.action?.viewState) {
          console.log('THIS IS THE LAST QUESTION BECAUSE submit btn is true');
          runInAction(() => {
            qnaStore.showExplanation = true;
            qnaStore.isLastQuestion = true;
          });
          qnaStore.showNextBtn();
        } else if (res?.data?.contentData?.length === 0) {
          Toast.show({text: 'Last Question3', duration: 3000});
          console.log(
            'THIS IS THE LAST QUESTION BECAUSE content data is empty',
          );
          runInAction(() => {
            qnaStore.showExplanation = true;
            qnaStore.isLastQuestion = true;
          });
          navigation.navigate('DiscreteSkillSessionReportScreen');
        } else {
          console.log('----------HAVE MORE QUESTIONS--------');
          setskipQuestion(res?.data?.contentData?.skipQuestion);
          qnaStore.setNextQuestionData(res.data);
          runInAction(() => {
            qnaStore.showExplanation = true;
          });
          qnaStore.showNextBtn();
        }
      } else if (
        res?.data?.resultCode === 'C004' &&
        res?.data?.redirectionCode === 'ContentPage'
      ) {
        navigation.navigate('DiscreteSkillSessionReportScreen');
      } else {
        store.uiStore.apiErrorInit({
          code: res.status,
          message: res.data?.resultMessage,
        });
      }
    } catch (err) {
      console.log('ERROR IN SUBMIT API', err);
      uiStore.setLoader(false);
    }
  };

  return {
    trueSelectionMcq,
    lockOptions,
    selectedMcq,
    falseSelectionMcq,
    createSubmitRequest,
    questionRef,
    interactiveRef,
    webref,
    csInit,
    csCheckIframe,
    csEvaluateAnswer,
    submitFunction,
    setEnableScroll,
    enableScroll,
    isSubmitClicked,
    showLoaderVisbility,
    isVisible,
    onWebViewMessage,
    renderCSHtmlView,
    scrollViewRef,
    parentScrollRef,
    dragAndDropCallback,
    classificationDragAndDrop,
    renderQuestionsItem,
    scrollToExplanation,
    renderExplanation,
    viewRef,
    reset,
    renderTimeoutDialog,
    skipQuestion,
    setskipQuestion,
    setStartTime,
    updateQuestion,
    qnaStore,
    setShowTimeTestModal,
    timerRef,
    getTimeTestPopup,
    callOpenActivity,
    callUpdateQuestionAttemptAPI,
    getWorksheetPopup,
    setShowSubmitPopup,
    setShowClosePopup,
    setShowTimesUp,
    playSound,
    initializeAudioSection,
    audioCleanup,
    showInsStVO,
    showQuesVO,
    qBodyVoiceOver,
    dontKnow,
    renderHomeworkSolutionView,
    setRewardModal,
    updateInputResponse,
    submitTimeTest,
    stopAudio,
    passageBtnClickHandler,
    exitQnARequest,
  };
};
