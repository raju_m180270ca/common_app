import React, {useState, useEffect, useRef} from 'react';
import Sound from 'react-native-sound';
import {Toast} from 'native-base';
import {useLanguage} from '@hooks';
import {AppState} from 'react-native';

const initialAudioObj = {
  quesVo: {
    clicked: false,
    initialised: false,
    instance: null,
  },
  instStimulusVO: {
    clicked: false,
    initialised: false,
    instance: null,
  },
  quesBodyVO: {
    clicked: false,
    initialised: false,
    instance: null,
  },
};

export const useSound = (store = null) => {
  var abortController;
  var abortSignal;
  var soundInstance;

  const [audioObj, setAudioObj] = useState(initialAudioObj);
  const [showInsStVO, setShowInsStVO] = useState(false);
  const [showQuesVO, setshowQuesVO] = useState(false);
  const [qBodyVoiceOver, setQBodyVoiceover] = useState(false);
  const [autoPlayVO, setAutoPlayVO] = useState(null);
  const [appState, setAppState] = useState(AppState.currentState);
  const handleAppStateChange = state => {
    setAppState(state);
  };

  const {
    invalidAudioFileText,
    audioInitialisingText,
    anotherAudioPlayingText,
    okayBtnText,
  } = useLanguage();

  useEffect(() => {
    abortController = new AbortController();
    abortSignal = abortController.signal;
    AppState.addEventListener('change', handleAppStateChange);

    return () => {
      abortController.abort();
      audioCleanup();
      AppState.removeEventListener('change', handleAppStateChange);
    };
  }, []);

  useEffect(() => {
    if (appState == 'background') {
      pauseAudio();
    }
  }, [appState]);

  //To play autoplay:true videos
  useEffect(() => {
    // console.log(`Audio object changed in effect>>>`);
    // console.log(audioObj);
    if (autoPlayVO) {
      if (store) {
        if (store?.qnaStore?.isViewQuestionBtnVisible === false) {
          playSound(autoPlayVO);
          setAutoPlayVO(null);
        }
      } else {
        playSound(autoPlayVO);
        setAutoPlayVO(null);
      }
    }
    return () => {};
  }, [autoPlayVO, store?.qnaStore?.isViewQuestionBtnVisible]);

  //   console.log(`Audio Obj>>>>${JSON.stringify(audioObj.quesBodyVO)}`);
  let soundTrack;
  // const error = 'Aborted!';
  // console.log(`Audio Object>>>`);
  // console.log(audioObj);
  const initialiseAudio = (soundUrl, signal) => {
    // console.log(`soundUrl>>>>${soundUrl}`);
    // soundUrl =
    // 'http://27.109.14.77:9002/Mindspark/Asset/qtypes/sounds_english/Listen to the word and choose the correct option.mp3';
    soundUrl = encodeURI(soundUrl);
    return new Promise((resolve, reject) => {
      var soundTrack = new Sound(soundUrl, null, error => {
        if (error) {
          console.log('failed to load the sound', error);
          return reject('failed to load the sound');
        }
        resolve(soundTrack);
      });
      if (signal && signal.aborted) {
        return reject(error);
      }
    });
  };

  const getQuestionVoiceUrl = str => {
    var audioData = str.substring(
      str.lastIndexOf('<audio>'),
      str.lastIndexOf('</audio>'),
    );

    var regex = /<audio.*?src='(.*?)'/;
    if (audioData != '') {
      var src = regex.exec(audioData)[1];
      if (src.length >= 1) return src;
    }

    return '';
  };

  const initializeAudioSection = async data => {
    console.log(`Audio initialise section starts>>>>>>`);
    let alreadyAdded = false;
    let instructorStimulus =
      data.hasOwnProperty('instructorStimulus') && data.instructorStimulus;
    if (
      instructorStimulus &&
      instructorStimulus?.hasOwnProperty('voiceover') &&
      instructorStimulus?.voiceover.length > 0
    ) {
      setShowInsStVO(true);
      await createAudioInstance(
        'instStimulusVO',
        instructorStimulus?.voiceover,
      );
      if (
        instructorStimulus?.hasOwnProperty('voiceoverAutoPlay') &&
        instructorStimulus?.voiceoverAutoPlay
      ) {
        setAutoPlayVO('instStimulusVO');
      }
    } else if (
      data.hasOwnProperty('quesVoiceover') &&
      data.quesVoiceover.length > 0
    ) {
      let extension = data.quesVoiceover.split('.').pop();
      if (extension == 'mp3') {
        console.log(`setshowQuesVO -------------------------------> true}`);
        setshowQuesVO(true);
        alreadyAdded = true;
        await createAudioInstance('quesVo', data.quesVoiceover);
        setAutoPlayVO('quesVo');
      }
    } else {
      setshowQuesVO(false);
      setShowInsStVO(false);
    }
    /*
       check either questionBody have Question audio url or not 
       if audio is available than we extract from there 
       and set showQuesVO as a true and initialized it...
    **/
    let QuestionBody = data.hasOwnProperty('questionBody');
    const QuestionBodyData = data.questionBody;
    if (
      QuestionBody &&
      alreadyAdded == false &&
      QuestionBodyData.includes('.mp3')
    ) {
      const getQuesVoUrl = getQuestionVoiceUrl(data.questionBody);
      if (getQuesVoUrl.length > 0) {
        await createAudioInstance('quesVo', getQuesVoUrl);
        setshowQuesVO(true);
      }
    }

    if (
      data.hasOwnProperty('display') &&
      data.display.hasOwnProperty('hideQuesInSeconds') &&
      data.display.hasOwnProperty('hideQuesInSeconds') &&
      data.display.hideQuesInSeconds != null
    ) {
      setTimeout(() => {
        setshowQuesVO(false);
      }, parseInt(data.display.hideQuesInSeconds) * 1000);
    }

    if (data?.questionBodyVoiceover) {
      console.log(
        `questionbodyvoiceover>>>>--------------------->${
          data?.questionBodyVoiceover
        }`,
      );
      setQBodyVoiceover(true);
      createAudioInstance('quesBodyVO', data?.questionBodyVoiceover);
    }
  };

  const createAudioInstance = async (type, audioFile, autoPlay = false) => {
    try {
      soundInstance = await initialiseAudio(audioFile, abortSignal);

      let audioObjModified = {};
      audioObjModified[type] = {
        initialised: true,
        instance: soundInstance,
      };
      setAudioObj(prevState => {
        let newState = {
          ...prevState,
          [type]: {
            initialised: true,
            instance: soundInstance,
          },
        };
        return newState;
      });
      autoPlay && setAutoPlayVO(type);
      // return soundInstance;
      // console.log(`audio Type>>>>${type}`);
    } catch (err) {
      // console.log(`Play Soind issue for type >> ${type} and error >>>>${err}`);
      let errorMsg = `Audio File Path: ${audioFile} && ERROR: ${err}`;
      Toast.show({
        text: errorMsg,
        duration: 50000,
        buttonText: okayBtnText,
      });
      setAudioObj(prevState => {
        let newState = {
          ...prevState,
          [type]: {
            initialised: false,
            instance: null,
            invalidFile: true,
          },
        };
        return newState;
      });
    }
  };

  const isOtherAudioPlaying = audioType => {
    let playing = false;
    for (const VO in audioObj) {
      if (
        !playing &&
        VO != audioType &&
        audioObj[VO].instance &&
        audioObj[VO].instance.isPlaying()
      ) {
        playing = true;
      }
    }

    return playing;
  };

  const audioCleanup = (reset = true) => {
    return new Promise((resolve, reject) => {
      // console.log(`Audio cleanup called`);
      for (const VO in audioObj) {
        if (audioObj[VO].instance && audioObj[VO].instance.isPlaying()) {
          audioObj[VO].instance.stop();
          audioObj[VO].instance.release();
        }
      }

      if (reset) {
        setAudioObj(initialAudioObj);
        setQBodyVoiceover(false);
        setShowInsStVO(false);
        setshowQuesVO(false);
      }
      resolve();
    });
  };

  const playSound = (audioType, audioUrl = null, localfileName) => {
    if (!isOtherAudioPlaying(audioType)) {
      if (audioObj[audioType] && audioObj[audioType].instance) {
        if (audioObj[audioType].instance.isPlaying()) {
          audioObj[audioType].instance.pause();
        } else {
          audioObj[audioType].instance.play(() => {});
        }
      } else {
        let toastMsg = audioInitialisingText;
        if (audioObj[audioType] && audioObj[audioType]?.invalidFile == true) {
          toastMsg = invalidAudioFileText;
        }
        if (!localfileName) {
          Toast.show({
            text: toastMsg,
            duration: 2000,
          });
        }

        if (!audioObj[audioType]) {
          if (audioUrl) {
            createAudioInstance(audioType, audioUrl, true);
          }
        }
        if (localfileName) {
          var mySound = new Sound(localfileName, Sound.MAIN_BUNDLE, error => {
            if (error) {
              console.log('Error loading sound: ' + error);
              return;
            } else {
              mySound.play(success => {
                if (success) {
                  console.log('Sound playing');
                } else {
                  console.log('Issue playing file');
                }
              });
            }
          });
        }
      }
    } else {
      Toast.show({
        text: anotherAudioPlayingText,
        duration: 2000,
      });
    }
  };

  const pauseAudio = () => {
    for (const VO in audioObj) {
      if (audioObj[VO].instance && audioObj[VO].instance.isPlaying()) {
        audioObj[VO].instance.pause();
      }
    }
  };

  const stopAudio = () => {
    setAutoPlayVO(null);
    audioCleanup(false);
  };

  return {
    initialiseAudio,
    audioObj,
    setAudioObj,
    showInsStVO,
    showQuesVO,
    qBodyVoiceOver,
    initializeAudioSection,
    audioCleanup,
    isOtherAudioPlaying,
    playSound,
    stopAudio,
    pauseAudio,
  };
};
