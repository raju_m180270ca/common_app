import React, {useState} from 'react';
import {useStores} from '@mobx/hooks';
import {API} from '@api';
import {ApiEndPoint} from '@constants';

const useStarQuestion = () => {
  const [starred, setStarred] = useState(false);
  const store = useStores();
  const onStarHandler = async (reqObj, starred) => {
    const req = {
      body: reqObj,
      store,
    };
    if (starred) {
      const response = await API(ApiEndPoint.REMOVE_FROM_FAVOURITES, req);
      if (response.data.resultCode == 'C001') {
        setStarred(false);
      } else {
        store.uiStore.apiErrorInit({
          code: response.status,
          message: response.data?.resultMessage,
        });
      }
    } else {
      const response = await API(ApiEndPoint.ADD_TO_FAVOURITES, req);
      if (response.data.resultCode == 'C001') {
        setStarred(true);
      } else {
        store.uiStore.apiErrorInit({
          code: response.status,
          message: response.data?.resultMessage,
        });
      }
    }
  };

  return {
    onStarHandler,
    starred,
    setStarred,
  };
};

export default useStarQuestion;
