import { observable, action } from 'mobx';

export class AppStore {
  @observable isException = false;
  @observable loader = false;
  @observable jwt = '';
  @observable userData = {};
  @observable userLanguage = '';
  @observable rewardData = {};
  @observable earnedRewardData = {};
  @observable activityID = '';
  @observable userRedirectionData = [];
  @observable screeningData = {};
  @observable screenTestQuestions = {};
  @observable screeningTestReport = {};
  @observable pushNotificationToken = '';
  @observable inputData = [];
  @observable ScreeningTestStatus = false;
  @observable isScreenTestActive = false;
  @observable sessionInformation = {};
  @observable isTrusted = false;
  @observable trustedDeviceId = '';
  @observable username = '';
  @observable subjects = '';
  @observable selectedSubject = '';


  @action setIsException(status) {
    this.isException = status;
  }
  constructor() {
    this.earnedRewardDataSchema = {
      isVisible: false,
      earnedContent: {},
      onPress: () => { },
    };
    this.setEarnedRewardData(this.earnedRewardDataSchema);
  }
  @action setLoader(status) {
    this.loader = status;
  }

  @action setJwt(jwt) {
    this.jwt = jwt;
  }

  @action setUserData(data) {
    this.userData = data;
  }

  @action setSessionInformation(data) {
    this.sessionInformation = data;
  }

  @action setRewardData(data = {}) {
    this.rewardData = data;
  }

  @action setUserRedirectionData(data) {
    this.userRedirectionData = data;
  }

  @action setScreeningData(data) {
    this.screeningData = data;
  }

  @action setScreenTestQuestions(data) {
    this.screenTestQuestions = data;
  }

  @action setScreeningTestReport(data) {
    this.screeningTestReport = data;
  }

  @action setUserLanguage(userLanguage) {
    this.userLanguage = userLanguage;
  }
  @action resetAppStoreOnLogout() {
    console.log('Clear AppStore');
    this.loader = false;
    this.jwt = '';
    this.userData = {};
    this.rewardData = {};
    this.activityID = '';
    this.userRedirectionData = [];
    this.screeningData = {};
    this.screenTestQuestions = {};
    this.screeningTestReport = {};
    this.inputData = [];
    this.ScreeningTestStatus = false;
  }

  @action setScreenTestActive(status) {
    this.isScreenTestActive = status;
  }
  @action setPushNotificationToken(token) {
    this.pushNotificationToken = token;
  }
  @action setEarnedRewardData(eRewardData) {
    this.earnedRewardData = eRewardData;
  }

  @action setScreeningTestStatus(staus) {
    this.ScreeningTestStatus = staus;
  }
  
  @action setTrusted(status) {
    this.isTrusted = status;
  }

  @action setTrustedDeviceId(deviceId) {
    this.trustedDeviceId = deviceId;
  }

  @action setUsername(user) {
    this.username = user;
  }

  @action setSubjects(value) {
    this.subjects = value;
  }

  @action setSelectedSubject(value) {
    this.selectedSubject = value;
  }
  
}
