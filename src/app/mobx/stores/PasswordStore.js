import {observable, action} from 'mobx';

export class LeaderBoardStore {
  @observable yourSectionDetails = {};

  @action setYourSectionDetails(data) {
    this.yourSectionDetails = data;
  }
}
