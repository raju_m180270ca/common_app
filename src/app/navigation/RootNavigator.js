import React, {useState} from 'react';
import {NavigationContainer} from '@react-navigation/native';
import StartUpScreen from '@screens/StartupScreen';
import {ThemeContext} from '@contexts/theme-context';

import {LoginNavigator, DashboardNavigator} from './AppNavigator';

import {useStores} from '@mobx/hooks';
import {observer} from 'mobx-react';
import {setTopLevelNavigator} from '@utils';
import moment from 'moment';

const RootNavigator = props => {
  console.log(`cmg here in rootnav screen>>>${moment()}`);
  const [selectedTheme, setSelectedTheme] = useState('ocean');

  const assignTheme = theme => {
    setSelectedTheme(theme);
  };

  const {loginStore} = useStores();
  const isAuth = loginStore.isAuth;

  console.log('RootNavigator---->');

  return (
    <NavigationContainer
      ref={navigatorRef => {
        setTopLevelNavigator(navigatorRef);
      }}>
      {isAuth && (
        <ThemeContext.Provider
          value={{
            name: selectedTheme,
            setTheme: assignTheme,
          }}>
          <DashboardNavigator />
        </ThemeContext.Provider>
      )}
      {!isAuth && loginStore.didTryAutoLogin && <LoginNavigator />}
      {!isAuth && !loginStore.didTryAutoLogin && <StartUpScreen />}
    </NavigationContainer>
  );
};

export default observer(RootNavigator);
