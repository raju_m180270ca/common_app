/* eslint-disable react-hooks/exhaustive-deps */
import React, {useEffect, Fragment, useContext} from 'react';
import SplashScreen from 'react-native-splash-screen';
import {getDeviceDetails, getAsValue, setAsValue} from '@utils';
import {v4 as uuidv4} from 'uuid';
import {runInAction} from 'mobx';
import {API} from '@api';
import {ApiEndPoint} from '@constants';
import {useStores} from '@mobx/hooks';
import {AuthContext} from '@contexts/auth-context';

const StartupScreen = props => {
  const store = useStores();
  const auth = useContext(AuthContext);

  const tryLogin = async () => {
    try {
      let appId;
      const tempJWT = await getAsValue('tempJWT');
      const jwt = await getAsValue('jwt');
      appId = await getAsValue('appId');
      const skipOnBoardingScreen = await getAsValue('skipOnBoardingScreen');
      const trustedDeviceId = await getAsValue('trustedDeviceId');
      const username = await getAsValue('userName');
      const productList = await getAsValue('userRedirectionData');
      if (productList && productList != '') {
        let products = productList.split(',');
        store.appStore.setSubjects(products);
      }
      if (trustedDeviceId && trustedDeviceId != '') {
        store.appStore.setTrustedDeviceId(trustedDeviceId);
        store.appStore.setTrusted(true);
        const subjectName = await getAsValue('subjectName');
        store.appStore.setSelectedSubject(subjectName);
      }

      if (skipOnBoardingScreen) {
        store.loginStore.setSkipOnBoardingScreen(true);
      } else {
        store.loginStore.setSkipOnBoardingScreen(false);
        // store.uiStore.setShowHomepageOverlay(true)
        // store.uiStore.setShowNavbarOverlay(true)
      }

      if (username) {
        store.appStore.setUsername(username);
      }

      if (!appId) {
        const app_id = uuidv4();
        //Store in asyncStorage
        setAsValue('appId', app_id);
        store.loginStore.setAppId(app_id);
        appId = app_id;
      } else {
        store.loginStore.setAppId(appId);
      }

      if (!tempJWT) {
        //Call Set Device Details API
        console.log('Call Set Device Details API');
        const deviceDetail = await getDeviceDetails();
        console.log('DEVICE DETAIL - ', deviceDetail);
        deviceDetail.app_id = store.loginStore.appId;
        const req = {
          body: deviceDetail,
          store,
        };
        const res = await API(ApiEndPoint.SET_DEVICE_DETAILS, req);
      }

      const req = {
        body: {
          version: store.loginStore.version,
          platform: store.loginStore.platform,
          app_id: store.loginStore.appId,
        },
        store,
      };
      const response = await API(ApiEndPoint.GET_CONFIG, req);
      runInAction(async () => {
        store.loginStore.setTempJwt(response.headers.tempjwt);
        await setAsValue('tempJWT', response.headers.tempjwt);
        store.loginStore.setConfig(response.data);
        store.loginStore.didTryAutoLogin = true;
      });
      let languageData = store?.uiStore?.languageData;
      if (!languageData?.Mindspark) {
        languageData = await getAsValue('languageData');
        store.uiStore.setLanguageData(JSON.parse(languageData));
      }
      if (skipOnBoardingScreen != 'true') {
        await setOnBoardingScreen(response.data);
      } else {
        store.loginStore.setSkipOnBoardingScreen(true);
      }

      if (jwt) {
        store.loginStore.setUserType(1);
        store.loginStore.setIsAuth(true);
        store.appStore.setJwt(jwt);
        auth.login();
        return;
      } else {
        store.loginStore.setDidTryAutoLogin(true);
        auth.setDidTryAutoLogin();
        SplashScreen.hide();
      }
    } catch (error) {
      console.log('StartupScreen ERROR - ', error);
      store.loginStore.setDidTryAutoLogin(true);
      auth.setDidTryAutoLogin();
      SplashScreen.hide();
    }
  };

  useEffect(() => {
    tryLogin();
  }, []);

  const setOnBoardingScreen = async response => {
    let data =
      response &&
      response.data &&
      response.data.featurePages &&
      response.data.featurePages.students;
    let filterData = [];
    if (data && data.length > 0) {
      filterData = data.filter(item => item.status === 'A');
      store.loginStore.setSkipOnBoardingScreen(
        filterData.length > 0 ? false : true,
      );
    } else {
      store.loginStore.setSkipOnBoardingScreen(true);
    }
    await setAsValue('skipOnBoardingScreen', 'true');
  };

  return <Fragment />;
};

export default StartupScreen;
