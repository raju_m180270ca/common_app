/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable react-native/no-inline-styles */
/**
|--------------------------------------------------
| Dashboard Screen
|--------------------------------------------------
*/
import React, {useState, useEffect, useContext, useCallback} from 'react';
import {Alert, View, BackHandler} from 'react-native';

import {
  DashboardContent,
  ListingScreen,
  DashboardFooter,
  TrustedDeviceCallout,
  Buddy,
} from '@components';
import {useStores} from '@mobx/hooks';
import {observer} from 'mobx-react';
import {
  getAsValue,
  setAsValue,
  configurePushNotification,
  getDeviceDetails,
  getProductName,
} from '@utils';
import {API} from '@api';
import {ApiEndPoint, GENERIC} from '@constants';
import {Toast} from 'native-base';
import {ThemeContext} from '@contexts/theme-context';
import styles from './indexCss';
import SplashScreen from 'react-native-splash-screen';
import {useFocusEffect} from '@react-navigation/native';
import {useNavigation} from '@react-navigation/native';
import {EarnedRewardPopup} from '@components';
import {AuthContext} from '@contexts/auth-context';
import {MixpanelCategories, MixpanelEvents, MixpanelActions} from '@constants';
import {useBackHandler} from '@react-native-community/hooks';
import {ScreenTestDialog} from '@hoc';
import {useLanguage} from '@hooks';
import DeviceInfo from 'react-native-device-info';

const DahsboardScreen = props => {
  const theme = useContext(ThemeContext);
  const {appStore, uiStore, loginStore, profileStore} = useStores();
  const store = useStores();
  const navigation = useNavigation();
  const [showSideDrawer, setShowSideDrawer] = useState(false);
  const [isBack, setIsBack] = useState(true);
  const auth = useContext(AuthContext);
  const [isStart, setIsStart] = useState(false);
  const [showTrustedPopUp, setShowTrustedPopUp] = useState(false);
  const {trustedDeviceMaxLimitMsg, loggedOutText, idleLoggedOutMsg} =
    useLanguage();

  const CancelBtnClickHandler = () => {
    setShowSideDrawer(false);
    setIsBack(true);
  };
  useBackHandler(() => {
    console.log('Entered BAck Handlerr : ');
    setIsBack(false);
    setShowSideDrawer(false);
    Alert.alert(
      'MindSpark Exit ',
      'Are you sure you want to exit App',
      [
        {
          text: 'Cancel',
          onPress: () => CancelBtnClickHandler(),
          style: 'cancel',
        },
        {
          text: 'OK',
          onPress: () => BackHandler.exitApp(),
        },
      ],
      {
        cancelable: false,
      },
    );
  });

  useFocusEffect(
    useCallback(() => {
      const reqBody = {
        jwt: getAsValue('jwt'),
        store: store,
      };
      if (store.uiStore.isAnyChangesInUserData) {
        fetchHomeDetails(reqBody);
        store.uiStore.setChangedInUserData(false);
      }
    }, []),
  );

  useEffect(() => {
    // configurePushNotification(
    //   appStore,
    //   navigation,
    //   profileStore?.enableNotificationSound,
    // );
  }, [profileStore?.enableNotificationSound]);

  useEffect(() => {
    DeviceInfo.getDeviceName().then(deviceName => {
      isDeviceTruted(deviceName);
    });

    if (loginStore.showTrustedPopUp) {
      loginStore.setShowTrustedPopUp(false);
      setShowTrustedPopUp(true);
    }
  }, []);

  useEffect(() => {
    (async () => {
      SplashScreen.hide();
      const reqBody = {
        jwt: getAsValue('jwt'),
        store: store,
      };
      const req = {
        body: reqBody.jwt,
        store: reqBody.store,
      };
      Promise.all([
        API(ApiEndPoint.MENU_LIST, req),
        API(ApiEndPoint.HOME_DETAILS, req),
      ])
        .then(response => {
          fetchMenuListAPI(reqBody, response[0]);
          fetchHomeDetails(reqBody, response[1]);
        })
        .catch(err => {
          console.log(err);
        });
    })();
  }, [store]);

  useEffect(() => {
    if (
      appStore?.pushNotificationToken !== null &&
      appStore?.pushNotificationToken !== ''
    ) {
      updateDeviceNotificationToken();
    }
  }, [store?.appStore?.pushNotificationToken]);

  useEffect(() => {
    (async () => {
      if (store.uiStore.showInactivePopUp) {
        store.uiStore.setShowInactivePopUp(false);
        if (store.loginStore.isAuth) {
          if (store.appStore.isTrusted) {
            // Update Session
            const reqBody = {
              body: {
                username: store.appStore.username,
                trustedDeviceId: store.appStore.trustedDeviceId,
                productName: getProductName(store.appStore.selectedSubject),
              },
              store: store,
            };
            const response = await API(ApiEndPoint.START_NEW_SESSION, reqBody);
            if (response.data.resultCode === 'C001') {
              appStore.setJwt(response.headers.jwt);
              await setAsValue('jwt', response.headers.jwt);
              await setAsValue('oldJWT', response.headers.jwt);
            }
          } else {
            store.uiStore.reset();
            await setAsValue('jwt', '');
            await setAsValue('oldJWT', '');
            store.leaderBoardStore.reset();
            store.appStore.setRewardData();
            store.loginStore.setIsAuth(false);
            store.loginStore.setFirstLogin(false);
            auth.logout();
            store.loginStore.setSkipOnBoardingScreen(true);
            Alert.alert(loggedOutText, idleLoggedOutMsg, [
              {
                text: 'Ok',
                onPress: () => console.log('Logout ALert btn clicked '),
                style: 'cancel',
              },
            ]);
          }
        }
      }
    })();
  }, [store?.uiStore?.showInactivePopUp]);

  const updateDeviceNotificationToken = async () => {
    const oldToken = await getAsValue('notificationToken');

    if (oldToken !== store?.appStore?.pushNotificationToken) {
      const reqBody = {
        store: store,
        body: {
          app_id: store?.loginStore?.appId,
          old_notification_token: oldToken,
          new_notification_token: store?.appStore?.pushNotificationToken,
        },
      };

      const response = await API(
        ApiEndPoint.UPDATE_DEVICE_NOTIFICATION_TOKEN,
        reqBody,
      );
      if (response?.data?.resultCode === 'C001') {
        await setAsValue(
          'notificationToken',
          store?.appStore?.pushNotificationToken,
        );
      }
    }
  };

  // Implementaion of Screening test
  const checkScreenTestStatus = async reqBody => {
    try {
      const response = await API(
        ApiEndPoint.CHECK_SCREENING_TEST_STATUS,
        reqBody,
      );
      setScreenTestData(response);
    } catch (e) {
      console.log(`Check Screen Test Status error>>>${e}`);
    }
  };

  const setScreenTestData = response => {
    if (response.data.resultCode === 'C001') {
      if (response.data.screeningTestFlag) {
        appStore.setScreenTestActive(true);
      } else if (response.data.levelTestFlag) {
        appStore.setScreenTestActive(false);
        appStore.setScreeningTestStatus(true);
      } else {
        console.log('No test is active');
        appStore.setScreeningTestStatus(true);
      }
      if (
        response.data.screeningTestFlag == true ||
        response.data.levelTestFlag == true
      ) {
        let testData = response.data.screeningTestFlag
          ? response.data.screeningData
          : response.data.levelTestData;
        appStore.setScreeningData(testData);
        switch (testData.pedagogyStatus) {
          case 'New':
            uiStore.setScreenTestDialog(true);
            setIsStart(true);
            break;
          case 'in-progress':
            uiStore.setScreenTestDialog(true);
            setIsStart(false);
            break;
          default:
            uiStore.setScreenTestDialog(false);
            break;
        }
      }
    }
  };

  const fetchHomeDetails = async (reqBody, response = undefined) => {
    try {
      const req = {
        body: reqBody.jwt,
        store: reqBody.store,
      };

      if (response == undefined) {
        response = await API(ApiEndPoint.HOME_DETAILS, req);
      }

      if (response.data.resultCode === 'C001') {
        //Set UI theme
        //popculture,robotics,music
        if (
          !GENERIC.LOTTIES.includes(response.data.userInformation.selectedTheme)
        ) {
          theme.setTheme('ocean');
        } else {
          theme.setTheme(response.data.userInformation.selectedTheme);
        }
        // set userData in appStore
        let userData = response.data.userInformation;
        if (userData.isB2CUser == true && store.uiStore.firstLogin == true) {
          store.uiStore.setFirstLogin(false);
          auth.trackEvent('cleaverTap', 'Logged in');
          auth.setUserProfile(userData, 'cleaverTap');
        } else {
          auth.setUserProfile(userData, 'mixpanel');
        }
        userData.sparkies = response.data.rewardSummary.sparkies;
        userData.notificationCount =
          response?.data?.notificationInformation?.totalUnreadNotification;
        auth.trackEvent('cleaverTap', MixpanelEvents.TOTAL_SPARKIE, {
          'Sparkie Count': userData.sparkies || '0',
        });
        let language = response?.data?.userInformation?.language;
        fetchLanguage(language);
        appStore.setUserLanguage(language);
        uiStore.setRTL(language);

        appStore.setUserData(userData);
        if (response?.data?.sessionInformation) {
          appStore.setSessionInformation(response?.data?.sessionInformation);
        }
      } else {
        if (
          response.status &&
          response.data?.resultMessage &&
          response.data?.resultMessage != ''
        ) {
          store.uiStore.apiErrorInit({
            code: response.status,
            message: response.data?.resultMessage,
          });
        }
      }
    } catch (e) {
      console.log(`Home_Details_error1>>>${e}`);
    }
  };

  const fetchMenuListAPI = async (reqBody, response) => {
    try {
      const req = {
        body: reqBody.jwt,
        store: reqBody.store,
      };
      // const response = await API(ApiEndPoint.MENU_LIST, req);
      if (response.data.resultCode === 'C001') {
        let menuPermission = response.data.permissions;
        uiStore.setMenuDataPermission(menuPermission);
        if (
          appStore.ScreeningTestStatus == false &&
          menuPermission.home.screeningTest == true
        ) {
          checkScreenTestStatus(reqBody);
        }
      } else {
        if (
          response.status &&
          response.data?.resultMessage &&
          response.data?.resultMessage != ''
        ) {
          store.uiStore.apiErrorInit({
            code: response.status,
            message: response.data?.resultMessage,
          });
        }
      }
    } catch (e) {
      console.log(`Menu List API error>>>${e}`);
    }
  };

  const fetchLanguage = async language => {
    try {
      let configTranslation = loginStore.getConfig?.data?.translation?.student;
      let userLanguage = language && language.length > 0 ? language : 'en-IN';
      if (
        configTranslation &&
        configTranslation?.hasOwnProperty(userLanguage)
      ) {
        let languageURL = configTranslation[userLanguage];
        let response = await fetch(languageURL);
        let responseJson = await response.json();
        store.uiStore.setLanguageData(responseJson);
      }
    } catch (e) {
      console.log(`Language API error>>>${e}`);
    }
  };

  const isDeviceTruted = async deviceName => {
    if (loginStore.isTrusted) {
      loginStore.setTrusted(false);

      const deviceDetail = await getDeviceDetails();
      const req = {
        body: {
          deviceName: `${deviceName}`,
          Os_type: deviceDetail.deviceOs == 'android' ? 'Android' : 'iOS',
          BrowserType: '',
          BrowserVersion: '',
        },
        store,
      };
      const response = await API(ApiEndPoint.ADD_TO_TRUSTED, req);
      if (response.data.resultCode === 'C001') {
        let responseData = response?.data?.trustedDeviceList;
        if (responseData) {
          let objectData = Object.keys(responseData);
          if (objectData.length > 0) {
            let tempArray = objectData.map(key => responseData[key]);
            appStore.setTrusted(true);
            appStore.setTrustedDeviceId(tempArray[0].deviceId);
            await setAsValue('trustedDeviceId', tempArray[0].deviceId);
            const username = await getAsValue('userName');

            if (username) {
              appStore.setUsername(username);
            }

            const productList = await getAsValue('userRedirectionData');
            if (productList && productList != '') {
              let products = productList.split(',');
              store.appStore.setSubjects(products);
            }
          }
        }
      } else if (response.data.resultCode === 'S0191') {
        Toast.show({text: trustedDeviceMaxLimitMsg, duration: 10000});
      }
    }
  };

  const headerBtnClickHandler = () => {
    auth.trackEvent('mixpanel', MixpanelEvents.HOME_HAMBURGER, {
      Category: MixpanelCategories.HAMBURGER,
      Action: MixpanelActions.CLICKED,
      Label: '',
    });
    setShowSideDrawer(!showSideDrawer);
  };

  const onSaveMySession = () => {
    auth.trackEvent('mixpanel', MixpanelEvents.TRUSTED_POP_UP_YES, {
      Category: MixpanelCategories.TRUSTEDDEVICE,
      Action: MixpanelActions.CLICKED,
      Label: '',
    });

    setShowTrustedPopUp(false);
    loginStore.setTrusted(true);

    DeviceInfo.getDeviceName().then(deviceName => {
      isDeviceTruted(deviceName);
    });
  };

  const onCloseTrustedPopUp = () => {
    auth.trackEvent('mixpanel', MixpanelEvents.TRUSTED_POP_UP_NO, {
      Category: MixpanelCategories.TRUSTEDDEVICE,
      Action: MixpanelActions.CLICKED,
      Label: '',
    });

    setShowTrustedPopUp(false);
  };

  if (uiStore.screenTestDialog === true) {
    return (
      <ScreenTestDialog
        isStart={isStart}
        onPress={() => {
          console.log('CLICKED CONTINUE');
          props.navigation.replace('ScreenTestScreen');
        }}
      />
    );
  } else {
    return (
      <ListingScreen
        testID="ListingScreenDashBoard"
        headerBtnType="menu"
        showSideDrawer={showSideDrawer && isBack}
        fromHome
        headerBtnClick={headerBtnClickHandler}>
        <Buddy style={{position: 'absolute', top: 10, right: 5}} />
        <View style={styles.contentContainer}>
          <DashboardContent
            testID="ContentDashBoard"
            clickedButton={name => {
              Toast.show({text: 'Work In Progress'});
            }}
          />
        </View>
        <EarnedRewardPopup />
        <DashboardFooter
          footerOnPress={() => {
            auth.trackEvent(
              'mixpanel',
              MixpanelEvents.GO_TO_HOME_FOOTER_PROFILE,
              {
                Category: MixpanelCategories.PROFILE,
                Action: MixpanelActions.CLICKED,
                Label: '',
              },
            );
            props.navigation.navigate('ProfileScreen');
          }}
          permissions={
            Object.keys(uiStore.menuDataPermissions).length > 0
              ? uiStore.menuDataPermissions.home
              : {}
          }
        />
        {showTrustedPopUp && (
          <TrustedDeviceCallout
            onSaveMySession={onSaveMySession}
            disableTrustedDevice={onCloseTrustedPopUp}
          />
        )}
      </ListingScreen>
    );
  }
};

export default observer(DahsboardScreen);
