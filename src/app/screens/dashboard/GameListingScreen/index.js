/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable react-native/no-inline-styles */
import React, {useState, useEffect} from 'react';
import {View, ScrollView} from 'react-native';

import {
  GameListingContent,
  BalooThambiRegTextView,
  RoundedButton,
  ListingScreen,
} from '@components';

import Orientation from 'react-native-orientation';
import {API} from '@api';
import {observer} from 'mobx-react';
import {useStores} from '@mobx/hooks';

import {ApiEndPoint} from '@constants';
import styles from './indexCss';
import {GameEmptyState8} from '@images';
import {getWp} from '@utils';
import {useLanguage} from '@hooks';
import {useBackHandler} from '@react-native-community/hooks';

const GameListingScreen = props => {
  const {} = props;
  const [gameList, setGameList] = useState([]);
  const [newGameList, setNewGameList] = useState([]);
  const store = useStores();
  const {
    playedGamesLabel,
    latestGamesLabel,
    lockedGamesLabel,
    noGamesAssignedText,
    goHomeBtnText,
  } = useLanguage();
  const [showMoreObj, setShowMoreObj] = useState({
    attempted: -1,
    unattempted: -1,
    locked: -1,
  });
  const [shouldScroll, setShouldScroll] = useState(false);

  const init = async () => {
    try {
      const req = {
        body: {},
        store,
      };
      const res = await API(ApiEndPoint.LIST_ACTIVITY, req);
      if (res.data.resultCode == 'C001') {
        let tempList = [];
        let newTempList = [];
        let showMoreObjTemp = showMoreObj;
        console.log('After:', JSON.stringify(showMoreObj));
        Object.keys(res.data.activityList).forEach(key => {
          let gameCategoryList = res.data.activityList[key];
          let keyName = '';
          let order = -1;
          let index = 0;
          if (key == 'attempted') {
            keyName = playedGamesLabel;
            order = 1;
            index = 0;
          }
          if (key == 'unattempted') {
            keyName = latestGamesLabel;
            order = 3;
            index = 2;
          }
          if (key == 'locked') {
            keyName = lockedGamesLabel;
            order = 2;
            index = 1;
          }
          if (gameCategoryList.length <= 3) {
            showMoreObjTemp[key] = -1;
          } else {
            showMoreObjTemp[key] = 0;
          }

          tempList.push({
            data: gameCategoryList,
            lessData:
              gameCategoryList.length > 3
                ? gameCategoryList.slice(0, 3)
                : gameCategoryList,
            showMoreBtn: gameCategoryList.length > 3 ? true : false,
            hideBtn: gameCategoryList.length > 3 ? false : true,
            title: keyName,
            key: key,
            order: order,
            index: index,
          });
          newTempList.push({
            data:
              gameCategoryList.length > 3
                ? gameCategoryList.slice(0, 3)
                : gameCategoryList,
            showMoreBtn: gameCategoryList.length > 3 ? true : false,
            hideBtn: gameCategoryList.length > 3 ? false : true,
            title: keyName,
            key: key,
            order: order,
            index: index,
          });
        });
        tempList.sort(function (a, b) {
          return a.order > b.order;
        });
        newTempList.sort(function (a, b) {
          return a.order > b.order;
        });
        setGameList(tempList);
        setNewGameList(newTempList);
        setShowMoreObj(showMoreObjTemp);
      } else {
        if (
          res.status &&
          res.data?.resultMessage &&
          res.data?.resultMessage != ''
        ) {
          store.uiStore.apiErrorInit({
            code: res.status,
            message: res.data?.resultMessage,
          });
        }
      }
    } catch (error) {
      //console.error(error);
      console.warn('ERROR>', JSON.stringify(error));
    }
  };

  useBackHandler(() => {
    //return props.navigation.goBack();
    return props.navigation.navigate('DashboardScreen');
  });
  useEffect(() => {
    Orientation.lockToPortrait();
    init();
  }, []);

  const toggleMoreOrLess = (key, index) => {
    let tempObj = {...showMoreObj};
    let tempList = newGameList;
    let itemIndex = 0;
    let sectionIndex = 0;
    if (key == 'attempted') {
      sectionIndex = 0;
    } else if (key == 'unattempted') {
      sectionIndex = 1;
    } else {
      sectionIndex = 2;
    }
    if (showMoreObj[key] == 0) {
      tempObj[key] = 1;
      tempList[index].data = gameList[index].data;
      itemIndex = gameList[index].data.length - 1;
    } else {
      tempObj[key] = 0;
      tempList[index].data = gameList[index].lessData;
      itemIndex = gameList[index].lessData.length - 1;
    }
    console.log('Before:', JSON.stringify(showMoreObj));
    setNewGameList(tempList);
    setShowMoreObj(tempObj);
    setShouldScroll({
      animated: true,
      itemIndex: itemIndex,
      sectionIndex: sectionIndex,
      viewPosition: 1,
    });
  };
  //console.log('Rendering Game Listing Screen:', JSON.stringify(newGameList));
  console.log('ShowMoreObj:', JSON.stringify(showMoreObj));

  const headerBtnClickHandler = () => {
    props.navigation.navigate('DashboardScreen');
  };
  console.log('JSON GAMES TEST <<< - ', JSON.stringify(newGameList));

  return (
    <ListingScreen
      testID="ListingScreenGameListing"
      headerBtnType="home"
      headerBtnClick={headerBtnClickHandler}>
      <View style={styles.contentContainer}>
        <ScrollView
          contentInsetAdjustmentBehavior="automatic"
          style={styles.scrollViewContainer}>
          {newGameList && newGameList.length > 0 ? (
            <View>
              <GameListingContent
                gameList={newGameList}
                showMoreObj={showMoreObj}
                toggleMoreOrLess={toggleMoreOrLess}
                shouldScroll={shouldScroll}
              />
              <View style={styles.bottomView} />
            </View>
          ) : (
            <View>
              <GameEmptyState8
                accessible={true}
                testID="GameListingGameEmptyState8"
                accessibilityLabel="GameListingGameEmptyState8"
                width={getWp(150)}
                style={styles.searchIcon}
              />
              <BalooThambiRegTextView
                testID="GameListingNoGamesAssignedText"
                style={styles.errorText}>
                {noGamesAssignedText}
              </BalooThambiRegTextView>
              <RoundedButton
                testID="RoundedButtonGameListingGoHomeBtn"
                onPress={() => {
                  props.navigation.navigate('DashboardScreen');
                }}
                type="primaryOrange"
                text={goHomeBtnText}
                width={150}
                containerStyle={{...styles.goHomeBtnContainer}}
              />
            </View>
          )}
        </ScrollView>
      </View>
    </ListingScreen>
  );

  return (
    <ListingScreen
      testID="ListingScreenGameListing"
      headerBtnType="home"
      headerBtnClick={headerBtnClickHandler}>
      <View style={styles.contentContainer}>
        <ScrollView
          contentInsetAdjustmentBehavior="automatic"
          style={styles.scrollViewContainer}>
          {newGameList.length > 0 ? (
            <View>
              <GameListingContent
                gameList={newGameList}
                showMoreObj={showMoreObj}
                toggleMoreOrLess={toggleMoreOrLess}
                shouldScroll={shouldScroll}
              />
              <View style={styles.bottomView} />
            </View>
          ) : (
            <View>
              <GameEmptyState8
                accessible={true}
                testID="GameListingGameEmptyState8"
                accessibilityLabel="GameListingGameEmptyState8"
                width={getWp(150)}
                style={styles.searchIcon}
              />
              <BalooThambiRegTextView
                testID="GameListingNoGamesAssignedText"
                style={styles.errorText}>
                {noGamesAssignedText}
              </BalooThambiRegTextView>
              <RoundedButton
                testID="RoundedButtonGameListingGoHomeBtn"
                onPress={() => {
                  props.navigation.navigate('DashboardScreen');
                }}
                type="primaryOrange"
                text={goHomeBtnText}
                width={150}
                containerStyle={{...styles.goHomeBtnContainer}}
              />
            </View>
          )}
        </ScrollView>
      </View>
    </ListingScreen>
  );
};

GameListingScreen.propTypes = {};

GameListingScreen.defaultProps = {};
export default observer(GameListingScreen);
