import {StyleSheet} from 'react-native';
import {COLORS, TEXTFONTSIZE} from '@constants';
import {getWp, getHp} from '@utils';

export default StyleSheet.create({
  contentContainer: {
    flex: 1,
    marginBottom: getHp(135),
  },
  scrollViewContainer: {
    marginBottom: getHp(9),
  },

  errorText: {
    fontSize: TEXTFONTSIZE.Text20,
    color: COLORS.white,
    alignSelf: 'center',
    textAlign: 'center',
   // marginTop: getHp(180),
  },

  bottomView: {
    marginBottom: getHp(45)
  },
  searchIcon : {
    alignSelf:'center',
    top:getWp(40),
    right:5
  },
  goHomeBtnContainer : {
    borderRadius: getWp(0),
    alignSelf:'center',
   // bottom:getWp(50),
   marginTop:getWp(10 )
  },
});
