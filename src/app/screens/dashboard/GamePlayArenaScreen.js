/* eslint-disable react-native/no-inline-styles */
import React, {useState, useEffect} from 'react';
import {View, Dimensions, StyleSheet} from 'react-native';
import {ApiEndPoint} from '@constants';
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen';
import Orientation from 'react-native-orientation';
import {
  RoundedButton,
  MyAutoHeightWebView,
  SVGImageBackground,
  SuccessPopup,
  BalooThambiBoldTextView,
  NewMessageModal,
} from '@components';
import {HigherMessage, GameBg} from '@images';
import {observer} from 'mobx-react';
import {useStores} from '@mobx/hooks';
import {API} from '@api';
import {useLanguage} from '@hooks';
import {getWp} from '@utils';
import {useQnA} from '@hooks';
import {runInAction} from 'mobx';
import {COLORS} from '@constants';

const GamePlayArenaScreen = props => {
  const {route} = props;
  const store = useStores();
  const {qnaStore} = store;
  const {doneBtnText, skipBtnText, msgSuccessText, closeText, activityTxt} =
    useLanguage();
  const [qUri, setQUri] = useState('');
  const [showMessage, setShowMessage] = useState(false);
  const [showSuccessPopup, setShowSuccessPopup] = useState(false);
  const permissions =
    Object.keys(store.uiStore.menuDataPermissions).length > 0
      ? store.uiStore.menuDataPermissions.question
      : {};
  let screenWidth =
    store.uiStore.initialScreenWidth == 0
      ? Dimensions.get('window').width - wp('10')
      : store.uiStore.initialScreenWidth;
  let screenHeight =
    store.uiStore.initialScreenHeight == 0
      ? Dimensions.get('window').height - hp('5')
      : store.uiStore.initialScreenHeight;
  const [submitData, setSubmitData] = useState({});
  const [isTopic, setIsTopic] = useState(false);
  const {reset} = useQnA('TOPICS');
  const callFetchContent = async () => {
    if (props?.route?.params?.data) {
      setIsTopic(false);
    } else {
      setIsTopic(true);
    }

    let req = {
      body: {},
      store,
    };
    const res = await API(ApiEndPoint.FETCH_CONTENT_V3, req);
    if (res.data.resultCode == 'C001') {
      console.log('RESPONSE GAME SUCCESS:', res.data.contentData.data[0].file);
      initializeIFrame(res.data.contentData.data[0].file);
      var contentData = res.data.contentData;

      var data = contentData.data[0];
      var contentInfo = {
        contentID: contentData.contentId,
        contentVersionID: data._id,
        contentType: contentData.contentType,
        activityType: 'regular',
        revisionNum: data.revisionNo,
        langCode: data.langCode,
      };
      var submitInfo = {
        contentID: contentData.contentId,
        contentInfo: contentInfo,
        contentSubMode: contentData.contentSubMode,
        contentSeqNum: contentData.contentSeqNum,
      };
      setSubmitData(submitInfo);
    } else {
      console.log('RES:', JSON.stringify(res.data));
    }
  };

  const submitActivityAttempt = async () => {
    const req = {
      store: store,
      body: submitData,
    };
    const res = await API(ApiEndPoint.SUBMIT_ACTIVITY_ATTEMPT_V3, req);
    if (res.data.resultCode === 'C001') {
      qnaStore.setNextQuestionData(res.data);
      runInAction(() => {
        qnaStore.showExplanation = false;
        qnaStore.isOpenActivity = false;
        qnaStore.isSubmitEnabled = true;
      });
      await reset();
      qnaStore.init(qnaStore.nextQuestionRes);
      props.navigation.goBack();
    } else if (
      res.data.resultCode === 'C004' &&
      res?.data?.redirectionData?.endTopicFlag === true
    ) {
      props.navigation.replace('TopicSummaryScreen');
    } else if (
      res.data.resultCode === 'C004' &&
      res?.data?.redirectionData?.endActivityFlag === true
    ) {
      props.navigation.goBack();
    } else {
      store.uiStore.apiErrorInit({
        code: res.status,
        message: res.data?.resultMessage,
      });
    }
  };

  useEffect(() => {
    Orientation.lockToLandscape();
    Orientation.addOrientationListener(onOrientationChangeHandler);
    if (store.uiStore.initialScreenWidth == 0) {
      store.uiStore.setInitialScreenWidth(screenWidth);
    }
    if (store.uiStore.initialScreenHeight == 0) {
      store.uiStore.setInitialScreenHeight(screenHeight);
    }
    return () => {
      Orientation.lockToPortrait();
      Orientation.removeOrientationListener(onOrientationChangeHandler);
    };
  }, []);

  const onOrientationChangeHandler = orientation => {
    if (orientation == 'LANDSCAPE') {
      if (route?.params?.file) {
        initializeIFrame(route?.params?.file);
      } else {
        callFetchContent();
      }
    }
  };

  const AvtivityTagView = () => {
    return (
      <View style={styles.ActivityViewStyle}>
        <BalooThambiBoldTextView style={styles.ActivityTxtStyle}>
          {activityTxt}
        </BalooThambiBoldTextView>
      </View>
    );
  };

  const getParams = () => {
    let data = {
      contentDetails: {
        contentType: qnaStore.contentData.contentType,
        context: qnaStore.currentQuestion.langCode,
        revisionNo: qnaStore.currentQuestion.revisionNo,
        contentSeqNum: qnaStore.contentData.contentSeqNum,
        contentAttempted: qnaStore.isNextBtnVisible,
      },
      contentID: qnaStore.contentData.contentId,
    };

    return data;
  };

  const initializeIFrame = file => {
    var qUriTemp = '';
    try {
      qUriTemp = decodeURI(file);
    } catch (err) {
      qUriTemp = file;
    }
    console.log('Game Frame' + qUriTemp);
    let gameHtml = `<!DOCTYPE html>
    <html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Document</title>
        <style>
        body {
          font-size:100% !important;
        }
        </style>
    </head>
    <body>
    <div><iframe id='quesInteractive' src='${qUriTemp}' height='${screenWidth}' width='${screenHeight}' scrolling='yes'></iframe></div>
    </body>
    </html>`;

    setQUri(gameHtml);
  };

  return (
    <View style={{flex: 1}}>
      <View
        style={{
          justifyContent: 'center',
          flex: 1,
          flexDirection: 'row',
        }}>
        <SVGImageBackground
          testID="SVGImageBackgroundGamePlayArena"
          SvgImage={GameBg}>
          <View style={{flex: 1}}>
            <MyAutoHeightWebView
              testID="MyAutoHeightWebViewGamePlayArena"
              onMessage={props.onWebViewCallback}
              style={{
                width: store.uiStore.initialScreenHeight,
                height: screenHeight,
                position: 'relative',
                fontSize: 16,
              }}
              onSizeUpdated={size => {
                ///console.log(size.height,size.width, screenWidth, screenHeight, 'screenHeight=============================================');
              }}
              source={{html: qUri}}
              zoomable={false}
              textZoom={100}
            />
          </View>
        </SVGImageBackground>
      </View>
      {isTopic ? null : <AvtivityTagView />}
      {/* {
        qnaStore?.enableActivitySkipButton &&
        <View
          style={{
            position: 'absolute',
            left: wp('10'),
            top: hp('2'),
            zIndex: 5,
          }}>
          <RoundedButton     
            testID="RoundedButtonGamePlayArenaDoneBtnText"
            type="squareOrange"
            text={isTopic ? doneBtnText : closeText}
            textStyle={{
              fontFamily: 'BalooThambi-Regular',
              fontSize: wp('4'),
            }}
            containerStyle={{
              marginTop: hp('2'),
              marginBottom: hp('2'),
              alignSelf: 'center',
            }}
            width={wp('12')}
            height={wp('12')}
            onPress={() => {
              if (qnaStore?.enableActivitySkipButton) {
                qnaStore.activityButtonType = 'done';
              }
              props.navigation.goBack();
            }}
          />
        </View>
      }  */}
      <View
        style={{
          position: 'absolute',
          right: wp('10'),
          top: hp('2'),
          zIndex: 5,
        }}>
        <RoundedButton
          type="squareOrange"
          text={isTopic ? doneBtnText : closeText}
          textStyle={{
            fontFamily: 'BalooThambi-Regular',
            fontSize: wp('4'),
          }}
          containerStyle={{
            marginTop: hp('2'),
            marginBottom: hp('2'),
            alignSelf: 'center',
          }}
          width={wp('12')}
          height={wp('12')}
          onPress={() => {
            if (qnaStore?.enableActivitySkipButton) {
              qnaStore.activityButtonType = 'done';
            }
            submitActivityAttempt();
          }}
        />
        {qnaStore?.enableActivitySkipButton && (
          <RoundedButton
            testID="RoundedButtonGamePlayArenaSkipBtnText"
            type="squareOrange"
            text={skipBtnText}
            textStyle={{
              fontFamily: 'BalooThambi-Regular',
              fontSize: wp('4'),
            }}
            containerStyle={{
              marginBottom: hp('2'),
              marginTop: hp('2'),
              alignSelf: 'center',
            }}
            width={wp('12')}
            height={wp('12')}
            onPress={() => {
              // qnaStore.activityButtonType = 'skip';
              // props.navigation.goBack();
              submitActivityAttempt();
            }}
          />
        )}
        {permissions.comment && (
          <View style={styles.mrgnRight6}>
            <HigherMessage
              onPress={() => {
                Orientation.lockToPortrait();
                setShowMessage(true);
              }}
              height={styles.bottomLeftSvgSize.height}
              width={styles.bottomLeftSvgSize.width}
            />
          </View>
        )}
      </View>
      <NewMessageModal
        isVisible={showMessage}
        pageId={'contentPage'}
        params={getParams()}
        onSuccess={() => {
          setShowMessage(false);
          Orientation.lockToLandscape();
          onOrientationChangeHandler('LANDSCAPE');
        }}
        onHide={() => {
          Orientation.lockToPortrait();
          setShowSuccessPopup(true);
        }}
        onclose={() => {
          Orientation.lockToLandscape();
          onOrientationChangeHandler('LANDSCAPE');
          setShowMessage(false);
        }}
      />

      <SuccessPopup
        isVisible={showSuccessPopup}
        text={msgSuccessText}
        onPress={() => {
          setShowSuccessPopup(false);
          Orientation.lockToLandscape();
          onOrientationChangeHandler('LANDSCAPE');
        }}
      />
    </View>
  );
};

GamePlayArenaScreen.propTypes = {};

GamePlayArenaScreen.defaultProps = {};
const styles = StyleSheet.create({
  mrgnRight6: {},
  bottomLeftSvgSize: {
    height: getWp(52),
    width: getWp(52),
  },
  ActivityViewStyle: {
    position: 'absolute',
    left: wp('2'),
    right: wp('2'),
    top: hp('2'),
    zIndex: 5,
    width: 120,
    height: 50,
    borderRadius: 10,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: COLORS.skyBlue,
  },
  ActivityTxtStyle: {
    color: COLORS.white,
    fontFamily: 'BalooThambi-Regular',
    fontSize: wp('6'),
  },
});
export default observer(GamePlayArenaScreen);
