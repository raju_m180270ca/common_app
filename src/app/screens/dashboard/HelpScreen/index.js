// External Imports
import React, {useEffect} from 'react';
import {View, Dimensions, ScrollView, Image, BackHandler} from 'react-native';
import {getHp} from '@utils';
import {CommonActions} from '@react-navigation/native';
import {Header} from '@components';
import styles from './style';
import {
  RobotoRegTextView,
  BalooThambiRegTextView,
} from '@components';
import {
  call_us,
  email_us,
  whatsapp_us,
  mit,
  standford,
  harvard,
  ComponentImg,
} from '@images';
import {useLanguage} from '@hooks';

const HelpScreen = props => {
  useEffect(() => {
    const backHandler = BackHandler.addEventListener(console.log('Back'));
    return () => backHandler.remove();
  }, []);

  const {
    About_mindspark,
    minSpark_disc,
    mindspark_disc2,
    Future1,
    Future2,
    Future3,
    Future4,
    Future5,
    Future6,
    Future7,
    Future8,
    futureOfMindSpark,
    ReviewedRecognised,
  } = useLanguage();

  return (
    <View style={styles.container}>
      <Header
        testID="HeaderPreviewQuestionBack"
        type="back"
        onClick={() => props.navigation.dispatch(CommonActions.goBack())}
      />
      <View style={styles.InnerContainer}>
        <ScrollView showsVerticalScrollIndicator={false}>
          <BalooThambiRegTextView style={styles.InnerHeaderText}>
            {About_mindspark}
          </BalooThambiRegTextView>
          <View style={styles.SocialConnectionView}>
            <Image
              width="45%"
              height="45%"
              source={call_us}
              style={styles.ComponentImageStyle}
            />
            <Image
              width="45%"
              height="45%"
              source={email_us}
              style={styles.ComponentImageStyle}
            />
            <Image
              width="45%"
              height="45%"
              source={whatsapp_us}
              style={styles.ComponentImageStyle}
            />
          </View>
          <View style={styles.MindSparkDesc}>
            <RobotoRegTextView style={styles.ContentText}>
              {minSpark_disc}
              {'\n'}
              {'\n'}
              {mindspark_disc2}
            </RobotoRegTextView>
          </View>

          <View style={{flexDirection: 'row'}}>
            <ComponentImg
              accessible={true}
              testID="HelpComponent"
              accessibilityLabel="HelpComponent"
              width={Dimensions.get('window').width}
              height={getHp(250)}
              style={styles.ComponentImg}
            />
          </View>

          <View style={{marginTop: 10}}>
            <BalooThambiRegTextView style={styles.InnerHeaderText}>
              {futureOfMindSpark}
            </BalooThambiRegTextView>
          </View>

          <View style={styles.ReviewFetureView}>
            <RobotoRegTextView style={styles.ContentText2}>
              • {Future1} {'\n'}• {Future2} {'\n'}• {Future3} {'\n'}• {Future4}{' '}
              {'\n'}• {Future5} {'\n'}• {Future6} {'\n'}• {Future7} {'\n'}•{' '}
              {Future8} {'\n'}
            </RobotoRegTextView>
          </View>
          <View>
            <BalooThambiRegTextView style={styles.InnerHeaderText}>
              {ReviewedRecognised}
            </BalooThambiRegTextView>
          </View>
          <View style={{marginTop: 8}}>
            <Image
              height="62"
              style={styles.RectangleComponent}
              source={harvard}
            />
            <Image
              height="62"
              style={styles.RectangleComponent}
              source={standford}
            />
            <Image height="62" style={styles.RectangleComponent} source={mit} />
          </View>
        </ScrollView>
      </View>
    </View>
  );
};

export default HelpScreen;
