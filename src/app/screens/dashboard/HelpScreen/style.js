import {StyleSheet, Dimensions} from 'react-native';
import { getHp, getWp } from '@utils';
import { COLORS, TEXTFONTSIZE } from '@constants';

export default StyleSheet.create({
  container : {
    flex:1,
    backgroundColor:COLORS.blue,
  },
InnerContainer:{
  backgroundColor:COLORS.white,
  marginTop:getHp(25.5),
  marginLeft:getWp(17.5),
  marginRight:getWp(17.5),
  marginBottom:getHp(17),
  borderRadius:getWp(15),
  flex:1,
},
InnerHeaderText:{
   textAlign:'center',
   marginTop:getHp(15),
   color:'#b8292c',
   fontSize:TEXTFONTSIZE.Text17,
},
SocialConnectionView:{
   flexDirection:'row',
   justifyContent:'center',
   alignItems:'center',
   marginTop:getHp(11),
},
ContentText:{
   textAlign:'center',
   fontSize:TEXTFONTSIZE.Text17,
   color: 'black',
   opacity:0.7,
},
MindSparkDesc : {
   marginLeft:10,
   marginRight:10,
},
ContentText2:{
   fontSize:TEXTFONTSIZE.Text17,
   color: 'black',
   opacity:0.7,
},
ReviewFetureView:{
   marginTop: 5,
   textAlign:'left',
   marginLeft:15,
   marginRight:11,
},
ComponentImg:{
   alignItems:'center',
   justifyContent:'center',
   marginTop:getHp(21),
   marginLeft:20,
  
},
RectangleComponent:{
   width:Dimensions.get('window').width-30,
   justifyContent:'center',
   alignItems:'center',  
},
ComponentImageStyle:{
   marginLeft:getWp(-10),
   marginRight:getWp(-10),
}

});
