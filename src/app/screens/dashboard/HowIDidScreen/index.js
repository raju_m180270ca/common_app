/* eslint-disable react-hooks/exhaustive-deps */
import React, {useState, useEffect, Fragment, useContext} from 'react';
import {FlatList, RefreshControl, Platform} from 'react-native';
import {API} from '@api';
import {useStores} from '@mobx/hooks';
import {ApiEndPoint} from '@constants';
import { NativeBaseProvider } from 'native-base';
import {
  HowIDidHeader,
  BalooThambiRegTextView,
  FilterItem,
  QuestionCard,
  PaginationView,
  DetailsScreen,
  SelectionPopup,
} from '@components';
import { useNavigation } from '@react-navigation/native';
import {QuestionItem} from '@hoc';
import {observer} from 'mobx-react';
import {View} from 'native-base';
import styles from './style';
import {GameEmptyState8} from '@images';
import {getWp} from '@utils';
import {useLanguage} from '@hooks';
import {useStarQuestion} from '@hooks';
import {useBackHandler} from '@react-native-community/hooks';
import {AuthContext} from '@contexts/auth-context';
import {MixpanelCategories, MixpanelEvents, MixpanelActions} from '@constants';

const HowIDidScreen = props => {
  const [refreshing, setRefreshing] = useState(false);
  const [topicList, setTopicList] = useState([]);
  const [currentMode, setCurrentMode] = useState('all');
  const [showRemoveFavPopUp, setShowRemoveFavPopUp] = useState(false);
  const [removeFavIndex, setShowRemoveFavIndex] = useState(0);
  const auth = useContext(AuthContext);
  const navigation = useNavigation();
  //navigation.navigate('TopicListingScreen');

  const {onStarHandler} = useStarQuestion();

  const {
    forAttemptNumber,
    filterNoQuestionFound,
    howIDidEmptyStateText,
    attentionPleaseText,
    thisWillRemoveDescText,
    noCancelBtnText,
    yesRemoveBtnText,
  } = useLanguage();

  const {topic} = props.route.params;
  console.log('\nparams', props.route.params);
  console.log('\ntopic', topic);
  const store = useStores();
  const {topicTrailsStore} = useStores();

  const permissions =
    Object.keys(store.uiStore.menuDataPermissions).length > 0
      ? store.uiStore.menuDataPermissions.howIDid
      : {};

  useBackHandler(() => {
    return navigation.navigate('DashboardScreen');
  });

  useEffect(() => {
    fetchContent(1);
  }, []);

  const fetchContent = async index => {
    if (!refreshing) {
      setRefreshing(true);
      let req = {
        body: {
          topicId: topic.contentID,
          index: index,
          limit: 20,
          startFrom: (index - 1) * 20 + 1,
        },
        store: store,
      };
      let res = await API(ApiEndPoint.GET_TOPIC_TRAILS, req);
      if (res.data.resultCode === 'C001') {
        if (index === 1) {
          let Q_id = 1;
          if (res.data.trailList && res.data.trailList.length > 0) {
            let trailList = res.data.trailList.map(data => {
              data.Q_id = Q_id++;
              return data;
            });
            res.data.topicList = trailList;
            //topicTrailsStore.setTrailList(trailList);
            topicTrailsStore.init(res.data);
          } else {
            res.data.topicList = [];
            topicTrailsStore.init(res.data);
          }
        } else {
          let Q_id = (index - 1) * 20 + 1;
          if (res.data.trailList && res.data.trailList.length > 0) {
            let trailList = res.data.trailList.map(data => {
              data.Q_id = Q_id++;
              return data;
            });
            topicTrailsStore.setTrailList(trailList);
          } else {
            topicTrailsStore.setTrailList([]);
          }

          //topicTrailsStore.setTrailList(res.data.trailList);
        }
        const {topicDetails} = res.data;
        topicTrailsStore.setTopicDetails(topicDetails);
        if (topicDetails) {
          auth.trackEvent('cleaverTap', MixpanelEvents.SESSION_REPORT);
          auth.trackEvent('mixpanel', MixpanelEvents.SESSION_REPORT, {
            Category: MixpanelCategories.TOPIC,
            Action: MixpanelActions.OPEN,
            Label: ``,
            Accuracy: topicDetails.accuracy || '0',
          });
        }
        filter(currentMode);
      } else {
        if (
          res.status &&
          res.data?.resultMessage &&
          res.data?.resultMessage != ''
        ) {
          store.uiStore.apiErrorInit({
            code: res.status,
            message: res.data?.resultMessage,
          });
        }
      }

      setRefreshing(false);
    }
  };

  const renderItem = (item, index) => {
    if (item.contentMode == 'TimedTest') {
      return (
        <QuestionCard
          accessible={true}
          testID={`HowIDidQuestionCard${item.data._id}`}
          accessibilityLabel={`HowIDidQuestionCard${item.data._id}`}
          response={item}
          permissions={permissions}
        />
      );
    } else {
      return (
        <QuestionItem
          accessible={true}
          testID={`HowIDidQuestionCard${item.data._id}`}
          accessibilityLabel={`HowIDidQuestionCard${item.data._id}`}
          response={item}
          permissions={permissions}
          onPressStar={() => onPressStar(item, index)}
        />
      );
    }
  };

  const filter = mode => {
    switch (mode) {
      case 'all':
        setTopicList(topicTrailsStore.trailList);
        break;
      case 'wrong':
        setTopicList(
          topicTrailsStore.trailList.filter(
            item =>
              item.userAttemptData.result === 'fail' ||
              item.userAttemptData.result === 'learn' ||
              item.userAttemptData.result === 'skip',
          ),
        );
        break;
      case 'right':
        setTopicList(
          topicTrailsStore.trailList.filter(
            item => item.userAttemptData.result === 'pass',
          ),
        );
        break;
    }
  };

  const headerBtnClickHandler = () => {
    navigation.goBack();
    //navigation.g
  };

  const onPressStar = (item, index) => {
    let trailListCopy = [...topicList];
    let itemCopy = {...item};
    if (itemCopy.isFavourite) {
      setShowRemoveFavPopUp(true);
      setShowRemoveFavIndex(index);
    } else {
      itemCopy.isFavourite = true;
      trailListCopy[index] = itemCopy;
      setTopicList(trailListCopy);
      let req = {};
      req = {
        conceptID: itemCopy.conceptID,
        topicID: topicTrailsStore.topicDetails.topicId,
        contentInfo: {
          contentID: itemCopy.contentID,
          contentVersionID: itemCopy.data._id,
          version: itemCopy.revisionNo,
          context: itemCopy.langCode,
        },
      };
      onStarHandler(req, false);
    }
  };

  const onCancelPressHandler = () => {
    setShowRemoveFavPopUp(false);
  };

  const onActionPressHandler = () => {
    let trailListCopy = [...topicList];
    let itemCopy = {...trailListCopy[removeFavIndex]};
    itemCopy.isFavourite = false;
    trailListCopy[removeFavIndex] = itemCopy;
    setTopicList(trailListCopy);
    setShowRemoveFavPopUp(false);
    let req = {};
    req = {
      contentId: itemCopy.contentID,
      topicId: topicTrailsStore.topicDetails.topicId,
    };
    onStarHandler(req, true);
  };

  return (
    <DetailsScreen
      testID="DetailsScreenHowIDidBackBtn"
      headerBtnType="back"
      headerBtnClick={headerBtnClickHandler}
      footerContainerStyle={styles.footerContainerStyle}>
      {topicTrailsStore.topicTrailResponse && (
        <NativeBaseProvider>
        <View style={styles.flexOne}>
          <BalooThambiRegTextView
            testID="HowIDidScreenAttemptedNumbers"
            style={styles.attemptsHeading}>
            {forAttemptNumber}
            {topicTrailsStore.topicDetails &&
              topicTrailsStore.topicDetails.attemptNumbers &&
              ` ${topicTrailsStore.topicDetails.attemptNumbers.join(' & ')}`}
          </BalooThambiRegTextView>
          {Object.keys(topicTrailsStore.topicDetails).length > 0 && (
            <HowIDidHeader
              testID="HeaderHowIDid"
              attempts={topicTrailsStore.topicDetails.totaltAttempts}
              questionsDone={topicTrailsStore.topicDetails.totalQuestion}
              accuracy={topicTrailsStore.topicDetails.accuracy}
              permissions={permissions}
            />
          )}

          {topicTrailsStore.trailList &&
          topicTrailsStore.trailList.length > 0 ? (
            <Fragment>
              <View style={styles.headerFilterContainer}>
                <FilterItem
                  testID="ItemHowIDidFilter"
                  questionCount={topicTrailsStore.topicDetails.totalQuestion}
                  wrongCount={topicTrailsStore.topicDetails.totalWrong}
                  rightCount={topicTrailsStore.topicDetails.totalCorrect}
                  onClick={mode => {
                    setCurrentMode(mode);
                    filter(mode);
                  }}
                />
                {topicTrailsStore?.topicDetails?.totalPages > 1 ? (
                  <PaginationView
                    testID="PaginationViewHowIDidBtns"
                    currentPage={topicTrailsStore?.topicDetails?.currentPage}
                    totalPage={topicTrailsStore?.topicDetails?.totalPages}
                    onNextBtnPressed={() => {
                      let index = topicTrailsStore?.topicDetails?.currentPage;
                      fetchContent(index + 1);
                    }}
                    onPreviousBtnPressed={() => {
                      let index = topicTrailsStore?.topicDetails?.currentPage;
                      fetchContent(index - 1);
                    }}
                  />
                ) : null}
              </View>
              {topicList !== null && topicList.length > 0 ? (
                <FlatList
                  data={topicList}
                  refreshControl={<RefreshControl refreshing={refreshing} />}
                  renderItem={({item, index}) => renderItem(item, index)}
                  keyExtractor={item => item.data._id}
                  contentContainerStyle={styles.listContainerStyle}
                  windowSize={4}
                  removeClippedSubviews={true}
                  initialNumToRender={2}
                />
              ) : (
                <BalooThambiRegTextView
                  testID="HowIDidNoQuestionsFound"
                  style={styles.attemptsHeading}>
                  {filterNoQuestionFound}
                </BalooThambiRegTextView>
              )}
            </Fragment>
          ) : (
            <View style={{alignSelf: 'center'}}>
              <GameEmptyState8
                accessible={true}
                testID="HowIDidGameEmptyState8"
                accessibilityLabel="HowIDidGameEmptyState8"
                width={getWp(150)}
                style={styles.searchIcon}
              />
              <BalooThambiRegTextView
                testID="HowIDidEmptyStateText"
                style={styles.attemptsHeadingEmpty}>
                {howIDidEmptyStateText}
              </BalooThambiRegTextView>
            </View>
          )}
        </View>
        </NativeBaseProvider>
      )}
      <SelectionPopup
        testID="SelectionPopupStarredQuestion"
        show={showRemoveFavPopUp}
        svgText={attentionPleaseText}
        desc={thisWillRemoveDescText}
        cancelBtnText={noCancelBtnText}
        actionBtnText={yesRemoveBtnText}
        onCancelPress={onCancelPressHandler}
        onActionPress={onActionPressHandler}
      />
    </DetailsScreen>
  );
};

export default observer(HowIDidScreen);
