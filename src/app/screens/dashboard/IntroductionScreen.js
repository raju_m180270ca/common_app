/* eslint-disable react-native/no-inline-styles */
import React, {useState, useEffect} from 'react';
import {View, Dimensions} from 'react-native';
import {ApiEndPoint} from '@constants';
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen';
import Orientation from 'react-native-orientation';
import {
  RoundedButton,
  MyAutoHeightWebView,
  SVGImageBackground,
  NewMessageModal,
  SuccessPopup,
} from '@components';
import {observer} from 'mobx-react';
import {useStores} from '@mobx/hooks';
import {API} from '@api';
import {useLanguage} from '@hooks';
import {runInAction} from 'mobx';
import {useQnA} from '@hooks';
import {HigherMessage, GameBg} from '@images';
import {getWp, getHp} from '@utils';

const IntroductionScreen = props => {
  const {route} = props;
  const store = useStores();
  const {qnaStore} = store;
  const {reset} = useQnA('TOPICS');

  const {nextText, msgSuccessText} = useLanguage();
  const [qUri, setQUri] = useState('');
  const [submitData, setSubmitData] = useState({});
  const [showMessage, setShowMessage] = useState(false);
  const [showSuccessPopup, setShowSuccessPopup] = useState(false);
  //const {msgSuccessText} = useLanguage();
  //const [showMessage, setShowMessage] = useState(false);

  let screenWidth = Dimensions.get('window').width - wp('10');
  let screenHeight = Dimensions.get('window').height - hp('5');

  const callFetchContent = async () => {
    let req = {
      body: {},
      store,
    };
    const res = await API(ApiEndPoint.FETCH_CONTENT_V3, req);
    if (res.data.resultCode == 'C001') {
      var contentData = res.data.contentData;

      var data = contentData.data[0];
      var contentInfo = {
        contentID: contentData.contentId,
        contentVersionID: data._id,
        contentType: contentData.contentType,
        activityType: 'introduction',
        revisionNum: data.revisionNo,
        langCode: data.langCode,
      };
      var submitInfo = {
        contentID: contentData.contentId,
        contentInfo: contentInfo,
        contentSubMode: contentData.contentSubMode,
        contentSeqNum: contentData.contentSeqNum,
      };
      setSubmitData(submitInfo);
      initializeIFrame(data.file);
    } else {
      console.log('RES:', JSON.stringify(res.data));
    }
  };

  const submitActivityAttempt = async option => {
    const req = {
      store: store,
      body: submitData,
    };

    const res = await API(ApiEndPoint.SUBMIT_ACTIVITY_ATTEMPT_V3, req);
    console.log('Option  = ' + option);
    if (res.data.resultCode === 'C001') {
      qnaStore.setNextQuestionData(res.data);
      runInAction(() => {
        qnaStore.showExplanation = false;
        qnaStore.isOpenActivity = false;
      });
      await reset();
      qnaStore.init(qnaStore.nextQuestionRes);
      // props.navigation.navigate('TopicQnAScreen');
      Orientation.lockToPortrait();
      if (option == 'done') {
        props.navigation.replace('TopicSummaryScreen');
      } else props.navigation.goBack();
    } else {
      store.uiStore.apiErrorInit({
        code: res.status,
        message: res.data?.resultMessage,
      });
    }
  };

  useEffect(() => {
    Orientation.lockToLandscape();

    Orientation.addOrientationListener(onOrientationChangeHandler);
    return () => {
      Orientation.lockToPortrait();
      Orientation.removeOrientationListener(onOrientationChangeHandler);
    };
  }, [onOrientationChangeHandler]);

  const onOrientationChangeHandler = orientation => {
    if (orientation == 'LANDSCAPE') {
      if (route?.params?.file) {
        initializeIFrame(route?.params?.file);
      } else {
        callFetchContent();
      }
    }
  };

  const initializeIFrame = file => {
    var qUriTemp = '';
    try {
      qUriTemp = decodeURI(file);
    } catch (err) {
      qUriTemp = file;
    }
    console.log('Game Frame' + qUriTemp);
    let gameHtml = `<!DOCTYPE html>
    <html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Document</title>
        <style>
        body {
          font-size:100% !important;
        }
        </style>
    </head>
    <body>
    <div><iframe id='quesInteractive' src='${qUriTemp}' height='${screenWidth}' width='${screenHeight}' scrolling='yes'></iframe></div>
    </body>
    </html>`;

    setQUri(gameHtml);
  };

  const getParams = () => {
    let data = {
      contentDetails: {
        contentType: qnaStore.contentData.contentType,
        context: qnaStore.currentQuestion.langCode,
        revisionNo: qnaStore.currentQuestion.revisionNo,
        contentSeqNum: qnaStore.contentData.contentSeqNum,
        contentAttempted: qnaStore.isNextBtnVisible,
      },
      contentID: qnaStore.contentData.contentId,
    };

    return data;
  };

  return (
    <View style={{flex: 1}}>
      <View
        style={{
          justifyContent: 'center',
          flex: 1,
          flexDirection: 'row',
          width: '100%',
        }}>
        <SVGImageBackground
          testID="SVGImageBackgroundGamePlayArena"
          SvgImage={GameBg}>
          <View style={{flex: 0.95, marginTop: '2%'}}>
            <MyAutoHeightWebView
              testID="MyAutoHeightWebViewGamePlayArena"
              onMessage={props.onWebViewCallback}
              containerStyle={{width: screenWidth, height: screenHeight}}
              style={{
                width: screenWidth,
                height: screenHeight,
                position: 'relative',
                fontSize: 16,
              }}
              onSizeUpdated={size => {
                // console.log(size.height, screenWidth, screenHeight, 'screenHeight=============================================');
              }}
              source={{html: qUri}}
              zoomable={false}
              textZoom={100}
            />
          </View>
        </SVGImageBackground>
      </View>
      <View
        style={{
          position: 'absolute',
          right: wp('35'),
          //top:-1,
          zIndex: 5,
        }}>
        <RoundedButton
          testID="RoundedButtonGamePlayArenaDoneBtnText"
          type="squareOrange"
          text={'Done'}
          textStyle={{
            fontFamily: 'BalooThambi-Regular',
            fontSize: wp('4'),
          }}
          containerStyle={{
            marginTop: hp('1'),
            marginBottom: hp('1'),
            alignSelf: 'center',
          }}
          width={wp('12')}
          height={wp('12')}
          onPress={() => {
            submitActivityAttempt('done');
            // Orientation.lockToPortrait();
            // props.navigation.replace('TopicSummaryScreen');
          }}
        />
      </View>
      <View
        style={{
          position: 'absolute',
          right: wp('10'),
          top: hp('2'),
          zIndex: 5,
        }}>
        <HigherMessage
          onPress={() => {
            setShowMessage(true);
          }}
          style={{marginTop: hp('2'), marginBottom: hp('2')}}
          accessible={true}
          testID="HigherMessageTopicQnA"
          accessibilityLabel="HigherMessageTopicQnA"
        />

        <RoundedButton
          testID="RoundedButtonGamePlayArenaDoneBtnText"
          type="squareOrange"
          text={nextText}
          textStyle={{
            fontFamily: 'BalooThambi-Regular',
            fontSize: wp('4'),
          }}
          containerStyle={{
            marginTop: hp('2'),
            marginBottom: hp('2'),
            alignSelf: 'center',
          }}
          width={wp('12')}
          height={wp('12')}
          onPress={() => {
            submitActivityAttempt('next');
          }}
        />
      </View>

      <NewMessageModal
        testID="NewMessageModalTopicQnA"
        isVisible={showMessage}
        pageId={'contentPage'}
        params={getParams()}
        style={{
          marginTop: '2%',
          width: '70%',
          height: '108%',
          paddingVertical: getHp(1),
        }}
        onSuccess={() => {
          setShowMessage(false);
        }}
        onHide={() => {
          setShowSuccessPopup(true);
        }}
        onclose={() => {
          setShowMessage(false);
        }}
      />
      <SuccessPopup
        testID="SuccessPopupTopicQnA"
        isVisible={showSuccessPopup}
        text={msgSuccessText}
        containerStyle={{height: '90%', width: '70%'}}
        onPress={() => {
          setShowSuccessPopup(false);
        }}
      />
    </View>
  );
};

IntroductionScreen.propTypes = {};

IntroductionScreen.defaultProps = {};

export default observer(IntroductionScreen);
