// External Import
import React, {useState, useEffect} from 'react';
import {FlatList, View,DetailsScreen} from 'react-native';
import {useStores} from '@mobx/hooks';
import {observer} from 'mobx-react';

// Internal Imports
import {BalooThambiRegTextView, NotificationListItem} from '@components';
// import {DetailsScreen} from '@hoc';

import styles from './style';
import {notificationTimeDifference} from '@utils';
import {ApiEndPoint} from '@constants';
import {API} from '@api';
import {useLanguage} from '@hooks';

const NotificationListScreen = props => {
  const {} = props;
  const {notificationPlural} = useLanguage();
  const store = useStores();
  const {uiStore, notificationStore} = useStores();
  const [errorMessage, setErrorMessage] = useState('');

  useEffect(() => {
    fetchNotification(1);
  }, []);

  const fetchNotification = async index => {
    if (index !== 1) {
      index = notificationStore?.paginationDetails?.currentPage + 1;
      if (index > notificationStore?.paginationDetails?.totalPages) {
        return;
      }
    }

    const reqBody = {
      store: store,
      body: {
        limit: 10,
        page: index,
      },
    };

    const response = await API(ApiEndPoint.VIEW_ALL_NOTIFICATIONS, reqBody);
    if (response?.data?.resultCode === 'C001') {
      if (index === 1) {
        let userData = store?.appStore?.userData;
        userData.notificationCount =
          response?.data?.notificationInformation?.totalUnreadNotification;
        store.appStore.setUserData(userData);
        notificationStore.init(response?.data?.notificationInformation);
      } else {
        const notificationInformation = response?.data?.notificationInformation;
        notificationStore.setNotificationList(
          notificationStore?.notificationList.concat(
            notificationInformation?.notificationList,
          ),
        );
        notificationStore.setPaginationDetails(
          notificationInformation?.paginationDetails,
        );
      }
    } else if (response?.data?.resultCode === 'V3S039') {
      setErrorMessage(response?.data?.resultMessage);
    } else {
      if (
        response.status &&
        response.data?.resultMessage &&
        response.data?.resultMessage != ''
      ) {
        uiStore.apiErrorInit({
          code: response.status,
          message: response.data?.resultMessage,
        });
      }
    }
  };

  const markAsReadNotification = async notification => {
    const params = {
      notificationIDs: Array.isArray(notification?._id)
        ? notification?._id
        : [notification?._id],
    };
    const reqBody = {
      store: store,
      body: params,
    };

    const resposne = await API(ApiEndPoint.MARK_AS_READ_NOTIFICATION, reqBody);
    // console.log('Mark as read Response', resposne?.data);
    console.log('here========================================888');

    if (resposne?.data?.resultCode === 'C001') {
      switch (notification?.details?.redirectionCode) {
        case 'message':
        case 'Message':
          props.navigation.navigate('MailBoxScreen');
          break;
        case 'settings':
        case 'Settings':
          props.navigation.navigate('ProfileScreen');
          break;
        case 'home':
          props.navigation.navigate('DashboardScreen');
          break;
        case 'worksheet_list_page':
        case 'worksheet_page':
        case 'WorksheetPage':
        case 'WorksheetListPage':
          props.navigation.navigate('WorksheetListScreen');
          break;
        case 'leaderboard':
          props.navigation.navigate('Leaderboard');
          break;
        case 'topic_map_page':
          if (typeof notification?.details?.redirectionData === 'object') {
            props.navigation.navigate('TopicMapScreen', {
              topicID: notification?.details?.redirectionData?.id,
            });
          } else {
            props.navigation.navigate('TopicListingScreen');
          }

          break;
        case 'topics_page':
        case 'TopicsPage':
        case 'TopicListPage':
          props.navigation.navigate('TopicListingScreen');
          break;
        default:
          props.navigation.navigate('DashboardScreen');
          break;
      }
      fetchNotification(1);
    } else {
      store.uiStore.apiErrorInit({
        code: response.status,
        message: response.data?.resultMessage,
      });
    }
  };

  const renderItem = ({item}) => {
    return (
      <NotificationListItem
        testID={item._id}
        imageURL={item?.details?.notificationIcon}
        title={item?.details?.title}
        message={item?.details?.body}
        dateTime={notificationTimeDifference(item?.updatedAt)}
        isActive={!item?.read}
        onPress={() => markAsReadNotification(item)}
      />
    );
  };

  const renderEmptyMessage = () => {
    return (
      <View style={styles.errorMessageContainer}>
        <BalooThambiRegTextView style={styles.errorMessage}>
          {errorMessage}
        </BalooThambiRegTextView>
      </View>
    );
  };

  return (
    <DetailsScreen
      testID="DetailsScreenNotificationListBackBtn"
      headerBtnType="back"
      headerBtnClick={() => props.navigation.pop()}>
      <BalooThambiRegTextView
        testID="NotificationListNotificationPlural"
        style={styles.titleText}>
        {notificationPlural}
      </BalooThambiRegTextView>
      {notificationStore?.notificationList !== null &&
      notificationStore?.notificationList.length > 0 ? (
        <FlatList
          style={styles.notificationContainer}
          data={notificationStore?.notificationList}
          renderItem={renderItem}
          keyExtractor={item => item?._id}
          onEndReached={event => {
            fetchNotification(event);
          }}
          onEndReachedThreshold={0.9}
          windowSize={4}
          removeClippedSubviews={true}
          initialNumToRender={2}
        />
      ) : (
        renderEmptyMessage()
      )}
    </DetailsScreen>
  );
};

NotificationListScreen.propTypes = {};

NotificationListScreen.defaultProps = {};

export default observer(NotificationListScreen);
