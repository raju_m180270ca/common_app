// External Import
import React, {useState, useEffect} from 'react';
import {View} from 'react-native';
import {useStores} from '@mobx/hooks';

// Internal Import
import {
  ListingScreen,
  SearchInput,
  PreviewFilterDropdown,
  RoundedButton,
} from '@components';
import styles from './style';
import {getAsValue, getWp, getHp} from '@utils';
import {API} from '@api';
import {ApiEndPoint} from '@constants';

const PreviewQuestionSearchScreen = props => {
  const {} = props;
  const store = useStores();

  const [showSideDrawer, setShowSideDrawer] = useState(false);
  const [qcode, setQCode] = useState(null);
  const [categories, setCategories] = useState([]);
  const [contexts, setContext] = useState([]);
  const [products, setProducts] = useState([]);
  const [selectedCategory, setSelectedCategry] = useState(null);
  const [selectedContext, setSelectedContext] = useState(null);
  const [selectedProduct, setSelectedProduct] = useState(null);

  useEffect(() => {
    fetchContentPreviewFilter();
  }, []);

  const headerBtnClickHandler = () => {
    props.navigation.navigate('DashboardScreen');
  };

  const fetchContentPreviewFilter = async () => {
    const reqBody = {
      jwt: await getAsValue('jwt'),
      store: store,
    };

    const response = await API(
      ApiEndPoint.GET_CONTENT_PREVIEW_FILTERS,
      reqBody,
    );
    if (response.data.resultCode === 'C001') {
      const categoryResponse = response?.data?.data?.category;
      setCategories(filterPreviewItems(categoryResponse));

      const contextResponse = response?.data?.data?.context;
      setContext(filterPreviewItems(contextResponse));

      const productResponse = response?.data?.data?.product;
      setProducts(filterPreviewItems(productResponse));
    } else {
      if (response.status && response.data?.resultMessage && response.data?.resultMessage != "") {
        store.uiStore.apiErrorInit({
          code: response.status,
          message: response.data?.resultMessage,
        });
      }
    }
  };

  const filterPreviewItems = item => {
    let filteredArray = [];
    if (item !== null) {
      const keys = Object.keys(item);
      filteredArray = keys.map(key => {
        let jsonObject = {};
        jsonObject.type = key;
        jsonObject.value = item[key];

        return jsonObject;
      });
    }

    return filteredArray;
  };

  const footerOnPress = () =>{
    props.navigation.navigate('ProfileScreen');
  }

  const onSearchBtnPressed = () => {
    const params = {
      qcode: qcode,
      context: selectedContext?.type,
      category: selectedCategory?.type,
      PID: selectedProduct?.type,
    };

    props.navigation.navigate('PreviewQnAScreen', {data: params});
  };

  return (
    <ListingScreen
      testID="ListingScreenPreviewQuestionSearch"
      headerBtnType="home"
      footerOnPress={footerOnPress}
      showSideDrawer={showSideDrawer}
      headerBtnClick={headerBtnClickHandler}>
      <View style={styles.container}>
        <SearchInput
          testID="SearchInputQuestionSearch"
          onChangeText={val => {
            setQCode(val.replace(/\s/g, ''));
          }}
          placeholder="QCode"
          value={qcode}
          containerStyle={styles.searchContainer}
        />
        <PreviewFilterDropdown
          testID="PreviewFilterDropdownPreviewQuestionSearchCategory"
          items={categories}
          onSelect={setSelectedCategry}
          placeholder="Category"
        />
        <PreviewFilterDropdown
          testID="PreviewFilterDropdownPreviewQuestionSearchContext"
          items={contexts}
          onSelect={setSelectedContext}
          placeholder="Context"
        />
        <PreviewFilterDropdown
          testID="PreviewFilterDropdownPreviewQuestionSearchProduct"
          items={products}
          onSelect={setSelectedProduct}
          placeholder="Product"
        />
        {qcode && selectedCategory && selectedContext && selectedProduct ? (
          <RoundedButton
            testID="RoundedButtonPreviewQuestionSearchPreviewEnabled"
            type="elevatedOrange"
            text="Preview"
            textStyle={styles.buttonTextstyle}
            containerStyle={styles.roundButtonContainerStyle}
            width={getWp(220)}
            height={getHp(50)}
            onPress={onSearchBtnPressed}
          />
        ) : (
          //   <View />
          <RoundedButton
            testID="RoundedButtonPreviewQuestionSearchPreviewDisabled"
            type="disabledGray"
            text="Preview"
            textStyle={styles.buttonTextstyle}
            containerStyle={styles.roundButtonContainerStyle}
            width={getWp(220)}
            height={getHp(50)}
            onPress={() => {}}
            disabled={true}
          />
        )}
      </View>
    </ListingScreen>
  );
};

PreviewQuestionSearchScreen.propTypes = {};

PreviewQuestionSearchScreen.defaultProps = {};

export default PreviewQuestionSearchScreen;
