import {StyleSheet} from 'react-native';
import {getWp} from '@utils';

export default StyleSheet.create({
    container: {
        flex: 1,
        height: '100%',
        alignItems: 'center',
        justifyContent: 'flex-start',
        paddingTop: getWp(50),
    },

    searchContainer: {
        width: getWp(220),
        height: getWp(49),
        alignItems: 'center',
        justifyContent: 'center'
    },

    buttonTextstyle: {
        fontFamily: 'BalooThambi-Regular',
        fontSize: getWp(24),
    },
    
    roundButtonContainerStyle: {
        backgroundColor: 'transparent',
        justifyContent: 'center',
        alignSelf: 'center',
        marginTop: getWp(20),
    },
});