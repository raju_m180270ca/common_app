/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable react/self-closing-comp */
/* eslint-disable react-native/no-inline-styles */
/**
|--------------------------------------------------
| Q and A Screen
|--------------------------------------------------
*/
import React, {useState, useEffect, useRef, useContext} from 'react';
import {
  View,
  ScrollView,
  findNodeHandle,
  Platform,
  FlatList,
  Keyboard,
  TouchableWithoutFeedback,
} from 'react-native';
import {useFocusEffect} from '@react-navigation/native';
import {observer} from 'mobx-react';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';
import {APP_VERSION} from '@env';
import {TimeTestModal, SessionTimeOutDialog} from '@hoc';
import {useStores} from '@mobx/hooks';
import {openActivityAPI} from '@api';

import {
  RoundedButton,
  Explanation,
  QHtmlTemplateForIframe,
  MyAutoHeightWebView,
  MCQ,
  BLANK,
  BLANK_DROPDOWN,
  InteractiveQ,
  DROPDOWN,
  Game,
  BalooThambiRegTextView,
  SourceSansProBoldTextView,
  QuestionLabel,
  NewMessageModal,
  Timer,
  TimedTestLiveStats,
  SuccessPopup,
  QnAHeader,
  DashboardFooter,
  SVGImageBackground,
  ConfirmationDialog,
  AlertPopup,
} from '@components';
import {
  ChallengeQuestion,
  StarAdd,
  StarAdded,
  HigherMessage,
} from '@images';
import moment from 'moment';
import {Toast} from 'native-base';
import base64 from 'react-native-base64';
//import styles from './indexCss';
import styles from '../QnAScreen/indexCss';
import {API} from '@api';
import {ApiEndPoint} from '@constants';
import {runInAction} from 'mobx';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import {
  getAsValue,
  setAsValue,
  getFormatedDateTime,
  getHp,
  getWp,
} from '@utils';
import {ThemeContext} from '@contexts/theme-context';
import {useBackHandler} from '@react-native-community/hooks';
import axios from 'axios';

const QnAScreen = props => {
  const [keyboardAwareState, setKeyboardAwareState] = useState(false);
  const theme = useContext(ThemeContext);
  const [isCollapsed, setisCollapsed] = useState(true);
  const [enableScroll, setEnableScroll] = useState(true);
  const [selectedMcq, setSelectedMcq] = useState({});
  const [isSubmitClicked, setIsSubmitClicked] = useState(false);
  const [startTime, setStartTime] = useState(moment());
  const [inputResponse, setInputResponse] = useState({});
  const [selectedChoice, setSelectedChoice] = useState(-1);
  const [lockOptions, setLockOptions] = useState(false);
  const [showTimeTestModal, setShowTimeTestModal] = useState(false);
  const [confirmationDialog, showConfirmationDialog] = useState(false);
  const [starred, setStarred] = useState(false);
  const scrollViewRef = useRef(null);
  const interactiveRef = useRef(null);
  const webref = useRef(null);
  const viewRef = useRef(null);
  const store = useStores();
  const {qnaStore} = useStores();
  const [showMessage, setShowMessage] = useState(false);
  const [showSuccessPopup, setShowSuccessPopup] = useState(false);
  const [effortPopup,setEffortPopup]=useState(false);
  const timerRef = useRef();

  const signal = axios.CancelToken.source();

  useBackHandler(() => {
    timerRef.current?.stop();
    reset();
    return false;
  });

  useFocusEffect(
    React.useCallback(() => {
      console.log('Focus effect exec>>>>>');
    }, []),
  );

  const permissions =
    Object.keys(store.uiStore.menuDataPermissions).length > 0
      ? store.uiStore.menuDataPermissions.question
      : {};

  useEffect(() => {
    console.log('Use Effect Ran>>>');
    reset();
    fetchContent();
    return () => {
      signal.cancel('FetchFirstContent API is cancelling..');
    };
  }, []);
  const handleKeyboardListener = params => {
    setKeyboardAwareState(p => params);
  };

  useEffect(() => {
    let didShowListener = Keyboard.addListener('keyboardDidShow', () =>
      handleKeyboardListener(true),
    );
    let didHideListener = Keyboard.addListener('keyboardDidHide', () =>
      handleKeyboardListener(false),
    );

    return () => {
      didShowListener.remove();
      didHideListener.remove();
    };
  }, []);
  
  const reset = async () => {
    return new Promise(resolve => {
      qnaStore.reset();
      setEnableScroll(true);
      setisCollapsed(true);
      setIsSubmitClicked(false);
      setStartTime(moment());
      setInputResponse({});
      setSelectedChoice(-1);
      setLockOptions(false);
      resolve();
    });
  };

  const onHeaderBtnPressHandler = () => {
    if (qnaStore.isTimeTest) {
      runInAction(() => {
        qnaStore.isTimeTestDone = true;
        qnaStore.isTimeUp = true;
      });
      setShowTimeTestModal(true);
      closeContent();
      return;
    } else {
      showConfirmationDialog(true);
    }
  };

  //starred question logic
  let StarSvg = StarAdd;

  if (starred) {
    StarSvg = StarAdded;
  }

  const onStarHandler = async () => {
    if (starred) {
      const req = {
        body: {
          contentId: qnaStore.contentData.contentId,
          topicId: qnaStore.topicId,
        },
        store,
      };
      const response = await API(ApiEndPoint.REMOVE_FROM_FAVOURITES, req);
      if (response.data.resultCode == 'C001') {
        setStarred(false);
      } else {
        store.uiStore.apiErrorInit({
          code: response.status,
          message: response.data?.resultMessage,
        });
      }
    } else {
      const req = {
        body: {
          conceptID: qnaStore.contentHeaderInfo.pedagogyChild.id,
          topicID: qnaStore.topicId,
          contentInfo: {
            contentID: qnaStore.contentData.contentId,
            contentVersionID: qnaStore.currentQuestion._id,
            version: qnaStore.currentQuestion.revisionNo,
            context: qnaStore.currentQuestion.langCode,
          },
        },
        store,
      };
      const response = await API(ApiEndPoint.ADD_TO_FAVOURITES, req);
      if (response.data.resultCode == 'C001') {
        setStarred(true);
      } else {
        store.uiStore.apiErrorInit({
          code: response.status,
          message: response.data?.resultMessage,
        });
      }
    }
  };

  const fetchContent = async () => {
    let req = {
      body: {},
      store: store,
      signal,
    };
    let res = await API(ApiEndPoint.FETCH_CONTENT_V3, req);
    console.log('\nfetch Content\n', res?.data);
    if (res.data.resultCode === 'C001') {
      qnaStore.init(res.data);
      setStartTime(moment());
    } else if (
      res.data.resultCode == 'C004' &&
      res.data.redirectionCode == 'CloseContent'
    ) {
      //Call Close topic
      closeContent();
    } else {
      if (
        res.status &&
        res.data?.resultMessage &&
        res.data?.resultMessage != ''
      ) {
        store.uiStore.apiErrorInit({
          code: res.status,
          message: res.data?.resultMessage,
        });
      }

      props.navigation.goBack();
    }
  };

  const closeContent = async () => {
    let req = {
      body: {
        endTopicFlag: false,
        endTopicHigherLevel: false,
        userTriggered: true,
        sessionTimeExceededFlag: false,
      },
      store: store,
    };
    let res = await API(ApiEndPoint.CLOSE_CONTENT, req);
    console.log('CLOSE CONTENT RESPONSE:', JSON.stringify(res.data));

    if (
      res.data.resultCode === 'C004' &&
      res.data.redirectionCode === 'TopicSessionReport'
    ) {
      //Check if Revice mode
      if (store.qnaStore.contentData.contentSubMode == 'revise') {
        props.navigation.replace('TopicListingScreen');
      } else {
        runInAction(() => {
          store.qnaStore.topicId = res.data.redirectionData.ID;
        });
        props.navigation.replace('TopicSummaryScreen');
      }
    } else {
      store.uiStore.apiErrorInit({
        code: res.status,
        message: res.data?.resultMessage,
      });
      console.log('CLOSE CONTENT ERROR' + JSON.stringify(res.data));
    }
  };

  const callSubmitQuestionAttemptAPI = async reqBody => {
    console.log(`Submit Question attempt API Request>>>${moment()}`);
    setStartTime(moment());
    try {
      let req = {
        body: reqBody.body,
        store: store,
      };
      const res = await API(ApiEndPoint.SUBMIT_QUESTION_ATTEMPT, req);
      console.log(
        ApiEndPoint.SUBMIT_QUESTION_ATTEMPT,
        'request========================================================================================================================',
        req.body,
      );
      console.log(
        'response========================================================================================================================',
        res,
      );
      console.log(
        'response data========================================================================================================================',
        res.data,
      );
      if (res?.data?.resultCode === 'C001') {
        qnaStore.setNextQuestionData(res.data);
        runInAction(() => {
          qnaStore.showExplanation = true;
        });
        qnaStore.showNextBtn();
      } else if (
        res?.data?.resultCode == 'C004' &&
        res?.data?.redirectionCode == 'CloseContent'
      ) {
        props.navigation.replace('TopicSummaryScreen');
      } else if (
        res.data.resultCode == 'C004' &&
        res.data.redirectionCode == 'redirect'
      ) {
        if (Object.keys(res.data?.contentHeaderInfo?.alert || {}).length > 0) {
          store.appStore.setEarnedRewardData({
            isVisible: true,
            earnedContent: res.data?.contentHeaderInfo?.alert,
            onPress: () => {
              props.navigation.replace('TopicSummaryScreen');
            },
          });
        } else {
          props.navigation.replace('TopicSummaryScreen');
        }
      } else {
        store.uiStore.apiErrorInit({
          code: res.status,
          message: res.data?.resultMessage,
        });
      }
    } catch (err) {
      console.log('ERROR IN SUBMIT API', err);
    }
  };

  const callUpdateQuestionAttemptAPI = async () => {
    try {
      let req = {
        body: {
          contentID: qnaStore.contentData.contentId,
          userAttemptData: {
            explanationRead: true,
            explanationReadTime: moment().diff(startTime, 'seconds'),
            explanationRating: null,
            gaveExplanation: false,
            feedbackResponse: null,
            userExplanation: null,
          },
          contentInfo: {
            contentID: qnaStore.contentData.contentId,
            contentVersionID: qnaStore.currentQuestion._id,
            contentType: qnaStore.contentData.contentType,
            questionType: qnaStore.currentQuestion.template,
            revisionNum: qnaStore.contentData.revisionNo,
            langCode: qnaStore.currentQuestion.langCode,
          },
        },
        store: store,
      };
      const res = await API(ApiEndPoint.UPDATE_QUESTION_ATTEMPT, req);

      if (res.data.resultCode === 'C001') {
        console.log("Next Question Res : ",qnaStore.nextQuestionRes);
        if (
          (qnaStore.nextQuestionRes.resultCode == 'C004' &&
            qnaStore.nextQuestionRes.redirectionCode == 'CloseContent') ||
          qnaStore.nextQuestionRes.redirectionData.endTopicFlag
        ) {
          console.log(" Navigating to Topic Summary screen ")
          props.navigation.replace('TopicSummaryScreen');
        } else {
          console.log("UPDATE_QUESTION_ATTEMPT Res = ",json.stringify(res.data))
          await reset();
          qnaStore.init(qnaStore.nextQuestionRes);
          setStarred(false);
        }
      } else {
        store.uiStore.apiErrorInit({
          code: res.status,
          message: res.data?.resultMessage,
        });
      }
    } catch (err) {
      console.log("Error : ",err);
    }
  };

  const renderTimeoutDialog = () => {
    if (store.uiStore.sessionExceeded == true) {
      return (
        <>
          <SessionTimeOutDialog
            testID="SessionTimeOutDialogQnA"
            onPress={() => {
              setAsValue('jwt', '');
              store.uiStore.reset();
              store.loginStore.setIsAuth(false);
            }}
          />
        </>
      );
    }
  };

  const dontKnow = async () => {
    store.uiStore.setLoader(true);
    let requestBody = {
      pedagogyID: qnaStore.questionResponse?.contentHeaderInfo?.pedagogyID,
      isDynamic: qnaStore.currentQuestion?.isDynamic,
      contentID: qnaStore.contentData?.contentId,
      result: base64.encode('skip'),
      timeTaken: moment().diff(startTime, 'seconds'),
      userResponse: {},
      userAttemptData: {},
      contentInfo: {
        contentID: qnaStore.contentData.contentId,
        contentVersionID: qnaStore.currentQuestion._id,
        contentType: qnaStore.contentData.contentType,
        questionType: qnaStore.currentQuestion.template,
        revisionNum: qnaStore.currentQuestion.revisionNo,
        langCode: qnaStore.currentQuestion.langCode,
      },
      contentSeqNum: qnaStore.contentData.contentSeqNum,
      mode:
        qnaStore.contentData.contentSubMode === 'remediation'
          ? 'learn'
          : qnaStore.contentData.contentSubMode,
      contentSubMode: qnaStore.contentData.contentSubMode,

      remainingTime: 0,
      nextContentSeqNum: null,
    };

    if (
      qnaStore.contentData.contentSubMode === 'remediation' ||
      qnaStore.contentData.contentSubMode === 'challenge'
    ) {
      requestBody.mode = 'learn';
    }

    const reqBody = {
      jwt: await getAsValue('jwt'),
      store: store,
      body: requestBody,
    };
    callSubmitQuestionAttemptAPI(reqBody);
  };

  const trueSelectionMcq = data => {
    console.log('TRUE SELECTION SELECTED:', data.index);
    data.correct = true;
    setSelectedMcq(data);
    submitFunction();
  };

  const falseSelectionMcq = data => {
    console.log('False SELECTION SELECTED:', data.index);
    data.correct = false;
    setSelectedMcq(data);
    submitFunction();
  };

  const dragAndDropCallback = isLongPressed => {
    if (isLongPressed) {
      console.log('Disabling the scroll');
      scrollViewRef.current.scrollEnabled = false;
      setEnableScroll(false);
      // setTimeout(() => {
      //   console.log('Enabling the scroll');
      //   // this.scrollViewRef.scrollEnabled = true;
      //   // this.setState({enableScroll: true});
      // }, 5000);
    } else {
      scrollViewRef.current.scrollEnabled = true;
      console.log('Enabling the scroll');
      setEnableScroll(true);
    }
  };

  const csEvaluateAnswer = (type, identifier) => {
    console.log(`Answer Evaluation starts>>>${moment()}`);
    console.log('\n\nDATA:' + JSON.stringify(qnaStore.contentData.data));
    console.log('\n\nInput Data:' + JSON.stringify(qnaStore.inputData));
    console.log('\n\nEVALUATION TYPE:' + type);
    let data = qnaStore.contentData.data;
    //TODO:Added this for check
    data[0].conditions = [];

    if (type == 'Interactive') {
      console.log('SS Evaluate Answer:Interactive');
      const run = `
      evaluateAnswer(${JSON.stringify(data)},${null},${JSON.stringify({
        id: identifier,
      })});
      `;
      interactiveRef.current.injectJavaScript(run);
    } else if (type == 'dontknow') {
      console.log('SS Evaluate Answer:Dontknow');
      const run = `
      evaluateAnswer(${JSON.stringify(data)},${null},${JSON.stringify({
        id: identifier,
      })});
      `;
      webref.current.injectJavaScript(run);
    } else {
      console.log('SS Evaluate Answer:Others');
      const run = `
      evaluateAnswer(${JSON.stringify(data)},${JSON.stringify(
        qnaStore.inputData,
      )},${JSON.stringify({id: identifier})} );
      `;
      webref.current.injectJavaScript(run);
    }
  };

  const csInit = q => {
    console.log('\n\nDATA:' + JSON.stringify(q.data));
    console.log(`Initiating Content Service>>>${moment()}`);
    let data = q.data;
    data[0].conditions = [];
    const run = `
    initContentService(${JSON.stringify(data)});
    `;
    console.log('SS initContentService');
    if (interactiveRef.current) {
      interactiveRef.current.injectJavaScript(run);
    }
    if (webref.current) {
      webref.current.injectJavaScript(run);
    }
  };

  const renderCSHtmlView = () => {
    let content = QHtmlTemplateForIframe('<p></p>', wp('1'));
    return (
      <MyAutoHeightWebView
        testID="MyAutoHeightWebViewQnA"
        ref={webref}
        onMessage={onWebViewMessage}
        style={styles.csHtmlStyle}
        source={{html: content}}
        zoomable={false}
      />
    );
  };

  const renderHeader = () => {
    return (
      <QnAHeader
        testID="QnAHeaderQnA"
        onPressBtn={onHeaderBtnPressHandler}
        permissions={permissions}
      />
    );
  };
  const scrollToExplanation = () => {
    // console.log('HEIGHT BEFORE:', this.state.scrollViewHeight);

    setTimeout(() => {
      try {
        requestAnimationFrame(() => {
          // console.log('VIEW REF:', viewRef.current);
          // console.log('SCROLL REF:', scrollViewRef.current);
          if (viewRef.current && scrollViewRef.current) {
            viewRef.current.measureLayout(
              findNodeHandle(scrollViewRef.current),
              (x, y) => {
                console.log('EXPLANATION NODE:', y);
                scrollViewRef.current.scrollTo({x: 0, y: y, animated: true});
              },
            );
          } else {
            console.log('REF ERROR scrollToExplanation>>');
          }
        });
      } catch (e) {
        // this.SCROLLVIEW_REF.scrollToEnd();
      }
    }, 500);
  };

  const renderExplanation = () => {
    try {
      if (qnaStore.showExplanation && qnaStore.currentQuestion.explanation) {
        let explanation = QHtmlTemplateForIframe(
          base64.decode(qnaStore.currentQuestion.explanation),
          wp('85%'),
        );
        if (
          qnaStore.showExplanation &&
          qnaStore.currentQuestion.template === 'MCQ'
        ) {
          let correct = Number(
            base64.decode(
              qnaStore.currentQuestion.response.mcqPattern.correctAnswer,
            ),
          );

          setTimeout(() => {
            scrollToExplanation();
            console.log('Ran Scroll:', selectedMcq);
          }, 200);

          return (
            <View ref={viewRef}>
              <Explanation
                testID="ExplanationQnA"
                type={qnaStore.currentQuestion.template}
                explanation={explanation}
                isCorrect={qnaStore.isAnswerCorrect}
                mcqData={selectedMcq}
                correctAnswer={correct}
              />
            </View>
          );
        } else {
          return (
            <View ref={viewRef}>
              <Explanation
                testID="ExplanationQnA"
                type={qnaStore.currentQuestion.template}
                explanation={explanation}
                isCorrect={qnaStore.isAnswerCorrect}
              />
            </View>
          );
        }
      }
    } catch (err) {
      console.log('ERROR IN RENDER EXPLANATION:', err);
    }
  };

  //Bridge
  const onWebViewMessage = async event => {
    var iframeResponse = null;
    console.log(
      'Message received from webview:::',
      JSON.parse(event.nativeEvent.data),
    );

    let msgData;
    try {
      msgData = JSON.parse(event.nativeEvent.data);
      console.log('---', msgData);

      if (msgData.hasOwnProperty('type') && msgData.type == 'IframeResponse') {
        console.log(`This is IFRAME response>>>${msgData.response}`);

        iframeResponse = msgData.response;
        if (isSubmitClicked) {
          setIsSubmitClicked(false);
          switch (iframeResponse[0]) {
            case '0':
              let count = qnaStore.trialCount - 1;
              qnaStore.setTrials(count);
              if (count > 0) {
                Toast.show({
                  text: `WRONG ANSWER, You have ${count} trial left.`,
                  buttonText: 'Okay',
                  duration: 2000,
                });
                let lengthVal = 0;
                if (qnaStore.currentQuestion.hints != null) {
                  console.log('test1');
                  lengthVal = qnaStore.currentQuestion.hints.length;
                  if (count > 0 && lengthVal > 0) {
                    console.log('\n\nhintQA1\n', qnaStore.isHintVisible);
                    qnaStore.showHint();
                    console.log('\n\nhintQA2\n', qnaStore.isHintVisible);
                  }
                }
                return;
              }
              csEvaluateAnswer('Interactive', 'interactive');
              break;
            case '1':
              csEvaluateAnswer('Interactive', 'interactive');
              break;
            case '2':
              store.uiStore.setLoader(false);
              Toast.show({
                text: 'Please enter the answer.',
              });

              break;
          }
        }
      } else if (
        msgData.hasOwnProperty('type') &&
        msgData.hasOwnProperty('function') &&
        msgData.type == 'ContentService'
      ) {
        console.log('This is content service response>>>');
        console.log(`Received response from webview>>>${moment()}`);
        switch (msgData.function) {
          case 'evaluateAnswer':
            console.log('Received evaluateAnswer=', msgData.evaluatedResult);

            store.uiStore.setLoader(false);
            let submitData = msgData.submitData;
            if (submitData.result == 'pass') {
              runInAction(() => {
                qnaStore.isAnswerCorrect = true;
                if (qnaStore.isTimeTest) {
                  qnaStore.correctAnswerCount = qnaStore.correctAnswerCount + 1;
                }
              });
            } else {
              runInAction(() => {
                qnaStore.isAnswerCorrect = false;
              });

              let count = qnaStore.trialCount - 1;
              qnaStore.setTrials(count);
              console.log('qnahint', qnaStore.isAnswerCorrect);
              if (count > 0) {
                Toast.show({
                  text: `WRONG ANSWER, You have ${count} trial left.`,
                  buttonText: 'Okay',
                  duration: 2000,
                });

                let lengthVal = 0;
                if (qnaStore.currentQuestion.hints != null) {
                  console.log('test1');
                  lengthVal = qnaStore.currentQuestion.hints.length;
                  if (count > 0 && lengthVal > 0) {
                    console.log('\n\nhintQA1\n', qnaStore.isHintVisible);
                    qnaStore.showHint();
                    console.log('\n\nhintQA2\n', qnaStore.isHintVisible);
                  }
                }

                return;
              }
            }
            submitData.timeTaken = moment().diff(startTime, 'seconds');
            if (
              submitData.userAttemptData &&
              submitData.userAttemptData.trials
            ) {
              submitData.userAttemptData.trials.map(item => {
                item.timeTaken = moment().diff(startTime, 'seconds');
              });
            }
            const reqBody = {
              jwt: await getAsValue('jwt'),
              store: store,
              body: submitData,
            };
            submitData.contentSeqNum = qnaStore.contentData.contentSeqNum;
            submitData.mode = qnaStore.contentData.contentSubMode;
            submitData.contentSubMode = qnaStore.contentData.contentSubMode;
            if (
              qnaStore.contentData.contentSubMode === 'remediation' ||
              qnaStore.contentData.contentSubMode === 'challenge'
            ) {
              submitData.mode = 'learn';
            }

            console.log('SUBMIT Data=', JSON.stringify(submitData));
            if (qnaStore.isTimeTest) {
              if (
                qnaStore.questionIndex !==
                qnaStore.timeTextQuestions.length - 1
              ) {
                console.log('\nbefore saving');
                qnaStore.saveTimeTestResponseAndShowNext(submitData);
                console.log('saved');
                return;
              } else if (
                qnaStore.questionIndex ===
                qnaStore.timeTextQuestions.length - 1
              ) {
                qnaStore.saveTimeTestResponse(submitData);
                var timeTaken = timerRef.current?.pause();
                qnaStore.setTimeTaken(qnaStore.timeTaken + timeTaken - 3);
                console.log('Time Taken', qnaStore.timeTaken);
                setShowTimeTestModal(true);
                console.log('completed');
                return;
              }
            }
            console.log('RequestBody', JSON.stringify(reqBody.body));
            callSubmitQuestionAttemptAPI(reqBody);
            break;
        }
        // console.log('Received Template=', msgData);
      } else {
        console.log('This is else response>>>');
        let key = Object.keys(msgData)[0];
        let val = msgData[key];
        let newResponse = inputResponse;
        newResponse[key] = val;
        setInputResponse(newResponse);

        console.log('NEW RESPONSE>', inputResponse);
      }
    } catch (err) {
      console.warn(err);
      return;
    }
  };

  const csCheckIframe = () => {
    try {
      console.log('cs Check Iframe');
      console.log('\n\nDATA:' + JSON.stringify(qnaStore.contentData));
      console.log('\n\nInput Data:' + JSON.stringify(qnaStore.inputData));
      let data = qnaStore.contentData.data;
      data[0].conditions = [];
      csInit(qnaStore.contentData);
      setTimeout(() => {
        const run = ` 
          checkIframe();   
        `;
        interactiveRef.current.injectJavaScript(run);
      }, 300);
    } catch (err) {
      Toast.show({text: 'ERROR CSCHECK '});
      console.log('ERROR CSCHECK ', err);
    }
  };

  const submitFunction = () => {
    console.log('TRIAL LEFT:' + qnaStore.trialCount);
    console.log(`About to call submit function>>>${moment()}`);

    store.uiStore.setLoader(true);
    if (qnaStore.currentQuestion.template === 'Interactive') {
      setIsSubmitClicked(true);
      csCheckIframe();
    } else if (qnaStore.currentQuestion.template == 'MCQ') {
      if (qnaStore.inputData.length == 0) {
        Toast.show({
          text: 'Please select an option.',
          duration: 5000,
          buttonText: 'OK',
        });
      } else {
        // if (trialCount > 0) {

        csInit(qnaStore.contentData);
        setTimeout(() => {
          csEvaluateAnswer('mcq', 'mcq');
        }, 200);
        // }
      }
    } else {
      // if (trialCount > 0) {
      console.log('CURRENT Q:', qnaStore.currentQuestion);
      console.log(
        'Total Answers:',
        Object.keys(qnaStore.currentQuestion.response),
      );
      //disable all blanks
      // const run = `
      // disableBlanks();
      // `;
      // webref.current.injectJavaScript(run);
      // webref.current.reload();

      console.log('Final Response', inputResponse);
      let allBlanksAnswered = true;
      let ipData = [];
      Object.keys(qnaStore.currentQuestion.response).forEach(key => {
        if (!inputResponse.hasOwnProperty(key)) {
          allBlanksAnswered = false;
        } else {
          ipData.push({name: key, value: inputResponse[key]});
        }
      });
      console.log('not Answered', allBlanksAnswered);

      if (allBlanksAnswered) {
        qnaStore.setTrials(qnaStore.trialCount - 1);
        runInAction(() => {
          qnaStore.inputData = ipData;
        });

        csInit(qnaStore.contentData);
        setTimeout(() => {
          csEvaluateAnswer('blank', 'blank');
        }, 200);
        console.log('InputData:', JSON.stringify(ipData));
      } else {
        store.uiStore.setLoader(false);
        Toast.show({
          text: 'Some answers have not been answered',
          buttonText: 'Okay',
          duration: 10000,
        });
      }
    }
  };

  const callOpenActivity = async () => {
    runInAction(() => {
      store.appStore.activityID = qnaStore.currentQuestion.contentID;
    });
    const res = await openActivityAPI(store);
    if (res.data.resultCode == 'C004') {
      props.navigation.navigate('GamePlayArenaScreen');
    } else if (res.data.resultCode == 'C002') {
      console.warn('Res data>', JSON.stringify(res.data));
    }
  };

  const renderQuestionsItem = templateType => {
    switch (templateType) {
      case 'MCQ':
        return (
          <MCQ
            lockOptions={lockOptions}
            selectedChoice={selectedChoice}
            setSelectedChoice={setSelectedChoice}
            isScreeningTest={false}
            questionData={qnaStore.currentQuestion}
            trueSelectionMcq={data => {
              console.log('DATA Selected True:', JSON.stringify(data));
              store.uiStore.setLoader(true);
              runInAction(() => {
                qnaStore.inputData = [{value: data.index}];
              });
              trueSelectionMcq(data);
              setLockOptions(true);
            }}
            falseSelectionMcq={data => {
              console.log('DATA Selected False:', JSON.stringify(data));
              store.uiStore.setLoader(true);
              runInAction(() => {
                qnaStore.inputData = [{value: data.index}];
              });
              falseSelectionMcq(data);
              setLockOptions(true);
            }}
            dragCallback={() => {}}
          />
        );
      case 'Blank':
        return (
          <BLANK
            questionRes={qnaStore.contentData}
            onWebViewCallback={onWebViewMessage}
            showHint={qnaStore.isHintVisible}
            dragCallback={dragAndDropCallback}
            hideHint={() => {
              this.setState({
                showHint: false,
              });
            }}
          />
        );
      case 'Blank_Dropdown':
        return (
          <BLANK_DROPDOWN
            questionRes={qnaStore.contentData}
            onWebViewCallback={onWebViewMessage}
            showHint={qnaStore.isHintVisible}
            dragCallback={dragAndDropCallback}
            hideHint={() => {
              this.setState({
                showHint: false,
              });
            }}
          />
        );
      case 'Dropdown':
        return (
          <DROPDOWN
            questionRes={qnaStore.contentData}
            onWebViewCallback={onWebViewMessage}
            showHint={qnaStore.isHintVisible}
            dragCallback={dragAndDropCallback}
            hideHint={() => {
              this.setState({
                showHint: false,
              });
            }}
          />
        );
      case 'Interactive':
        return (
          <InteractiveQ
            interactiveRef={interactiveRef}
            onWebViewCallback={onWebViewMessage}
            showHint={qnaStore.isHintVisible}
            dragCallback={dragAndDropCallback}
            questionRes={qnaStore.contentData}
          />
        );

      case 'Game':
      case 'Remedial':
        return (
          <View style={{alignSelf: 'center', flex: 1, marginTop: getHp(80)}}>
            <Game
              image={qnaStore.currentQuestion.thumbImg}
              title={qnaStore.currentQuestion.name}
              onPress={() => {
                callOpenActivity();
              }}
              isActive={true}
            />
          </View>
        );

      default:
        return null;
    }
  };

  const renderBottomButtons = () => {
    return (
      <View key="btm_buttons" style={styles.bottomBtnContainer}>
        <View style={styles.leftButtons}>
          {permissions.starredQuestions && !qnaStore.isTimeTest && (
            <View style={styles.mrgnRight6}>
              <StarSvg
                onPress={onStarHandler}
                height={styles.bottomLeftSvgSize.height}
                width={styles.bottomLeftSvgSize.width}
              />
            </View>
          )}
          {permissions.comment && (
            <HigherMessage
              onPress={() => {
                setShowMessage(true);
              }}
              height={styles.bottomLeftSvgSize.height}
              width={styles.bottomLeftSvgSize.width}
            />
          )}
        </View>
        {qnaStore.isSkipBtnVisible && (
          <RoundedButton
            onPress={() => {
              dontKnow();
            }}
            width={getWp(120)}
            height={getWp(46)}
            textStyle={styles.buttonText}
            type="secondaryWhite"
            text="I Don't Know"
          />
        )}
        {qnaStore.isSkipBtnVisible && <View style={{width: getWp(15)}} />}
        {qnaStore.isSubmitBtnVisible && (
          <RoundedButton
            type="squareOrange"
            text="Submit"
            textStyle={styles.bottomBtnText}
            containerStyle={{
              backgroundColor: 'transparent',
            }}
            width={styles.bottomRightBtnSize.width}
            height={styles.bottomRightBtnSize.height}
            onPress={() => {
              store.uiStore.setLoader(true);
              submitFunction();
            }}
          />
        )}
        {qnaStore.isNextBtnVisible && (
          <RoundedButton
            type="squareOrange"
            text="Next"
            textStyle={styles.bottomBtnText}
            containerStyle={{
              backgroundColor: 'transparent',
            }}
            width={styles.bottomRightBtnSize.width}
            height={styles.bottomRightBtnSize.height}
            onPress={() => {
              callUpdateQuestionAttemptAPI();
            }}
          />
        )}
      </View>
    );
  };

  let contentMode = qnaStore.contentData.contentSubMode
    ? qnaStore.contentData.contentSubMode
    : null;

  // console.log(`WEB VIEW HEIGHT>>>>${JSON.stringify(webref)}`);
  const getParams = () => {
    let data = {
      contentDetails: {
        contentType: qnaStore.contentData.contentType,
        context: qnaStore.currentQuestion.langCode,
        revisionNo: qnaStore.currentQuestion.revisionNo,
        contentSeqNum: qnaStore.contentData.contentSeqNum,
        contentAttempted: qnaStore.isNextBtnVisible,
      },
      contentID: qnaStore.contentData.contentId,
    };

    return data;
  };

  const submitTimeTest = async () => {
    var contentData = qnaStore.questionRes.contentData;
    var data = contentData.data[0];
    var contentInfo = {
      contentID: contentData.contentId,
      contentVersionID: data._id,
      contentType: contentData.contentType,
      groupType: data.type,
      revisionNum: data.revisionNo,
      langCode: data.langCode,
    };
    var submitData = {
      contentID: contentData.contentId,
      result: 'pass',
      timeTaken: qnaStore.timeTaken,
      timedTestInfo: {
        contentInfo: contentInfo,
      },
      userAttemptData: {
        userAttemptInfo: {
          totalCorrect: qnaStore.correctAnswerCount,
          totalAttempted: qnaStore.questionIndex,
          totalQuestions: qnaStore.timeTextQuestions.length,
          finished: !qnaStore.isTimeUp,
        },
        questionWiseData: qnaStore.timeTestUserAnswers,
      },
      contentInfo: contentInfo,
      contentSeqNum: contentData.contentSeqNum,
      mode: 'learn',
      contentSubMode: contentData.contentSubMode,
    };

    const reqBody = {
      jwt: await getAsValue('jwt'),
      store: store,
      body: submitData,
    };
    console.log('RequestBody', JSON.stringify(reqBody.body));
    callSubmitQuestionAttemptAPI(reqBody);
  };

  const getTimeTestPopup = () => {
    if (qnaStore.isTimeTestDone) {
      return (
        <TimeTestModal
          isVisible={showTimeTestModal}
          showAttemptLaterModal={true}
          onPress={() => {
            setShowTimeTestModal(false);
            runInAction(() => {
              qnaStore.isTimeTestDone = false;
              qnaStore.isTimeUp = false;
            });
          }}
          onCompleteLater={() => {
            setShowTimeTestModal(false);
            submitTimeTest();
          }}
        />
      );
    } else if (qnaStore.isTimeUp) {
      return (
        <TimeTestModal
          isVisible={showTimeTestModal}
          onPress={() => {
            setShowTimeTestModal(false);
            submitTimeTest();
          }}
          title={'Try again next time!'}
          topic={qnaStore.timeTestData.title}
          isStatusShown={false}
          subtitle={`Your time Is up.\n You had ${
            qnaStore.timeTestData.durationClass
          }  minutes to do this test.`}
        />
      );
    } else {
      var correct = qnaStore.correctAnswerCount;
      var wrong = qnaStore.questionIndex - qnaStore.correctAnswerCount;
      var accuracy = ((correct / qnaStore.questionIndex) * 100).toFixed(0);
      if (accuracy >= 75) {
        return (
          <TimeTestModal
            isVisible={showTimeTestModal}
            onPress={() => {
              setShowTimeTestModal(false);
              submitTimeTest();
            }}
            title={'Excellent Work!'}
            topic={qnaStore.timeTestData.title}
            isStatusShown={true}
            correct={correct}
            wrong={wrong}
            accuracy={accuracy}
            infoMessage={`Given Time:${
              qnaStore.timeTestData.durationClass
            } minutes`}
            subtitle={`Your Time: ${moment
              .utc(qnaStore.timeTaken * 1000)
              .format('mm [min] ss [sec]')}`}
          />
        );
      } else {
        return (
          <TimeTestModal
            isVisible={showTimeTestModal}
            onPress={() => {
              setShowTimeTestModal(false);
              submitTimeTest();
            }}
            title={'Try Harder!'}
            topic={qnaStore.timeTestData.title}
            isStatusShown={true}
            correct={correct}
            wrong={wrong}
            accuracy={accuracy}
            infoMessage={''}
            subtitle={'You need to get more questions right'}
          />
        );
      }
    }
  };

  return (
    <>
      <View style={styles.mainContainer}>
        <SVGImageBackground SvgImage="bgDashboard" themeBased screenBg>
          <DashboardFooter
            permissions={permissions}
            detailPage
            containerStyle={styles.footerContainerStyle}
          />
          <ConfirmationDialog
            isVisible={confirmationDialog}
            title="Confirmation"
            text="Do you want exit from the session?"
            primaryButton="Exit"
            secondaryButton="Continue"
            primaryBtnPressed={() => {
              showConfirmationDialog(false);
              closeContent();
            }}
            secondaryBtnPressed={() => {
              showConfirmationDialog(false);
            }}
          />
          <TouchableWithoutFeedback onPress={() => Keyboard.dismiss}>
            <KeyboardAwareScrollView scrollEnabled={keyboardAwareState}>
              <View style={styles.cardContainer}>
                {contentMode == 'challenge' && (
                  <ChallengeQuestion
                    width={getWp(130)}
                    height={getWp(26)}
                    style={styles.challengeQuestion}
                  />
                )}
                <View style={styles.questionNumberContainer}>
                  <BalooThambiRegTextView style={styles.questionNumberText}>
                    {qnaStore.contentData?.contentSeqNum}
                  </BalooThambiRegTextView>
                </View>
                <View
                  style={styles.questionTagContainer, styles.questionTagContainer {marginLeft: '6%'}]}>
                  <QuestionLabel type={contentMode} />
                </View>
                {qnaStore.isTimeTest && (
                  <Timer
                    start={qnaStore.timeTestData.durationClass}
                    ref={timerRef}
                    onTimeUp={() => {
                      console.log('timeup');
                      runInAction(() => {
                        qnaStore.isTimeUp = true;
                        qnaStore.timeTaken =
                          qnaStore.timeTestData.durationClass * 60;
                      });
                      setShowTimeTestModal(true);
                    }}
                    containerStyle={styles.timerContainer}
                  />
                )}
                {/* {showAudio == true ? (
              <TouchableOpacity
                style={styles.soundIconContainer}
                onPress={() => {
                  if (currentQuestion.quesVoiceover.length > 0 && sound) {
                    if (sound.isPlaying()) {
                      sound.pause();
                    } else {
                      sound.play();
                    }
                  } else {
                    Toast.show({text: 'Audio is not available'});
                  }
                }}>
                <SoundSvg width={getWp(20)} height={getWp(20)} />
              </TouchableOpacity>
            ) : null} */}
                <ScrollView
                  key="qnaScrollView"
                  disableScrollViewPanResponder={true}
                  horizontal={false}
                  persistentScrollbar={false}
                  showsVerticalScrollIndicator={false}
                  scrollEnabled={enableScroll}
                  keyboardDismissMode="interactive"
                  keyboardShouldPersistTaps="always"
                  nestedScrollEnabled={true}
                  style={{marginBottom: 15}}
                  automaticallyAdjustContentInsets={false}
                  onResponderMove={() => {
                    console.log('outer responding');
                  }}
                  ref={scrollViewRef}>
                  {qnaStore.isTimeTest && (
                    <View style={styles.timeTestDetailsContainer}>
                      <BalooThambiRegTextView style={styles.timeTestTitle}>
                        {qnaStore.timeTestData.title}
                      </BalooThambiRegTextView>
                      <TimedTestLiveStats
                        attempted={qnaStore.timeTestUserAnswers.length}
                        correct={qnaStore.correctAnswerCount}
                        total={qnaStore.timeTextQuestions.length}
                      />
                    </View>
                  )}
                  {renderQuestionsItem(qnaStore.currentQuestion?.template)}
                  {renderExplanation()}
                </ScrollView>

              {renderBottomButtons()}

              <SourceSansProBoldTextView style={styles.sessionIDText}>
                {APP_VERSION + '| '}
                {qnaStore.sessionInformation?.sessionID} |{' '}
                {getFormatedDateTime(
                  qnaStore.sessionInformation?.sessionStartAt,
                )}
              </SourceSansProBoldTextView>
              {renderCSHtmlView()}
              {renderTimeoutDialog()}
              {renderHeader()}
              <NewMessageModal
                isVisible={showMessage}
                pageId={'contentPage'}
                params={getParams()}
                onSuccess={() => {
                  setShowMessage(false);
                }}
                onHide={() => {
                  setShowSuccessPopup(true);
                }}
                onclose={() => {
                  setShowMessage(false);
                }}
              />
              <SuccessPopup
                isVisible={showSuccessPopup}
                text={'Message sent successfully'}
                onPress={() => {
                  setShowSuccessPopup(false);
                }}
              />
              {effortPopup && (
                <AlertPopup
                  testID="SuccessPopupTopicQnA"
                  isVisible={donepopupflg}
                  svgText="Attention Please!"
                  text={
                    'Would you like to continue doing a \n higher level topic next time'
                  }
                  onPress={() => {
                    setEffortPopup(!effortPopup);
                    callUpdateQuestionAttemptAPI();
                    //closeContent();
                  }}
                  onBackPress={() => {
                    setEffortPopup(!effortPopup)
                  }}
                />
              )}
              {/* {store.qnaStore.contentHeaderInfo.pedagogyType=='remediation'&& !effortPopup?setEffortPopup(true):null} */}
              {qnaStore.isTimeTest && getTimeTestPopup()}
            </KeyboardAwareScrollView>
          </TouchableWithoutFeedback>
        </SVGImageBackground>
      </View>
    </>
  );
};

export default observer(QnAScreen);
