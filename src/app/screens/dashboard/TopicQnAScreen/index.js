/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable react/self-closing-comp */
/* eslint-disable react-native/no-inline-styles */
/**
|--------------------------------------------------
| Q and A Screen
|--------------------------------------------------
*/
import React, {useState, useEffect, useContext} from 'react';
import {View, Keyboard, Platform} from 'react-native';
import {observer} from 'mobx-react';
import {useStores} from '@mobx/hooks';
import {
  RoundedButton,
  NewMessageModal,
  QnAHeader,
  SuccessPopup,
  AlertPopup,
  EffortPopup,
  HomeworkInstruction,
} from '@components';
import {StarAdd, StarAdded, HigherMessage} from '@images';
import moment from 'moment';
import styles from './indexCss';
import {API} from '@api';
import {ApiEndPoint, ContentIDs} from '@constants';
import {runInAction} from 'mobx';
import {getHp, getWp} from '@utils';
import {useBackHandler} from '@react-native-community/hooks';
import {useQnA, useStarQuestion} from '@hooks';
import axios from 'axios';
import {QnAScreen, ActivityStartModal, RewardCollectionModal} from '@hoc';
import {useLanguage} from '@hooks';
import {AuthContext} from '@contexts/auth-context';
import {MixpanelCategories, MixpanelEvents, MixpanelActions} from '@constants';

const TopicQnAScreen = props => {
  const [keyboardAwareState, setKeyboardAwareState] = useState(false);
  const [confirmationDialog, showConfirmationDialog] = useState(false);
  // const [starred, setStarred] = useState(false);
  const store = useStores();
  const {qnaStore} = useStores();
  const [showMessage, setShowMessage] = useState(false);
  const [showSuccessPopup, setShowSuccessPopup] = useState(false);
  const {onStarHandler, starred, setStarred} = useStarQuestion();
  const [donepopupflg, setdonepopupflg] = useState(false);
  const {
    skipBtnText,
    idontknowText,
    submitText,
    nextText,
    msgSuccessText,
    confirmationText,
    sessionEndingConfirmMessage,
    exitBtnText,
    continueText,
  } = useLanguage();

  const {
    submitFunction,
    enableScroll,
    renderCSHtmlView,
    scrollViewRef,
    renderQuestionsItem,
    renderExplanation,
    reset,
    setShowTimeTestModal,
    timerRef,
    getTimeTestPopup,
    callUpdateQuestionAttemptAPI,
    parentScrollRef,
    showQuesVO,
    setStartTime,
    dontKnow,
    callOpenActivity,
    initializeAudioSection,
    playSound,
    showInsStVO,
    qBodyVoiceOver,
    stopAudio,
  } = useQnA('TOPICS');

  const signal = axios.CancelToken.source();
  const auth = useContext(AuthContext);

  useBackHandler(() => {
    timerRef.current?.stop();
    reset();
    return props.navigation.replace('TopicListingScreen');
  });

  const permissions =
    Object.keys(store.uiStore.menuDataPermissions).length > 0
      ? store.uiStore.menuDataPermissions.question
      : {};

  useEffect(() => {
    reset();
    fetchContent();
    return () => {
      signal.cancel('FetchFirstContent API is cancelling..');
    };
  }, []);

  useEffect(() => {
    if (qnaStore?.isOpenActivity == true) {
      callOpenActivity();
    }
  }, [qnaStore?.isOpenActivity]);

  useEffect(() => {
    if (
      qnaStore?.activityButtonType !== null &&
      qnaStore?.activityButtonType !== '' &&
      qnaStore?.activityButtonType !== undefined
    ) {
      if (qnaStore?.activityButtonType === 'done') {
        store.uiStore.setLoader(true);
        //submitFunction();

        closeContent();
      } else if (qnaStore?.activityButtonType === 'skip') {
        dontKnow();
        qnaStore.setDisableBTn(true);
        callUpdateQuestionAttemptAPI();
        closeContent();
      }
    }
  }, [qnaStore?.activityButtonType]);

  const handleKeyboardListener = params => {
    setKeyboardAwareState(p => params);
  };
  useEffect(() => {
    let didShowListener = Keyboard.addListener('keyboardDidShow', () =>
      handleKeyboardListener(true),
    );
    let didHideListener = Keyboard.addListener('keyboardDidHide', () =>
      handleKeyboardListener(false),
    );

    return () => {
      didShowListener.remove();
      didHideListener.remove();
    };
  }, []);

  const onHeaderBtnPressHandler = () => {
    timerRef.current?.stop();
    //props.navigation.navigate('IntroductionScreen');
    let contentMode = qnaStore.contentData.contentSubMode
      ? qnaStore.contentData.contentSubMode
      : '';

    if (qnaStore.isTimeTest) {
      runInAction(() => {
        qnaStore.isTimeTestDone = true;
        qnaStore.isTimeUp = true;
      });
      setShowTimeTestModal(true);
      // closeContent();
      return;
    } else {
      if (contentMode == 'higherLevel') {
        setdonepopupflg(!donepopupflg);
      } else {
        // showConfirmationDialog(true);
        closeContent();
        auth.trackEvent('mixpanel', MixpanelEvents.TOPIC_DONE, {
          Category: MixpanelCategories.TOPIC,
          Action: MixpanelActions.CLICKED,
          Label: ``,
        });
      }
    }
  };

  //starred question logic
  let StarSvg = StarAdd;

  if (starred) {
    StarSvg = StarAdded;
  }

  const onStarClick = () => {
    let req = {};
    if (starred) {
      req = {
        contentId: qnaStore.contentData.contentId,
        topicId: qnaStore.topicId,
      };
    } else {
      req = {
        conceptID: qnaStore.contentHeaderInfo.pedagogyChild.id,
        topicID: qnaStore.topicId,
        contentInfo: {
          contentID: qnaStore.contentData.contentId,
          contentVersionID: qnaStore.currentQuestion._id,
          version: qnaStore.currentQuestion.revisionNo,
          context: qnaStore.currentQuestion.langCode,
        },
      };
    }

    onStarHandler(req, starred);
  };

  const fetchContent = async () => {
    let req = {
      body: {},
      store: store,
      signal,
    };
    let apiUrl = '';
    apiUrl = ApiEndPoint.FETCH_CONTENT_V3;

    let res = await API(apiUrl, req);
    if (res.data.resultCode === 'C001') {
      qnaStore.init(res.data);
      if (ContentIDs.includes(qnaStore.contentData.contentId)) {
        qnaStore.showSkip();
        qnaStore.skipQnaQuestion();
      }
      initializeAudioSection(res?.data?.contentData?.data[0]);

      setStartTime(moment());
    } else if (
      res.data.resultCode == 'C004' &&
      res.data.redirectionCode == 'CloseContent'
    ) {
      //Call Close topic
      closeContent();
    } else {
      if (
        res.status &&
        res.data?.resultMessage &&
        res.data?.resultMessage != ''
      ) {
        store.uiStore.apiErrorInit({
          code: res.status,
          message: res.data?.resultMessage,
        });
        props.navigation.goBack();
      }
    }
  };

  const quitHigherLevel = async () => {
    stopAudio();
    let req = {
      body: {},
      store: store,
    };

    let res = await API(ApiEndPoint.QUIT_HIGHER_LEVEL, req);

    if (
      res.data.resultCode === 'C004' &&
      res.data.redirectionCode === 'TopicSessionReport'
    ) {
      runInAction(() => {
        store.qnaStore.topicId = res.data.redirectionData.topicID;
      });
      props.navigation.replace('TopicSummaryScreen');
    } else {
      store.uiStore.apiErrorInit({
        code: res.status,
        message: res.data?.resultMessage,
      });
      console.log('CLOSE CONTENT ERROR' + JSON.stringify(res.data));
    }
  };

  const closeContent = async () => {
    stopAudio();

    let req = {
      body: {
        endTopicFlag: false,
        endTopicHigherLevel: false,
        userTriggered: true,
        sessionTimeExceededFlag: false,
      },
      store: store,
    };
    let res = await API(ApiEndPoint.CLOSE_CONTENT, req);

    if (
      res.data.resultCode === 'C004' &&
      res.data.redirectionCode === 'TopicSessionReport'
    ) {
      //Check if Revice mode
      if (store.qnaStore.contentData.contentSubMode == 'revise') {
        //props.navigation.replace('TopicListingScreen');
        props.navigation.replace('TopicSummaryScreen');
      } else {
        runInAction(() => {
          store.qnaStore.topicId = res.data.redirectionData.ID;
        });
        props.navigation.replace('TopicSummaryScreen');
      }
    } else {
      store.uiStore.apiErrorInit({
        code: res.status,
        message: res.data?.resultMessage,
      });
      console.log('CLOSE CONTENT ERROR' + JSON.stringify(res.data));
    }
  };

  const renderHeader = () => {
    return (
      <QnAHeader
        onPressBtn={onHeaderBtnPressHandler}
        permissions={permissions}
      />
    );
  };

  const renderBottomButtons = () => {
    let showFullScreenSubmit =
      qnaStore.isSkipBtnVisible &&
      qnaStore?.enableActivityStartModal === false &&
      qnaStore?.currentQuestion?.template !== 'MCQ';
    let showSbumitBtn = true;
    if (
      qnaStore?.currentQuestion?.template === 'Game' &&
      !qnaStore?.isSubmitEnabled
    ) {
      showSbumitBtn = false;
    }

    return (
      <View key="btm_buttons" style={styles.bottomBtnContainer}>
        {showFullScreenSubmit && showSbumitBtn && (
          <RoundedButton
            testID="RoundedButtonTopicQnASubmitText"
            type="squareOrange"
            text={submitText}
            textStyle={styles.bottomBtnText}
            containerStyle={{
              backgroundColor: 'transparent',
              marginBottom: getHp(5),
            }}
            width={'100%'}
            height={styles.bottomRightBtnSize.height}
            onPress={() => {
              qnaStore.setDisableBTn(true);
              submitFunction();
            }}
            disabled={qnaStore.disiableBtn}
          />
        )}
        {qnaStore?.enableActivityStartModal === false && (
          <View style={styles.bottomBtnSubContainer}>
            <View style={styles.leftButtons}>
              {permissions.comment && (
                <View style={styles.mrgnRight6}>
                  <HigherMessage
                    onPress={() => {
                      setShowMessage(true);
                    }}
                    height={styles.bottomLeftSvgSize.height}
                    width={styles.bottomLeftSvgSize.width}
                    accessible={true}
                    testID="HigherMessageTopicQnA"
                    accessibilityLabel="HigherMessageTopicQnA"
                  />
                </View>
              )}
              {permissions.starredQuestions && !qnaStore.isTimeTest && (
                <StarSvg
                  accessible={true}
                  testID="StarSvgTopicQnA"
                  accessibilityLabel="StarSvgTopicQnA"
                  onPress={onStarClick}
                  height={styles.bottomLeftSvgSize.height}
                  width={styles.bottomLeftSvgSize.width}
                />
              )}
            </View>
            {!qnaStore.isNextBtnVisible &&
            qnaStore.contentData.contentSubMode == 'challenge' &&
            qnaStore?.currentQuestion?.template === 'MCQ' ? (
              <RoundedButton
                testID="RoundedButtonTopicQnADontKnowBtn"
                onPress={() => {
                  dontKnow();
                }}
                width={styles.bottomRightBtnSize.width}
                height={styles.bottomRightBtnSize.height}
                containerStyle={styles.skipButtonContainer}
                textStyle={styles.buttonText}
                type="squareOrange"
                text={
                  qnaStore.contentData.contentType === 'activity'
                    ? skipBtnText
                    : idontknowText
                }
                disabled={qnaStore.disiableBtn}
              />
            ) : null}

            {(qnaStore.isSkipBtnVisible &&
              qnaStore?.currentQuestion?.template === 'MCQ') ||
              (showFullScreenSubmit && (
                <RoundedButton
                  testID="RoundedButtonTopicQnADontKnowBtn"
                  onPress={() => {
                    dontKnow();
                  }}
                  width={
                    qnaStore.contentData.contentType === 'activity' ||
                    qnaStore.isQnaSkip
                      ? styles.bottomRightBtnSize.width
                      : getWp(200)
                  }
                  height={styles.bottomRightBtnSize.height}
                  containerStyle={styles.skipButtonContainer}
                  textStyle={styles.buttonText}
                  type="squareOrange"
                  text={
                    qnaStore.contentData.contentType === 'activity' ||
                    qnaStore.isQnaSkip
                      ? skipBtnText
                      : idontknowText
                  }
                  disabled={qnaStore.disiableBtn}
                />
              ))}
            {qnaStore.isSubmitBtnVisible && !showFullScreenSubmit && (
              <RoundedButton
                testID="RoundedButtonTopicQnASubmitText"
                type="squareOrange"
                text={submitText}
                textStyle={styles.bottomBtnText}
                containerStyle={{
                  backgroundColor: 'transparent',
                }}
                width={styles.bottomRightBtnSize.width}
                height={styles.bottomRightBtnSize.height}
                onPress={() => {
                  qnaStore.setDisableBTn(true);
                  submitFunction();
                }}
                disabled={qnaStore.disiableBtn}
              />
            )}
            {qnaStore.isEffortpopupVisible && (
              <EffortPopup
                testID="MessagePopupChangePicPass"
                accessible={true}
                testID="changePicPassMsgPopup"
                accessibilityLabel="changePicPassMsgPopup"
                isVisible={qnaStore.isEffortpopupVisible}
                text={' Time to grow your brain stronger '}
                onPress={() => {
                  qnaStore.hideEffortpopup();
                  qnaStore.setDisableBTn(true);
                  callUpdateQuestionAttemptAPI();
                }}
                svgText={'Effort Mode !'}
                buttonText={"Let's go"}
              />
            )}
            {qnaStore.isNextBtnVisible && (
              <RoundedButton
                testID="RoundedButtonTopicQnANextText"
                type="squareOrange"
                text={nextText}
                textStyle={styles.bottomBtnText}
                containerStyle={{
                  backgroundColor: 'transparent',
                }}
                width={styles.bottomRightBtnSize.width}
                height={styles.bottomRightBtnSize.height}
                onPress={() => {
                  auth.trackEvent(
                    'mixpanel',
                    MixpanelEvents.TOPIC_NEXT_QUESTION,
                    {
                      Category: MixpanelCategories.TOPIC,
                      Action: MixpanelActions.CLICKED,
                      Label: ``,
                    },
                  );
                  qnaStore.setDisableBTn(true);
                  callUpdateQuestionAttemptAPI();
                  setStarred(false);
                }}
                disabled={qnaStore.disiableBtn}
              />
            )}
          </View>
        )}
      </View>
    );
  };

  const getParams = () => {
    let data = {
      contentDetails: {
        contentType: qnaStore.contentData.contentType,
        context: qnaStore.currentQuestion.langCode,
        revisionNo: qnaStore.currentQuestion.revisionNo,
        contentSeqNum: qnaStore.contentData.contentSeqNum,
        contentAttempted: qnaStore.isNextBtnVisible,
      },
      contentID: qnaStore.contentData.contentId,
    };

    return data;
  };

  const renderInstructionView = () => {
    const instruction = qnaStore?.timeTestData?.instruction;
    if (
      instruction &&
      typeof instruction !== 'undefined' &&
      instruction !== ''
    ) {
      return (
        <HomeworkInstruction
          testID="HomeworkInstructionHomeworkQnA"
          instruction={instruction}
        />
      );
    }
    return <View />;
  };
  return (
    <QnAScreen
      testID="QnAScreenTopicQnA"
      qnaStore={qnaStore}
      renderHeader={renderHeader}
      renderBottomButtons={renderBottomButtons}
      parentScrollRef={parentScrollRef}
      enableScroll={enableScroll}
      scrollViewRef={scrollViewRef}
      renderQuestionsItem={renderQuestionsItem}
      renderExplanation={renderExplanation}
      renderCSHtmlView={renderCSHtmlView}
      qnaStore={qnaStore}
      timerRef={timerRef}
      setShowTimeTestModal={setShowTimeTestModal}
      getTimeTestPopup={getTimeTestPopup}
      playSound={playSound}
      showInsStVO={showInsStVO}
      qBodyVoiceOver={qBodyVoiceOver}
      showQuesVO={showQuesVO}
      isBuddyVisible={!qnaStore.isTimeTest}
      renderHomeworkInstructionView={renderInstructionView}>
      <ActivityStartModal
        testID="ActivityStartModalTopicQnA"
        isVisible={qnaStore?.enableActivityStartModal}
        onStartBtnPressed={() => {
          qnaStore.enableActivityStartModal = false;
          callOpenActivity();
        }}
      />
      <NewMessageModal
        testID="NewMessageModalTopicQnA"
        isVisible={showMessage}
        pageId={'contentPage'}
        params={getParams()}
        onSuccess={() => {
          setShowMessage(false);
        }}
        onHide={() => {
          setShowSuccessPopup(true);
        }}
        onclose={() => {
          setShowMessage(false);
        }}
      />
      <SuccessPopup
        testID="SuccessPopupTopicQnA"
        isVisible={showSuccessPopup}
        text={msgSuccessText}
        onPress={() => {
          setShowSuccessPopup(false);
        }}
      />
      {/* <ConfirmationDialog
        testID="ConfirmationDialogTopicQnA"
        isVisible={confirmationDialog}
        title={confirmationText}
        text={sessionEndingConfirmMessage}
        primaryButton={exitBtnText}
        secondaryButton={continueText}
        primaryBtnPressed={() => {
          showConfirmationDialog(false);
          closeContent();
        }}
        secondaryBtnPressed={() => {
          showConfirmationDialog(false);
        }}
      /> */}

      <RewardCollectionModal
        isVisible={qnaStore.enableRewardCollectModal}
        item={qnaStore.titleRewardObject}
        rewardtype={'title'}
        onStartBtnPressed={() => {
          qnaStore.enableRewardCollectModal = false;
          closeContent();
        }}
      />

      {donepopupflg && (
        <AlertPopup
          testID="SuccessPopupTopicQnA"
          isVisible={donepopupflg}
          svgText="Attention Please!"
          text={
            'Would you like to continue doing a \n higher level topic next time'
          }
          onPress={() => {
            setdonepopupflg(!donepopupflg);
            closeContent();
          }}
          onBackPress={() => {
            setdonepopupflg(!donepopupflg);
            quitHigherLevel();
          }}
        />
      )}

      {Object.keys(qnaStore?.contentHeaderInfo?.alert || {}).length > 0
        ? store.appStore.setEarnedRewardData({
            isVisible: true,
            earnedContent: qnaStore?.contentHeaderInfo?.alert,
            onPress: () => {
              props.navigation.replace('TopicSummaryScreen');
            },
          })
        : null}
    </QnAScreen>
  );
};

export default observer(TopicQnAScreen);
