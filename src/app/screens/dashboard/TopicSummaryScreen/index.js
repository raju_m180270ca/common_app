/* eslint-disable react-hooks/exhaustive-deps */
import React, {useEffect, useState, Fragment, useContext} from 'react';
import {View, ScrollView} from 'react-native';
import moment from 'moment';

import {
  BalooThambiRegTextView,
  RoundedButton,
  NumberSquareButton,
  SparkieItem,
  DetailsScreen,
  SVGImageBackground,
} from '@components';
import {Coin} from '@images';
import {COLORS, ApiEndPoint} from '@constants';
import {API} from '@api';
import {getAsValue, replaceString} from '@utils';
import {useStores} from '@mobx/hooks';
import styles from './indexCss';
import {useLanguage} from '@hooks';
import {useBackHandler} from '@react-native-community/hooks';
import DeviceInfo from 'react-native-device-info';
import {AuthContext} from '@contexts/auth-context';
import {MixpanelCategories, MixpanelEvents, MixpanelActions} from '@constants';

const TopicSummaryScreen = props => {
  const store = useStores();
  const [topicReport, setTopicReport] = useState({});
  const auth = useContext(AuthContext);
  const {
    sparkieEarnedText,
    timeTakenText,
    youDidQuestionText,
    viewMapText,
    correctText,
    wrongText,
    attemptText,
  } = useLanguage();

  const permissions =
    Object.keys(store.uiStore.menuDataPermissions).length > 0
      ? store.uiStore.menuDataPermissions.topicSessionReport
      : {};

  useBackHandler(() => {
    return props.navigation.navigate('DashboardScreen');
  });

  useEffect(() => {
    (async () => {
      const reqBody = {
        jwt: getAsValue('jwt'),
        store: store,
        body: {
          topicID: store.qnaStore.topicId,
        },
      };
      try {
        const response = await API(ApiEndPoint.TOPIC_SESSION_REPORT, reqBody);
        console.log('\n\n response', JSON.stringify(response.data));
        if (response.data.resultCode === 'C001') {
          let sessionReport = response.data.sessionReport;
          sessionReport.sessionReward = response.data.sessionReward;
          store.uiStore.setChangedInUserData(true);
          setTopicReport(sessionReport);
        } else {
          if (
            response.status &&
            response.data?.resultMessage &&
            response.data?.resultMessage != ''
          ) {
            store.uiStore.apiErrorInit({
              code: response.status,
              message: response.data?.resultMessage,
            });
          }
        }
      } catch (e) {
        console.log(`Reward Details error>>>${e}`);
      }
    })();
  }, [store]);

  const headerBtnClickHandler = () => {
    auth.trackEvent('mixpanel', MixpanelEvents.SESSION_REPORT_GO_TO_HOME, {
      Category: MixpanelCategories.TOPIC,
      Action: MixpanelActions.CLICKED,
      Label: '',
    });
    props.navigation.navigate('DashboardScreen');
  };

  const ViewMapLearningClickHandler = () => {
    auth.trackEvent('mixpanel', MixpanelEvents.SESSION_REPORT_LEARNING_MAP, {
      Category: MixpanelCategories.TOPIC,
      Action: MixpanelActions.CLICKED,
      Label: '',
    });
    props.navigation.replace('TopicMapScreen', {
      topic: store.qnaStore.selectedTopic,
    });
  };
  console.log(
    topicReport,
    'topicReport=====================================================================================================================================',
  );
  return (
    <DetailsScreen
      testID="DetailsScreenTopicSummary"
      headerBtnType="home"
      headerBtnClick={headerBtnClickHandler}
      footerContainerStyle={styles.footerContainerStyle}
      showAnimation
      headerTitle={topicReport?.topicName}
      svgUrl={topicReport?.topicIcon}
      bgName="bgSummary"
      bgFooterName="bgFooterInner"
      animationName="rightSummaryAnimation">
      <ScrollView
        showsVerticalScrollIndicator={false}
        contentContainerStyle={{
          flex: DeviceInfo.isTablet() ? 1 : 0.9,
          height: '90%',
          alignItems: 'center',
        }}>
        <View style={styles.subContainer}>
          <View style={styles.sparkieContainer}>
            {permissions.mySparkies && (
              <SparkieItem
                testID="TopicSummaryTopicSummarySparkieEarnedText"
                title={sparkieEarnedText}
                value={topicReport?.sessionReward?.sparkie}
                ImageSVG={Coin}
                containerStyle={styles.mrgnBtm30}
              />
            )}
            <SparkieItem
              testID="TopicSummaryTopicSummarySparkieTimeTakenText"
              title={timeTakenText}
              value={
                topicReport
                  ? moment
                      .utc(
                        moment
                          .duration(topicReport.timeSpent, 'seconds')
                          .as('milliseconds'),
                      )
                      .format('mm:ss')
                  : 0
              }
              ImageSVG="timeTaken"
              themeBased
              containerStyle={styles.mrgnBtm120}
            />
          </View>

          <BalooThambiRegTextView
            testID="TopicSummaryTopicSummaryYouDidQuestionText"
            style={styles.questionCountText}>
            {replaceString(
              youDidQuestionText,
              'questions_attempt',
              topicReport?.questionAttempted,
            )}
          </BalooThambiRegTextView>

          <View style={styles.countContainer}>
            {permissions.correct && (
              <NumberSquareButton
                testID="NumberSquareButtoTopicSummarynCorrectText"
                text={topicReport?.questionCorrect}
                title={correctText}
                contentStyle={styles.numberSquareStyle}
              />
            )}

            {permissions.wrong && (
              <NumberSquareButton
                testID="NumberSquareButtonTopicSummaryWrongText"
                text={topicReport?.questionWrong}
                title={wrongText}
                containerStyle={{
                  backgroundColor: COLORS.pink,
                }}
                contentStyle={styles.numberSquareStyle}
                isLastItem
              />
            )}
          </View>

          {permissions.attempt && !topicReport?.revise && (
            <Fragment>
              <BalooThambiRegTextView
                testID="TopicSummaryAttemptedText"
                style={styles.titleText}>
                {attemptText}
              </BalooThambiRegTextView>
              <View style={styles.svgContainer}>
                <SVGImageBackground
                  testID="SVGImageBackgroundTopicSummary"
                  SvgImage="attempt"
                  themeBased
                  customContainerStyle={styles.svgBgStyle}
                  style={styles.attemptSvgStyle}
                />
              </View>
              <BalooThambiRegTextView
                testID="TopicSummarytopicReportTxt"
                style={styles.attemptCountText}>
                {topicReport?.topicAttemptNum}
              </BalooThambiRegTextView>
            </Fragment>
          )}
          <View style={styles.buttonContainer}>
            <RoundedButton
              testID="RoundedButtonTopicSummarySelectedTopic"
              onPress={ViewMapLearningClickHandler}
              width={'100%'}
              height={styles.btnStyle.height}
              textStyle={styles.btnText}
              type="elevatedOrange"
              text={viewMapText}
            />
          </View>
        </View>
      </ScrollView>
    </DetailsScreen>
  );
};

TopicSummaryScreen.propTypes = {};

TopicSummaryScreen.defaultProps = {};

export default TopicSummaryScreen;
