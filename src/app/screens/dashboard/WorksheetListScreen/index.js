/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable react-native/no-inline-styles */
import React, { useState, useEffect } from 'react';
import { useFocusEffect } from '@react-navigation/native';
import { View } from 'react-native';

import { WorkListingContent, ListingScreen } from '@components';
import { useStores } from '@mobx/hooks';
import { API } from '@api';
import { ApiEndPoint } from '@constants';
import { useLanguage } from '@hooks';
import styles from './indexCss';
import { useBackHandler } from '@react-native-community/hooks';


const WorksheetListScreen = props => {
  const { liveWorksheetText, olderWorksheetText } = useLanguage();
  const store = useStores();
  const { } = props;
  const [allWorkSheet, setAllWorkSheet] = useState([]);
  const [sectionedWorkSheet, setSectionedWorkSheet] = useState([]);
  const [searchQuery, setSearchQuery] = useState('');
  const [showEmptyMessage, setShowEmptyMessage] = useState(false);
  const [showSearchQueryEmptyMessage, setShowSearchQueryEmptyMessage] = useState(false);
  const [loading, setLoading] = useState(false);

  const permissions =
    Object.keys(store.uiStore.menuDataPermissions).length > 0
      ? store.uiStore.menuDataPermissions.worksheetList
      : {};
  let response;
  useFocusEffect(
    React.useCallback(() => {
      (async () => {
        const req = {
          body: {},
          store,
        };
        setLoading(true)
        try {
          response = await API(ApiEndPoint.FETCH_WORKSHEETS, req);
          if (response.data.resultCode === 'C001') {
            setShowEmptyMessage(!response.data.worksheetList.length > 0);
            setAllWorkSheet(response.data.worksheetList);
            setLoading(false);
            //Find if priority topic
            let liveWorksheet = [];
            let expiredWorksheet = [];
            response.data.worksheetList.forEach(item => {
              if (item.actions.actionText === 'expired' || item.contentStatus === 'deactive') {
                expiredWorksheet.push(item);
              } else {
                liveWorksheet.push(item);
              }
            });

            setSectionList(liveWorksheet, expiredWorksheet, "calledByApi");
          } else {
            if (response.status && response.data?.resultMessage && response.data?.resultMessage != "") {
              store.uiStore.apiErrorInit({
                code: response.status,
                message: response.data?.resultMessage,
              });
            }
            setLoading(false);
          }
        } catch (e) {
          setLoading(false);
          console.log(`Topic list error>>>${e}`);
        }
      })();
    }, []),
  );

  useBackHandler(() => {
    //return props.navigation.goBack();
    return props.navigation.navigate('DashboardScreen');
  });

  useEffect(() => {
    console.log('CALLED1', searchQuery);
    let liveWorksheet = [];
    let expiredWorksheet = [];

    allWorkSheet.forEach(item => {
      if (item.contentName.indexOf(searchQuery) != -1) {
        if (item.actions.actionText === 'expired' || item.contentStatus === 'deactive') {
          expiredWorksheet.push(item);
        } else {
          liveWorksheet.push(item);
        }
      }
    });

    let emptyStateTriggeredBy
    if (searchQuery == '') {
      emptyStateTriggeredBy = "apiData"
    } else {
      emptyStateTriggeredBy = "serachQuery"
    }

    setSectionList(liveWorksheet, expiredWorksheet, emptyStateTriggeredBy);
  }, [allWorkSheet, searchQuery]);

  const setSectionList = (liveWorksheet, expiredWorksheet, emptyStateTriggeredBy) => {
    let section = [];
    if (liveWorksheet.length > 0) {
      section.push({
        title: liveWorksheetText,
        data: liveWorksheet,
      });
    }
    if (expiredWorksheet.length > 0) {
      section.push({
        title: olderWorksheetText,
        data: expiredWorksheet,
      });
    }

    if (emptyStateTriggeredBy == "apiData") {
      setShowEmptyMessage(!section.length > 0);
    }

    if (emptyStateTriggeredBy == "serachQuery") {
      setShowSearchQueryEmptyMessage(!section.length > 0);
    }
    setSectionedWorkSheet(section);
  };

  const onSearchChange = val => {
    console.log('---', val);
    setSearchQuery(val);
  };

  const headerBtnClickHandler = () => {
    props.navigation.navigate('DashboardScreen');
  };

  return (
    <ListingScreen testID="ListingScreenWorksheetList"
      headerBtnType="home"
      headerBtnClick={headerBtnClickHandler}
      footerOnPress={() => {
        props.navigation.navigate('ProfileScreen');
      }}>
      <View style={styles.contentContainer}>
        <WorkListingContent
          testID="WorkListingContentWorksheetList"
          sectionList={sectionedWorkSheet}
          onSearch={onSearchChange}
          searchQuery={searchQuery}
          permissions={permissions}
          showEmptyMessage={showEmptyMessage}
          showSearchQueryEmptyMessage={showSearchQueryEmptyMessage}
          loading={loading}
        />
      </View>
    </ListingScreen>
  );
};

export default WorksheetListScreen;
