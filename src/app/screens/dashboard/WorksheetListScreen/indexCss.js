import {StyleSheet} from 'react-native';
import {getWp, getHp} from '@utils';

export default StyleSheet.create({
  container: {
    flex: 1,
  },
  innerContainer: {
    flex: 1,
  },
  subContainer: {flex: 1, elevation: 2, zIndex: 2},
  innerSubContainer: {
    flex: 2,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  sideMenuContainer: {
    flex: 1,
  },
  contentContainer: {
    flex: 4,
    marginBottom: getHp(117),
  },
  buddy: {position: 'absolute', right: getWp(16), top: getHp(18)},
});
