import {StyleSheet} from 'react-native';
import {COLORS, TEXTFONTSIZE} from '@constants';
import {getWp, getHp} from '@utils';
import DeviceInfo from 'react-native-device-info';

export default StyleSheet.create({
  flexOne: {
    flex: 1,
    marginBottom: getHp(42),
  },

  title: {
    color: COLORS.white,
    fontSize: TEXTFONTSIZE.Text26,
    marginTop: getHp(35),
    textAlign: 'center',
  },

  filterView: {
    marginBottom: getHp(5),
  },

  headerTitileStyle:{
    lineHeight: DeviceInfo.isTablet() ? getHp(8) :  getHp(22),
    marginTop:15,
  },
  headerFilterContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    marginTop: getHp(26),
    marginStart: getWp(12),
    marginEnd: getWp(12),
  },
});
