// External Imports
import React, {useEffect, useState, useContext} from 'react';
import {View} from 'react-native';
import {useStores} from '@mobx/hooks';
import {runInAction} from 'mobx';
import moment from 'moment';

// Internal Imports
import {
  ListingScreen,
  RoundedButton,
  BalooThambiRegTextView,
} from '@components';
import styles from './style';
import {API} from '@api';
import {ApiEndPoint} from '@constants';
import {getMonthYear} from '@utils';
import {HomeworkListContent, HomeworkStartModal} from '@hoc';
import {HomeworkEmptyState} from '@images';
import {getWp} from '@utils';
import {useLanguage} from '@hooks';
import {AuthContext} from '@contexts/auth-context';
import {MixpanelCategories, MixpanelEvents, MixpanelActions} from '@constants';
import {useBackHandler} from '@react-native-community/hooks';

const HomeworkListScreen = props => {
  const {} = props;
  const store = useStores();
  const {homeWorkEmptyStateText, homeWorkSearchEmptyText, goHomeBtnText} =
    useLanguage();
  const {homeworkStore} = store;
  const [allLiveHomeworks, setAllLiveHomeworks] = useState([]);
  const [allCompletedHomeworks, setAllCompletedHomeworks] = useState([]);
  const [liveHomeworks, setLiveHomeworks] = useState([]);
  const [completedHomeworks, setCompletedHomeworks] = useState([]);
  const [searchQuery, setSearchQuery] = useState('');
  const [emptyMessage, setEmptyMessage] = useState(homeWorkEmptyStateText);
  const [showEmptyMessage, setShowEmptyMessage] = useState(false);
  const [enableHomeworkModal, setEnableHomeworkModal] = useState(false);
  const [selectedHomework, setSelectedHomework] = useState(null);
  const [isWorkSheetButtonEnabled, setIsWorkSheetButtonEnabled] =
    useState(true);
  const [loading, setLoading] = useState(false);

  const auth = useContext(AuthContext);

  const permissions =
    Object.keys(store.uiStore.menuDataPermissions).length > 0
      ? store.uiStore.menuDataPermissions.homeworkList
      : {};

  useBackHandler(() => {
    return props.navigation.navigate('DashboardScreen');
  });

  useEffect(() => {
    homeworkStore.setHomeworkDates([moment()]);
    homeworkStore.setCurrentDate(moment());
    fetchHomeworks();
  }, []);

  useEffect(() => {
    if (selectedHomework !== null) {
      setEnableHomeworkModal(true);
    }
  }, [selectedHomework]);

  useEffect(() => {
    if (
      (allCompletedHomeworks !== null && allCompletedHomeworks.length > 0) ||
      (allLiveHomeworks !== null && allLiveHomeworks.length > 0)
    ) {
      if (searchQuery !== null && searchQuery !== '') {
        let liveHomeworks = [];
        let completedHomeworks = [];

        allLiveHomeworks.forEach(homework => {
          if (homework?.name.indexOf(searchQuery) > -1) {
            liveHomeworks.push(homework);
          }
        });

        allCompletedHomeworks.forEach(homework => {
          if (homework?.name.indexOf(searchQuery) > -1) {
            completedHomeworks.push(homework);
          }
        });

        setEmptyMessage(homeWorkSearchEmptyText);
        setShowEmptyMessage(
          !(liveHomeworks.length > 0 && completedHomeworks.length > 0),
        );
        setLiveHomeworks(liveHomeworks);
        setCompletedHomeworkSection(completedHomeworks);
      } else {
        setEmptyMessage(homeWorkEmptyStateText);
        setLiveHomeworks(allLiveHomeworks);
        setCompletedHomeworkSection(allCompletedHomeworks);
      }
    }
  }, [allCompletedHomeworks, allLiveHomeworks, searchQuery]);

  const fetchHomeworks = async () => {
    try {
      const req = {
        body: {
          homeworkType: 'all',
        },
        store,
      };
      setLoading(true);
      const response = await API(ApiEndPoint.GET_MY_HOMEWORKKS_V3, req);
      if (response.data.resultCode === 'C001') {
        const dataResponse = response?.data?.data;
        //setShowEmptyMessage(!(dataResponse?.live.length > 0 && dataResponse?.completed.length > 0));
        setAllLiveHomeworks(dataResponse?.live);
        setLiveHomeworks(dataResponse?.live);
        const updateHomeworks = updateCompletedHomeworks(
          dataResponse?.completed,
        );
        setAllCompletedHomeworks(updateHomeworks);
        setCompletedHomeworkSection(updateHomeworks);
        setLoading(false);
      }
    } catch (e) {
      setLoading(false);
      console.log(`Homework List Error>>>${e}`);
    }
  };

  const fetchHomeworkByMonth = async date => {
    try {
      const req = {
        body: {
          homeworkType: 'completed',
          month: moment(date).format('M'),
        },
        store,
      };
      setLoading(true);
      const response = await API(ApiEndPoint.GET_MY_HOMEWORKKS_V3, req);
      if (response.data.resultCode === 'C001') {
        const dataResponse = response?.data?.data;
        const allCompletedHomeworkList = allCompletedHomeworks.concat(
          updateCompletedHomeworks(dataResponse?.completed),
        );
        setAllCompletedHomeworks(allCompletedHomeworkList);
        setLiveHomeworks(allLiveHomeworks);
        setCompletedHomeworkSection(allCompletedHomeworkList);
        setLoading(false);
      }
    } catch (e) {
      setLoading(false);
      console.log(`Homework List Error>>>${e}`);
    }
  };

  const updateCompletedHomeworks = completedHomeworks => {
    if (completedHomeworks !== null && completedHomeworks !== undefined) {
      completedHomeworks.map(
        homework =>
          (homework.month_year = getMonthYear(homeworkStore?.currentMonth)),
      );
    }

    return completedHomeworks;
  };

  const setCompletedHomeworkSection = completedHomeworks => {
    if (
      completedHomeworks !== null &&
      typeof completedHomeworks !== 'undefined'
    ) {
      let allMonthYear = homeworkStore?.homeworkDates.map(date =>
        getMonthYear(date),
      );
      const completedHomeworkSection = allMonthYear.map(month => {
        let sectionData = {};
        const filterHomeworks = completedHomeworks.filter(
          item => item?.month_year === month,
        );
        sectionData.title = month;
        sectionData.data = filterHomeworks;

        return sectionData;
      });

      setCompletedHomeworks(completedHomeworkSection);
    }
  };

  const openHomework = async homework => {
    if (
      homework != null &&
      typeof homework !== 'undefined' &&
      isWorkSheetButtonEnabled
    ) {
      if (homework?.status) {
        auth.trackEvent('mixpanel', MixpanelEvents.HOMEWORK_CONTINUE, {
          Category: MixpanelCategories.HOMEWORK,
          Action: MixpanelActions.CLICKED,
          Label: '',
        });
      } else {
        auth.trackEvent('mixpanel', MixpanelEvents.HOMEWORK_OPEN, {
          Category: MixpanelCategories.HOMEWORK,
          Action: MixpanelActions.CLICKED,
          Label: '',
        });
      }
      setIsWorkSheetButtonEnabled(false);
      setEnableHomeworkModal(false);
      try {
        const req = {
          body: {
            homeworkID: homework?.homeworkId,
            mode: 'test',
          },
          store,
        };

        const res = await API(ApiEndPoint.OPEN_HOMEWORK, req);
        if (
          res.data.resultCode === 'C004' &&
          res.data.redirectionCode === 'ContentPage'
        ) {
          runInAction(() => {
            store.qnaStore.setHomeworkID(homework?.homeworkId);
            store.qnaStore.setHomeworkInfo(homework);
          });
          props.navigation.replace('HomeworkQnAScreen', {homework: homework});
          setSelectedHomework(null);
        } else if (res.data.resultCode === 'C900') {
          Toast.show({
            text: res.data.resultMessage,
            duration: 2000,
          });
        }
        setIsWorkSheetButtonEnabled(true);
      } catch (e) {
        console.log(`Open Homework Error>>>${e}`);
      }
    }
  };

  const headerBtnClickHandler = () => {
    props.navigation.navigate('DashboardScreen');
  };

  const onHomeworkPressed = homework => {
    const actionText =
      homework?.actions &&
      homework?.actions.length > 0 &&
      homework?.actions[0] &&
      homework?.actions[0].actionText
        ? homework?.actions[0].actionText
        : '';

    if (homework?.status === 'in-progress' && actionText !== 'See Report') {
      openHomework(homework);
    } else if (homework?.status !== 'complete' && actionText !== 'See Report') {
      setSelectedHomework(homework);
    } else {
      auth.trackEvent('mixpanel', MixpanelEvents.HOMEWORK_SEE_REPORT, {
        Category: MixpanelCategories.HOMEWORK,
        Action: MixpanelActions.CLICKED,
        Label: '',
      });
      props.navigation.navigate('HomeworkReportScreen', {homework: homework});
    }
  };

  return (
    <ListingScreen
      testID="ListingScreenHomeworkList"
      headerBtnType="home"
      headerBtnClick={headerBtnClickHandler}
      shadeTopContainer={styles.shadeTopContainer}
      shadeBottomContainer={styles.shadeBottomContainer}>
      <View style={styles.contentContainer}>
        {liveHomeworks.length === 0 && completedHomeworks.length === 0 ? (
          <View style={styles.emptyContainer}>
            <HomeworkEmptyState
              accessible={true}
              testID="HomeworkListEmptyStateSvg"
              accessibilityLabel="HomeworkListEmptyStateSvg"
              width={getWp(150)}
              style={styles.emptyStateIcon}
            />
            {loading === false && (
              <BalooThambiRegTextView
                testID="HomeworkListEmptyStateText"
                style={styles.textColor}>
                {homeWorkEmptyStateText}
              </BalooThambiRegTextView>
            )}
            {loading === false && (
              <RoundedButton
                testID="RoundedButtonHomeworkListGoHomeBtn"
                onPress={() => {
                  props.navigation.navigate('DashboardScreen');
                }}
                type="primaryOrange"
                text={goHomeBtnText}
                width={150}
                containerStyle={{...styles.goHomeBtnContainer}}
              />
            )}
          </View>
        ) : (
          <HomeworkListContent
            testID="HomeworkListContentHomeworkList"
            liveHomeworks={liveHomeworks}
            completedHomeworks={completedHomeworks}
            onSearch={setSearchQuery}
            searchQuery={searchQuery}
            permissions={permissions}
            showEmptyMessage={showEmptyMessage}
            onPress={onHomeworkPressed}
            emptyMessage={emptyMessage}
            oldHomeworkBtnPressed={() => {
              const previouseDate = moment(
                homeworkStore?.currentMonth,
              ).subtract(1, 'months');
              homeworkStore.setCurrentDate(previouseDate);
              homeworkStore.setHomeworkDates(
                homeworkStore?.homeworkDates.concat(previouseDate),
              );
              fetchHomeworkByMonth(previouseDate);
            }}
          />
        )}
        <HomeworkStartModal
          testID="HomeworkStartHomeworkListContent"
          isVisible={enableHomeworkModal}
          homework={selectedHomework}
          onSkipBtnPressed={() => {
            setSelectedHomework(null);
            setEnableHomeworkModal(false);
          }}
          onStartBtnPressed={openHomework}
        />
      </View>
    </ListingScreen>
  );
};

HomeworkListScreen.propTypes = {};

HomeworkListScreen.defaultProps = {};

export default HomeworkListScreen;
