// External Imports
import React, {useState, useEffect, useContext} from 'react';
import {View} from 'react-native';
import {observer} from 'mobx-react';
import {useStores} from '@mobx/hooks';
import {useBackHandler} from '@react-native-community/hooks';
import {Toast} from 'native-base';
import {useLanguage} from '@hooks';

// Internal Imports
import {
  RoundedButton,
  NewMessageModal,
  SuccessPopup,
  WorksheetQnAHeader,
  HomeworkInstruction,
} from '@components';
import {HigherMessage} from '@images';
import styles from './style';
import {API} from '@api';
import {ApiEndPoint} from '@constants';
import {useQnA} from '@hooks';
import {QnAScreen, HomeworkTimedModal} from '@hoc';
import {getWp, getHp} from '@utils';
import {AuthContext} from '@contexts/auth-context';
import {MixpanelCategories, MixpanelEvents, MixpanelActions} from '@constants';

const HomeworkQnAScreen = props => {
  const {} = props;
  const store = useStores();
  const auth = useContext(AuthContext);

  const {
    submitText,
    pleaseCompleteAllQuestion,
    nextText,
    msgSuccessText,
    submitHwModalText,
    hwWillBeSubmitedText,
    closeText,
  } = useLanguage();

  const {qnaStore} = useStores();
  const [showMessage, setShowMessage] = useState(false);
  const [showSuccessPopup, setShowSuccessPopup] = useState(false);
  const [confirmationDialog, showConfirmationDialog] = useState(false);
  const [submitDialog, showSubmitDialog] = useState(false);

  const permissions =
    Object.keys(store.uiStore.menuDataPermissions).length > 0
      ? store.uiStore.menuDataPermissions.homeworkQuestion
      : {};

  const {
    submitFunction,
    enableScroll,
    renderCSHtmlView,
    scrollViewRef,
    renderQuestionsItem,
    reset,
    timerRef,
    parentScrollRef,
    renderHomeworkSolutionView,
  } = useQnA('HOMEWORK_EDICINE');

  useBackHandler(() => {
    showConfirmationDialog(true);
    //return true;
  });

  useEffect(() => {
    reset();
    fetchContent();
  }, []);

  const fetchContent = async () => {
    let req = {
      body: {},
      store: store,
    };
    const response = await API(ApiEndPoint.FETCH_HOMEWORK, req);
    if (response?.data?.resultCode === 'C001') {
      await reset();
      qnaStore.init(response?.data);
    } else if (
      response?.data?.resultCode == 'C004' &&
      response?.data?.redirectionCode == 'CloseContent'
    ) {
      props.navigation.goBack();
    } else {
      store.uiStore.apiErrorInit({
        code: response.status,
        message: response?.data?.resultMessage,
      });
      props.navigation.replace('HomeworkListScreen');
    }
  };

  const quitHomework = async () => {
    try {
      showSubmitDialog(false);
      const req = {
        store: store,
        body: {
          homeworkId: qnaStore?.homeworkID,
        },
      };

      const response = await API(ApiEndPoint.QUIT_HOMEWORK, req);
      if (response.data.resultCode === 'C004') {
        if (response.data.redirectionCode === 'HomeworkSessionReport') {
          props.navigation.replace('HomeworkSummaryScreen');
        } else if (response.data.redirectionCode === 'ContentPage') {
          props.navigation.replace('HomeworkSummaryScreen');
        }
      } else {
        store.uiStore.apiErrorInit({
          code: response.status,
          message: response?.data?.resultMessage,
        });
      }
    } catch (ex) {
      console.log('Quit Homework API Error', ex);
    }
  };

  const renderHeader = () => {
    return (
      <WorksheetQnAHeader
        testID="WorksheetQnAHeaderHomeworkQnA"
        onPressBtn={() => showConfirmationDialog(true)}
        onPaginationItemPressed={submitFunction}
        permissions={permissions}
      />
    );
  };

  const renderBottomButtons = () => {
    return (
      <View key="btm_buttons" style={styles.bottomBtnContainer}>
        <View style={styles.bottomLeftButtonContainer}>
          {permissions.comment && (
            <HigherMessage
              accessible={true}
              testID="HomeworkQnAHigherMessageBtn"
              accessibilityLabel="HomeworkQnAHigherMessageBtn"
              onPress={() => {
                setShowMessage(true);
              }}
              height={getWp(48)}
              width={getWp(48)}
            />
          )}
        </View>
        <RoundedButton
          testID="RoundedButtonHomeworkQnASubmitBtn"
          type={qnaStore.isSubmitEnabled ? 'squareOrange' : 'squareDisabled'}
          text={submitText}
          textStyle={styles.bottomBtnText}
          width={getWp(145)}
          height={getHp(48)}
          onPress={() => {
            if (qnaStore.isSubmitEnabled) {
              auth.trackEvent('mixpanel', MixpanelEvents.HOMEWORK_SUBMIT, {
                Category: MixpanelCategories.HOMEWORK,
                Action: MixpanelActions.CLICKED,
                Label: '',
              });
              showSubmitDialog(true);
            } else {
              Toast.show({text: pleaseCompleteAllQuestion});
            }
          }}
        />
        <RoundedButton
          testID="RoundedButtonHomeworkQnANextBtn"
          type="hintBlue"
          text={nextText}
          textStyle={styles.bottomBtnText}
          width={getWp(145)}
          height={getHp(48)}
          onPress={() => {
            submitFunction();
          }}
        />
      </View>
    );
  };

  const getParams = () => {
    let data = {
      contentDetails: {
        contentType: qnaStore?.contentData?.contentType,
        context: qnaStore?.currentQuestion?.langCode,
        revisionNo: qnaStore?.currentQuestion?.revisionNo,
        contentSeqNum: qnaStore?.contentData?.contentSeqNum,
        contentAttempted: Boolean(qnaStore?.userResponse),
      },
      contentID: qnaStore?.contentData?.contentId,
    };

    return data;
  };

  const renderInstructionView = () => {
    const questionInstruction = qnaStore?.currentQuestion?.instructorStimulus;
    if (
      questionInstruction &&
      typeof questionInstruction !== 'undefined' &&
      questionInstruction !== ''
    ) {
      return (
        <HomeworkInstruction
          testID="HomeworkInstructionHomeworkQnA"
          instruction={questionInstruction.value}
        />
      );
    }

    return <View />;
  };

  return (
    <QnAScreen
      testID="QnAScreenHomeworkQnA"
      renderHeader={renderHeader}
      renderBottomButtons={renderBottomButtons}
      parentScrollRef={parentScrollRef}
      enableScroll={enableScroll}
      scrollViewRef={scrollViewRef}
      renderQuestionsItem={renderQuestionsItem}
      renderCSHtmlView={renderCSHtmlView}
      qnaStore={qnaStore}
      renderHomeworkSolutionView={renderHomeworkSolutionView}
      renderHomeworkInstructionView={renderInstructionView}
      timerRef={timerRef}>
      <NewMessageModal
        testID="NewMessageModalHomeworkQnA"
        isVisible={showMessage}
        pageId={'workSheetsPage'}
        params={getParams()}
        onSuccess={() => {
          setShowMessage(false);
        }}
        onHide={() => {
          setShowSuccessPopup(true);
        }}
        onclose={() => {
          setShowMessage(false);
        }}
      />
      <SuccessPopup
        testID="SuccessPopupHomeworkQna"
        isVisible={showSuccessPopup}
        text={msgSuccessText}
        onPress={() => {
          setShowSuccessPopup(false);
        }}
      />
      <HomeworkTimedModal
        isVisible={confirmationDialog}
        homework={qnaStore?.homeworkInfo}
        onPress={() => showConfirmationDialog(false)}
        onCompleteLater={() => {
          auth.trackEvent('mixpanel', MixpanelEvents.HOMEWORK_DONE, {
            Category: MixpanelCategories.HOMEWORK,
            Action: MixpanelActions.CLICKED,
            Label: '',
          });
          showConfirmationDialog(false);
          props.navigation.replace('HomeworkListScreen');
        }}
      />

      <HomeworkTimedModal
        isVisible={submitDialog}
        submitHwModal={true}
        homework={qnaStore?.homeworkInfo}
        onPress={() => showSubmitDialog(false)}
        onCompleteLater={() => {
          showSubmitDialog(false);
          quitHomework();
        }}
        testID="ConfirmationDialogHomeworkQnAHwWillBeSubmitedText"
      />
      {/* <ConfirmationDialog
                isVisible={submitDialog}
                title={submitHwModalText}
                text={hwWillBeSubmitedText}
                primaryButton={submitText}
                secondaryButton={closeText}
                primaryBtnPressed={() => quitHomework()}
                secondaryBtnPressed={() => showSubmitDialog(false)} /> */}
    </QnAScreen>
  );
};

HomeworkQnAScreen.propTypes = {};

HomeworkQnAScreen.defaultProps = {};

export default observer(HomeworkQnAScreen);
