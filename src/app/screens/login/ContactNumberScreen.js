import React, {useEffect, useContext, useState} from 'react';
import {View, StyleSheet, ScrollView, Text} from 'react-native';
import {getHp, getWp} from '@utils';
import {COLORS, TEXTFONTSIZE} from '@constants';
import {Logo, GuestAccount} from '@images';
import {
  BottomSheet,
  CustomCheckBox,
  SourceSansProBoldTextView,
  SourceSansProRegTextView,
  CustomButton,
} from '@components';

import {observer} from 'mobx-react';
import {useStores} from '@mobx/hooks';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {runInAction} from 'mobx';
import {API} from '@api';
import {ApiEndPoint, MixpanelEvents} from '@constants';
import {useLanguage} from '@hooks';
import {TouchableOpacity} from 'react-native-gesture-handler';
import {AuthContext} from '@contexts/auth-context';
import NeedHelp from './NeedHelp';
import PhoneInput from 'react-native-phone-number-input';
import {useNavigation} from '@react-navigation/native';
import DeviceInfo from 'react-native-device-info';

const ContactNumberScreen = props => {
  const store = useStores();
  const {loginStore} = useStores();
  var navigation = useNavigation();

  const {loginBtnName, haveAnAccount, loginWithExistingMindsparkId} =
    useLanguage();
  const auth = useContext(AuthContext);

  const loginScreenApi = async () => {
    const loginScreenData = await API(ApiEndPoint.LOGIN_SCREEN, {store});
    if (loginScreenData.data.resultCode === 'C001') {
      const sparkiesChamp = loginScreenData.data.caurosal[0]?.studentList;
      if (sparkiesChamp) {
        loginStore.setSparkiesChamp(sparkiesChamp);
      }
      runInAction(() => {
        loginStore.sparkyFromDate = loginScreenData.data.fromDate;
        loginStore.sparkyToDate = loginScreenData.data.toDate;
        loginStore.loginPermissions = loginScreenData.data.permissions;
      });
    }
  };

  const [mobileNumber, setMobileNumber] = useState('');
  const [countryCode, setCountryCode] = useState('+91');
  const [phoneNumberError, setPhoneNumberError] = useState('');
  const [whatsAppCheck, setWhatsAppCheck] = useState(false);

  useEffect(() => {
    auth.trackEvent('cleaverTap', MixpanelEvents.LOGIN_PAGE_OPEN);

    store.uiStore.setLoader(false);
    store.loginStore.setLoader(false);
    loginScreenApi();

    return () => {};
  }, [loginStore, store]);

  const onSubmit = async () => {
    if (validateInputFields(mobileNumber)) {
      let formatedCode = countryCode.slice(0, -mobileNumber.length);

      const req = {
        body: {mobileNo: mobileNumber, countryCode: formatedCode},
        store,
      };

      try {
        const response = await API(ApiEndPoint.GENERATE_OTP, req, false);
        if (response.data.resultCode === 'C001') {
          loginStore.setParentMobile(mobileNumber);
          loginStore.setCountryCode(formatedCode);
          loginStore.setWhatsAppConsent(whatsAppCheck);

          navigation.navigate('ValidateOTPScreen');
        }
      } catch (err) {
        console.log(`API Error>>>>${JSON.stringify(err)}`);
      }
    }
  };

  const login = () => {
    navigation.navigate('LoginScreen');
  };

  const validateInputFields = phone => {
    let phoneNumberStatus = true;

    if (phone == '') {
      setPhoneNumberError('Phone Number is required');
      phoneNumberStatus = false;
    }
    if (phoneNumberStatus && countryCode.startsWith('+91')) {
      if (phone.length < 10) {
        setPhoneNumberError('Minimum length required is 10');
        phoneNumberStatus = false;
      }

      if (phone.length > 10) {
        setPhoneNumberError('Please enter a valid Phone Number');
        phoneNumberStatus = false;
      }
    } else {
      if (phoneNumberStatus && phone.length < 4) {
        setPhoneNumberError('Minimum length required is 4');
        phoneNumberStatus = false;
      }
    }

    if (phoneNumberStatus) {
      setPhoneNumberError('');
    }

    return phoneNumberStatus;
  };

  const onTextChange = text => {
    setMobileNumber(text);
  };

  const onChangeCountryCode = text => {
    setCountryCode(text);
  };

  let bdrColor = null;
  if (phoneNumberError != '') {
    bdrColor = {borderColor: COLORS.red};
  }

  return (
    <BottomSheet>
      <ScrollView
        keyboardShouldPersistTaps="always"
        keyboardDismissMode="on-drag">
        <View style={styles.screen}>
          <View style={styles.innerContainer}>
            <View style={styles.logoContainer}>
              <Logo
                accessible={true}
                testID="LoginMSLogo"
                accessibilityLabel="LoginMSLogo"
                width={styles.logo.width}
                height={styles.logo.height}
              />
              <NeedHelp />
            </View>
            <SourceSansProBoldTextView
              testID="LoginText"
              style={styles.heading}>
              {loginBtnName}
            </SourceSansProBoldTextView>
            <View style={{width: wp(80)}}>
              <View style={styles.seperator} />
              <PhoneInput
                defaultValue={''}
                containerStyle={{...styles.inputContainerStyle, ...bdrColor}}
                textContainerStyle={styles.textContainerStyle}
                textInputStyle={styles.textInputStyle}
                codeTextStyle={styles.codeTextStyle}
                countryPickerButtonStyle={styles.countryPickerStyle}
                defaultCode="IN"
                layout="second"
                placeholder={'Enter mobile number'}
                onChangeText={text => {
                  onTextChange(text, 'phonenumber');
                }}
                onChangeFormattedText={text => {
                  onChangeCountryCode(text);
                }}
              />
              {phoneNumberError != '' && (
                <SourceSansProRegTextView
                  style={styles.errorMessage}
                  testID="phoneNumberError">
                  {phoneNumberError}
                </SourceSansProRegTextView>
              )}
              <CustomCheckBox
                label={'Get important updates on WhatsApp'}
                isSelected={whatsAppCheck}
                setIsSelected={setWhatsAppCheck}
              />
              <View style={styles.btnContainer}>                
                <CustomButton
                  disabled={false}
                  btnText={'Request OTP'}
                  onSubmit={onSubmit}
                />
              </View>
              <View style={styles.footerContainer}>
                <GuestAccount
                  accessible={true}
                  testID="LoginGuestAccountImg"
                  accessibilityLabel="LoginGuestAccountImg"
                  width={styles.footerSvg.width}
                  height={styles.footerSvg.height}
                />
                <TouchableOpacity
                  onPress={login}
                  style={styles.footerTextContainer}>
                  <SourceSansProRegTextView
                    testID="LoginNewToMindSparkText"
                    style={styles.footerText}>
                    {haveAnAccount}
                  </SourceSansProRegTextView>
                  <View style={styles.linkContainer}>
                    <SourceSansProRegTextView
                      testID="LoginCreateGuestAccountText"
                      style={styles.link}>
                      {`${loginWithExistingMindsparkId}`}
                    </SourceSansProRegTextView>
                  </View>
                </TouchableOpacity>
              </View>
            </View>
          </View>
        </View>
      </ScrollView>
    </BottomSheet>
  );
};

const styles = StyleSheet.create({
  screen: {
    flex: 1,
    justifyContent: 'flex-start',
    alignItems: 'center',
    backgroundColor: 'white',
    height: hp('100'),
  },
  innerContainer: {
    width: '100%',
    paddingHorizontal: getWp(32),
    paddingTop: getHp(80),
    alignItems: 'center',
  },
  logoContainer: {
    width: getWp(190),
    height: getHp(110),
    marginBottom: getHp(80),
  },
  logo: {
    width: '100%',
    height: '100%',
  },
  heading: {
    fontSize: TEXTFONTSIZE.Text28,
    color: COLORS.subtitleDarkBlue,
    marginBottom: getHp(36),
    textAlign: 'center',
  },
  input: {
    width: '100%',
    height: getHp(60),
    justifyContent: 'center',
    textAlign: 'center',
    borderColor: COLORS.disabledGray,
    borderWidth: 1,
    fontSize: TEXTFONTSIZE.Font20,
    color: COLORS.subtitleDarkBlue,
    marginBottom: getHp(24),
  },
  footerContainer: {
    flexDirection: 'row',
    justifyContent: 'center',
    width: '100%',
    marginTop: getHp(66),
  },
  footerSvg: {
    width: getWp(78),
    height: getHp(130),
  },
  footerTextContainer: {
    paddingLeft: 5,
    justifyContent: 'center',
    maxWidth: getWp(220),
  },
  footerText: {
    fontSize: TEXTFONTSIZE.Text20,
    marginLeft: 5,
    marginBottom: getHp(16),
  },
  link: {
    fontSize: TEXTFONTSIZE.Text20,
    color: COLORS.primary,
  },
  linkContainer: {
    borderWidth: 1,
    borderColor: COLORS.primary,
    borderRadius: 15,
    paddingHorizontal: 7,
    paddingVertical: 4,
  },
  errorMessage: {
    color: COLORS.errorMessage,
    fontSize: TEXTFONTSIZE.Text16,
    textAlign: 'left',
    marginTop: getHp(8),
    marginLeft: getWp(12),
  },
  inputContainerStyle: {
    backgroundColor: 'white',
    borderWidth: 1,
    borderColor: COLORS.disabledGray,
    borderRadius: getHp(60),
    height: getHp(60),
  },
  textContainerStyle: {
    backgroundColor: 'transparent',
    height: getHp(60),
    paddingLeft: 0,
  },
  textInputStyle: {
    height: getHp(60),
    fontSize: TEXTFONTSIZE.Font20,
  },
  codeTextStyle: {
    fontSize: TEXTFONTSIZE.Font20,
  },
  countryPickerStyle: {
    width: getWp(80),
  },
  btnContainer: {
    marginTop: getHp(15),
    marginBottom: getHp(20),
  },
  seperator: {
    height: 30,
    width: 1,
    zIndex: 1,
    backgroundColor: COLORS.disabledGray,
    position: 'absolute',
    left: DeviceInfo.isTablet() ? 120 : 70,
    top: DeviceInfo.isTablet() ? 20 : 10,
  },
});

export default observer(ContactNumberScreen);
