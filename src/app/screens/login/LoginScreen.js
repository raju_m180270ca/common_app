/* eslint-disable react-hooks/exhaustive-deps */
import React, {useReducer, useEffect, useContext} from 'react';
import {
  View,
  StyleSheet,
  ScrollView,
  Linking,
  TouchableWithoutFeedback,
} from 'react-native';
import {getHp, getWp, setAsValue, formValidation} from '@utils';
import {COLORS, TEXTFONTSIZE} from '@constants';
import {Logo, WhatsApp} from '@images';
import {
  CustomTextInput,
  BottomSheet,
  CustomButton,
  SmallRoundButton,
} from '@components';
import {
  SourceSansProBoldTextView,
  SourceSansProRegTextView,
} from '@components';
import {observer} from 'mobx-react';
import {useStores} from '@mobx/hooks';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {runInAction} from 'mobx';
import {API} from '@api';
import {
  ApiEndPoint,
  MixpanelCategories,
  MixpanelEvents,
  MixpanelActions,
} from '@constants';
import {ServerMessages} from '@constants';
import {useLanguage} from '@hooks';
import {AuthContext} from '@contexts/auth-context';
import {useNavigation} from '@react-navigation/native';
import NeedHelp from './NeedHelp';

const TEXT_CHANGE = 'TEXT_CHANGE';
const INVALID = 'INVALID';
const BOTTOMSHEET_OPEN = 'BOTTOMSHEET_OPEN';
const BOTTOMSHEET_CLOSE = 'BOTTOMSHEET_CLOSE';

const initialState = {
  enteredText: '',
  isError: false,
  errorMessage: '',
  enableSubmitBtn: false,
  bottomSheetOpen: false,
  isTouched: false,
};
var pattern = new RegExp(/[^a-zA-Z0-9_\-.]/);

const reducer = (state, action) => {
  switch (action.type) {
    case TEXT_CHANGE:
      return {
        ...state,
        enteredText: action.value.replace(pattern, '').toLowerCase(),
        enableSubmitBtn: true,
        isError: false,
        isTouched: true,
      };
    case INVALID:
      return {
        ...state,
        isError: action.isError,
        errorMessage: action.message,
      };
    case BOTTOMSHEET_OPEN:
      return {
        ...state,
        bottomSheetOpen: true,
      };
    case BOTTOMSHEET_CLOSE:
      return {
        ...state,
        bottomSheetOpen: false,
      };
    default:
      break;
  }
};

const LoginScreen = props => {
  const [state, dispatch] = useReducer(reducer, initialState);
  const store = useStores();
  const {loginBtnName, userNamePlaceHolder, nextText, teacherLoginError} =
    useLanguage();
  const auth = useContext(AuthContext);

  let {enteredText, isError, errorMessage, isTouched} = state;

  const {loginStore} = useStores();
  var navigation = useNavigation();

  const loginScreenApi = async () => {
    const loginScreenData = await API(ApiEndPoint.LOGIN_SCREEN, {store});
    if (loginScreenData.data.resultCode === 'C001') {
      const sparkiesChamp = loginScreenData.data.caurosal[0]?.studentList;
      if (sparkiesChamp) {
        loginStore.setSparkiesChamp(sparkiesChamp);
      }
      runInAction(() => {
        loginStore.sparkyFromDate = loginScreenData.data.fromDate;
        loginStore.sparkyToDate = loginScreenData.data.toDate;
        loginStore.loginPermissions = loginScreenData.data.permissions;
      });
    }
  };

  const fetchLanguage = async language => {
    try {
      let configTranslation = loginStore.getConfig?.data?.translation?.student;
      let userLanguage = language && language.length > 0 ? language : 'en-IN';
      if (
        configTranslation &&
        configTranslation?.hasOwnProperty(userLanguage)
      ) {
        let languageURL = configTranslation[userLanguage];
        let response = await fetch(languageURL);
        let responseJson = await response.json();
        store.uiStore.setLanguageData(responseJson);
      }
    } catch (e) {
      console.log(`Language API error>>>${e.message}`);
    }
  };

  useEffect(() => {
    auth.trackEvent('cleaverTap', MixpanelEvents.LOGIN_PAGE_OPEN);

    store.uiStore.setLoader(false);
    store.loginStore.setLoader(false);
    loginScreenApi();

    return () => {};
  }, [loginStore, store]);

  useEffect(() => {
    if (enteredText != '') {
      let {isValid, errMsg} = formValidation('Username', enteredText, {
        noSpecialCharsExceptAtChar: true,
        noWhiteSpace: true,
        noFirstDot: true,
      });
      if (!isValid) {
        dispatch({
          type: INVALID,
          isError: true,
          message: errMsg,
        });
      }
    }
  }, [enteredText]);

  let errorMessageView = null;

  if (isError) {
    errorMessageView = (
      <SourceSansProRegTextView
        style={styles.errorMessage}
        testID="LoginInputError">
        {errorMessage}
      </SourceSansProRegTextView>
    );
  }

  const onTextChange = text => {
    //text = text.toLowerCase();
    dispatch({type: TEXT_CHANGE, value: text});
  };

  const onSubmit = async () => {
    //Call checkUsername API
    const req = {
      body: {
        username: enteredText,
        app_id: loginStore.appId,
        platform: 'app',
      },
      store,
    };
    try {
      const response = await API(ApiEndPoint.CHECK_USERNAME, req);
      //Check No Password //
      loginStore.setUsername(enteredText);
      console.log(`CheckUserName JWT>>>>${JSON.stringify(response._headers)}`);
      console.log(
        req.body,
        'CHECK_USERNAME RESPONSE -============================================================================================================================== ',
        JSON.stringify(response),
      );
      if (response.data.resultCode === 'C001') {
        if (response?.data?.userInformation?.category === 'student') {
          let language = response?.data?.userInformation?.languageContext;
          if (response?.data?.userInformation?.userType === 'vernacular') {
            loginStore.setVernacularUser(true);
          }
          fetchLanguage(language);
          store.appStore.setUserLanguage(language);
          store.uiStore.setRTL(language);
          store.appStore.setScreeningTestStatus(false);

          setAsValue('userName', enteredText);
          auth.trackIdentity(enteredText);
          auth.trackEvent('mixpanel', MixpanelEvents.VALIDATE_USERNAME, {
            Category: MixpanelCategories.LOGIN,
            Action: MixpanelActions.CLICKED,
            Label: '',
          });

          setAsValue(
            'passwordType',
            response.data.userInformation.passwordType,
          );
          loginStore.setPasswordType(
            response.data.userInformation.passwordType,
          );
          if (response.data.userInformation.passwordType === 'picture') {
            props.navigation.navigate('PicturePasswordScreen', {
              setNewPassword:
                response.data.userInformation.callSetNewPasswordType,
              setPasswordAfterReset:
                response.data.userInformation.callSetNewPasswordType,
            });
          } else if (response.data.userInformation.passwordType === 'text') {
            props.navigation.navigate('TextPasswordScreen', {});
          }
          // ** passwordType == 'no' **//
          else if (response.data.userInformation.passwordType === 'no') {
            const noPassReq = {
              body: {
                username: enteredText,
                password: 'null',
                passwordType: 'no',
                app_id: loginStore.appId,
              },
              store,
            };
            const noPassResp = await API(
              ApiEndPoint.VALIDATE_PASSWORD,
              noPassReq,
            );
            await setAsValue('userRedirectionData', '');
            await setAsValue('subjectName', '');
            await setAsValue('trustedDeviceId', '');
            store.appStore.setTrusted(false);
            store.appStore.setSelectedSubject('');
            store.appStore.setTrustedDeviceId('');
            store.appStore.setSubjects('');

            if (
              noPassResp.data.resultCode === 'C004' &&
              noPassResp.data.redirectionCode == 'GetLandingPage'
            ) {
              // if productlist is there set it in store
              if (
                noPassResp.data.redirectionData &&
                noPassResp.data.redirectionData.productList
              ) {
                if (response.data.redirectionData.productList.length > 1) {
                  await setAsValue(
                    'userRedirectionData',
                    response.data.redirectionData.productList.join(','),
                  );
                  store.appStore.setUserRedirectionData(
                    response.data.redirectionData.productList,
                  );
                } else {
                  let productList = response.data.redirectionData.productList;
                  if (productList.length > 0) {
                    let product = productList[0];
                    let subjectName = product == 'MS3' ? 'Science' : 'Maths';
                    setAsValue('subjectName', subjectName);
                  }
                }
              }

              setAsValue('jwt', noPassResp.headers.jwt);
              store.appStore.setJwt(noPassResp.headers.jwt);
              loginStore.setShowTrustedPopUp(true);
              loginStore.setIsAuth(true);
            } else if (
              noPassResp.data.resultCode === 'C004' &&
              noPassResp.data.redirectionCode == 'SubscriptionEnded'
            ) {
              props.navigation.navigate('SubscriptionEndedScreen', {
                username: enteredText,
              });
            } else {
              loginStore.setLoader(false);
              dispatch({
                type: INVALID,
                isError: true,
                message: noPassResp.data.resultMessage,
              });
            }
          }
        } else {
          loginStore.setLoader(false);
          dispatch({
            type: INVALID,
            isError: true,
            message: teacherLoginError,
          });
        }
        // ** passwordType == 'no' **//
      } else if (
        response.data.resultCode === 'C004' &&
        response.data.redirectionCode === 'AccountLocked'
      ) {
        props.navigation.navigate('MessagesScreen', {type: 'accountLocked'});
      } else if (
        response.data.resultCode === 'C004' &&
        response.data.redirectionCode === 'SetPasswordAfterReset'
      ) {
        console.log(`CheckUserName JWT>>>>${response.headers.jwt}`);
        await setAsValue('jwt', response.headers.jwt);
        store.appStore.setJwt(response.headers.jwt);
        if (response.data.redirectionData.newPasswordType === 'picture') {
          props.navigation.navigate('PicturePasswordScreen', {
            setNewPassword: true,
            setPasswordAfterReset: true,
          });
        } else {
          props.navigation.navigate('TextPasswordScreen', {
            setNewPassword: true,
            setPasswordAfterReset: true,
          });
        }
      } else {
        auth.trackEvent('cleaverTap', MixpanelEvents.INVALID_USER_NAME);
        loginStore.setLoader(false);
        dispatch({
          type: INVALID,
          isError: true,
          message:
            ServerMessages[response?.data?.resultMessage?.toLowerCase()] ||
            response?.data?.resultMessage,
        });
      }
    } catch (err) {
      console.log(`API Error>>>>${JSON.stringify(err)}`);
    }
    //
  };

  // createAccount = () => {
  //   props.navigation.navigate('CreateAccountScreen', {

  //   });
  // }

  let submitBtn = (
    <CustomButton
      disabled={!isError && enteredText !== '' ? false : true}
      testId={'loginNextButton'}
      onSubmit={onSubmit}
      btnText={nextText}
    />
  );

  let whatsAppButton = (
    <View style={{position: 'absolute', bottom: getHp(-150), right: 0}}>
      <TouchableWithoutFeedback
        onPress={() => {
          Linking.openURL(
            'https://api.whatsapp.com/send/?phone=917066028641&app_absent=0',
          );
        }}>
        <WhatsApp />
      </TouchableWithoutFeedback>
    </View>
  );

  let backButton = (
    <View style={{position: 'absolute', top: 102, left: 52}}>
      <SmallRoundButton
        onPress={() => navigation.goBack()}
        iconName={'left'}
        iconColor={COLORS.maroon}
        iconTheme={'AntDesign'}
        type={'maroon'}
        width={30}
        height={30}
        borderRadius={20}
      />
    </View>
  );

  return (
    <BottomSheet>
      <ScrollView
        keyboardShouldPersistTaps="always"
        keyboardDismissMode="on-drag">
        <View style={styles.screen}>
          <View style={styles.innerContainer}>
            {backButton}
            <View style={styles.logoContainer}>
              <Logo
                accessible={true}
                testID="LoginMSLogo"
                accessibilityLabel="LoginMSLogo"
                width={styles.logo.width}
                height={styles.logo.height}
              />
              <NeedHelp />
            </View>
            <SourceSansProBoldTextView
              testID="LoginText"
              style={styles.heading}>
              {loginBtnName}
            </SourceSansProBoldTextView>
            <View style={{width: wp(80)}}>
              <CustomTextInput
                testID="CustomTextInputLoginInput"
                value={enteredText}
                placeholder={!isTouched ? userNamePlaceHolder : ''}
                isError={isError}
                style={styles.input}
                onChangeText={onTextChange}
                autoCapitalize="none"
                autoCorrect={true}
                autoCompleteType="off"
              />
              {errorMessageView}
              {submitBtn}
              {whatsAppButton}
            </View>
          </View>
        </View>
      </ScrollView>
    </BottomSheet>
  );
};

const styles = StyleSheet.create({
  screen: {
    flex: 1,
    justifyContent: 'flex-start',
    alignItems: 'center',
    backgroundColor: 'white',
    height: hp('100'),
  },
  innerContainer: {
    width: '100%',
    paddingHorizontal: getWp(32),
    paddingTop: getHp(80),
    alignItems: 'center',
  },
  logoContainer: {
    width: getWp(190),
    height: getHp(110),
    marginBottom: getHp(80),
  },
  logo: {
    width: '100%',
    height: '100%',
  },
  heading: {
    fontSize: TEXTFONTSIZE.Text28,
    color: COLORS.subtitleDarkBlue,
    marginBottom: getHp(36),
    textAlign: 'center',
  },
  input: {
    width: '100%',
    height: getHp(60),
    justifyContent: 'center',
    textAlign: 'center',
    borderColor: COLORS.disabledGray,
    borderWidth: 1,
    fontSize: TEXTFONTSIZE.Font20,
    color: COLORS.subtitleDarkBlue,
    marginBottom: getHp(24),
  },
  whatsAppButton: {
    width: '100%',
    height: getHp(60),
    marginTop: getHp(12),
    marginBottom: getHp(20),
  },
  footerContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingLeft: getWp(37),
    paddingRight: getWp(36),
    width: '100%',
    marginTop: getHp(76),
  },
  footerSvg: {
    width: getWp(74),
    height: getHp(124),
  },
  footerText: {
    fontSize: TEXTFONTSIZE.Text20,
  },
  link: {
    fontSize: TEXTFONTSIZE.Text20,
    color: COLORS.primary,
  },
  errorMessage: {
    color: COLORS.errorMessage,
    fontSize: TEXTFONTSIZE.Text16,
    textAlign: 'center',
    marginBottom: hp('2'),
    marginTop: hp('-2.5'),
  },
});

export default observer(LoginScreen);
