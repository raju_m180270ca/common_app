/**
|--------------------------------------------------
| Onboarding Screen
|--------------------------------------------------
*/
import React, {useState, useEffect} from 'react';
import {View, StyleSheet, TouchableOpacity} from 'react-native';
import {Button, NativeBaseProvider} from 'native-base';
import {
  Carousel,
  Onboarding,
  SourceSansProBoldTextView,
  SourceSansProRegTextView,
} from '@components';
import {COLORS, TEXTFONTSIZE} from '@constants';
import {
  slugify,
  onboardingLottie,
  getWp,
  getHp,
  configurePushNotification,
} from '@utils';
import {useStores} from '@mobx/hooks';
import {observer} from 'mobx-react';
import {useLanguage} from '@hooks';
import {useNavigation} from '@react-navigation/native';

const OnBoardingScreen = props => {
  const navigation = useNavigation();
  const {loginStore, appStore} = useStores();
  const {nextText, skipBtnText, continueText} = useLanguage();
  let data =
    loginStore.getConfig &&
    loginStore.getConfig.data &&
    loginStore.getConfig.data.featurePages &&
    loginStore.getConfig.data.featurePages.students;

  let [currItemIndex, setCurrItemIndex] = useState(0);
  let [carousel, setCarousel] = useState(null);
  let [onboardingFinished, setOnboardingFinished] = useState(false);

  useEffect(() => {
    // configurePushNotification(appStore, navigation, true);
  }, []);

  const onSlide = index => {
    setCurrItemIndex(index);
    if (index === data.length - 1) {
      setOnboardingFinished(true);
    } else {
      setOnboardingFinished(false);
    }
  };
  const getRef = c => {
    setCarousel(c);
  };

  let filterData = [];
  if (data && data.length > 0) {
    filterData = data.filter(item => item.status === 'A');
  }

  const renderFunc = ({item}) => {
    return (
      <Onboarding
        testID="onboardingDetails"
        title={item.title}
        desc={item.description}
        lottieFileName={onboardingLottie[slugify(item.title)]}
        currIndex={currItemIndex}
      />
    );
  };

  let footerBtnText = nextText;

  if (onboardingFinished) {
    footerBtnCallback = () => props.navigation.replace('ContactNumberScreen');
    footerBtnText = continueText;
  }

  const onClickFooterBtn = () => {
    if (onboardingFinished) {
      props.navigation.replace('ContactNumberScreen');
      loginStore.setSkipOnBoardingScreen(true);
    } else {
      carousel.snapToNext();
    }
  };

  let skipBtn = (
    <TouchableOpacity
      onPress={() => props.navigation.replace('ContactNumberScreen')}>
      <SourceSansProRegTextView
        testID="onBoardingSkipBtnText"
        style={styles.upperText}>
        {skipBtnText}
      </SourceSansProRegTextView>
    </TouchableOpacity>
  );

  if (onboardingFinished) {
    skipBtn = null;
  }

  console.log('onboarding screen');

  return (
    <View key="screen" style={styles.screen}>
      <View key="upperContainer" style={styles.upperContainer}>
        {/* {skipBtn} */}
      </View>
      <View style={styles.middleContainer}>
        {filterData && (
          <Carousel
            testID="onBoardingCarousel"
            data={filterData}
            renderFunc={renderFunc}
            onSlide={onSlide}
            currItemIndex={currItemIndex}
            getRef={getRef}
          />
        )}
      </View>
      <View style={styles.lowerContainer}>
        {/* <View style={styles.actions} /> */}
        <NativeBaseProvider>
          <Button
            block
            style={styles.btn}
            accessible={true}
            testID="onBoardingfooterBtn"
            accessibilityLabel="onBoardingFooterBtn"
            onPress={onClickFooterBtn}>
            <SourceSansProBoldTextView
              testID="onBoardingFooterText"
              style={styles.btnText}>
              {footerBtnText}
            </SourceSansProBoldTextView>
          </Button>
        </NativeBaseProvider>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  screen: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'flex-start',
    marginHorizontal: getWp(32),
  },
  upperContainer: {
    position: 'absolute',
    top: getHp(65),
    right: getWp(32),
  },
  upperTextContainer: {
    flex: 1,
    alignItems: 'flex-end',
    paddingTop: getHp(30.4),
  },
  upperText: {fontSize: TEXTFONTSIZE.Text16, color: COLORS.subtitleDarkBlue},
  middleContainer: {flex: 8},
  lowerContainer: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'flex-end',
  },
  actions: {
    flex: 1,
    justifyContent: 'flex-end',
    paddingBottom: getHp(39.4),
  },
  btn: {
    backgroundColor: COLORS.orange,
    height: getHp(60),
    alignSelf: 'center',
    elevation: 0,
    width: '100%',
    borderRadius:getHp(60),
    shadowOffset: {height: 0, width: 0},
    shadowOpacity: 0,
  },
  btnText: {fontSize: TEXTFONTSIZE.Text20, color: COLORS.white},
});

export default observer(OnBoardingScreen);
