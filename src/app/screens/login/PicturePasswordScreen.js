import React, {useState, useEffect, Fragment, useContext} from 'react';
import {View, StyleSheet, ScrollView, TouchableOpacity} from 'react-native';
import {getHp, getWp, replaceString} from '@utils';
import {COLORS, TEXTFONTSIZE} from '@constants';
import {
  PicturePassword,
  LoginHeader,
  SourceSansProBoldTextView,
  SourceSansProRegTextView,
  LoginFooterBtn,
  TrustedDeviceCallout,
} from '@components';
import {observer} from 'mobx-react';
import {useStores} from '@mobx/hooks';
import {heightPercentageToDP as hp} from 'react-native-responsive-screen';

import {useAuth, useLanguage} from '@hooks';
import {AuthContext} from '@contexts/auth-context';
import {MixpanelCategories, MixpanelEvents, MixpanelActions} from '@constants';
import DeviceInfo from 'react-native-device-info';
import {CheckedBox, UncheckedBox} from '@images';

const PicturePasswordScreen = props => {
  const [passwordSeleted, setPasswordSelected] = useState(0);
  const [passwordSelectionCompleted, setPasswordSelectionCompleted] =
    useState(false);
  const [row1, setRow1] = useState('');
  const [row2, setRow2] = useState('');
  const [row3, setRow3] = useState('');
  const [subHeading, setsubHeading] = useState('');
  // const [errorMessage, setErrorMessage] = useState('');
  const store = useStores();
  const {loginStore} = useStores();
  const {
    onSubmitResetPassword,
    onSubmitPassword,
    dispatch,
    errorMessage,
    PASSWORD_TEXT_CHANGE,
    trustedDevice,
    trusetedDeviceConfirmed,
    TRUSTED_DEVICE_CONFIRMED,
    TRUSTED_DEVICE,
  } = useAuth('picture');
  const auth = useContext(AuthContext);

  const {
    enterPasswordText,
    choosePasswordText,
    userNameText,
    saveMyLoginIdAndPassword,
    loginBtnName,
  } = useLanguage();

  console.log(`username from store>>>>${store.loginStore.username}`);
  useEffect(() => {
    console.log(
      `This is password selected effect >> count >> ${passwordSeleted}`,
    );
    if (passwordSeleted === 3) {
      setPasswordSelectionCompleted(true);
    } else {
      setPasswordSelectionCompleted(false);
    }
  }, [passwordSeleted]);

  useEffect(() => {
    dispatch({
      type: PASSWORD_TEXT_CHANGE,
      value: `${row1}|${row2}|${row3}`,
    });
  }, [row1, row2, row3]);

  useEffect(() => {
    if (props.route.params.setNewPassword) {
      setsubHeading(choosePasswordText);
    } else {
      setsubHeading(enterPasswordText);
    }
  }, [
    choosePasswordText,
    enterPasswordText,
    props.route.params.setNewPassword,
  ]);

  const onSelectPassword = type => {
    if (type === 'inc' && passwordSeleted < 3) {
      setPasswordSelected(passwordSeleted + 1);
    }

    if (type !== 'inc') {
      setPasswordSelected(passwordSeleted - 1);
    }
  };

  const onBackPress = () => {
    loginStore.setFirstLogin(false);
    props.navigation.goBack();
  };

  const onLogin = () => {
    if (
      props.route.params.setPasswordAfterReset ||
      props.route.params.setNewPassword
    ) {
      onSubmitResetPassword();
    } else {
      onSubmitPassword();
    }
    auth.trackEvent('mixpanel', MixpanelEvents.LOGIN, {
      Category: MixpanelCategories.LOGIN,
      Action: MixpanelActions.CLICKED,
      Label: '',
    });
  };

  let loginBtn = (
    <LoginFooterBtn
      testID="loginBtn1"
      type="disabledGray"
      text={loginBtnName}
      onPress={() => onLogin()}
      disabled={true}
      ppContainerStyle={styles.submitBtn}
      btnStyle={styles.btnStyle}
      forgotpassword={!props.route.params.setNewPassword}
    />
  );

  let errorMessageView = null;
  if (errorMessage) {
    errorMessageView = (
      <SourceSansProRegTextView
        testID="PicturePasswordErrorMsg"
        style={styles.errorMessage}>
        {errorMessage}
      </SourceSansProRegTextView>
    );
  }

  const onSaveMySession = async () => {
    auth.trackEvent('mixpanel', MixpanelEvents.TRUSTED_POP_UP_YES, {
      Category: MixpanelCategories.TRUSTEDDEVICE,
      Action: MixpanelActions.CLICKED,
      Label: '',
    });
    dispatch({type: TRUSTED_DEVICE_CONFIRMED, value: !trusetedDeviceConfirmed});
    loginStore.setTrusted(true);
  };

  showTrustedDevicePopUp = () => {
    auth.trackEvent('mixpanel', MixpanelEvents.TRUSTED_CHECKBOX, {
      Category: MixpanelCategories.TRUSTEDDEVICE,
      Action: MixpanelActions.CLICKED,
      Label: '',
    });
    if (trusetedDeviceConfirmed) {
      loginStore.setTrusted(false);
      dispatch({
        type: TRUSTED_DEVICE_CONFIRMED,
        value: !trusetedDeviceConfirmed,
      });
    }
    dispatch({type: TRUSTED_DEVICE, value: !trustedDevice});
  };

  disableTrustedDevice = () => {
    auth.trackEvent('mixpanel', MixpanelEvents.TRUSTED_POP_UP_NO, {
      Category: MixpanelCategories.TRUSTEDDEVICE,
      Action: MixpanelActions.CLICKED,
      Label: '',
    });
    dispatch({type: TRUSTED_DEVICE, value: !trustedDevice});
  };

  useEffect(() => {
    if (errorMessage) {
      auth.trackEvent('cleaverTap', MixpanelEvents.INCORRECT_PASSWORD);
    }

    return () => {};
  }, [errorMessage]);

  if (passwordSelectionCompleted) {
    loginBtn = (
      <>
        {errorMessageView}
        <LoginFooterBtn
          testID="loginBtn1"
          type="primaryOrange"
          text={loginBtnName}
          onPress={() => {
            props.route.params.setPasswordAfterReset
              ? onSubmitResetPassword()
              : onSubmitPassword();
          }}
          ppContainerStyle={styles.submitBtn}
          btnStyle={styles.btnStyle}
          forgotpassword={!props.route.params.setNewPassword}
        />
      </>
    );
  }

  return (
    <Fragment>
      <ScrollView
        showsVerticalScrollIndicator={false}
        style={{backgroundColor: COLORS.white, flex: 1}}>
        <View style={styles.screen}>
          <LoginHeader
            testID="LoginHeaderPicturePassword"
            theme={'generic'}
            lottieFileName={'naandi_header'}
            containerStyle={styles.header}
            hideBackButton={false}
            onBack={onBackPress}
          />
          <View style={styles.innerContainer}>
            <SourceSansProBoldTextView
              testID="PicturePasswordUserNameTxt"
              style={{...styles.text, ...styles.title}}>
              {replaceString(
                userNameText,
                'userName',
                store.loginStore.username,
              )}
            </SourceSansProBoldTextView>
            <SourceSansProBoldTextView
              testID="PicturePasswordSubHeading"
              style={{...styles.text, ...styles.subTitle}}>
              {subHeading}
            </SourceSansProBoldTextView>
            <PicturePassword
              testID="PicturePasswordPPAnimalPicture"
              onSelectPassword={onSelectPassword}
              category="animals"
              onPress={val => {
                setRow1(val);
                // setErrorMessage('');
              }}
              row="a"
              textStyle={{color: '#222E59'}}
            />
            <PicturePassword
              testID="PicturePasswordPPFruitPicture"
              onSelectPassword={onSelectPassword}
              category="fruits"
              row="b"
              onPress={val => {
                setRow2(val);
                // setErrorMessage('');
              }}
              textStyle={{color: '#222E59'}}
            />
            <PicturePassword
              testID="PicturePasswordPPFoodPicture"
              category="food"
              onSelectPassword={onSelectPassword}
              row="c"
              onPress={val => {
                setRow3(val);
                // setErrorMessage('');
              }}
              textStyle={{color: '#222E59'}}
            />
            {!props.route.params.setPasswordAfterReset &&
              !props.route.params.setNewPassword && (
                <TouchableOpacity onPress={() => showTrustedDevicePopUp()}>
                  <View style={styles.saveMyLoginConatiner}>
                    {trustedDevice ? <CheckedBox /> : <UncheckedBox />}
                    <SourceSansProRegTextView style={styles.saveMyLoginText}>
                      {saveMyLoginIdAndPassword}
                    </SourceSansProRegTextView>
                  </View>
                </TouchableOpacity>
              )}
            <View style={styles.btnStyle}>{loginBtn}</View>
          </View>
        </View>
      </ScrollView>
      {trustedDevice && !trusetedDeviceConfirmed && (
        <TrustedDeviceCallout
          onSaveMySession={onSaveMySession}
          disableTrustedDevice={disableTrustedDevice}
        />
      )}
    </Fragment>
  );
};

const styles = StyleSheet.create({
  screen: {
    alignSelf: 'center',
    alignItems: 'center',
    backgroundColor: COLORS.white,
    //minHeight: Platform.OS === 'ios' ? getHp(830) : getHp(860),
    flex: 1,
  },

  header: {
    marginBottom: DeviceInfo.isTablet ? getHp(42) : getHp(42),
    flex: 0.2,
  },

  text: {
    marginBottom: getHp(36),
    fontSize: TEXTFONTSIZE.Text20,
  },

  RTLTitle: {
    textAlign: 'right',
    marginBottom: getHp(25),
    fontSize: TEXTFONTSIZE.Text28,
  },

  title: {
    fontSize: DeviceInfo.isTablet ? TEXTFONTSIZE.Text28 : TEXTFONTSIZE.Text28,
    marginBottom: DeviceInfo.isTablet ? getHp(15) : getHp(15),
    color: COLORS.titleDarkBlue,
  },

  subTitle: {
    fontSize: TEXTFONTSIZE.Text20,
    color: COLORS.titlePink,
    marginBottom: DeviceInfo.isTablet ? getHp(15) : getHp(15),
  },

  innerContainer: {
    alignItems: 'center',
    paddingHorizontal: getWp(33),
    flex: 0.8,
  },

  submitBtn: {
    paddingHorizontal: getWp(33),
    alignItems: 'center',
    // flex: 1,
    width: '100%',
    marginBottom: getHp(40),
  },
  btnStyle: {
    width: getWp(360),
  },

  errorMessage: {
    color: COLORS.errorMessage,
    fontSize: TEXTFONTSIZE.Text16,
    textAlign: 'center',
    marginBottom: hp('2'),
  },
  saveMyLoginConatiner: {
    flexDirection: 'row',
    marginBottom: getHp(20),
  },
  saveMyLoginText: {
    marginLeft: getWp(8),
  },
});

export default observer(PicturePasswordScreen);
