/* eslint-disable react-hooks/exhaustive-deps */
import React, {useEffect, useContext} from 'react';
import {View, ScrollView, Linking, TouchableOpacity} from 'react-native';
import {AuthContext} from '@contexts/auth-context';
import {MixpanelCategories, MixpanelEvents, MixpanelActions} from '@constants';
import {
  LoginHeader,
  SVGImageBackground,
  SourceSansProBoldTextView,
  Loader,
  SourceSansProRegTextView,
} from '@components';
import styles from './style';
import {
  Angreji,
  Bhasha,
  Science,
  Ganith,
  Gujarati,
  Kannada,
  Marathi,
  Punjabi,
  Tamil,
  Telugu,
  Urdu,
  subjectBg,
} from '@images';
import {useStores} from '@mobx/hooks';
import {observer} from 'mobx-react';
import {API} from '@api';
// import jwt_decode from 'jwt-decode';
import {getHp, getAsValue, setAsValue, getProductName} from '@utils';
import {ApiEndPoint} from '@constants';
import {useLanguage} from '@hooks';
const SelectSubjectScreen = props => {
  const accessFromTeacherDashboard =
    props &&
    props.route &&
    props.route.params &&
    props.route.params.accessFromTeacherDashboard
      ? props.route.params.accessFromTeacherDashboard
      : false;
  const {
    choseSubjectTxt,
    preciousTreasureText,
    mathsSubject,
    englishSubjectText,
    scienceSubjectText,
  } = useLanguage();
  const [isVisible, showLoaderVisbility] = React.useState(false);
  const store = useStores();
  const auth = useContext(AuthContext);
  const {appStore, uiStore, loginStore} = store;
  let data = appStore.userRedirectionData;
  if (props.route && props.route.params && props.route.params.fromNavHeader) {
    data = appStore.subjects;
  }

  const languageData = uiStore.languageData;
  const isRTL = uiStore.isRTL;

  useEffect(() => {
    getSubjectList();
  }, []);

  const getSubjectList = () => {
    var subjectList = [];
    data.map(product => {
      subjectList.push(getSubject(product));
    });
    return subjectList;
  };

  const getSubject = product => {
    var Image;
    var subjectName;
    switch (product) {
      case 'MS1':
        Image = Ganith;
        subjectName = mathsSubject;
        break;

      case 'MS2':
        Image = Angreji;
        subjectName = englishSubjectText;
        break;

      case 'MS3':
        Image = Science;
        subjectName = scienceSubjectText;
        break;

      case 'MS4':
        Image = Bhasha;
        subjectName = languageData?.MSHi;
        break;

      case 'MS5':
        Image = Urdu;
        subjectName = languageData?.MSUr;
        break;

      case 'MS6':
        Image = Marathi;
        subjectName = languageData?.MSMa;
        break;

      case 'MS7':
        Image = Punjabi;
        subjectName = languageData?.MSPu;
        break;

      case 'MS8':
        Image = Gujarati;
        subjectName = languageData?.MSGu;
        break;

      case 'MS9':
        Image = Telugu;
        subjectName = languageData?.MSTe;
        break;

      case 'MS10':
        Image = Tamil;
        subjectName = languageData?.MSTa;
        break;

      case 'MS11':
        Image = Kannada;
        subjectName = languageData?.MSKa;
        break;
    }

    return (
      <TouchableOpacity
        style={styles.itemContainer}
        onPress={() => {
          auth.trackEvent('mixpanel', MixpanelEvents.SUBJECT_SELECTION, {
            Category: MixpanelCategories.LOGIN,
            Action: MixpanelActions.CLICKED,
            Label: '',
          });
          onSubjectSelected(product, subjectName);
        }}>
        <SVGImageBackground
          testID="SVGImageBackgroundSelectedSubject"
          SvgImage={subjectBg}
          subjectSelection={true}>
          <View style={styles.itemSubContainer}>
            <Image
              accessible={true}
              testID="SelectedSubjectImage"
              accessibilityLabel="SelectedSubjectImage"
              style={styles.iconStyle}
              height={getHp(55)}
            />
            <SourceSansProBoldTextView
              testID="SelectedSubjectNameText"
              style={styles.subjectText}>
              {subjectName}
            </SourceSansProBoldTextView>
          </View>
        </SVGImageBackground>
      </TouchableOpacity>
    );
  };

  const onSubjectSelected = async (product, subjectName) => {
    console.log('APP respp product selected', product);
    await setAsValue('subjectName', subjectName);

    if (props.route && props.route.params && props.route.params.fromNavHeader) {
      const reqBody = {
        body: {
          username: store.appStore.username,
          trustedDeviceId: store.appStore.trustedDeviceId,
          productName: getProductName(product),
        },
        store: store,
      };
      const response = await API(ApiEndPoint.START_NEW_SESSION, reqBody);
      if (response.data.resultCode === 'C001') {
        loginStore.setUserType(1);
        appStore.setJwt(response.headers.jwt);
        await setAsValue('jwt', response.headers.jwt);
        await setAsValue('oldJWT', response.headers.jwt);
        appStore.setScreeningTestStatus(false);

        props.navigation.navigate('DashboardScreen');
      }
    } else {
      try {
        const jwt = await getAsValue('jwt');
        const reqBody = {
          body: {
            token: jwt,
            productSelection: product,
            platform: 'mobile',
          },
          jwt: jwt,
          store: store,
        };

        const response = await API(ApiEndPoint.GET_LANDING_PAGE, reqBody);

        if (
          response.data.resultCode === 'C001' &&
          response.data.resultMessage === 'success'
        ) {
          loginStore.setUserType(1);
          appStore.setJwt(response.headers.jwt);
          await setAsValue('jwt', response.headers.jwt);
          await setAsValue('oldJWT', response.headers.jwt);
          loginStore.setIsAuth(true);
          props.navigation.navigate('DashboardScreen');
        } else if (response.data.resultCode == 'CL0028') {
          store.uiStore.apiErrorInit({
            code: 'Oops!',
            message: response?.data?.resultMessage,
          });
        } else {
          uiStore.apiErrorInit({
            code: response.status,
            message: response.data?.resultMessage,
          });
        }
      } catch (e) {
        console.log('APP Respp LoginScreenAPI Error ', e);
      }
    }
  };

  let loader = null;
  if (isVisible) {
    loader = <Loader />;
  }

  return (
    <View style={styles.screen}>
      <LoginHeader
        testID="LoginHeaderSelectSubjectScreen"
        theme={'generic'}
        lottieFileName={'naandi_header'}
        containerStyle={styles.header}
        hideBackButton={!accessFromTeacherDashboard}
      />
      <ScrollView
        showsVerticalScrollIndicator={false}
        style={styles.scrollViewStyle}>
        <View style={styles.innerContainer}>
          <SourceSansProBoldTextView
            testID="SelectedSubjectSubjectTxt"
            style={styles.title}>
            {choseSubjectTxt}
          </SourceSansProBoldTextView>
          <SourceSansProRegTextView
            testID="SelectedSubjectPreciousTreasureText"
            style={styles.subTitle}>
            {preciousTreasureText}
          </SourceSansProRegTextView>
          <View
            style={[isRTL ? styles.gridRTLContainer : styles.gridContainer]}>
            {getSubjectList()}
          </View>
        </View>
      </ScrollView>
      {/* {loader} */}
    </View>
  );
};

export default observer(SelectSubjectScreen);
