import React, {Fragment, useContext, useEffect} from 'react';
import {
  View,
  StyleSheet,
  TouchableOpacity,
  TouchableWithoutFeedback,
  Keyboard,
} from 'react-native';

import {getHp, getWp, PwdValidation} from '@utils';
import {COLORS, TEXTFONTSIZE} from '@constants';

import {
  InputWithRightIcon,
  LoginHeader,
  LoginFooterBtn,
  SourceSansProBoldTextView,
  SourceSansProRegTextView,
  TrustedDeviceCallout,
} from '@components';
import {Password, PasswordSee, CheckedBox, UncheckedBox} from '@images';
import {useStores} from '@mobx/hooks';
import {observer} from 'mobx-react';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import {useAuth, useLanguage} from '@hooks';
import {AuthContext} from '@contexts/auth-context';
import {MixpanelCategories, MixpanelEvents, MixpanelActions} from '@constants';
import {useNavigation} from '@react-navigation/native';

const Screen = props => {
  const {
    onSubmitResetPassword,
    onSubmitPassword,
    dispatch,
    passwordSecure,
    isTouched,
    errorMessage,
    passwordText,
    isPasswordError,
    confirmPasswordSecure,
    confirmPasswordText,
    isConfirmPasswordError,
    SEE_PASSWORD,
    SEE_CONFIRM_PASSWORD,
    PASSWORD_TEXT_CHANGE,
    CONFIRM_PASSWORD_TEXT_CHANGE,
    TRUSTED_DEVICE,
    INVALID,
    CONFIRM_PWD_INVALID,
    paswordRules,
    confirmPasswordRules,
    trustedDevice,
    trusetedDeviceConfirmed,
    TRUSTED_DEVICE_CONFIRMED,
  } = useAuth('text');

  const {saveMyLoginIdAndPassword} = useLanguage();
  const {loginStore} = useStores();
  const store = useStores();
  const auth = useContext(AuthContext);
  var navigation = useNavigation();

  const onPressIcon = type => {
    type === 'pwd'
      ? dispatch({type: SEE_PASSWORD})
      : dispatch({type: SEE_CONFIRM_PASSWORD});
  };

  const onPasswordChange = password => {
    console.log('onPasswordChange' + password);
    const {isValid, errMsg} = PwdValidation('password', password, paswordRules);

    dispatch({type: INVALID, isError: !isValid, message: errMsg});
    dispatch({type: PASSWORD_TEXT_CHANGE, value: password});
  };
  const onConfirmPasswordChange = confirmPassword => {
    const {isValid, errMsg} = PwdValidation(
      'password',
      confirmPassword,
      confirmPasswordRules,
    );

    dispatch({type: CONFIRM_PWD_INVALID, isError: !isValid, message: errMsg});
    dispatch({type: CONFIRM_PASSWORD_TEXT_CHANGE, value: confirmPassword});
  };

  const onLogin = () => {
    loginStore.setIsOtpLogin(false);
    if (
      props.route.params.setPasswordAfterReset ||
      props.route.params.setNewPassword
    ) {
      onSubmitResetPassword();
    } else {
      onSubmitPassword();
    }
    auth.trackEvent('mixpanel', MixpanelEvents.LOGIN, {
      Category: MixpanelCategories.LOGIN,
      Action: MixpanelActions.CLICKED,
      Label: '',
    });
  };

  const onSaveMySession = async () => {
    auth.trackEvent('mixpanel', MixpanelEvents.TRUSTED_POP_UP_YES, {
      Category: MixpanelCategories.TRUSTEDDEVICE,
      Action: MixpanelActions.CLICKED,
      Label: '',
    });
    dispatch({type: TRUSTED_DEVICE_CONFIRMED, value: !trusetedDeviceConfirmed});
    loginStore.setTrusted(true);
  };

  const showTrustedDevicePopUp = () => {
    auth.trackEvent('mixpanel', MixpanelEvents.TRUSTED_CHECKBOX, {
      Category: MixpanelCategories.TRUSTEDDEVICE,
      Action: MixpanelActions.CLICKED,
      Label: '',
    });

    if (trusetedDeviceConfirmed) {
      loginStore.setTrusted(false);
      dispatch({
        type: TRUSTED_DEVICE_CONFIRMED,
        value: !trusetedDeviceConfirmed,
      });
    }
    dispatch({type: TRUSTED_DEVICE, value: !trustedDevice});
  };

  const disableTrustedDevice = () => {
    auth.trackEvent('mixpanel', MixpanelEvents.TRUSTED_POP_UP_NO, {
      Category: MixpanelCategories.TRUSTEDDEVICE,
      Action: MixpanelActions.CLICKED,
      Label: '',
    });

    dispatch({type: TRUSTED_DEVICE, value: !trustedDevice});
  };

  let PwdSvg = Password;
  let ConPwdSvg = Password;
  let passwordHeading = 'Create New Password';
  let passwordFields = (
    <Fragment>
      <InputWithRightIcon
        testID="InputWithRightIconTextPasswordEnterNewPasswordInput"
        SvgImage={PwdSvg}
        containerStyle={styles.password}
        secureTextEntry={passwordSecure}
        onPressIcon={onPressIcon.bind(this, 'pwd')}
        onChangeText={onPasswordChange}
        value={passwordText}
        textContentType="oneTimeCode"
        placeholder={passwordHeading} //{!isTouched ? passwordHeading : ''}
        placeholderTextColor={COLORS.popUpTextColor}
        isError={isPasswordError}
        autoCapitalize="none"
      />
      <InputWithRightIcon
        testID="InputWithRightIconTextPasswordReEnterNewPasswordInput"
        SvgImage={ConPwdSvg}
        containerStyle={styles.confirmPassword}
        secureTextEntry={confirmPasswordSecure}
        textContentType="oneTimeCode"
        onPressIcon={onPressIcon.bind(this, 'conpwd')}
        onChangeText={onConfirmPasswordChange}
        value={confirmPasswordText}
        placeholder={'Re-enter New Password'} //{!isTouched ? 'Re-enter New Password' : ''}
        placeholderTextColor={COLORS.popUpTextColor}
        isError={isConfirmPasswordError}
        autoCapitalize="none"
      />
    </Fragment>
  );

  let passwordInstructions = (
    <View style={styles.textContainer}>
      <SourceSansProBoldTextView
        testID="TextPasswordYourPassword"
        style={styles.instructionText}>
        Your Password
      </SourceSansProBoldTextView>
      <SourceSansProRegTextView
        testID="TextPasswordYourPasswordValidation"
        style={{...styles.instructionText, ...styles.desc}}>
        {`${'\u2B24'} Can contain _ @ . -${'\n'}${'\u2B24'} Cannot contain special characters like !#$%^&*(){}[]${'\n'}${'\u2B24'} Minimum character length is 4${'\n'}`}
      </SourceSansProRegTextView>
    </View>
  );

  if (!passwordSecure) {
    PwdSvg = PasswordSee;
  }

  if (!confirmPasswordSecure) {
    ConPwdSvg = PasswordSee;
  }
  if (!props.route.params.setNewPassword) {
    passwordHeading = 'Enter password';
    passwordFields = (
      <InputWithRightIcon
        testID="InputWithRightIconTextPasswordInput"
        SvgImage={PwdSvg}
        containerStyle={styles.password}
        secureTextEntry={passwordSecure}
        onPressIcon={onPressIcon.bind(this, 'pwd')}
        onChangeText={onPasswordChange}
        value={passwordText}
        placeholder={!isTouched ? passwordHeading : ''}
        placeholderTextColor={COLORS.popUpTextColor}
        isError={errorMessage && true}
        autoCapitalize="none"
      />
    );
    passwordInstructions = null;
  }

  let loginBtn = (
    <LoginFooterBtn
      testID="loginBtn"
      type="disabledGray"
      text="LOGIN"
      disabled={true}
      forgotpassword={!props.route.params.setNewPassword}
    />
  );

  if (
    (loginStore.firstLogin &&
      passwordText != '' &&
      confirmPasswordText != '' &&
      !isPasswordError &&
      !isConfirmPasswordError) ||
    (!loginStore.firstLogin && passwordText != '' && !isPasswordError)
  ) {
    loginBtn = (
      <LoginFooterBtn
        testID="loginBtn"
        type="primaryOrange"
        text="LOGIN"
        onPress={() => onLogin()}
        forgotpassword={!props.route.params.setNewPassword}
      />
    );
  }

  let errorMessageView = null;

  if (errorMessage) {
    errorMessageView = (
      <SourceSansProRegTextView
        style={styles.errorMessage}
        testID="TextPasswordErrorText">
        {errorMessage}
      </SourceSansProRegTextView>
    );
  }

  const onBackPress = () => {
    loginStore.setFirstLogin(false);
    if (loginStore.isOtpLogin) {
      navigation.navigate('SelectionScreen');
      loginStore.setIsOtpLogin(false);
    } else {
      navigation.goBack();
    }
  };

  useEffect(() => {
    if (errorMessage) {
      auth.trackEvent('cleaverTap', MixpanelEvents.INCORRECT_PASSWORD);
    }

    return () => {};
  }, [errorMessage]);

  return (
    <TouchableWithoutFeedback onPress={Keyboard.dismiss} accessible={false}>
      <View>
        <KeyboardAwareScrollView
          keyboardShouldPersistTaps="always"
          keyboardDismissMode="on-drag"
          style={{backgroundColor: 'white'}}>
          <View style={styles.screen}>
            <LoginHeader
              testID="LoginHeaderTextPassword"
              //theme={'generic'}
              //lottieFileName={'naandi_header'}
              containerStyle={styles.header}
              hideBackButton={false}
              onBack={onBackPress}
            />
            <View style={styles.innerContainer}>
              <SourceSansProBoldTextView
                testID="TextPasswordUserNameText"
                style={{...styles.text, ...styles.title}}>
                Hey, {store.loginStore.username}
              </SourceSansProBoldTextView>
              <SourceSansProBoldTextView
                testID="TextPasswordUserPasswordHeading"
                style={{...styles.text, ...styles.subTitle}}>
                {passwordHeading}
              </SourceSansProBoldTextView>
              <View style={styles.inputContainer}>
                {passwordFields}
                {errorMessageView}
                {!props.route.params.setPasswordAfterReset &&
                  !props.route.params.setNewPassword && (
                    <TouchableOpacity onPress={() => showTrustedDevicePopUp()}>
                      <View style={styles.saveMyLoginConatiner}>
                        {trustedDevice ? <CheckedBox /> : <UncheckedBox />}
                        <SourceSansProRegTextView
                          style={styles.saveMyLoginText}>
                          {saveMyLoginIdAndPassword}
                        </SourceSansProRegTextView>
                      </View>
                    </TouchableOpacity>
                  )}
              </View>
              {passwordInstructions}
            </View>
            {loginBtn}
          </View>
        </KeyboardAwareScrollView>
        {trustedDevice && !trusetedDeviceConfirmed && (
          <TrustedDeviceCallout
            onSaveMySession={onSaveMySession}
            disableTrustedDevice={disableTrustedDevice}
          />
        )}
      </View>
    </TouchableWithoutFeedback>
  );
};

const styles = StyleSheet.create({
  screen: {
    flex: 1,
    justifyContent: 'flex-start',
    alignItems: 'center',
    backgroundColor: 'white',
    minHeight: getHp(840),
  },
  modalHeader: {
    marginTop: getHp(30),
    position: 'absolute',
    alignSelf: 'center',
    fontSize: TEXTFONTSIZE.Text32,
    color: COLORS.statTextColor,
  },
  modalContentText: {
    width: getWp(204),
    marginTop: getHp(30),
    alignSelf: 'center',
    justifyContent: 'center',
    fontSize: TEXTFONTSIZE.Text16,
    color: '#464646',
  },
  modalButtonRow: {
    position: 'absolute',
    bottom: 0,
    paddingBottom: getHp(48),
    marginTop: getHp(40),
    flexDirection: 'row',
  },
  buttonSkipText: {
    color: COLORS.orange,
    fontSize: TEXTFONTSIZE.Text20,
    fontFamily: 'BalooThambi-Regular',
  },
  buttonYesText: {
    color: COLORS.white,
    fontSize: TEXTFONTSIZE.Text20,
    fontFamily: 'BalooThambi-Regular',
  },
  modalOuter: {
    backgroundColor: 'rgba(0,0,0,.8)',
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  headerSvg: {
    marginTop: getHp(10),
    width: getWp(230),
    height: getHp(110),
  },
  modalContainer: {
    borderRadius: getWp(50),
    borderColor: '#DEE2EB',
    borderWidth: getWp(3),
    height: getHp(520),
    width: getWp(340),
    alignItems: 'center',
    backgroundColor: COLORS.white,
  },
  header: {marginBottom: getHp(42)},
  text: {
    marginBottom: getHp(36),
    fontSize: TEXTFONTSIZE.Text20,
  },
  title: {fontSize: TEXTFONTSIZE.Text28, color: COLORS.titleDarkBlue},
  subTitle: {fontSize: TEXTFONTSIZE.Text20, color: COLORS.titlePink},

  innerContainer: {
    alignItems: 'center',
    paddingHorizontal: getWp(33),
    paddingVertical: getHp(50),
    width: '100%',
  },
  submitBtn: {
    width: '100%',
    height: getHp(60),
    marginBottom: getHp(40),
  },
  inputContainer: {
    marginBottom: getHp(70),
  },
  password: {
    width: '100%',
    height: getHp(60),
    marginBottom: getHp(24),
  },
  confirmPassword: {
    width: '100%',
    height: getHp(60),
    marginBottom: getHp(20),
  },
  textContainer: {
    alignItems: 'flex-start',
    justifyContent: 'center',
    marginBottom: getHp(30),
  },
  instructionText: {
    fontSize: TEXTFONTSIZE.Text14,
    lineHeight: getHp(24),
    color: COLORS.inputTextBlack,
  },
  desc: {
    color: COLORS.infoMessageGray,
  },
  errorMessage: {
    color: COLORS.errorMessage,
    fontSize: TEXTFONTSIZE.Text16,
    textAlign: 'center',
  },
  modalStyle: {
    borderRadius: getWp(20),
    justifyContent: 'center',
  },
  saveMyLoginConatiner: {
    flexDirection: 'row',
  },
  saveMyLoginText: {
    marginLeft: getWp(8),
  },
});

export default observer(Screen);
