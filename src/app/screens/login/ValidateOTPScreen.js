import React, {Fragment, useEffect, useState, useRef} from 'react';
import {View, StyleSheet, Keyboard} from 'react-native';

import {getHp, getWp} from '@utils';
import {COLORS, TEXTFONTSIZE} from '@constants';
import {
  LoginHeader,
  SourceSansProBoldTextView,
  SourceSansProRegTextView,
  OTPInput,
  CustomButton,
  RenewUserList,
} from '@components';
import {useStores} from '@mobx/hooks';
import {observer} from 'mobx-react';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import {useNavigation} from '@react-navigation/native';
import {TouchableOpacity} from 'react-native-gesture-handler';
import RNOtpVerify from 'react-native-otp-verify';
import {API} from '@api';
import {ApiEndPoint} from '@constants';
import moment from 'moment';
import {useAuth} from '@hooks';

const ValidateOTPScreen = props => {
  const {parseSubmitPasswordRes} = useAuth('text');

  const store = useStores();
  const {loginStore} = useStores();

  var navigation = useNavigation();
  const [secs, setSecs] = useState(0);
  const [mins, setMins] = useState(2);
  const [otpError, setOtpError] = useState('');
  const [showResendOtp, setShowResendOtp] = useState(false);
  const otpRef = useRef();
  const [otpArray, setOtpArray] = useState([]);
  const [isModalVisible, setIsModalVisible] = useState(false);
  const [intervalId, setIntervalId] = useState(0);

  const onSubmit = async () => {
    const inputOtp = otpRef.current.getInputOtp().toString();
    if (inputOtp.length === 4) {
      setOtpError('');

      const req = {
        body: {
          mobileNo: loginStore.parentMobile,
          countryCode: loginStore.countryCode,
          OTP: inputOtp,
        },
        store,
      };

      try {
        const response = await API(ApiEndPoint.VALIDATE_OTP, req, false);
        if (response.data.resultCode === 'CL0023') {
          navigation.navigate('SelectionScreen');
        } else if (response.data.resultCode === 'C004') {
          parseSubmitPasswordRes(response);
        } else if (response.data.resultCode === 'CL0031') {
          loginStore.setRenewStudents(response.data.userData);
          setIsModalVisible(true);
        } else {
          setOtpError(response.data.resultMessage);
          otpRef.current.resetOtp();
        }
      } catch (err) {
        console.log(`API Error>>>>${JSON.stringify(err)}`);
      }
    } else {
      // Show Error
      setOtpError('Please check and enter the valid OTP');
    }
  };

  useEffect(() => {
    startTimer();
    getOtp();
    return () => {
      if (intervalId) {
        clearInterval(intervalId);
      }
      RNOtpVerify.removeListener();
    };
  }, []);

  const getOtp = async () => {
    await RNOtpVerify.getOtp();
    RNOtpVerify.addListener(message => {
      const otp = /(\d{4})/g.exec(message)[1];
      setOtpArray(otp.split(''));
      RNOtpVerify.removeListener();
      Keyboard.dismiss();
    });
  };

  const startTimer = () => {
    var interval = 1000;
    var counter = 120000;

    const endTime = moment().add(2, 'minutes');
    let diff = 0;

    if (intervalId) {
      clearInterval(intervalId);
    }

    setIntervalId(
      setInterval(() => {
        diff = endTime.diff(moment(), 'seconds');

        counter = counter - (counter / 1000 - diff) * 1000;
        if (counter <= 0) {
          setMins(0);
          setSecs(0);
          clearInterval(intervalId);
          setShowResendOtp(true);
        } else {
          var minutes = Math.floor(counter / 60000);
          var seconds = ((counter % 60000) / 1000).toFixed(0);
          setMins(minutes);
          setSecs(seconds);
        }
      }, interval),
    );
  };

  const resendOtp = async () => {
    const formData = new FormData();
    setOtpError('');

    formData.append('mobileNo', loginStore.parentMobile);
    formData.append('countryCode', loginStore.countryCode);

    const req = {
      body: formData,
      store,
    };

    try {
      const response = await API(ApiEndPoint.GENERATE_OTP, req, false);
      if (response.data.resultCode === 'C001') {
        setMins(2);
        setSecs(0);
        clearInterval(intervalId);
        setShowResendOtp(false);
        startTimer();
        getOtp();
      }
    } catch (err) {
      console.log(`API Error>>>>${JSON.stringify(err)}`);
    }
  };

  const hideRenewModel = () => {
    setOtpArray([]);
    otpRef.current.resetOtp();
    setIsModalVisible(false);
    setMins(2);
    setSecs(0);
    clearInterval(intervalId);
    setShowResendOtp(true);
  };

  return (
    <Fragment>
      <KeyboardAwareScrollView
        keyboardShouldPersistTaps="always"
        keyboardDismissMode="on-drag"
        style={{backgroundColor: 'white'}}>
        <View style={styles.screen}>
          {isModalVisible && (
            <RenewUserList isNewFlow={true} hideRenewModel={hideRenewModel} />
          )}

          <LoginHeader
            testID="LoginHeaderTextPassword"
            containerStyle={styles.header}
            hideBackButton={false}
            iconStyle={{
              iconName: 'left',
              iconColor: COLORS.maroon,
              iconTheme: 'AntDesign',
              type: 'maroon',
              width: 30,
              height: 30,
            }}
            isNewFlow={true}
          />
          <View style={styles.innerContainer}>
            <SourceSansProBoldTextView testID="OTPText" style={styles.subTitle}>
              Enter OTP
            </SourceSansProBoldTextView>
            <SourceSansProRegTextView style={styles.instructionStyle}>
              We have sent an OTP on your phone number, please enter it below to
              login
            </SourceSansProRegTextView>
            <View style={styles.otpInputContainer}>
              <OTPInput
                accessible={true}
                testID="otpInput"
                accessibilityLabel="otpInput"
                otpLength={4}
                ref={otpRef}
                otp={otpArray}
                isNewFlow={true}
              />

              {showResendOtp ? (
                <TouchableOpacity
                  onPress={() => {
                    resendOtp();
                  }}>
                  <View>
                    <SourceSansProRegTextView
                      testID="ResendOtp"
                      style={styles.resendOtp}>
                      Resend OTP
                    </SourceSansProRegTextView>
                  </View>
                </TouchableOpacity>
              ) : (
                <SourceSansProRegTextView style={styles.otpExpiry}>
                  OTP expires in{' '}
                  <SourceSansProBoldTextView
                    testID="OTPTimer"
                    style={styles.otpTimer}>
                    {mins >= 10 ? mins : `0${mins}`}:
                    {secs >= 10 ? secs : `0${secs}`}
                  </SourceSansProBoldTextView>{' '}
                  sec
                </SourceSansProRegTextView>
              )}
            </View>
            {otpError != '' && (
              <SourceSansProRegTextView
                style={styles.errorMessage}
                testID="otpError">
                {otpError}
              </SourceSansProRegTextView>
            )}

            <TouchableOpacity onPress={() => navigation.goBack()}>
              <View style={styles.footerContainer}>
                <SourceSansProBoldTextView
                  testID="phoneNumberText"
                  style={styles.otpTimer}>
                  {loginStore.parentMobile}
                </SourceSansProBoldTextView>
                <SourceSansProRegTextView
                  testID="LoginNewToMindSparkText"
                  style={styles.footerText}>
                  Wrong information?
                </SourceSansProRegTextView>

                <SourceSansProRegTextView
                  testID="LoginCreateGuestAccountText"
                  style={styles.link}>
                  Click here to change
                </SourceSansProRegTextView>
              </View>
            </TouchableOpacity>

            <View style={styles.btnContainer}>
              <CustomButton
                disabled={false}
                testId={'sendOtpButton'}
                onSubmit={onSubmit}
                btnText={'Verify'}
              />
            </View>
          </View>
        </View>
      </KeyboardAwareScrollView>
    </Fragment>
  );
};

const styles = StyleSheet.create({
  screen: {
    flex: 1,
    justifyContent: 'flex-start',
    alignItems: 'center',
    backgroundColor: 'white',
    minHeight: getHp(840),
  },

  header: {marginBottom: getHp(42)},
  innerContainer: {
    paddingHorizontal: getWp(33),
    paddingVertical: getHp(50),
    width: '100%',
  },
  errorMessage: {
    color: COLORS.errorMessage,
    fontSize: TEXTFONTSIZE.Text16,
    textAlign: 'center',
  },
  instructionStyle: {
    textAlign: 'center',
    marginTop: getHp(30),
    fontSize: TEXTFONTSIZE.Text18,
    color: COLORS.sortListSeparateColor,
    marginHorizontal: getWp(16),
  },
  subTitle: {
    fontSize: TEXTFONTSIZE.Text22,
    color: COLORS.orange,
    textAlign: 'center',
  },
  otpExpiry: {
    color: COLORS.inputHeaderColor,
    marginTop: getHp(30),
  },
  otpTimer: {
    fontSize: TEXTFONTSIZE.Text18,
  },
  otpInputContainer: {
    marginTop: getHp(35),
    alignSelf: 'center',
    alignItems: 'center',
    justifyContent: 'center',
  },
  btnContainer: {
    marginTop: getHp(60),
  },
  resendOtp: {
    fontSize: TEXTFONTSIZE.Text20,
    color: COLORS.orange,
    marginTop: getHp(30),
  },
  footerText: {
    fontSize: TEXTFONTSIZE.Font20,
  },
  link: {
    fontSize: TEXTFONTSIZE.Font20,
    color: '#b91e1e',
  },
  footerContainer: {
    marginTop: getHp(40),
  },
});

export default observer(ValidateOTPScreen);
