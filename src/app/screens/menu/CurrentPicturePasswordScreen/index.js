/* eslint-disable react-hooks/exhaustive-deps */
import React, {useState, useEffect, useMemo} from 'react';
import {View, Text, TouchableOpacity, Image, Clipboard} from 'react-native';
import {
  Header,
  BalooThambiRegTextView,
  LottieBackground,
  RoundedButton,
  PicturePassword,
} from '@components';
import styles from './style';
import {useStores} from '@mobx/hooks';
import {getWp, getHp} from '@utils';
import {ScrollView} from 'react-native-gesture-handler';
import {useLanguage} from '@hooks';
import {ApiEndPoint} from '@constants';
import {API} from '@api';

const CurrentPicturePasswordScreen = props => {
  const [passwordSeleted, setPasswordSelected] = useState(0);
  const [row1, setRow1] = useState('');
  const [row2, setRow2] = useState('');
  const [row3, setRow3] = useState('');
  const [ErrorMessage, setErrorMessage] = useState('');

  const text = 'Select your current favorite';
  const {changePassNolineBreak, currentPassText, nextText} = useLanguage();
  const store = useStores();
  const {loginStore} = store;

  const Validate_Password = async () => {
    try {
      let selectedPassword = row1 + '|' + row2 + '|' + row3;
      const req = {
        body: {
          username: loginStore.username,
          password: selectedPassword,
          passwordType: 'picture',
          app_id: store.loginStore.appId,
          platform: 'app'
        },
        store,
      };

      var response;
      response = await API(ApiEndPoint.VALID_PASSWORD_V3, req);
      if (response.data.resultCode === 'CL001') {
        setErrorMessage(response.data.resultMessage);
        // setShowMessagePopup(true);
        //dispatch({type: INVALID, message: response.data.resultMessage});
      } else {
        setErrorMessage('');
        props.navigation.navigate('ChangeNewPicturePasswordScreen', {
          selectedPassword: selectedPassword,
        });
      }
    } catch (err) {
      console.log('Validate_Password = ', err);
    }
  };

  const onSelectPassword = type => {
    setPasswordSelected(passwordSeleted =>
      type === 'inc'
        ? passwordSeleted >= 3
          ? 3
          : passwordSeleted + 1
        : passwordSeleted <= 0
        ? 0
        : passwordSeleted - 1,
    );

    console.log('passwordSeleted:', passwordSeleted);
  };

  return (
    <View style={styles.flexOne}>
      <LottieBackground
        testID="LottieBackgroundChangeCurrPicPassBg"
        lottieJsonName="bg8">
         <View style={{flex:0.1}}>
        <Header
          testID="HeaderChangeCurrPicPassBackBtn"
          type={'back'}
          containerStyle={{flex:1}}
          onClick={() => {
            props.navigation.goBack();
          }}
        />
        </View>
        <ScrollView style={{flex:0.9}}>
          <View style={styles.contentStyle}>
            <BalooThambiRegTextView
              testID="ChangeCurrPicPassNoLineBreak"
              style={styles.title}>
              {changePassNolineBreak}
            </BalooThambiRegTextView>
            <View style={styles.flexOne}>
              <BalooThambiRegTextView
                testID="ChangeCurrPicPassText"
                style={styles.subtitle}>
                {currentPassText}
              </BalooThambiRegTextView>
              <PicturePassword
                testID="PicturePasswordChangeCurrPicPassAnimals"
                onSelectPassword={onSelectPassword}
                category="animals"
                textStyle={styles.whiteText}
                onPress={val => {
                  setRow1(val);
                }}
                row="a"
                text={text}
              />
              <PicturePassword
                testID="PicturePasswordChangeCurrPicPassAnimals"
                onSelectPassword={onSelectPassword}
                category="fruits"
                textStyle={styles.whiteText}
                row="b"
                text={text}
                onPress={val => {
                  setRow2(val);
                }}
              />
              <PicturePassword
                testID="PicturePasswordChangeCurrPicPassFood"
                category="food"
                textStyle={styles.whiteText}
                onSelectPassword={onSelectPassword}
                row="c"
                text={text}
                onPress={val => {
                  setRow3(val);
                }}
              />

              {ErrorMessage ? 
                <Text
                  style={{
                    color: 'red',
                    fontWeight: 'bold',
                    fontSize: 18,
                    padding: '5%',
                  }}>
                  {ErrorMessage}
                </Text>:null}
              <RoundedButton
                testID="RoundedButtonChangeCurrPicPass"
                text={nextText}
                type={
                  passwordSeleted === 3
                    ? 'elevatedOrange'
                    : 'squareDisabledElevated'
                }
                width={getWp(180)}
                height={getHp(70)}
                disabled={passwordSeleted === 3 ? false : true}
                onPress={() => {
                  Validate_Password();
                  // let selectedPassword = row1 + '|' + row2 + '|' + row3;
                  // console.warn('Password:', selectedPassword);
                  // props.navigation.navigate('ChangeNewPicturePasswordScreen', {
                  //   selectedPassword: selectedPassword,
                  // });
                }}
                textStyle={styles.whiteButtonText}
              />
            </View>
          </View>
        </ScrollView>
      </LottieBackground>
    </View>
  );
};

export default CurrentPicturePasswordScreen;
