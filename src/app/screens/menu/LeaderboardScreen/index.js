// External Imports
import React, {useState, useEffect, Fragment, useContext} from 'react';
import {FlatList, View, TouchableOpacity} from 'react-native';
import {useStores} from '@mobx/hooks';
import {observer} from 'mobx-react';
import {useLanguage} from '@hooks';

// Internal Imports
import {BalooThambiRegTextView,DetailsScreen} from '@components';
import {Leaderboard} from '@hoc';
import {API} from '@api';
import {ApiEndPoint} from '@constants';
import styles from './style';
import {useBackHandler} from '@react-native-community/hooks';

import {AuthContext} from '@contexts/auth-context';
import {MixpanelEvents, MixpanelCategories, MixpanelActions} from '@constants';

const LeaderboardScreen = props => {
  const store = useStores();
  const auth = useContext(AuthContext);

  const {leaderBoardStore} = store;
  const [selected, setSelected] = useState(0);
  const [isEveryCountZero, setIsEveryCountZero] = useState(false);
  const {
    yourSection,
    yourCity,
    yourCountry,
    leaderboardLabelText,
    sectionLeaderBoardEmpty,
    cityLeaderBoardEmpty,
    worldLeaderBoardEmpty,
  } = useLanguage();
  const [filterItems, setFilterItem] = useState([
    {
      id: '1',
      title: yourSection,
      selectedTab: 'yourSection',
    },
  ]);
  var countryObj = {
    id: '3',
    title: yourCountry,
    selectedTab: 'yourCountry',
  };

  var cityObj = {
    id: '2',
    title: yourCity,
    selectedTab: 'yourCity',
  };

  const permissions =
    Object.keys(store.uiStore.menuDataPermissions).length > 0
      ? store.uiStore.menuDataPermissions.leaderBoard
      : {};

  useEffect(() => {
    if (permissions.yourCity) {
      filterItems.push(cityObj);
    }

    if (permissions.yourCountry) {
      filterItems.push(countryObj);
    }
  }, []);

  useEffect(() => {
    leaderBoardStore.reset();
    fetchLeaderboard(1);
  }, [selected]);

  useBackHandler(() => {
    return props.navigation.navigate('DashboardScreen');
  });

  const fetchLeaderboard = async index => {
    try {
      if (index !== 1) {
        if (typeof leaderBoardStore?.currentPage !== 'undefined') {
          index = leaderBoardStore?.currentPage + 1;
          if (index > leaderBoardStore.totalPages) return;
        } else return;
      }

      const selectedTab = filterItems[selected]?.selectedTab;
      const req = {
        body: {
          selectedTab: selectedTab,
          index: index,
          startFrom: (index - 1) * 50 + 1,
          limit: 50,
        },
        store: store,
      };

      const response = await API(ApiEndPoint.UPDATED_GET_LEADERBOARD_INFO, req);
      if (response?.data?.resultCode === 'C001') {
        if (response?.data?.userInformation.selectedTheme) {
          if (
            `${response?.data?.userInformation.selectedTheme}`.includes(
              'science',
            )
          ) {
            let temp = [];
            temp.push(filterItems[0]);
            setFilterItem(temp);
          }
        }
        console.log(
          'is every count zerio',
          response?.data.groupSparkieData.every(item => item.count == 0),
        );
        setIsEveryCountZero(
          response?.data.groupSparkieData.every(item => item.count == 0),
        );
        if (index === 1) leaderBoardStore.init(response?.data);
        else {
          leaderBoardStore.setGroupSparkieData(
            leaderBoardStore?.groupSparkieData.concat(
              response?.data?.groupSparkieData,
            ),
          );
          leaderBoardStore.setPaginationDetails(response?.data);
        }
      } else {
        // console.log("in the else");
        // store.uiStore.apiErrorInit({
        //   code: 401,
        //   message: 'logout message',
        // });
      }
    } catch (ex) {
      console.log('Leaderboard Error>>>', ex);
    }
  };

  const renderLeaderboardSection = () => {
    console.log('================');
    console.log(selected);
    if (
      leaderBoardStore?.groupSparkieData === undefined ||
      leaderBoardStore?.groupSparkieData.length === 0 ||
      isEveryCountZero
    )
      return (
        <View style={styles.emptyMessageContainer}>
          <BalooThambiRegTextView
            testID="LeaderBoardEmptyTxt"
            style={styles.emptyMessageText}>
            {selected === 0
              ? sectionLeaderBoardEmpty
              : selected === 1
              ? cityLeaderBoardEmpty
              : worldLeaderBoardEmpty}
          </BalooThambiRegTextView>
        </View>
      );

    return (
      <Leaderboard
        testID="leaderBoardScreen"
        permissions={permissions}
        onEndReached={fetchLeaderboard}
        type={filterItems[selected]?.selectedTab}
      />
    );
  };

  const selectTabs = index => {
    setSelected(index);
    switch (index) {
      case 0:
        auth.trackEvent('cleaverTap', MixpanelEvents.SECTION_LEADERBOARD);
        auth.trackEvent('mixpanel', MixpanelEvents.SECTION_LEADERBOARD, {
          Category: MixpanelCategories.LEADERBOARD,
          Action: MixpanelActions.CLICKED,
          Label: '',
        });
        break;
      case 1:
        auth.trackEvent('cleaverTap', MixpanelEvents.CITY_LEADERBOARD);
        auth.trackEvent('mixpanel', MixpanelEvents.CITY_LEADERBOARD, {
          Category: MixpanelCategories.LEADERBOARD,
          Action: MixpanelActions.CLICKED,
          Label: '',
        });
        break;
      default:
        auth.trackEvent('cleaverTap', MixpanelEvents.WORLD_LEADERBOARD);
        auth.trackEvent('mixpanel', MixpanelEvents.WORLD_LEADERBOARD, {
          Category: MixpanelCategories.LEADERBOARD,
          Action: MixpanelActions.CLICKED,
          Label: '',
        });
        break;
    }
  };

  const renderItem = ({item, index}) => {
    return (
      <Fragment>
        <TouchableOpacity onPress={() => selectTabs(index)}>
          <View
            style={[
              styles.filterItemStyle,
              selected === index && styles.selectedFilterItemStyle,
            ]}>
            <BalooThambiRegTextView
              testID={`LeaderBoardTitleText${item.id}`}
              style={[
                styles.textStyle,
                selected === index && styles.selectedTextStyle,
              ]}>
              {item?.title}
            </BalooThambiRegTextView>
          </View>
        </TouchableOpacity>
      </Fragment>
    );
  };

  const headerBtnClickHandler = () => {
    props.navigation.goBack();
  };

  return (
    <DetailsScreen
      testID="DetailsScreenLeaderBoardBackBtn"
      headerBtnType="back"
      headerBtnClick={headerBtnClickHandler}
      footerContainerStyle={styles.footerContainerStyle}>
      <BalooThambiRegTextView
        testID="DetailsScreenLeaderBoardLabel"
        style={styles.titleStyle}>
        {leaderboardLabelText}
      </BalooThambiRegTextView>
      <View style={styles.listContainerStyle}>
        <FlatList
          showsHorizontalScrollIndicator={false}
          data={filterItems}
          renderItem={renderItem}
          horizontal={true}
        />
      </View>
      {permissions?.sparkieLeaderboard && renderLeaderboardSection()}
    </DetailsScreen>
  );
};

export default observer(LeaderboardScreen);
