import React, {useEffect, useState, useContext} from 'react';
import {View, FlatList} from 'react-native';
import {NativeBaseProvider, Tabs} from 'native-base';
import {SimpleLottie,DetailsScreen} from '@components';
import {
  BadgeList,
  TitleList,
  RewardShowcase,
  AppliedReward,
  RewardCollectionModal,
} from '@hoc';
import {observer} from 'mobx-react';
import {API} from '@api';
import {useStores} from '@mobx/hooks';
import {ApiEndPoint, REWARD_TYPES, REWARD_TYPES_CATEGORY} from '@constants';
import styles from './indexCss';
import {RewardTabs} from './rewardTabs';
import {AuthContext} from '@contexts/auth-context';
import {ThemeContext} from '@contexts/theme-context';
import {MixpanelCategories, MixpanelEvents, MixpanelActions} from '@constants';
import {useLanguage} from '@hooks';
import {useBackHandler} from '@react-native-community/hooks';

const RewardsScreen = props => {
  const {} = props;
  const theme = useContext(ThemeContext);
  const store = useStores();
  const auth = useContext(AuthContext);
  const {appStore} = useStores();
  const {profileDetails} = appStore.userData;
  const {badgesLabel, titleLabel} = useLanguage();
  //const [dummyRewardData] = useState(userRewardInfoSt);
  const [rewardShowCaseDetails, setRewardShowCaseDetails] = useState({
    type: '',
    item: undefined,
  });
  const [showAppliedStatus, setShowAppliedStatus] = useState(false);
  const [filterRewardTypes] = useState([
    {
      id: 0,
      title: badgesLabel,
      type: REWARD_TYPES.BADGES,
    },
    {
      id: 1,
      title: titleLabel,
      type: REWARD_TYPES.TITLES,
    },
  ]);
  const [currentRewardTab, setcurrentRewardTab] = useState(
    filterRewardTypes[0],
  );

  const [rewardModal, setRewardModal] = useState(false);

  useEffect(() => {
    // store.appStore.setRewardData(dummyRewardData);
    // setRewardShowCaseDetails({ type: REWARD_TYPES.BADGES, item: dummyRewardData.badges.earned[0] });
    (async () => {
      await fetchRewards(REWARD_TYPES.BADGES);
      await fetchRewards(REWARD_TYPES.TITLES, false);
    })();
  }, []);

  useBackHandler(() => {
    return props.navigation.navigate('DashboardScreen');
  });
  // useBackHandler(() => {
  //   return props.navigation.goBack();
  // });

  const fetchRewards = async (
    type = REWARD_TYPES.BADGES,
    setSelectedRewardShowCase = true,
  ) => {
    const req = {
      body: {
        type,
      },
      store: store,
    };
    try {
      const response = await API(ApiEndPoint.GET_REWARD_INFO, req);
      if (response.data.resultCode === 'C001') {
        if (setSelectedRewardShowCase) {
          let earnedBadge =
            response.data.userRewardInfo[type]?.earned?.length == 0
              ? undefined
              : response.data.userRewardInfo[type]?.earned?.find(
                  earnedBadge => earnedBadge.isApplied,
                ) || undefined;
          if (earnedBadge) {
            setRewardShowCaseDetails({
              item: {...earnedBadge, category: REWARD_TYPES_CATEGORY.EARNED},
              type,
            });
          }
        }
        let newRewardData = {...store.appStore.rewardData};
        newRewardData[type] = response.data.userRewardInfo[type];
        store.appStore.setRewardData(newRewardData);
      } else {
        store.uiStore.apiErrorInit({
          code: response.status,
          message: response.data?.resultMessage,
        });
      }
    } catch (e) {
      console.log(`Reward Details error>>>${e}`);
    }
  };

  const applyRewards = async (type, Id) => {
    try {
      store.uiStore.setChangedInUserData(true);
      let newRewardData = {...store.appStore.rewardData};
      newRewardData[type].earned = newRewardData[type].earned.map(badgeData => {
        if (badgeData.isApplied) {
          badgeData.isApplied = false;
        }
        if (type == 'badges') {
          if (badgeData.badgeID == Id) {
            badgeData.isApplied = true;
            return badgeData;
          } else {
            return badgeData;
          }
        } else {
          if (badgeData.titleID == Id) {
            badgeData.isApplied = true;
            if (profileDetails != null && profileDetails.title != null) {
              profileDetails.title.titleName = badgeData?.name;
              profileDetails.title.titleID = badgeData?.titleID;
              profileDetails.title.titleImg = badgeData?.titleIcon;
            }
            return badgeData;
          } else {
            return badgeData;
          }
        }
      });
      store.appStore.setRewardData(newRewardData);
    } catch (err) {
      console.log('Error  = ', err);
    }
  };

  const onSetRewardsClick = async () => {
    console.log('Reward On Click Entered : ');
    let engagementId =
      rewardShowCaseDetails.type == REWARD_TYPES.BADGES
        ? rewardShowCaseDetails.item.badgeID
        : rewardShowCaseDetails.item.titleID;
    const req = {
      body: {
        ID: engagementId,
        type: rewardShowCaseDetails.type.slice(0, -1),
      },
      store: store,
    };
    console.log('payload', req.body);
    try {
      const response = await API(ApiEndPoint.APPLY_ENGAGEMENT_REWARD, req);
      console.log('response-', response);
      if (response.data.resultCode === 'C001') {
        setRewardShowCaseDetails({
          type: rewardShowCaseDetails.type,
          item: {...rewardShowCaseDetails.item, isApplied: true},
        });
        //await fetchRewards(currentRewardTab.type);
        await applyRewards(currentRewardTab.type, engagementId);
      } else {
        store.uiStore.apiErrorInit({
          code: response.status,
          message: response.data?.resultMessage,
        });
      }
    } catch (e) {
      console.log('SET_ENGAGEMENT_ERROR - ', JSON.stringify(e));
    }
  };

  const switchTabContent = async renderContent => {
    setcurrentRewardTab(renderContent);
    if (renderContent.type === 'badges') {
      auth.trackEvent('mixpanel', MixpanelEvents.BADGES_REWARDS, {
        Category: MixpanelCategories.REWARDS,
        Action: MixpanelActions.CLICKED,
        Label: '',
      });
    } else {
      auth.trackEvent('mixpanel', MixpanelEvents.TITLES_REWARDS, {
        Category: MixpanelCategories.TITLE,
        Action: MixpanelActions.CLICKED,
        Label: '',
      });
    }

    setRewardShowCaseDetails({type: '', item: undefined});
    await fetchRewards(renderContent.type, true);
  };

  return (
    <DetailsScreen
      testID="DetailScreenRewards"
      headerBtnType="back"
      headerBtnClick={props.navigation.goBack}
      footerContainerStyle={styles.footerContainerStyle}>
      <View style={styles.lottieAnimationLeftContainer}>
        <SimpleLottie
          testID="SimpleLottieRewardsDrawerAnimation"
          theme={theme.name}
          jsonFileName="drawerAnimation"
        />
      </View>
      <View style={styles.lottieAnimationRightContainer}>
        <SimpleLottie
          testID="SimpleLottieRewardsDashboardAnimation"
          theme={theme.name}
          jsonFileName="dashboardAnimation"
        />
      </View>
      <View style={styles.childContainer}>
        <View style={styles.rewardShowCaseSection}>
          <RewardShowcase
            testID="rewardShowCaseRewardsDetails"
            rewardShowCaseDetails={rewardShowCaseDetails}
            onSetRewardsClick={onSetRewardsClick}
          />
        </View>
        <View style={styles.rewardContentSection}>
          <View style={styles.rewardContentFlatListStyle}>
            <FlatList
              horizontal={true}
              showsHorizontalScrollIndicator={false}
              data={filterRewardTypes}
              renderItem={renderContent => (
                <RewardTabs
                  {...renderContent}
                  testID={renderContent.item.id}
                  onTabTitlePress={() => switchTabContent(renderContent.item)}
                  isActive={currentRewardTab.id == renderContent.item.id}
                />
              )}
              keyExtractor={({id}) => id}
            />
          </View>
          <View>
            <BadgeList
              testID="BadgeListRewards"
              setRewardShowCase={setRewardShowCaseDetails}
              selectedReward={rewardShowCaseDetails}
              heading={''}
            />
            <TitleList
              testID="TitleListRewards"
              setRewardShowCase={setRewardShowCaseDetails}
              selectedReward={rewardShowCaseDetails}
              heading={''}
            />
          </View>
        </View>
        <AppliedReward
          testID="AppliedAwardRewards"
          isVisible={showAppliedStatus}
        />
        <RewardCollectionModal
          testID="CollectionModalRewards"
          isVisible={rewardModal}
          onSkipBtnPressed={() => {
            setRewardModal(false);
          }}
          onStartBtnPressed={() => {
            setRewardModal(false);
          }}
        />
      </View>
    </DetailsScreen>
  );

  // return (
  //   <DetailsScreen
  //     testID="DetailScreenRewards"
  //     headerBtnType="back"
  //     headerBtnClick={props.navigation.goBack}
  //     footerContainerStyle={styles.footerContainerStyle}>
  //     <View style={styles.lottieAnimationLeftContainer}>
  //       <SimpleLottie
  //         testID="SimpleLottieRewardsDrawerAnimation"
  //         theme={theme.name}
  //         jsonFileName="drawerAnimation"
  //       />
  //     </View>
  //     <View style={styles.lottieAnimationRightContainer}>
  //       <SimpleLottie
  //         testID="SimpleLottieRewardsDashboardAnimation"
  //         theme={theme.name}
  //         jsonFileName="dashboardAnimation"
  //       />
  //     </View>
  //     <View style={styles.childContainer}>
  //       <View style={styles.rewardShowCaseSection}>
  //         <RewardShowcase
  //           testID="rewardShowCaseRewardsDetails"
  //           rewardShowCaseDetails={rewardShowCaseDetails}
  //           onSetRewardsClick={onSetRewardsClick}
  //         />
  //       </View>
  //       <View style={styles.rewardContentSection}>
  //         <View style={styles.rewardContentFlatListStyle}>
  //           <FlatList
  //             horizontal={true}
  //             showsHorizontalScrollIndicator={false}
  //             data={filterRewardTypes}
  //             renderItem={renderContent => (
  //               <RewardTabs
  //                 {...renderContent}
  //                 testID={renderContent.item.id}
  //                 onTabTitlePress={() => switchTabContent(renderContent.item)}
  //                 isActive={currentRewardTab.id == renderContent.item.id}
  //               />
  //             )}
  //             keyExtractor={({id}) => id}
  //           />
  //         </View>
  //         <Tabs
  //           accessible={true}
  //           testID="rewardscurrentRewardTab"
  //           accessibilityLabel="rewardscurrentRewardTab"
  //           initialPage={currentRewardTab.id}
  //           page={currentRewardTab.id}
  //           locked={true}
  //           renderTabBar={false}>
  //           <BadgeList
  //             testID="BadgeListRewards"
  //             setRewardShowCase={setRewardShowCaseDetails}
  //             selectedReward={rewardShowCaseDetails}
  //             heading={''}
  //           />
  //           <TitleList
  //             testID="TitleListRewards"
  //             setRewardShowCase={setRewardShowCaseDetails}
  //             selectedReward={rewardShowCaseDetails}
  //             heading={''}
  //           />
  //         </Tabs>
  //       </View>
  //       <AppliedReward
  //         testID="AppliedAwardRewards"
  //         isVisible={showAppliedStatus}
  //       />
  //       <RewardCollectionModal
  //         testID="CollectionModalRewards"
  //         isVisible={rewardModal}
  //         onSkipBtnPressed={() => {
  //           setRewardModal(false);
  //         }}
  //         onStartBtnPressed={() => {
  //           setRewardModal(false);
  //         }}
  //       />
  //     </View>
  //   </DetailsScreen>
  // );
};

RewardsScreen.propTypes = {};

RewardsScreen.defaultProps = {};

export default observer(RewardsScreen);
