import React from 'react';
import { TouchableOpacity } from 'react-native';
import {
    BalooThambiRegTextView,
} from '@components';
import styles from './indexCss';

export const RewardTabs = ({ testID, item, index, onTabTitlePress, isActive = false }) => {
    const {
        title,
    } = item;
    return (
        <TouchableOpacity accessible={true} testID={`RewardTabsRewardsContent${testID}`} accessibilityLabel={`RewardTabsRewardsContent${testID}`} style={{ ...styles.tabContainerStyle, borderBottomWidth: isActive ? 3 : 0 }} onPress={onTabTitlePress}>
            <BalooThambiRegTextView testID="rewardTabsTitle" style={{ ...styles.tabContainerTextStyle, color: isActive ? `#FEDB31` : `#FFFFFF` }}>
                {title}
            </BalooThambiRegTextView>
        </TouchableOpacity>
    );
} 