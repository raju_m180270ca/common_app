/* eslint-disable react-hooks/exhaustive-deps */
import React, {useState, useEffect} from 'react';
import {
  FlatList,
  RefreshControl,
  ScrollView,
  TouchableOpacity,
  View,
} from 'react-native';
import {API} from '@api';
import {useStores} from '@mobx/hooks';
import {ApiEndPoint} from '@constants';
import styles from './style';
import {
  BalooThambiRegTextView,
  SelectionPopup,
  ChoiceList,
  SourceSansProRegTextView,
  RoundedButton,
  DetailsScreen,
} from '@components';
import {QuestionItem} from '@hoc';
import {observer} from 'mobx-react';
import {StarRemove, ArrowDownRed, StarredQuestionEmpty9} from '@images';
import {getWp} from '@utils/ViewUtils';
import {useLanguage} from '@hooks';
import {toJS} from 'mobx';
import {useStarQuestion} from '@hooks';
import {useBackHandler} from '@react-native-community/hooks';

const StarredQuestionScreen = props => {
  const [refreshing, setRefreshing] = useState(false);
  const [topicList, setTopicList] = useState([]);
  const [selectedTopic, setSelectedTopic] = useState('');
  const [removeFav, setRemoveFav] = useState(false);
  const [clickDropdown, setClickDropDown] = useState(false);
  const [selectedLable, setSelectedLabel] = useState('');
  const [removeFavContent, setRemoveFavContent] = useState(null);
  const {onStarHandler} = useStarQuestion();

  // const [heading, setHeading] = useState('No starred questions found');
  const store = useStores();
  const {starredQuestionStore} = useStores();
  const {
    bookmarkedQuestion,
    bookmarks,
    goHomeBtnText,
    attentionPleaseText,
    noCancelBtnText,
    yesRemoveBtnText,
    thisWillRemoveDescText,
    starredQuestionEmptyState,
  } = useLanguage();

  const permissions =
    Object.keys(store.uiStore.menuDataPermissions).length > 0
      ? store.uiStore.menuDataPermissions.starredQuestionList
      : {};
  let topic;

  //To show explanation in QuestionItem
  permissions.viewExplanation = true;

  let heading = null;
  let topicListDropdown = [];
  useEffect(() => {
    fetchContent(1);
  }, [selectedTopic]);

  useBackHandler(() => {
    headerBtnClickHandler();
  });

  const fetchContent = async index => {
    if (!refreshing) {
      console.log('\nIndex', index);
      if (index !== 1) {
        index =
          starredQuestionStore?.favouritesResponse?.paginationDetails
            ?.currentPage + 1;
        if (
          index >
          starredQuestionStore?.favouritesResponse?.paginationDetails
            ?.totalPages
        ) {
          return;
        }
      }
      setRefreshing(true);
      let reqBody = {
        limit: 20,
        startFrom: (index - 1) * 20 + 1,
      };
      if (selectedTopic) {
        reqBody.topicID = selectedTopic;
      }
      let req = {
        body: reqBody,
        store: store,
      };
      let res = await API(ApiEndPoint.GET_FAVOURITES_LIST, req);
      if (res.data.resultCode === 'C001') {
        if (index === 1) {
          console.log('Favourites list res>>>>', res.data);
          starredQuestionStore.init(res.data);
        } else {
          console.log('Favourites list res after 1st page', res.data);
          starredQuestionStore.setFavouriteList(
            starredQuestionStore.favouritesList.concat(
              res.data.favouritesDetails.favouritesList,
            ),
          );
          starredQuestionStore.topicList(res.data.topicDetails.topicList);
        }

        //create topicList array for dropdown

        for (topic of starredQuestionStore.topicList) {
          topicListDropdown.push({
            value: `• ${topic.topicName}`,
            id: topic.topicID,
          });
        }
        setTopicList(topicListDropdown);

        //Set selected topic
        let topicSelected = starredQuestionStore.topicList.find(
          topic => topic.selected,
        );
        setSelectedTopic(topicSelected.topicID);
        setSelectedLabel(`• ${topicSelected.topicName}`);
      } else if (res.data.resultCode === 'S014') {
        starredQuestionStore.init(res.data);
      } else {
        store.uiStore.apiErrorInit({
          code: res.status,
          message: res.data?.resultMessage,
        });
      }
      setRefreshing(false);
    }
  };

  const renderItem = ({item, index}) => {
    if (item.data) {
      if (item.settings && !item.settings.showAnswer) {
        permissions.viewExplanation = false;
      }
      console.log('APP respp item', toJS(item));
      return (
        // <View>
        //   <BalooThambiRegTextView>hello</BalooThambiRegTextView>
        // </View>
        <QuestionItem
          accessible={true}
          testID={`starredQuestionAnswerItem${item.id}`}
          accessibilityLabel={`starredQuestionAnswerItem${item.id}`}
          response={item}
          seqNum={index + 1}
          showAns={false}
          showTimeTaken={false}
          showAnswerStatus={false}
          StarSvg={StarRemove}
          isFromStarredQuestions
          onPressStar={() => onPressStarHandler(item.contentID)}
          howIDid={false}
          permissions={permissions}
        />
      );
    }
  };

  const onPressStarHandler = async contentId => {
    setRemoveFavContent(contentId);
    setRemoveFav(true);
  };

  const onSelectHandler = topicId => {
    setClickDropDown(false);
    setSelectedLabel(topicId.value);
    setSelectedTopic(topicId.id);
  };

  if (
    starredQuestionStore.favouritesList &&
    starredQuestionStore.favouritesList.length &&
    starredQuestionStore.favouritesList[0].data
  ) {
    heading = `${
      starredQuestionStore.favouritesList.length
    } ${bookmarkedQuestion}${
      starredQuestionStore.favouritesList.length > 1 ? 's' : ''
    }`;
  }

  const onCancelPressHandler = () => {
    setRemoveFav(false);
  };

  const onActionPressHandler = async () => {
    setRemoveFav(false);
    const req = {
      topicId: selectedTopic,
      contentId: removeFavContent,
    };
    let favouritesListCopy = [...starredQuestionStore.favouritesList];
    favouritesListCopy.splice(favouritesListCopy, 1);
    if (favouritesListCopy.length == 0) {
      starredQuestionStore.setFavouriteList(null);
    } else {
      starredQuestionStore.setFavouriteList(favouritesListCopy);
    }
    onStarHandler(req, true);
  };

  const headerBtnClickHandler = () => {
    props.navigation.goBack();
  };

  return (
    <DetailsScreen
      testID="DetailsScreenStarredQuestionsBackBtn"
      headerBtnType="back"
      headerBtnClick={headerBtnClickHandler}
      footerContainerStyle={styles.footerContainerStyle}>
      {starredQuestionStore.favouritesResponse && (
        <ScrollView>
          <View style={styles.flexOne}>
            <View style={styles.contentStyle}>
              <BalooThambiRegTextView
                testID="StarredQuestionStaredPularTex"
                style={styles.title}>
                {bookmarks}
              </BalooThambiRegTextView>
              {starredQuestionStore.topicList &&
                starredQuestionStore.topicList.length > 0 && (
                  <View
                    style={{
                      ...styles.DropDowncontainer,
                      ...styles.dropDownContainer,
                    }}>
                    <View style={{left: 0}}>
                      <SourceSansProRegTextView numberOfLines={1}>
                        {selectedLable}
                      </SourceSansProRegTextView>
                    </View>
                    <View style={styles.ArrowDownStyle}>
                      <TouchableOpacity
                        onPress={() => {
                          setClickDropDown(true);
                        }}>
                        <ArrowDownRed />
                      </TouchableOpacity>
                    </View>
                    <ChoiceList
                      choices={topicList}
                      show={clickDropdown}
                      selectChoiceHandler={onSelectHandler}
                      onBackdropHandler={() => {}}
                    />
                  </View>
                )}
              <BalooThambiRegTextView
                testID="StarredQuestionHeading"
                style={{...styles.title, ...styles.desc}}>
                {heading}
              </BalooThambiRegTextView>
            </View>
            {starredQuestionStore.favouritesList &&
              starredQuestionStore.favouritesList.length > 0 &&
              starredQuestionStore.favouritesList[0].data && (
                <FlatList
                  refreshControl={<RefreshControl refreshing={refreshing} />}
                  data={starredQuestionStore.favouritesList}
                  renderItem={renderItem}
                  keyExtractor={item => item.contentID}
                  onEndReached={fetchContent}
                  onEndReachedThreshold={0.9}
                  contentContainerStyle={styles.listContainerStyle}
                />
              )}

            {!starredQuestionStore.favouritesList && (
              <View style={{bottom: 60}}>
                <StarredQuestionEmpty9
                  accessible={true}
                  testID="StarredQuestionEmpty"
                  accessibilityLabel="StarredQuestionEmpty"
                  width={getWp(150)}
                  style={styles.searchIcon}
                />
                <BalooThambiRegTextView
                  testID="StarredQuestionEmptyStateText"
                  style={{
                    textAlign: 'center',
                    paddingHorizontal: 10,
                    color: 'white',
                    bottom: 50,
                  }}>
                  {starredQuestionEmptyState}
                </BalooThambiRegTextView>
                <RoundedButton
                  testID="RoundedButtonStarredQuestionGoHomeBtnText"
                  onPress={() => {
                    props.navigation.navigate('DashboardScreen');
                  }}
                  type="primaryOrange"
                  text={goHomeBtnText}
                  width={150}
                  containerStyle={{...styles.goHomeBtnContainer}}
                />
              </View>
            )}
          </View>
        </ScrollView>
      )}
      <SelectionPopup
        testID="SelectionPopupStarredQuestion"
        show={removeFav}
        svgText={attentionPleaseText}
        desc={thisWillRemoveDescText}
        cancelBtnText={noCancelBtnText}
        actionBtnText={yesRemoveBtnText}
        onCancelPress={onCancelPressHandler}
        onActionPress={onActionPressHandler}
      />
    </DetailsScreen>
  );
};

export default observer(StarredQuestionScreen);
