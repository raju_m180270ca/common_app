import {StyleSheet} from 'react-native';
import {COLORS, TEXTFONTSIZE} from '@constants';
import {getWp, getHp} from '@utils';

export default StyleSheet.create({
  DropDowncontainer: {
    position:'relative',
    backgroundColor: COLORS.white,
    borderRadius: getWp(13),
    paddingVertical: getHp(10),
    paddingHorizontal: getWp(16),
    marginBottom: getHp(20),
    justifyContent: 'center',
    alignItems: 'flex-start',
    width: '100%',
    height:getHp(55)
},
  ArrowDownStyle:{
    position:'absolute',
    right:0,
    alignItems:'center',
    marginRight:20
  },

  flexOne: {
    flex: 1,
  },
  filterStyle: {
    marginTop: getHp(26),
  },
  title: {
    color: COLORS.white,
    fontSize: TEXTFONTSIZE.Text24,
    textAlign: 'center',
    padding: 0,
    marginBottom: getWp(30),
  },

  desc: {
    fontSize: TEXTFONTSIZE.Text20,
    marginBottom: getWp(10),
  },

  contentStyle: {
    // flex: 1,
    alignItems: 'center',
    marginHorizontal: getWp(16),
    marginTop: getHp(30),
  },
  dropDownContainer: {
    marginBottom: getWp(20),
  },
  footerContainerStyle: {
    height: getHp(79),
  },
  listContainerStyle: {
    marginBottom: getHp(120),
  },
  searchIcon : {
    alignSelf:'center',
    right:5
  },
  goHomeBtnContainer : {
    marginTop:getWp(25),
    borderRadius: getWp(0),
    alignSelf:'center',
    bottom:getWp(50)
  },
});
