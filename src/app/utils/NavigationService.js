import React from 'react';

let navigator;

function setTopLevelNavigator(navigatorRef) {
  navigator = navigatorRef;
}

function navigate(routeName, params) {
  navigator.navigate(routeName, params);
}

// add other navigation functions that you need and export them

export {navigate, setTopLevelNavigator};
