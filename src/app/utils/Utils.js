import moment from 'moment';
export const replaceInput = response => {
  // if (body.indexOf('[blank') !== -1) {
  // 	body = body.replace(
  // 		/\[([^\]\[]+)\]/g,
  // 		'<input type="text" disabled="disabled">'
  // 	);
  // }
  // return body;
  console.log(
    `response data in utils>>>>${JSON.stringify(response.userAttemptData)}`,
  );
  let body = response?.data?.questionBody;
  let answers = response?.data?.response;
  let borderColor =
    response?.userAttemptData?.result == 'fail'
      ? 'border-color:red;'
      : 'border-color:green;';
  if (response.data.template === 'Blank') {
    let blankArr = Object.keys(answers);
    let user_answers = Object.keys(answers).map(ans => {
      return answers[ans].userAnswer;
    });
    blankArr.forEach((field, index) => {
      body = body.replace('[' + field + ']', function () {
        // let value = user_answers[index];
        return `<input style="text-align:center; font-weight:bold; background-color:white; color:black; ${borderColor}; border-radius:25px;" type="text" value="" disabled="disabled">`;
      });
    });
  } else if (response.data.template === 'Dropdown') {
    let ddArr = Object.keys(answers);
    // let user_answers = Object.keys(answers).map(ans => {
    // 	return answers[ans].userAnswer;
    // });
    ddArr.forEach((field, index) => {
      let dropDownChoices = response.data?.response[field]?.choices;
      body = body.replace('[' + field + ']', function () {
        // let value = user_answers[index];
        let dropdown = ` <div class="select_box"><select id="resizing_select" onchange="changeFunc()" name="${field}" id="${field}" style="${borderColor}">`;
        dropdown +=
          '<option hidden disabled selected value="">  Select  </option>';
        dropDownChoices.forEach((choice, choiceIndex) => {
          let option = `<option 
					>${choice.value}</option>`;
          dropdown += option;
        });
        dropdown += '  </select></div>';
        return dropdown;
      });
    });
  } else if (response.data.template === 'Blank_Dropdown') {
    let fieldArr = Object.keys(answers);
    // let user_answers = Object.keys(answers).map(ans => {
    // 	return answers[ans].userAnswer;
    // });
    fieldArr.forEach((field, index) => {
      let type = answers[field].type;
      if (type == 'Blank') {
        body = body.replace('[' + field + ']', function () {
          // let value = user_answers[index];
          return `<input style="text-align:center; font-weight:bold; background-color:white; color:black; ${borderColor};border-radius:25px;" type="text" value="" disabled="disabled">`;
        });
      } else {
        let dropDownChoices = response.data?.response[field]?.choices;
        body = body.replace('[' + field + ']', function () {
          // let value = user_answers[index];
          let dropdown = ` <div class="select_box"><select id="resizing_select" onchange="changeFunc()" name="${field}" id="${field}" style="${borderColor}">`;
          dropdown +=
            '<option hidden disabled selected value="">  Select  </option>';
          dropDownChoices.forEach((choice, choiceIndex) => {
            let option = `<option 
					>${choice.value}</option>`;
            dropdown += option;
          });
          dropdown += '  </select></div>';
          return dropdown;
        });
      }
    });
  }

  return body;
};

export const replaceInputWithAnswer = response => {
  let body = response?.data?.questionBody;
  let questionResponse = response?.data?.response;
  let answers = response?.userAttemptData?.userResponse;
  let borderColor =
    response?.userAttemptData?.result == 'pass'
      ? 'border-color:green;'
      : 'border-color:red;';
  if (response.data.template === 'Blank') {
    let blankArr = Object.keys(questionResponse);
    let user_answers = Object.keys(answers).map(ans => {
      if (
        answers[ans].userAnswer !== null &&
        answers[ans].userAnswer !== undefined
      )
        return answers[ans].userAnswer;
      else return '';
    });
    blankArr.forEach((field, index) => {
      let responseValue = questionResponse[field];
      let actualAnswer = '';
      if (responseValue) {
        let correctAnswers = responseValue.correctAnswers;
        actualAnswer = correctAnswers ? correctAnswers.join(',') : '';
      }
      body = body.replace('[' + field + ']', function () {
        let value = user_answers[index];
        let blankBorder =
          value == actualAnswer ? 'border-color:green;': 'border-color:red;';
        return `<input style="text-align:center; font-weight:bold; background-color:white; color:black; ${blankBorder};border-radius:25px;" type="text" value="${value}" disabled="disabled">`;
      });
    });
  } else if (response.data.template === 'Dropdown') {
    let ddArr = Object.keys(answers);
    let user_answers = Object.keys(answers).map(ans => {
      return answers[ans].userAnswer;
    });
    ddArr.forEach((field, index) => {
      let dropDownChoices = response.data?.response[field]?.choices;
      body = body.replace('[' + field + ']', function () {
        let value = user_answers[index];
        //let dropdown = `<div class="select_box"><select id="resizing_select" onchange="changeFunc()" name="${field}" id="${field}" style="${borderColor}">`;
        let dropdown = `<div class="select_box"><select id="resizing_select" name="${field}" id="${field}" style="${borderColor}" disabled>`;
        dropdown +=
          '<option hidden disabled selected value="">  Select  </option>';
        dropDownChoices.forEach((choice, choiceIndex) => {
          let option = `<option ${user_answers[index] == choiceIndex ? 'selected' : ''
            }>${choice.value}</option>`;
          //dropdown += option;
          if (user_answers[index] == choiceIndex) {
            dropdown += option;
          }
        });
        dropdown += '  </select></div>';
        return dropdown;
      });
    });
  } else if (response.data.template === 'Blank_Dropdown') {
    let fieldArr = Object.keys(answers);
    let user_answers = Object.keys(answers).map(ans => {
      return answers[ans].userAnswer;
    });
    fieldArr.forEach((field, index) => {
      let type = answers[field].type;
      if (type == 'Blank') {
        body = body.replace('[' + field + ']', function () {
          let value = user_answers[index];
          return `<input style="text-align:center; font-weight:bold; background-color:white; color:black; ${borderColor};border-radius:25px;" type="text" value="${value}" disabled="disabled">`;
        });
      } else {
        let dropDownChoices = response.data?.response[field]?.choices;
        body = body.replace('[' + field + ']', function () {
          let value = user_answers[index];
          let dropdown = `<div class="select_box"><select id="resizing_select" onchange="changeFunc()" name="${field}" id="${field}" style="${borderColor}" disabled>`;
          dropdown +=
            '<option hidden disabled selected value="">  Select  </option>';
          dropDownChoices.forEach((choice, choiceIndex) => {
            let option = `<option ${user_answers[index] == choiceIndex ? 'selected' : ''
              }>${choice.value}</option>`;
            dropdown += option;
          });
          dropdown += '  </select></div>';
          return dropdown;
        });
      }
    });
  } else if (response.data.template === 'Interactive') {
    body = `<div style="pointer-events: none">${body}</div>`
  }

  return body;
};

export const replaceString = (input, string, value) => {
  let replaced = input.toString();
  replaced = replaced.replace(/[{}]/g, '');
  replaced = replaced.replace(string, value);

  return replaced;
};

export const replaceTwiceString = (input, string1, value1, string2, value2) => {
  let replaced = input.toString();
  replaced = replaced.replace(/[{}]/g, '');
  replaced = replaced.replace(string1, value1);
  replaced = replaced.replace(string2, value2);

  return replaced;
};

export const getParsedTimeDifference = dateString => {
  var today = moment();
  var yesterday = moment().subtract(1, 'day');
  var date = moment(dateString);

  if (date.isSame(today, 'day')) {
    return date.format('h:mm a');
  } else if (date.isSame(yesterday, 'day')) {
    return 'Yesterday';
  } else {
    return date.format('DD MMM');
  }
};

export const getCircularReplacer = () => {
  const seen = new WeakSet();
  return (key, value) => {
    if (typeof value === 'object' && value !== null) {
      if (seen.has(value)) {
        return;
      }
      seen.add(value);
    }
    return value;
  };
};

export const slugify = string => {
  return string
    .toString()
    .toLowerCase()
    .replace(/\s+/g, '_') // Replace spaces with -
    .replace(/&/g, '-and-') // Replace & with 'and'
    .replace(/[^\w\-]+/g, '') // Remove all non-word characters
    .replace(/^-+/, '') // Trim - from start of text
    .replace(/-+$/, ''); // Trim - from end of text
};

const capitalize = s => {
  if (typeof s !== 'string') return '';
  return s.charAt(0).toUpperCase() + s.slice(1);
};
export const validateUserResponseArrays = (array1, array2) => {
  let validatedArray = [];
  if (array1 && array2 && array1.length === array2.length) {
    validatedArray = array1.filter(item1 => {
      return array2.find(item2 => item1 === item2?.identifier);
    });
  }

  return validatedArray.length === array1.length;
};

export const validateArrays = (array1, array2) => {
  let validatedArray = [];
  if (array1 && array2) {
    validatedArray = array1.filter(item1 => {
      return array2.find(item2 => item1.toString() === item2.toString());
    });
  }

  return validatedArray.length === array1.length;
};

export const generateValidArray = arryString => {
  let replacedString = arryString.replace(/[\[\]']+/g, '');
  replacedString = replacedString.replace(/['"]+/g, '');
  return replacedString.split(',');
};

export const createValidURL = urlString => {
  return urlString.replace(/ /g, '%20');
};

export const convertFirstCharToCaptial = string => {
  return string?.charAt(0).toUpperCase() + string?.slice(1);
};

export function HandleTheMathJax(data) {
  var optionVal = data !== null && typeof data !== 'undefined' ? data : '';
  if (optionVal.indexOf('</equ>') > 0) {
    optionVal = optionVal
      .replace(new RegExp('<equ>', 'g'), '<span class="math">')
      .replace(new RegExp('</equ>', 'g'), '</span>');
  }

  var res = optionVal.match(/\{frac\((.*?)\|(.*?)\)\}/g);
  if (res) {
    var res1;
    for (let val of res) {
      res1 = val.split('frac')[1].replace(/[()}]/g, '');
      res1 = res1.split('|');
      res1[0] = isNaN(res1[0]) ? res1[0] : `${res1[0]}`;
      res1[1] = isNaN(res1[1]) ? res1[1] : `${res1[1]}`;
      res1 = `<span class="math">${res1[0]} \\over ${res1[1]}</span>`;
      optionVal = optionVal.replace(val, res1);
    }
  }

  optionVal = optionVal.replace(/class='math'/gi, 'class="math"');
  // const isMath = optionVal.indexOf('class="math"');
  // const isFrac = optionVal.indexOf('\\over');
  // if (isMath > 0 && isFrac > 0) {
  //   optionVal = optionVal?.split(/<span class="math">(.*?)<\/span>/g);
  //   const rex = /(<([^>]+)>)/gi;

  //   if (optionVal?.length) {
  //     optionVal?.forEach(function (val, key) {
  //       if (key % 2 === 1) {
  //         let numerator = val.replace(rex, '').split('\\over')[0];
  //         let denominator = val.replace(rex, '').split('\\over')[1];

  //         if (isNaN(numerator)) {
  //           numerator = `\\text {${numerator}}`;
  //         }
  //         if (isNaN(denominator)) {
  //           denominator = `\\text {${denominator}}`;
  //         }

  //         // console.log('Numerator:', numerator);
  //         // console.log('Denominator:', denominator);
  //         let newVal = `${numerator} \\over ${denominator}`;
  //         // console.log('New Val:', newVal);
  //         //val = '<span class="math">$' + val.replace(rex, '') + '$</span>';
  //         val = '<span class="math">$' + newVal + '$</span>';
  //       }

  //       optionVal[key] = val;
  //     });

  //     optionVal = optionVal.join(' ');
  //   }
  // }
  // if (isMath > 0 && isFrac < 0) {
  optionVal = optionVal.replace(
    new RegExp('math">', 'g'),
    'math">$',
  );
  let index1 = optionVal.indexOf(`math">`, 0);
  while (index1 >= 0) {
    let index2 = optionVal.indexOf(`</span>`, index1 + 1);
    optionVal = optionVal.slice(0, index2) + optionVal.slice(index2).replace("</span>", "$</span>");
    index1 = optionVal.indexOf(`math">`, index2);
  }
  // }

  // console.log('After:', optionVal);
  return optionVal;
}

export const isHTML = text => {
  try {
    const fragment = new DOMParser().parseFromString(text, 'text/html');
    return fragment.body.children.length > 0;
  } catch (error) { }
  return false;
};

export const unitsCheck = (unitsNumber) => {
  let unitsCount = unitsNumber > 1 ? 'units' : 'unit';
  return unitsNumber + ' ' + unitsCount;
};

export const getProductName = (product) => {
  switch (product) {
    case 'MS1':
      return 'Mindspark';
    case 'MS2':
      return 'MSE';
    case 'MS3':
      return 'MSS';
    case 'MS4':
      return 'MSHi';
    case 'MS5':
      return 'MSUr';
    case 'MS6':
      return 'MSMa';
    case 'MS7':
      return 'MSPu';
    case 'MS8':
      return 'MSGu';
    case 'MS9':
      return 'MSTe';
    case 'MS10':
      return 'MSTa';
    case 'MS11':
      return 'MSKa';
    default:
      return 'Mindspark';
  }
}