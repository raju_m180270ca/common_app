import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {Platform, ToastAndroid, AlertIOS} from 'react-native';

const X_WIDTH = 375;
const X_HEIGHT = 812;

const XSMAX_WIDTH = 414;
const XSMAX_HEIGHT = 896;

let isIPhoneX = false;

if (Platform.OS === 'ios' && !Platform.isPad && !Platform.isTVOS) {
  isIPhoneX =
    (width === X_WIDTH && height === X_HEIGHT) ||
    (width === XSMAX_WIDTH && height === XSMAX_HEIGHT);
}

const height = 896;
const width = 414;

// const height = deviceHeight;
// const width = deviceWidth;

export const respHp = (orientation, percentage) => {
  if (orientation === 'PORTRAIT') {
    return hp(percentage);
  } else {
    return wp(percentage);
  }
};

export const respWp = (orientation, percentage) => {
  if (orientation === 'PORTRAIT') {
    return wp(percentage);
  } else {
    return hp(percentage);
  }
};

export const getHp = (pixels = height) => {
  return hp(((pixels / height) * 100).toFixed(2));
  // return ((pixels / height) * 100);
};

export const getWp = (pixels = width) => {
  return wp(((pixels / width) * 100).toFixed(2));
  // return ((pixels / width) * 100)
};

export const getStatusBarHeight = () => {
  return isIPhoneX ? 44 : 20;
};

export const showToastMessage = message => {
  if (Platform.OS === 'android') {
    ToastAndroid.show(message, ToastAndroid.SHORT);
  } else {
    AlertIOS.alert(message);
  }
};

export const getAspectRation = () => {
  return wp('100') / hp('100');
};

export const toTitleCase = str => {
  return str.replace(/\w\S*/g, function (txt) {
    return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
  });
};

export const checkForAudio = quesData => {
  console.log('CHECK AUDIO:', quesData.indexOf('<audio>'));
  if (quesData.indexOf('<audio>') > -1) {
    quesData = quesData.replace('controls', '');
    // quesData += `
    // <br/>
    // <div class="img-container" >
    // <img src="https://ov.mindspark.in/Mindspark/Asset/Common/images/speaker.jpeg" class="play" width="40px" height="40px" id="audioPlay"/></div>
    // `;

    return quesData;
  } else {
    console.log('ELSE');
    return quesData;
  }
};

export const sortArrayObject = (
  data,
  field,
  field_type,
  sort_type = 'ascending',
) => {
  data = JSON.parse(JSON.stringify(data));
  switch (field_type) {
    case 'string':
      data.sort(function (a, b) {
        let str1 = a[field].toLowerCase(),
          str2 = b[field].toLowerCase();
        if (sort_type === 'ascending') {
          if (str1 < str2) {
            return -1;
          }
          if (str1 > str2) {
            return 1;
          }
          return 0;
        } else {
          if (str1 < str2) {
            return 1;
          }
          if (str1 > str2) {
            return -1;
          }
          return 0;
        }
      });
      return data;
    case 'number':
      data.sort(function (a, b) {
        if (sort_type === 'ascending') {
          return a[field] - b[field];
        } else {
          return b[field] - a[field];
        }
      });
      return data;
    case 'date':
      data.sort(function (a, b) {
        var date1 = new Date(a[field]),
          date2 = new Date(b[field]);
        if (sort_type === 'ascending') {
          return date1 - date2;
        } else {
          return date2 - date1;
        }
      });
      return data;
    default:
      return data;
  }
};

export const camelCaseToString = (string = '') => {
  return toTitleCase(string.replace(/([a-z0-9])([A-Z])/g, '$1 $2'));
};

export {hp, wp};
