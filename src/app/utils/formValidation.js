const formValidation = (fieldName, value, rules) => {
  let isValid = true;
  let errMsg = '';
  let specialCharsRegExp = /^[a-zA-Z0-9_\-. ]+$/;
  let includeAtCharRegExp = /^[a-zA-Z0-9@_\-.]*$/;
  let noSpecialCharsExceptA = /^[a-zA-Z0-9@_\-. ]+$/;

  let whiteSpaceRegExp = /^\S+$/;
  const {
    required,
    minLength,
    maxLength,
    noSpecialChars,
    noWhiteSpace,
    includeAtChar,
    noFirstDot,
    noSpecialCharsExceptAtChar,
  } = rules;
  if (required) {
    isValid = value.trim() !== '';
    errMsg = !isValid && `${fieldName} is required`;
  }
  if (noFirstDot) {
    isValid = value.charAt(0) !== '.';
    errMsg = !isValid && `Dot is not allowed as the first character`;
    if (isValid) {
      isValid = value.indexOf('..') == -1;
      errMsg = !isValid && `Two or more consecutive dots are not allowed`;
    }
  }
  if (minLength && isValid) {
    isValid = value.length >= minLength;
    errMsg =
      !isValid && `${fieldName} must be minimum of ${minLength} characters`;
  }

  if (maxLength && isValid) {
    isValid = value.length <= maxLength;
    errMsg =
      !isValid && `${fieldName} must be maximum of ${minLength} characters`;
  }

  if (noSpecialChars && isValid) {
    console.log(`Val>>>${value}`);
    isValid = specialCharsRegExp.test(value);
    console.log(`valid val>>>${isValid}`);
    errMsg = !isValid && `${fieldName} cannot contain special characters`;
  }

  if (noSpecialCharsExceptAtChar && isValid) {
    console.log(`Val>>>${value}`);
    isValid = noSpecialCharsExceptA.test(value);
    console.log(`valid val>>>${isValid}`);
    errMsg = !isValid && `${fieldName} cannot contain special characters`;
  }

  if (includeAtChar && isValid) {
    console.log(`Val>>>${value}`);
    isValid = includeAtCharRegExp.test(value);
    console.log(`valid val>>>${isValid}`);
    errMsg = !isValid && `${fieldName} cannot contain special characters`;
  }

  if (noWhiteSpace && isValid) {
    isValid = whiteSpaceRegExp.test(value);
    errMsg = !isValid && `Space is not allowed`;
  }

  return {
    isValid,
    errMsg,
  };
};

export default formValidation;
