import {QHtmlTemplateForIframe} from '@components';
import {
  SimpleHtmlTemplate,
  getWp,
  ContentServiceHTMLTemplate,
  MathHTMLTemplate,
  MathContentServiceHTMLTemplate,
  HandleTheMathJax,
  IframeHTMLTemplate,
  IframeMathHTMLTemplate,
} from '@utils';

export default (getHtmlTemplate = (
  str,
  largeFont = false,
  isRTL = false,
  userLang = null,
  alignLeft = false,
  isExplanation = false,
) => {
  if (str) {
    //Check for Math expressions and string manipulation
    // if (str.search(/frac/g) > 0) {
    //   str = HandleTheMathJaxWithFrac(str);
    // } else {
    // }
    //str =`We use money to buy things.<strong><br><br>Click on a coin which is used to buy things in India.</strong><br><br><iframe id='quesInteractive' src='https://d2tl1spkm4qpax.cloudfront.net/Enrichment_Modules/html5/questions/TMP/TMP_mcq/src/index.html?type=shape&selection=single&optionDetails=coin2.png|coin3.png|1_rupee.png|1_cent.png&correctAnswer=3&displayAnswer=0&optionText=|||' height='250px' width='600px' frameborder='0' scrolling='no'></iframe>`;
    //str = `<iframe id='quesInteractive' src='https://d2tl1spkm4qpax.cloudfront.net/Enrichment_Modules/html5/questions/VSA/VSA_qcode_37882_1/src/index.html?language=en_in' height='200px' width='550px' frameborder='0' scrolling='no'></iframe>`;
    str = HandleTheMathJax(str);
      // console.log("is frac", str)
    let isAudio = false;
    let isImage = false;
    let isMath = false;
    let isIframe = false;
    let htmlTemplate = str;
    let isFrac = false;
    let isBlankDD = false;
    let isTable = false;

    // str = HandleTheMathJaxWithFrac(str);
    // console.log('Is frac', str);

    //Check for audio
    if (str.indexOf('<audio') > -1) {
      isAudio = true;
    }

    //Check for image
    if (str.indexOf('<img') > -1) {
      isImage = true;
    }

    //Check for Math expression
    if (str.indexOf('class="math"') > -1 || isFrac) {
      isMath = true;
    }

    //Check for IFRAME
    if (str.indexOf('<iframe') > -1) {
      isIframe = true;
    }

    //Check for BLANK & DropDown

    if (str.indexOf('blank_') > -1 || str.indexOf('dropdown_') > -1) {
      isBlankDD = true;
    }

    if (str.indexOf('<table') > -1 || str.indexOf('<TABLE') > -1) {
      isTable = true;
    }

    if (isMath && isIframe) {
      let width = 300;
      // let tempPos = str.indexOf("iframe");
      // let pos = str.indexOf("width", tempPos);
      // if(pos > 0) width = parseInt(str.substring(pos+7, pos+10));
      
      htmlTemplate = IframeMathHTMLTemplate(
        str,
        true,
        isRTL,
        getWp(width),
        isTable,
        isExplanation,
      );
      console.log('here 1', isExplanation, isTable);
    } else if (isIframe) {
      let width = 370;
      // let tempPos = str.indexOf("iframe");
      // let pos = str.indexOf("width", tempPos);
      // if(pos > 0) width = parseInt(str.substring(pos+7, pos+10));

      htmlTemplate = IframeHTMLTemplate(
        str,
        true,
        isRTL,
        getWp(width),
        isTable,
        isExplanation,
      );
      console.log('here 2', isExplanation, isTable);
    } else if (isMath && isBlankDD) {
      console.log('here 3');
      htmlTemplate = MathContentServiceHTMLTemplate(
        str,
        true,
        isRTL,
        userLang,
        alignLeft,
        isTable,
      );
      //Load current te,plate
      // htmlTemplate = QHtmlTemplateForIframe(
      //   str,
      //   getWp(373),
      //   null,
      //   null,
      //   null,
      //   true,
      // );
    } else if (isBlankDD) {
      console.log('here 4');
      htmlTemplate = ContentServiceHTMLTemplate(
        str,
        largeFont,
        isRTL,
        userLang,
        alignLeft,
        isTable,
        isExplanation
      );
    } else if (isMath) {
      console.log('here 5');
      htmlTemplate = MathHTMLTemplate(
        str,
        largeFont,
        isRTL,
        userLang,
        alignLeft,
        isTable,
        isExplanation
      );
    } else {
      // Load simplified template
      console.log('here 6');
      htmlTemplate = SimpleHtmlTemplate(
        str,
        largeFont,
        isRTL,
        userLang,
        alignLeft,
        isTable,
        isExplanation
      );
    }
    return htmlTemplate;
  } else {
    return str;
  }
});
