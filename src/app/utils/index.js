import getDeviceDetails from './getDeviceDetails';
import {
  setAsValue,
  getAsValue,
  multiRemoveValue,
  clearStorage,
} from './asyncStorage';
import formValidation from './formValidation';
import {navigate, setTopLevelNavigator} from './NavigationService';
import {
  respHp,
  respWp,
  getHp,
  getWp,
  getStatusBarHeight,
  showToastMessage,
  getAspectRation,
  toTitleCase,
  sortArrayObject,
  camelCaseToString,
  checkForAudio,
  hp,
  wp,
} from './ViewUtils';
import {
  getFormatedDateTime,
  getAllDatesOfSpecificMonth,
  getCurrentMonthName,
  getParsedDate,
  getParsedTime,
  getBetweenDates,
  dateToString,
  getCurrentMonth,
  getCurrentYear,
  getFormatedDate,
  getTimeDiff,
  getMonthYear,
  notificationTimeDifference,
} from './DateUtils';

import {
  replaceInput,
  replaceInputWithAnswer,
  replaceString,
  replaceTwiceString,
  getParsedTimeDifference,
  slugify,
  getCircularReplacer,
  capitalize,
  validateUserResponseArrays,
  validateArrays,
  generateValidArray,
  createValidURL,
  convertFirstCharToCaptial,
  HandleTheMathJax,
  isHTML,
  unitsCheck,
  getProductName
} from './Utils';
import onboardingLottie from './onboardingLottie';

import {timeDifference} from './DateUtils';

import {configurePushNotification} from './notification';
import PwdValidation from './PwdValidation';

import SimpleHtmlTemplate from './SimpleHtmlTemplate';
import ContentServiceHTMLTemplate from './ContentServiceHTMLTemplate';
import MathHTMLTemplate from './MathHTMLTemplate';
import MathContentServiceHTMLTemplate from './MathContentServiceHTMLTemplate';
import IframeMathHTMLTemplate from './IframeMathHTMLTemplate';
import IframeHTMLTemplate from './IframeHTMLTemplate';

export {
  checkForAudio,
  getAspectRation,
  getDeviceDetails,
  setAsValue,
  getAsValue,
  multiRemoveValue,
  formValidation,
  respHp,
  respWp,
  getHp,
  getWp,
  getStatusBarHeight,
  setTopLevelNavigator,
  navigate,
  getFormatedDateTime,
  getFormatedDate,
  getTimeDiff,
  showToastMessage,
  replaceInput,
  replaceInputWithAnswer,
  replaceString,
  replaceTwiceString,
  getParsedTimeDifference,
  onboardingLottie,
  slugify,
  getCircularReplacer,
  capitalize,
  validateUserResponseArrays,
  validateArrays,
  generateValidArray,
  toTitleCase,
  sortArrayObject,
  getParsedDate,
  getParsedTime,
  getBetweenDates,
  dateToString,
  getCurrentMonth,
  getCurrentMonthName,
  getMonthYear,
  camelCaseToString,
  getAllDatesOfSpecificMonth,
  timeDifference,
  getCurrentYear,
  createValidURL,
  clearStorage,
  convertFirstCharToCaptial,
  hp,
  wp,
  notificationTimeDifference,
  configurePushNotification,
  PwdValidation,
  SimpleHtmlTemplate,
  ContentServiceHTMLTemplate,
  MathHTMLTemplate,
  MathContentServiceHTMLTemplate,
  HandleTheMathJax,
  IframeMathHTMLTemplate,
  IframeHTMLTemplate,
  isHTML,
  unitsCheck,
  getProductName
};
