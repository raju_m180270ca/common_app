import PushNotification from 'react-native-push-notification';
import PushNotificationIOS from '@react-native-community/push-notification-ios';
import {Platform} from 'react-native';

export function configurePushNotification(appStore, navigation, enableSound) {
  PushNotification.configure({
    onRegister: function (token) {
      console.log('Push Notification Token', token);
      appStore.setPushNotificationToken(token?.token);
    },
    onNotification: async function (notification) {
      console.log('Push Notification Response:', JSON.stringify(notification));
      handlePushNotification(notification, navigation);
      if (Platform.OS === 'ios') {
        notification.finish(PushNotificationIOS.FetchResult.NoData);
      }
    },
    onAction: function (notification) {
      console.log('ACTION:', notification.action);
      console.log('NOTIFICATION in ACTION:', notification);

      handlePushNotification(notification, navigation);
    },
    onRegistrationError: function (error) {
      console.log(error.message, error);
    },
    permissions: {
      alert: true,
      badge: true,
      sound: enableSound,
    },
    popInitialNotification: true,
    requestPermissions: true,
  });

  PushNotification.popInitialNotification(notification => {
    console.log('Push Notification Response:', JSON.stringify(notification));
    handlePushNotification(notification, navigation);
  });
}

const handlePushNotification = (notification, navigation) => {
  if (
    notification?.data !== null &&
    typeof notification?.data !== 'undefined'
  ) {
    const notificationData = notification?.data;
    switch (notificationData?.redirectionCode) {
      case 'message':
      case 'Message':
        navigation.navigate('MailBoxScreen');
        break;
      case 'settings':
      case 'Settings':
        navigation.navigate('ChangeTextPassWordScreen');
        break;
      case 'profile':
        navigation.navigate('ProfileScreen');
        break;
      case 'topicmap':
      case 'topics_page':
        if (
          notificationData.hasOwnProperty('redirectionData') &&
          notificationData?.redirectionData !== null &&
          notification?.redirectionData?.id !== null &&
          notification?.redirectionData?.id !== '' &&
          typeof notification?.redirectionData?.id !== 'undefined'
        ) {
          navigation.navigate('TopicMapScreen', {
            topicID: notification?.redirectionData?.id,
          });
        } else {
          navigation.navigate('TopicListingScreen');
        }
        break;
      case 'topics_page':
      case 'TopicsPage':
      case 'TopicListPage':
        navigation.navigate('TopicListingScreen');
        break;
      case 'worksheet_list_page':
      case 'worksheet_page':
      case 'WorksheetPage':
      case 'WorksheetListPage':
      case 'worksheetlist':
      case 'new_worksheet':
        navigation.navigate('WorksheetListScreen');
        break;
      case 'leaderboard':
        navigation.navigate('Leaderboard');
        break;
      case 'bell':
        navigation.navigate('NotificationScreen');
        break;
      case 'home':
        navigation.navigate('DashboardScreen');
        break;
    }
  }
};
